'use strict'
const ServerHttp = require('http');
const ServerWebSocket = require('websocket').server;
const url = require('url');
const fs = require('fs')
const path = require('path');
const ipc = require('node-ipc');
const log4js = require('log4js');

const util = require('util');
const text_decoder = new util.TextDecoder();
const text_encoder = new util.TextEncoder();

const is_windows = process.platform === 'win32';
const is_mac = process.platform === 'darwin';
const is_linux = process.platform === 'linux';

var date = new Date();
let format_str = 'YYYY-MM-DD_hh-mm';
format_str = format_str.replace(/YYYY/g, date.getFullYear());
format_str = format_str.replace(/MM/g, date.getMonth());
format_str = format_str.replace(/DD/g, date.getDate());
format_str = format_str.replace(/hh/g, date.getHours());
format_str = format_str.replace(/mm/g, date.getMinutes());
format_str = format_str.replace(/ss/g, date.getSeconds());

log4js.configure({
    appenders: {
        system: { type: 'file', filename: '../CCM_NODEJS_' + format_str }
    },
    categories: {
        default: { appenders: ['system'], level: 'debug' },
    }
});
const logger = log4js.getLogger('system');

// Web Server Configuration
const port = 8000;
const mime = {
    ".html": "text/html",
    ".htm": "text/html",
    ".xml": "text/xml",
    ".css": "text/css",
    ".js": "text/javascript",
    ".gif": "image/gif",
    ".png": "image/png",
    ".jpg": "image/jpeg",
    ".jpeg": "image/jpeg",
    // 読み取りたいMIMEタイプはここに追記
};

// CCM SDK API Server Configuration
const ccmapiHostname = '127.0.0.1';
const ccmapiPort = 11600;

// 接続された WebSocketクライアントの一覧
const webSocketClients = [];

/**
 * 接続された WebSocketクライアントの一覧から、Closeされたものを削除する
 */
const optimizeWebSocketClients = () => {
    const len = webSocketClients.length;
    for (let i = len - 1; i >= 0; --i) {
        if (webSocketClients[i].state === 'closed') {
            webSocketClients.splice(i, 1);
        }
    }
};

const getWebSocketAcceptIPAddresses = () => {
    let addresses = ['localhost', '127.0.0.1'];
    const interfaces = require('os').networkInterfaces();
    for (let devName in interfaces) {
        const iface = interfaces[devName];
        for (let i = 0; i < iface.length; i++) {
            const alias = iface[i];
            if (alias.family === 'IPv4' && alias.address !== '127.0.0.1' && !alias.internal) {
                addresses.push(alias.address);
            }
        }
    }
    return addresses;
}




/* --- Webサーバー --- */

/**
 * Webサーバーの作成＆要求応答
 */
const webServer = ServerHttp.createServer((req, res) => {
    logger.info(`[HTTP Server] (request : ${req.url})`);

    // 指定された url の最後が "/" で終わっている場合は index.html を返す
    let filePath;
    const lastChar = req.url.slice(-1);
    if (lastChar == '/') {
        filePath = `${req.url}index.html`;
    } else {
        filePath = `${req.url}`;
    }
    const fullPath = __dirname + filePath;

    // 応答用ファイルを読み込み応答を返す
    fs.readFile(fullPath, (err, data) => {
        if (err) {
            res.statusCode = 404;
            res.end("");
        } else {
            res.statusCode = 200;
            res.setHeader('Content-Type', mime[path.extname(fullPath)] || "text/plain");
            res.end(data);
        }
    });
});

/**
 * Webサーバーの起動
 */
webServer.listen(port, () => {
    console.log(`[HTTP Server] Server running. (port : ${port})`);
    logger.info(`[HTTP Server] Server running. (port : ${port})`);
});

/* --- IPCクライアント --- */
let ipc_data = Buffer.alloc(0);
const socketId = 'ccm_ipc_obj';
ipc.config.silent = true;
ipc.config.id = 'ccmsdk';
ipc.config.socketRoot = '/tmp/';
ipc.config.appspace = '';
ipc.config.retry = 3000;
ipc.config.rawBuffer = true;
ipc.config.encoding = 'hex';
if (is_windows === true) {
    ipc.config.socketRoot = '';
}

/**
 * 文字列 を Uint8Array へ変換
 * @param {*} src 文字列
 */
const string_to_buffer = (src) => {
    let dataBuffer = text_encoder.encode(src);
    //
    let buffer = new ArrayBuffer(4);
    let dview = new DataView(buffer);
    dview.setUint32(0, dataBuffer.length, true);
    let lengthBuffer = new Uint8Array(buffer);

    const len = dataBuffer.length + lengthBuffer.length;
    const stream = new Uint8Array(len);
    stream.set(lengthBuffer, 0);
    stream.set(dataBuffer, 4);

    return stream;
}
/**
 * Uint8Array を 文字列 へ変換
 * @param {*} buf Uint8Arrayデータ
 */
const buffer_to_string = (buf) => {
    const str = text_decoder.decode(Uint8Array.from(buf).buffer);
    return str;
}

const getDataLength = (data) => {
    if (data.length < 4) {
        return -1;
    }
    else {
        const len = data.slice(0, 4);
        const uarray = Uint8Array.from(len);
        const dview = new DataView(uarray.buffer)
        const val = dview.getUint32(0, true);
        return val;
    }
}

ipc.connectTo(
    socketId,
    function () {
        ipc.of[socketId].on(
            'connect',
            function () {
                //console.log("Connected!!");
                logger.info("CCM IPC Connected");
                //ipc.log('## connected to world ##'.rainbow, ipc.config.delay);
            }
        );
        ipc.of[socketId].on(
            'disconnect',
            function () {
                //console.log("Disconnected!!");
                logger.info("CCM IPC Disconnected");
                ipc.log('disconnected from world'.notice);
            }
        );
        ipc.of[socketId].on(
            'message',  //any event or message type your server listens for
            function (data) {
                //console.log("Got a message!!");
                logger.info("CCM IPC OnMessage |" + data + "|");
                //ipc.log('got a message from world : '.debug, data);

                webSocketClients.forEach((websocket) => {
                    websocket.send(data);
                });
            }
        );
        ipc.of[socketId].on(
            'data',
            function (d) {
                ipc_data = Buffer.concat([ipc_data, d])

                let length = getDataLength(ipc_data);
                do {
                    if (length !== -1) {
                        if (ipc_data.length < (4 + length)) {
                            break;
                        }

                        const dataBuffer = ipc_data.slice(4, 4 + length);
                        const data = buffer_to_string(dataBuffer);
                        logger.info("CCM IPC OnData |" + data + "|");
                        webSocketClients.forEach((websocket) => {
                            websocket.send(data);
                        });

                        ipc_data = ipc_data.slice(4 + length);
                        length = getDataLength(ipc_data);
                    }
                } while (length >= 0);
            }
        );
    }
);




/* --- WebSocketサーバー --- */

// WebSocketサーバーの起動
const webSocketServer = new ServerWebSocket({ httpServer: webServer });
const accept = getWebSocketAcceptIPAddresses();
webSocketServer.on('request', function (req) {
    req.origin = req.origin || '*';
    if (accept.indexOf(url.parse(req.origin).hostname) === -1) {
        req.reject();
        logger.info("WebSocket " + req.origin + ' access not allowed.');
        return;
    }

    const websocket = req.accept(null, req.origin);
    webSocketClients.push(websocket);

    // ブラウザからのリクエスト
    websocket.on('message', function (msg) {
        logger.info('WebSocket OnReceived (data : ' + msg.utf8Data + ',  from: ' + req.origin + ')');
        // ipc.of[socketId].emit('message', msg.utf8Data)
        ipc.of[socketId].emit(string_to_buffer(msg.utf8Data));
    });

    websocket.on('close', function (code, desc) {
        logger.info('WebSocket OnClosed! :' + code + ' - ' + desc);
        optimizeWebSocketClients();
    });
});
