class Log {
    /**
     * ログ出力
     * @param {*} category 
     * @param {*} msg 
     */
    static logStr(category, msg) {
        console.log(`${category} : ${msg}`);
    }
}

class CcmWebSocket {
    constructor(ccm) {
        this.isOpened = false;
        this.ccm = ccm;
    }

    initialize() {
        this.ws = new WebSocket(`ws://${location.host}`);

        this.ws.onopen = (event) => {
            Log.logStr('[WebSocket]', `Opened. (${location.host})`);
            this.isOpened = true;
            this.ccm.onOpened(event);
        };
        this.ws.onmessage = (event) => {
            Log.logStr('[WebSocket]', `Recieved : ${event.data}`);
            this.ccm.onReceived(event);
        };
        this.ws.onclose = (event) => {
            Log.logStr('[WebSocket]', `Closed. (${event.code} - ${event.type})`);
            this.isOpened = false;
            this.ccm.onClosed(event);
        };
    }

    send(req) {
        if (this.isOpened) {
            Log.logStr('[WebSocket]', `Send :${req}.`);
            this.ws.send(req);
        } else {
            Log.logStr('[WebSocket]', `Send skiped : WebSocket is closed.)`);
        }
    }
}

class Ccm {
    constructor(control) {
        this.control = control;
        this.ws = new CcmWebSocket(this);
    }

    initialize() {
        this.ws.initialize();
    }

    onOpened(event) {
        this.control.onOpenedWebSocket(event);
    }
    onReceived(event) {
        this.control.onReceivedWebSocket(event);
    }
    onClosed(event) {
        this.control.onClosedWebSocket(event);
    }

    cmdGetNetworkClient(data) {
        const req = {
            command: "GetNetworkClient",
            type: "request",
            data: data,
        };

        this.ws.send(JSON.stringify(req));
    }

    /**
     * NetworkClient一覧取得要求の送信
     */
    cmdGetNetworkClientList() {
        const req = {
            command: "GetNetworkClientList",
            type: "request",
            data: "",
        };

        this.ws.send(JSON.stringify(req));
    }

    /**
     * クリップ一覧取得要求の送信
     * @param {*} data 
     */
    cmdGetClipList(data) {
        const req = {
            command: "GetClipList",
            type: "request",
            data: data
        };

        this.ws.send(JSON.stringify(req));
    }

    /**
     * ジョブ登録要求の送信
     * @param {*} data 
     */
    cmdAddUploadFiles(data) {
        const req = {
            command: "AddUploadFiles",
            type: "request",
            data: data
        };

        this.ws.send(JSON.stringify(req));
    }

    /**
     * ジョブ一覧取得要求の送信
     * @param {*} data 
     */
    cmdGetUploadList(data) {
        const req = {
            command: "GetUploadList",
            type: "request",
            data: data
        };

        this.ws.send(JSON.stringify(req));
    }

    /**
     * ストリーミング開始要求の送信
     * @param {*} data 
     */
    cmdStartQosStreamingSend(data) {
        const req = {
            command: "StartQosStreamingSend",
            type: "request",
            data: data
        };

        this.ws.send(JSON.stringify(req));
    }

    /**
     * ストリーミング停止要求の送信
     * @param {*} data 
     */
    cmdStopStreamingSend(data) {
        const req = {
            command: "StopStreamingSend",
            type: "request",
            data: data
        };

        this.ws.send(JSON.stringify(req));
    }

    /**
     * ストリーミング開始要求の送信
     * @param {*} data 
     */
    cmdStartQosStreamingReceive(data) {
        const req = {
            command: "StartQosStreamingReceive",
            type: "request",
            data: data
        };

        this.ws.send(JSON.stringify(req));
    }

    /**
     * ストリーミング停止要求の送信
     * @param {*} data 
     */
    cmdStopStreamingReceive(data) {
        const req = {
            command: "StopStreamingReceive",
            type: "request",
            data: data
        };

        this.ws.send(JSON.stringify(req));
    }

    /**
     * PlanningMetadata一覧取得要求の送信
     */
    cmdGetPlanningMetadataList(data) {
        const req = {
            command: "GetPlanningMetadataList",
            type: "request",
            data: data
        };

        this.ws.send(JSON.stringify(req));
    }

    /**
     * 現在の PlanningMetadata の取得
     */
    cmdGetCurrentPlanningMetadata(data) {
        const req = {
            command: "GetCurrentPlanningMetadata",
            type: "request",
            data: data
        };

        this.ws.send(JSON.stringify(req));
    }

    /**
     * 指定した name の PlanningMetadata の取得
     */
    cmdGetPlanningMetadata(data) {
        const req = {
            command: "GetPlanningMetadata",
            type: "request",
            data: data
        };

        this.ws.send(JSON.stringify(req));
    }

    cmdSetPlanningMetadata(data) {
        const req = {
            command: "SetPlanningMetadata",
            type: "request",
            data: data
        };

        this.ws.send(JSON.stringify(req));
    }

    cmdPutPlanningMetadata(data) {
        const req = {
            command: "PutPlanningMetadata",
            type: "request",
            data: data
        };

        this.ws.send(JSON.stringify(req));
    }
}
