/*--- CCM Web Application 設定ファイル ---*/

/**
 * Preset - Clip転送開始
 */
const clip_transfer_preset = [
    { display_name: 'local ftp1', ftp_host_name: '127.0.0.1', ftp_port: 21, ftp_user: 'user',  ftp_password: 'password',  upload_directory: '/SM-F',     is_use_ftpes: true,  is_use_passive: true,  is_verify_certification: true,  is_use_mcb_setting: true,  mcb_host_name: 'McbHost', mcb_port: 2233, mcb_user: 'McbUser', mcb_password: 'McbPass' },
    { display_name: 'local ftp2', ftp_host_name: '127.0.0.1', ftp_port: 21, ftp_user: 'user',  ftp_password: 'password',  upload_directory: '/SM-F',     is_use_ftpes: true,  is_use_passive: true,  is_verify_certification: true,  is_use_mcb_setting: false, mcb_host_name: '',        mcb_port: 0,    mcb_user: '',        mcb_password: ''        },
    { display_name: 'local ftp3', ftp_host_name: '127.0.0.1', ftp_port: 21, ftp_user: 'user3', ftp_password: 'password3', upload_directory: '/SM-G930F', is_use_ftpes: false, is_use_passive: false, is_verify_certification: false, is_use_mcb_setting: true,  mcb_host_name: 'McbHost', mcb_port: 2233, mcb_user: 'McbUser', mcb_password: 'McbPass' },
    { display_name: '',           ftp_host_name: '',          ftp_port: 0,  ftp_user: '',      ftp_password: '',          upload_directory: '',          is_use_ftpes: false, is_use_passive: false, is_verify_certification: false, is_use_mcb_setting: false, mcb_host_name: '',        mcb_port: 0,    mcb_user: '',        mcb_password: ''        }
];

/**
 * Preset - ストリーミング送信開始
 */
const streaming_send_start_preset = [
    { address: '192.168.0.143', port: 11300, videoformat_index: 0, gop: 60, buffertime: 1000, chiperkey: '41',                                                                                           ssrc: 1418847257, qosinterfaces_index: [1],          metainterfaces_index: [1]          },
    { address: '192.168.0.143', port: 11300, videoformat_index: 0, gop: 60, buffertime: 1000, chiperkey: '38373434343737323530353230353736333535363431353035343933343338333139323736323330313736313632', ssrc: 1504397019, qosinterfaces_index: [0],          metainterfaces_index: [0]          },
    { address: 'rx.com',        port: 5000,  videoformat_index: 0, gop: 60, buffertime: 1000, chiperkey: 'PSK-PSK-AES256-SHA256',                                                                        ssrc: 1,          qosinterfaces_index: [0, 1, 2, 3], metainterfaces_index: [0, 1, 2, 3] }
];

/**
 * Preset - ストリーミング受信開始
 */
const streaming_receive_start_preset = [
    { id: 'NC_1', port: 11300, videoformat_index: 0, gop: 60, buffertime: 1000, chiperkey: '41', ssrc: 1418847257, qosinterfaces_index: [1],    metainterfaces_index: [1]    },
    { id: 'NC_1', port: 11300, videoformat_index: 1, gop: 60, buffertime: 1000, chiperkey: '41', ssrc: 1418847257, qosinterfaces_index: [1, 3], metainterfaces_index: [0, 2] }
];

/**
 * Preset - VoIP送信開始
 */
const voip_send_start_preset = [
    { type:'primary',   destination_port: 11500, receiving_port: 11500, frame_length_IntercomTx: 60000,  frame_length_IntercomRx: 40000, cipher_key_IntercomTx: '82a71940eeb0494d15ca1743ac2edc139c8e2533f4059ad124df9001d748e6ca0edfb09a7978376148fa10cca82c', cipher_key_IntercomRx: '4a122afd7aeee03c954d7a04b39824a4479d6bb8718132010edfe39e2ddead8df2c2bdeff5e4ab42063851d485d6', ssrc_IntercomTx: '5bbf7159', ssrc_IntercomRx: 'ceaa4f6b' },
    { type:'secondary', destination_port: 11500, receiving_port: 11500, frame_length_IntercomTx: 40000,  frame_length_IntercomRx: 60000, cipher_key_IntercomTx: '4a122afd7aeee03c954d7a04b39824a4479d6bb8718132010edfe39e2ddead8df2c2bdeff5e4ab42063851d485d6', cipher_key_IntercomRx: '82a71940eeb0494d15ca1743ac2edc139c8e2533f4059ad124df9001d748e6ca0edfb09a7978376148fa10cca82c', ssrc_IntercomTx: 'ceaa4f6b', ssrc_IntercomRx: '5bbf7159' }
];

/**
 * Webアプリケーションバージョン
 */
const version = {
    major: 0,
    minor: 6,
    maintenance: 0,
    build: 1
};

/**
 * HTML表示完了後の処理
 */
$(window).on('load', () => {
    const ver = `Ver: ${version.major}.${version.minor}.${version.maintenance}.${version.build}`;
    $('#system_footer_view_version').text(ver);
});
