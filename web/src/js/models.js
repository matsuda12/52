class CcmModel {
    constructor() {
        // 接続されている NetworkClient 一覧
        this.networkClientList = [];
        // Tx一覧で選択されている NetworkClient の識別子
        this.selectedTxNetworkClientId = null;

        // NetworkClient と クリップ一覧 のマップ
        this.clipList = {};

        // NetworkClient と ジョブ一覧 のマップ
        this.upload_files = {};

        // 実行しているコマンド
        this.command = {};

        // Clip Inspection Viewの表示/非表示フラグ
        this.showClipInspectionView = false;

        // Recorder PlanningMetadata Inspection View の表示/非表示フラグ
        this.showPlanningMetadataInspectionView = true;

        // McbNetworkInfo
        this.mcb_network_info = [{
            ID: "dummy",
            timestamp: [],
            loss: { nic1: [], nic2: [], nic3: [], nic4: [] },
            bitrate: { nic1: [], nic2: [], nic3: [], nic4: [], total: [] },
            rtt: { nic1: [], nic2: [], nic3: [], nic4: [] }
        }];

        // NetworkInfo
        this.network_info = [{
            ID: "dummy",
            CellularInfo: [{
                SignalStrength: '',
                SignalStrengthUnit: '',
                Operator: '',
                PLMN: '',
                Band: '',
                CellID: '',
                AirInterface: '',
                Manufacture: '',
                ModelName: '',
                IMEI: ''
            }],
            QosInterfaceType: []
        }];

        // 位置(GPS)情報
        this.gps_info = [{
            ID: "dummy",
            update_time: "",
            latitude: 0,
            longitude: 0,
        }];

        this.recorder_control_status = [{
            ID: "dummy",
            low_resolution: false,
            high_resolution: false
        }];

        this.recorder_cliprecorder_status = [{
            ID: "dummy",
            result: [{
                recorder: "",
                status: "INACTIVE"
            }]
        }];

        this.planning_metadata_list = [{
            ID: "dummy",
            PlanningMetadata: [{
                name: "",
                date: "",
                filesize: 0
            }]
        }];

        // this.recorder_drive_status = [{
        //     ID: "dummy",
        //     drives: [{
        //         id: "",
        //         status: "",
        //         capacity: 0,
        //         available: 0
        //     }]
        // }];

        // Recorder PlanningMetadata制御機能の有効/無効フラグ
        this.recorder_planningmetadata_control = [{
            ID: "dummy",
            Status: false
        }];

        // ストリーミング情報
        this.streaming_dynamic_property = {
            timestamp: [],
            loss: { nic1: [], nic2: [], nic3: [], nic4: [] },
            bitrate: { nic1: [], nic2: [], nic3: [], nic4: [], total: [] },
            delay: { nic1: [], nic2: [], nic3: [], nic4: [] },
            rtt: { nic1: [], nic2: [], nic3: [], nic4: [] }
        };

        // クリップ一覧画面で表示するクリップの順番
        this.clipListOrder = 'file_desc';
    }

    /* --- 関数 --- */

    initialize() {

    }

    setClipListOrder(orderType) {
        this.clipListOrder = orderType;
    }

    /**
     * クリップ一覧情報の初期化
     * @param {*} networkClientId 
     */
    clearClipList(networkClientId) {
        let clips = this.clipList[networkClientId];
        if (clips !== undefined) {
            clips.length = 0;
        }
    }

    /**
     * クリップ一覧の取得
     * @param {*} networkClientId 
     */
    getClipList(networkClientId) {
        let clips = this.clipList[networkClientId];
        if (clips) {
            switch (this.clipListOrder) {
                case 'file_desc':
                    clips.sort((a, b) => {
                        const clipNameA = CcmModel.getClipName(a.uri);
                        const clipNameB = CcmModel.getClipName(b.uri);
                        if (clipNameA < clipNameB) return 1;
                        else if (clipNameA > clipNameB) return -1;
                        else return 0;
                    });
                    break;
                case 'file_asc':
                    clips.sort((a, b) => {
                        const clipNameA = CcmModel.getClipName(a.uri);
                        const clipNameB = CcmModel.getClipName(b.uri);
                        if (clipNameA < clipNameB) return -1;
                        else if (clipNameA > clipNameB) return 1;
                        else return 0;
                    });
                    break;
            }
        }
        return clips;
    }

    /**
     * ジョブ一覧の取得
     * @param {*} networkClientId 
     */
    getUploadList(networkClientId) {
        return this.upload_files[networkClientId];
    }


    /**
     * 
     * @param {*} networkClientID 
     * @param {*} data 
     */
    setUploadList(networkClientID, data) {
        let ncUploadFiles = this.upload_files[networkClientID];
        if (ncUploadFiles) {
            ncUploadFiles.length = 0;
        }

        for (const file of data) {
            const id = file.ID;
            if (!this.upload_files[id]) {
                this.upload_files[id] = [];
            }
            //clip.isSelected = false;
            this.upload_files[id].push(file);
        }
    }

    /**
     * 
     * @param {*} networkClientID 
     * @param {*} data 
     */
    setClipList(networkClientID, data) {
        let ncClip = this.clipList[networkClientID];
        if (ncClip) {
            ncClip.length = 0;
        }

        for (const clip of data) {
            const id = clip.ID;
            if (!this.clipList[id]) {
                this.clipList[id] = [];
            }
            clip.isSelected = false;
            this.clipList[id].push(clip);
        }
    }

    clipSelectionChanged(networkClientID, clipName, isChecked) {
        const clipInfo = this.getClipInfo(networkClientID, clipName);
        if (clipInfo.index >= 0) {
            clipInfo.clip.isSelected = isChecked;
        }
    }

    getClipInfo(networkClientID, clipName) {
        const clipList = this.clipList[networkClientID];
        const index = clipList.findIndex(i => CcmModel.getClipName(i.uri) === clipName);

        const result = { index: index };
        if (index >= 0) {
            result.clip = clipList[index];
        }
        return result;
    }

    /**
     * 選択された Clip が存在するか判定
     * @param {*} networkClientID 
     */
    isClipSelect(networkClientID) {
        let selected = false;
        const clipList = this.clipList[networkClientID];
        for (const clip of clipList) {
            if (clip.isSelected) {
                selected = true;
                break;
            }
        }

        return selected;
    }

    /**
     * 選択された Clip一覧 の取得
     * @param {*} networkClientID 
     */
    getSelectedClipList(networkClientID) {
        const clipList = this.clipList[networkClientID];
        const selectedClipList = [];

        for (const clip of clipList) {
            if (clip.isSelected) {
                selectedClipList.push(clip);
            }
        }

        return selectedClipList;
    }

    static getClipName(uri) {
        let name = '';
        const index = uri.lastIndexOf('/');
        if (index > 0) {
            name = uri.substring(index + 1);
        }

        return name;
    }




    /**
     * 非同期コマンド実行時のパラメータの登録
     * @param {*} networkClientId 
     * @param {*} command 
     * @param {*} data 
     */
    addExecutedCommandRequest(networkClientId, command, data) {
        this.command.Id = networkClientId;
        this.command.Command = command;
        this.command.Data = data;
        this.command.Result = {};
    }

    /**
     * 非同期コマンド実行時のレスポンス時のシーケンスIDの登録
     * @param {*} networkClientId 
     * @param {*} command 
     * @param {*} sequenceId 
     */
    addExecutedCommandResponse(networkClientId, command, sequenceId) {
        if ((this.command.Id === networkClientId) &&
            (this.command.Command === command)) {
            this.command.SequenceId = sequenceId;
        }
    }

    addExecutedCommandResult(networkClientId, command, sequenceId, result) {
        const isExecuted = this.isCommandExecuted(networkClientId, command, sequenceId);
        if (isExecuted) {
            this.command.Result = result;
        }
        return isExecuted;
    }

    getExecutedCommandResult(result) {
        result.success = 0;
        result.failed = 0;
        result.result = [];

        for (const i of this.command.Result.result) {
            if (i.error.code === 0) { result.success++; } else { result.failed++; }
            result.result.push(i);
        }
    }
    getAddUploadFilesCommandResult() {
        const result = {};
        result.success = 0;
        result.failed = 0;
        result.result = [];
        if (this.command.Command === 'AddUploadFiles') {
            const len = this.command.Data.files.length;
            for (let i = 0; i < len; ++i) {
                let item;
                if (this.command.Result.result) {
                    item = this.command.Result.result[i];
                }

                if (typeof (item) !== "undefined") {
                    item.file = CcmModel.getClipName(this.command.Data.files[i]);
                    if (item.error !== void 0) {
                        if (item.error.code === 0) { result.success++; } else { result.failed++; }
                    } else {
                        result.failed++;
                    }
                } else {
                    item = {};
                    item.file = CcmModel.getClipName(this.command.Data.files[i]);
                    item.error = {
                        code: 219,
                        message: 'unknown error',
                    };
                    result.failed++;
                }
                result.result.push(item);
            }
        }

        return result;
    }
    getGetUploadListCommandResult() {
        let result = {};
        if (this.command.Command === 'GetUploadList') {
            result = this.command.Result;
        }
        return result;
    }

    isCommandExecuted(networkClientId, command, sequenceId) {
        let isExecuted = false;
        if ((this.command.Id === networkClientId) &&
            (this.command.Command === command) &&
            (this.command.SequenceId === sequenceId)) {
            isExecuted = true;
        }

        return isExecuted;
    }

    removeCommand() {
        this.command.Id = '';
        this.command.Command = '';
        this.command.SequenceId = -1;
    }

    setNetworkClientStreamingStatus(networkClientId, status) {
        const networkClient = this.getNetworkClient(networkClientId);
        if (networkClient) {
            networkClient.StreamingStatus = status;
        }
    }




    /* --- NetworkClient (start) --- */

    /**
     * NetworkClientId から NetworkClient情報を取得
     * @param {*} networkClientId 
     */
    getNetworkClient(networkClientId) {
        return this.networkClientList.find(item => item.ID === networkClientId);
    }
    getNetworkClientIndex(networkClientId) {
        return this.networkClientList.findIndex(i => i.ID === networkClientId);
    }

    setNetworkClient(networkClient) {
        let index = this.getNetworkClientIndex(networkClient.ID);
        if (index === -1) {
            this.networkClientList.push(networkClient);
        } else {
            this.networkClientList.splice(index, 1, networkClient);
        }
    }

    setNetworkClientStatus(networkClientId, isConnected) {
        const networkClient = this.getNetworkClient(networkClientId);
        if (networkClient) {
            if (isConnected === true) {
                networkClient.Status = 'connect';
            }
            else {
                networkClient.Status = 'disconnect';
            }
        }
    }

    /* --- NetworkClient (end) --- */

    /* --- NetworkClient一覧 (start) --- */

    /**
     * 管理している NetworkClient一覧をすべて切断済みに変更
     */
    clearNetworkClientList() {
        for (const networkClient of this.networkClientList) {
            this.setNetworkClientStatus(networkClient.ID, false);
        }
    }

    /**
     * 渡されたデータにしたがって NetworkClient情報を更新
     * @param {*} data 
     */
    setNetworkClientList(data) {
        for (const networkClient of data) {
            this.setNetworkClient(networkClient);
        }
    }

    /**
     * NetworkClient一覧の取得
     */
    getNetworkClientList() {
        this.networkClientList.sort((a, b) => {
            const clipNameA = a.DisplayName;
            const clipNameB = b.DisplayName;
            if (clipNameA < clipNameB) return -1;
            else if (clipNameA > clipNameB) return 1;
            else return 0;
        });
        return this.networkClientList;
    }

    /* --- NetworkClient一覧 (end) --- */




    /* --- 選択されたNetworkClient情報 (start) --- */

    /***
     * Tx一覧画面で選択された NetworkClient の NetworkClientID を設定
     */
    setSelectedTxNetworkClientId(networkClientId) {
        this.selectedTxNetworkClientId = networkClientId;
    }

    /**
     * Tx一覧で選択されている NetworkClient情報 の取得
     */
    getSelectedTxNetworkClient() {
        const nc = this.networkClientList.find(item => item.ID === this.selectedTxNetworkClientId);
        return nc;
    }

    getSelectedTxNetworkClientId() {
        const networkClient = this.getSelectedTxNetworkClient();
        if (networkClient) {
            return networkClient.ID;
        }
        return "";
    }

    /* --- 選択されたNetworkClient情報 (end) --- */

    /* --- ClipInspectionView (start) --- */

    hideClipInspectionView() {
        this.showClipInspectionView = false;
    }

    toggleClipInspectionView() {
        this.showClipInspectionView = !this.showClipInspectionView;
        return this.showClipInspectionView;
    }

    /* --- ClipInspectionView (end) --- */

    /* --- PlanningMetadata Inspection View (start) --- */

    hidePlanningMetadataInspectionView() {
        this.showPlanningMetadataInspectionView = false;
    }

    togglePlanningMetadataInspectionView() {
        this.showPlanningMetadataInspectionView = !this.showPlanningMetadataInspectionView;
        return this.showPlanningMetadataInspectionView;
    }

    /* --- PlanningMetadata Inspection View (end) --- */

    /* --- ジョブ登録中 McbNetworkInfo --- (start) */

    /**
     * NetworkClientから送信された McbNetworkInfo の設定
     * @param {*} data NetworkClientから送信された McbNetworkInfo
     */
    setMcbNetworkInfo(data) {
        let mcbNetworkInfo = this.getMcbNetworkInfoData(data.ID);
        if (mcbNetworkInfo === undefined) {
            mcbNetworkInfo = {
                ID: data.ID,
                timestamp: [],
                loss: { nic1: [], nic2: [], nic3: [], nic4: [] },
                bitrate: { nic1: [], nic2: [], nic3: [], nic4: [], total: [] },
                rtt: { nic1: [], nic2: [], nic3: [], nic4: [] }
            };
            this.mcb_network_info.push(mcbNetworkInfo);
        }

        // 現在時刻を取得
        const dt = new Date();
        const dtMoment = moment(dt);
        const timestamp = dtMoment.format('YYYY-MM-DD HH:mm:ss');

        // source情報に入っている情報をNIC毎に分解
        let totalBitrate = 0;
        for (let nicIndex = 0; nicIndex < data.McbNetworkInfo.length; nicIndex++) {
            const nicdata = data.McbNetworkInfo[nicIndex];
            const nic = nicdata.ID;
            const loss = isNaN(nicdata["loss"]) == true ? NaN : nicdata["loss"];
            const bitrate = isNaN(nicdata["rate"]) == true ? NaN : nicdata["rate"];
            const rtt = isNaN(nicdata["rtt"]) == true ? NaN : nicdata["rtt"];
            console.log(`transmit info | nic:${nic}, loss:${loss}, bitrate:${bitrate}, rtt:${rtt}, timestamp:${timestamp}`);

            if (nic === "NIC1") {
                console.log("McbNetworkInfo : nic1");
                // --- loss
                if (mcbNetworkInfo.loss.nic1.length >= 60) {
                    mcbNetworkInfo.loss.nic1.shift();
                }
                mcbNetworkInfo.loss.nic1.push(loss);
                // --- bitrate
                if (mcbNetworkInfo.bitrate.nic1.length >= 60) {
                    mcbNetworkInfo.bitrate.nic1.shift();
                }
                mcbNetworkInfo.bitrate.nic1.push(bitrate);
                // --- rtt
                if (mcbNetworkInfo.rtt.nic1.length >= 60) {
                    mcbNetworkInfo.rtt.nic1.shift();
                }
                mcbNetworkInfo.rtt.nic1.push(rtt);
            } else if (nic === "NIC2") {
                console.log("McbNetworkInfo : nic2");
                // --- loss
                if (mcbNetworkInfo.loss.nic2.length >= 60) {
                    mcbNetworkInfo.loss.nic2.shift();
                }
                mcbNetworkInfo.loss.nic2.push(loss);
                // --- bitrate
                if (mcbNetworkInfo.bitrate.nic2.length >= 60) {
                    mcbNetworkInfo.bitrate.nic2.shift();
                }
                mcbNetworkInfo.bitrate.nic2.push(bitrate);
                // --- rtt
                if (mcbNetworkInfo.rtt.nic2.length >= 60) {
                    mcbNetworkInfo.rtt.nic2.shift();
                }
                mcbNetworkInfo.rtt.nic2.push(rtt);
            } else if (nic === "NIC3") {
                console.log("McbNetworkInfo : nic3");
                // --- loss
                if (mcbNetworkInfo.loss.nic3.length >= 60) {
                    mcbNetworkInfo.loss.nic3.shift();
                }
                mcbNetworkInfo.loss.nic3.push(loss);
                // --- bitrate
                if (mcbNetworkInfo.bitrate.nic3.length >= 60) {
                    mcbNetworkInfo.bitrate.nic3.shift();
                }
                mcbNetworkInfo.bitrate.nic3.push(bitrate);
                // --- rtt
                if (mcbNetworkInfo.rtt.nic3.length >= 60) {
                    mcbNetworkInfo.rtt.nic3.shift();
                }
                mcbNetworkInfo.rtt.nic3.push(rtt);
            } else if (nic === "NIC4") {
                console.log("McbNetworkInfo : nic4");
                // --- loss
                if (mcbNetworkInfo.loss.nic4.length >= 60) {
                    mcbNetworkInfo.loss.nic4.shift();
                }
                mcbNetworkInfo.loss.nic4.push(loss);
                // --- bitrate
                if (mcbNetworkInfo.bitrate.nic4.length >= 60) {
                    mcbNetworkInfo.bitrate.nic4.shift();
                }
                mcbNetworkInfo.bitrate.nic4.push(bitrate);
                // --- rtt
                if (mcbNetworkInfo.rtt.nic4.length >= 60) {
                    mcbNetworkInfo.rtt.nic4.shift();
                }
                mcbNetworkInfo.rtt.nic4.push(rtt);
            }

            if (isNaN(bitrate) == false) {
                totalBitrate += Number(bitrate);
            }
        }
        // bitrate total.
        if (mcbNetworkInfo.bitrate.total.length >= 60) {
            mcbNetworkInfo.bitrate.total.shift();
        }
        mcbNetworkInfo.bitrate.total.push(totalBitrate);
        // Timestamp
        if (mcbNetworkInfo.timestamp.length == 0) {
            mcbNetworkInfo.timestamp.push(dt);
        } else {
            if (dt.getTime() > mcbNetworkInfo.timestamp[mcbNetworkInfo.timestamp.length - 1].getTime()) {
                mcbNetworkInfo.timestamp.push(dt);
            }
            while (mcbNetworkInfo.timestamp.length > 60) {
                mcbNetworkInfo.timestamp.shift();
            }
        }
    }

    /**
     * 指定された NetworkClient の McbNetworkInfo の取得
     * @param {*} networkClientId 取得するデータのNetworkClientID
     */
    getMcbNetworkInfoData(networkClientId) {
        return this.mcb_network_info.find(item => item.ID === networkClientId);
    }

    /* --- ジョブ登録中 NetworkInfo --- (end) */

    /* --- ストリーミング情報 NetworkInfo --- (start) */

    setStreamingDynamicProperty(data) {
        // 現在時刻
        const dt = new Date();
        const dtMoment = moment(dt);
        const timestamp = dtMoment.format('YYYY-MM-DD HH:mm:ss');
        /// source情報に入っている情報をNIC毎に分解
        let totalBitrate = 0;
        for (let nicIndex = 0; nicIndex < data.length; nicIndex++) {
            const nicdata = data[nicIndex];
            const nic = nicdata.nic_name;
            const loss = isNaN(nicdata["loss_rate"]) == true ? NaN : nicdata["loss_rate"];
            const bitrate = isNaN(nicdata["rate"]) == true ? NaN : nicdata["rate"];
            const delay = isNaN(nicdata["delay"]) == true ? NaN : nicdata["delay"];
            const rtt = isNaN(nicdata["rtt"]) == true ? NaN : nicdata["rtt"];
            console.log(`transmit info | nic: ${nic}, loss: ${loss}, bitrate: ${bitrate}, delay: ${delay}, rtt: ${rtt}, timestamp: ${timestamp}`);
            //
            if (nic === "NIC1") {
                console.log("StreamingDynamicProperty : nic1");
                if (this.streaming_dynamic_property.loss.nic1.length >= 60) {
                    this.streaming_dynamic_property.loss.nic1.shift();
                }
                this.streaming_dynamic_property.loss.nic1.push(loss);
                if (this.streaming_dynamic_property.bitrate.nic1.length >= 60) {
                    this.streaming_dynamic_property.bitrate.nic1.shift();
                }
                this.streaming_dynamic_property.bitrate.nic1.push(bitrate);
                if (this.streaming_dynamic_property.delay.nic1.length >= 60) {
                    this.streaming_dynamic_property.delay.nic1.shift();
                }
                this.streaming_dynamic_property.delay.nic1.push(delay);
                if (this.streaming_dynamic_property.rtt.nic1.length >= 60) {
                    this.streaming_dynamic_property.rtt.nic1.shift();
                }
                this.streaming_dynamic_property.rtt.nic1.push(rtt);
            } else if (nic === "NIC2") {
                console.log("StreamingDynamicProperty : nic2");
                if (this.streaming_dynamic_property.loss.nic2.length >= 60) {
                    this.streaming_dynamic_property.loss.nic2.shift();
                }
                this.streaming_dynamic_property.loss.nic2.push(loss);
                if (this.streaming_dynamic_property.bitrate.nic2.length >= 60) {
                    this.streaming_dynamic_property.bitrate.nic2.shift();
                }
                this.streaming_dynamic_property.bitrate.nic2.push(bitrate);
                if (this.streaming_dynamic_property.delay.nic2.length >= 60) {
                    this.streaming_dynamic_property.delay.nic2.shift();
                }
                this.streaming_dynamic_property.delay.nic2.push(delay);
                if (this.streaming_dynamic_property.rtt.nic2.length >= 60) {
                    this.streaming_dynamic_property.rtt.nic2.shift();
                }
                this.streaming_dynamic_property.rtt.nic2.push(rtt);
            } else if (nic === "NIC3") {
                console.log("StreamingDynamicProperty : nic3");
                if (this.streaming_dynamic_property.loss.nic3.length >= 60) {
                    this.streaming_dynamic_property.loss.nic3.shift();
                }
                this.streaming_dynamic_property.loss.nic3.push(loss);
                if (this.streaming_dynamic_property.bitrate.nic3.length >= 60) {
                    this.streaming_dynamic_property.bitrate.nic3.shift();
                }
                this.streaming_dynamic_property.bitrate.nic3.push(bitrate);
                if (this.streaming_dynamic_property.delay.nic3.length >= 60) {
                    this.streaming_dynamic_property.delay.nic3.shift();
                }
                this.streaming_dynamic_property.delay.nic3.push(delay);
                if (this.streaming_dynamic_property.rtt.nic3.length >= 60) {
                    this.streaming_dynamic_property.rtt.nic3.shift();
                }
                this.streaming_dynamic_property.rtt.nic3.push(rtt);
            } else if (nic === "NIC4") {
                console.log("StreamingDynamicProperty : nic4");
                if (this.streaming_dynamic_property.loss.nic4.length >= 60) {
                    this.streaming_dynamic_property.loss.nic4.shift();
                }
                this.streaming_dynamic_property.loss.nic4.push(loss);
                if (this.streaming_dynamic_property.bitrate.nic4.length >= 60) {
                    this.streaming_dynamic_property.bitrate.nic4.shift();
                }
                this.streaming_dynamic_property.bitrate.nic4.push(bitrate);
                if (this.streaming_dynamic_property.delay.nic4.length >= 60) {
                    this.streaming_dynamic_property.delay.nic4.shift();
                }
                this.streaming_dynamic_property.delay.nic4.push(delay);
                if (this.streaming_dynamic_property.rtt.nic4.length >= 60) {
                    this.streaming_dynamic_property.rtt.nic4.shift();
                }
                this.streaming_dynamic_property.rtt.nic4.push(rtt);
            }

            if (isNaN(bitrate) == false) {
                totalBitrate += Number(bitrate);
            }
        }
        // bitrate total.
        if (this.streaming_dynamic_property.bitrate.total.length >= 60) {
            this.streaming_dynamic_property.bitrate.total.shift();
        }
        this.streaming_dynamic_property.bitrate.total.push(totalBitrate);
        // Timestamp
        if (this.streaming_dynamic_property.timestamp.length == 0) {
            this.streaming_dynamic_property.timestamp.push(dt);
        } else {
            if (dt.getTime() > this.streaming_dynamic_property.timestamp[this.streaming_dynamic_property.timestamp.length - 1].getTime()) {
                this.streaming_dynamic_property.timestamp.push(dt);
            }
            while (this.streaming_dynamic_property.timestamp.length > 60) {
                this.streaming_dynamic_property.timestamp.shift();
            }
        }
    }

    getStreamingDynamicPropertyData() {
        return this.streaming_dynamic_property;
    }

    /* --- ストリーミング情報 NetworkInfo --- (end) */

    /* --- ネットワーク情報 NetworkInfo --- (start) */

    setNetworkInfo(data) {
        let networkInfo = this.getNetworkInfoData(data.ID);
        if (networkInfo === undefined) {
            networkInfo = {
                ID: data.ID,
                CellularInfo: data.CellularInfo,
                QosInterfaceType: data.QosInterfaceType
            }
            this.network_info.push(networkInfo);
        }
        else {
            networkInfo.CellularInfo = data.CellularInfo;
            networkInfo.QosInterfaceType = data.QosInterfaceType;
        }
    }

    getNetworkInfoData(networkClientId) {
        return this.network_info.find(item => item.ID === networkClientId);
    }

    /* --- ネットワーク情報 NetworkInfo --- (end) */

    /* --- 位置情報 GPSInfo --- (start) */

    setGPSInfo(data) {
        let gpsInfo = this.getGpsInfoData(data.ID);
        if (gpsInfo === undefined) {
            gpsInfo = {
                ID: data.ID,
                update_time: data.update_time,
                latitude: data.latitude,
                longitude: data.longitude
            };
            this.gps_info.push(gpsInfo);
        }
        else {
            gpsInfo.update_time = data.update_time;
            gpsInfo.latitude = data.latitude;
            gpsInfo.longitude = data.longitude;
        }
    }

    getGpsInfoData(networkClientId) {
        return this.gps_info.find(item => item.ID === networkClientId);
    }

    /* --- 位置情報 GPSInfo --- (end) */

    /* --- Recorder 制御状態 --- (start) */

    setRecorderControlStatus(data) {
        let statusData = this.getRecorderControlStatusData(data.ID);
        if (statusData === undefined) {
            statusData = {
                ID: data.ID,
                low_resolution: data.low_resolution,
                high_resolution: data.high_resolution
            };
            this.recorder_control_status.push(statusData);
        }
        else {
            statusData.low_resolution = data.low_resolution;
            statusData.high_resolution = data.high_resolution;
        }
    }

    getRecorderControlStatusData(networkClientId) {
        return this.recorder_control_status.find(item => item.ID === networkClientId);
    }

    /* --- Recorder 制御状態 --- (end) */

    /* --- Recorder Rec状態 --- (start) */

    setClipRecorderStatus(data) {
        let statusData = this.getClipRecorderStatusData(data.ID);
        if (statusData === undefined) {
            statusData = {
                ID: data.ID,
                result: data.result,
            };
            this.recorder_cliprecorder_status.push(statusData);
        }
        else {
            statusData.result = data.result;
        }
    }

    getClipRecorderStatusData(networkClientId) {
        const statusData = this.recorder_cliprecorder_status.find(item => item.ID === networkClientId);
        return statusData;
    }

    /* --- Recorder Rec状態 --- (end) */

    /* --- Recorder クリップ保存メディア状態 --- (start) */

    setDriveStatus(data) {
        let statusData = this.getDriveStatusData(data.ID);
        if (statusData !== undefined) {
            statusData.Media = data.drives;
        }
    }

    getDriveStatusData(networkClientId) {
        const networkClient = this.getNetworkClient(networkClientId);
        return networkClient;
    }

    /* --- Recorder クリップ保存メディア状態 --- (end) */

    /* --- Recorder PlanningMetadata制御 --- (start) */

    setPlanningMetadataList(data) {
        let metadata = this.getPlanningMetadataList(data.ID);
        if (metadata === undefined) {
            metadata = {
                ID: data.ID,
                PlanningMetadata: data.PlanningMetadata
            }
            this.planning_metadata_list.push(metadata);
        }
        else {
            metadata.PlanningMetadata = data.PlanningMetadata;
        }
    }

    getPlanningMetadataList(networkClientId) {
        const list = this.planning_metadata_list.find(item => item.ID === networkClientId);
        return list;
    }

    getPlanningMetadataInfo(networkClientId, name) {
        const list = this.getPlanningMetadataList(networkClientId);
        if(list !== undefined) {
            const item = list.PlanningMetadata.find(item => item.name === name);
            return item;
        }
        else {
            return undefined;
        }
    }

    setPlanningMetadataControlStatus(data) {
        let status = this.getPlanningMetadataControlStatus(data.ID);
        if (status === undefined) {
            status = {
                ID: data.ID,
                Status: data.PlanningMetadata
            }
            this.recorder_planningmetadata_control.push(status);
        }
        else {
            status.Status = data.PlanningMetadata;
        }
    }

    getPlanningMetadataControlStatus(networkClientId) {
        const status = this.recorder_planningmetadata_control.find(item => item.ID === networkClientId);
        return status;
    }

    /* --- Recorder PlanningMetadata制御 --- (start) */
}
