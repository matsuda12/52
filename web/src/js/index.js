class Controller {
    /**
     * コンストラクタ
     * @param {Object} model モデルオブジェクト
     */
    constructor(model) {
        this.model = model;
        this.ccm = new Ccm(this);

        // WebSocket受信データ解析用データ
        //   複数のコマンドが結合して送られてくることがあるため、解析用に準備
        this.receivedData = [];
        this.receivedRemainData = '';

        // McbNetworkInfo チャート用データ
        this.lossMcbNetworkInfoChart;
        this.bitrateMcbNetworkInfoChart;
        this.rttMcbNetworkInfoChart;
        this.dataMcbNetworkInfoChart = this.model.getMcbNetworkInfoData("dummy");

        // Streamingチャート用データ
        this.lossStreamingChart;
        this.bitrateStreamingChart;
        this.delayStreamingChart;
        this.rttStreamingChart;
        this.dataStreamingChart = this.model.getStreamingDynamicPropertyData();
        this.timerStreamingChart;
    }

    /**
     * 初期化処理
     */
    initialize() {
        this.ccm.initialize();
        this._initializeActionHandler();
        this._initializeMcbNetworkInfoChart();
        this._initializeStreamingDynamicPropertyChart();
        this._initializeClipTransferPreset();
        this._initializeStreamingSendStartPreset();
        this._initializeStreamingReceiveStartPreset();
        this._initializeVoIPStartPreset();
    }

    /* --- WebSocket処理 (Start) --- */

    /**
     * WebSocketデータの解析
     * @param {String} data WebSocketデータ
     */
    _parseWebSocketData(data) {
        let parseData = this.receivedRemainData;
        parseData += data;

        let index = 0;
        let tmp = '';
        while (index >= 0) {
            index = parseData.indexOf('}', index);
            if (index >= 0) {
                tmp = parseData.substring(0, index + 1);
                try {
                    const jsonObject = JSON.parse(tmp);
                    this.receivedData.push(jsonObject);
                    parseData = parseData.substr(index + 1);
                    index = 0;
                } catch (e) {
                    index = index + 1;
                }
            }
        }

        this.receivedRemainData = parseData;
    }
    onOpenedWebSocket(event) {
        this.ccm.cmdGetNetworkClientList();
    }
    onReceivedWebSocket(event) {
        event.data.replace(/\r?\n/g, '');
        this._parseWebSocketData(event.data);
        for (const json of this.receivedData) {
            if (json['type'] === 'response') {
                if (json['command'] === 'GetNetworkClient') {
                    this._onResponseGetNetworkClient(json.data);
                }
                else if (json['command'] === 'GetNetworkClientList') {
                    this._onResponseGetNetworkClientList(json.data);
                }
                else if (json['command'] === 'GetClipList') {
                    this._onResponseGetClipList(json.data);
                }
                else if (json['command'] === 'AddUploadFiles') {
                    this._onResponseAddUploadFiles(json.data);
                }
                else if (json['command'] === 'GetUploadList') {
                    this._onResponseGetUploadList(json.data);
                }
                else if (json['command'] === 'StartQosStreamingSend') {
                    this._onResponseStartQosStreamingSend(json.data);
                }
                else if (json['command'] === 'StopStreamingSend') {
                    this._onResponseStopStreamingSend(json.data);
                }
                else if (json['command'] === 'StartQosStreamingReceive') {
                    this._onResponseStartQosStreamingReceive(json.data);
                }
                else if (json['command'] === 'StopStreamingReceive') {
                    this._onResponseStopStreamingReceive(json.data);
                }
                else if (json['command'] === 'GetPlanningMetadataList') {
                    // PlanningMetadata一覧取得
                    this._onResponseGetPlanningMetadataList(json.data);
                }
                else if (json['command'] === 'GetCurrentPlanningMetadata') {
                    // カレントのPlanningMetadata情報の取得
                    this._onResponseGetCurrentPlanningMetadata(json.data);
                }
                else if (json['command'] === 'GetPlanningMetadata') {
                    // 指定したPlanningMetadata情報の取得
                    this._onResponseGetPlanningMetadata(json.data);
                }
                else if (json['command'] === 'SetPlanningMetadata') {
                    this._onResponseSetPlanningMetadata(json.data)
                }
                else if (json['command'] === 'PutPlanningMetadata') {
                    this._onResponsePutPlanningMetadata(json.data)
                }
            } else if (json['type'] === 'result') {
                if (json['command'] === 'AddUploadFiles') {
                    this._onResultAddUploadFiles(json.data);
                }
                // else if(json['command'] === 'GetUploadList') {
                //     this._onResultGetUploadList(json.data);
                // }
            } else if (json['type'] === 'notify') {
                if (json['command'] === 'NetworkClientConnect') {
                    this._onNotifyNetworkClientConnect(json.data);
                }
                else if (json['command'] === 'StreamingStatus') {
                    this._onNotifyStreamingStatus(json.data);
                }
                else if (json['command'] === 'VoIPStatus') {
                    this._onNotifyVoipStatus(json.data);
                }
                else if (json['command'] === 'StreamingDynamicProperty') {
                    this._onNotifyStreamingDynamicProperty(json.data);
                }
                else if (json['command'] === 'ReceiveThumbnail') {
                    this._onNotifyReceiveThumbnail(json.data);
                }
                else if (json['command'] === 'McbNetworkInfo') {
                    this._onNotifyMcbNetworkInfo(json.data);
                }
                else if (json['command'] === 'NetworkInfo') {
                    this._onNotifyNetworkInfo(json.data);
                }
                else if (json['command'] === 'GPSInfo') {
                    this._onNotifyGPSInfo(json.data);
                }
                else if (json['command'] === 'RecorderControlStatus') {
                    this._onNotifyRecorderControlStatus(json.data);
                }
                else if (json['command'] === 'ClipRecorderStatus') {
                    this._onNotifyClipRecorderStatus(json.data);
                }
                else if (json['command'] === 'DriveStatus') {
                    this._onNotifyDriveStatus(json.data);
                }
                else if (json['command'] === 'PlanningMetadataControlStatus') {
                    // PlanningMetadata機能有効・無効の変更通知
                    this._onNotifyPlanningMetadataControlStatus(json.data);
                }
            }
        }
        this.receivedData.length = 0;
    }
    onClosedWebSocket(event) {
    }

    /* --- WebSocket処理 (End) --- */

    /* --- 初期化処理 (Start) --- */

    /**
     * イベント通知の初期化処理
     */
    _initializeActionHandler() {
        /**
         * Operation Menu View タブ切り替え時の処理
         */
        $('a[data-toggle="tab"]').on('shown.bs.tab', { controller: this }, (event) => {
            event.data.controller._navChanged();
        })

        /**
         * ジョブ登録画面表示処理
         */
        $('#cliptransfer_view').on('show.bs.modal', (event) => {
            $('#cliptransferview_form').removeClass('d-none');
            $('#cliptransferview_preset').removeClass('d-none');
            $('#cliptransferview_result').addClass('d-none');
            $('#cliptransferview_cancel').removeClass('d-none');
            $('#cliptransferview_cancel').prop('disabled', false);
            $('#cliptransferview_close').removeClass('d-none');
            $('#cliptransferview_close').prop('disabled', false);
            $('#cliptransferview_submit_spinner').addClass('d-none');

            const valPreset = $('#cliptransferview_preset').val();
            this._onChangeClipTransferViewPreset(valPreset);
        })

        /**
         * ClipList内の Mediaが変更された時の処理
         */
        $('#cliplist_media').on('change', { controller: this }, (event) => {
            event.data.controller._mediaChanged();
        });

        $('#cliplist_sort').on('change', { controller: this }, (event) => {
            event.data.controller._changeClipListSort();
        });

        /**
         * Clip Transfer View のプリセット選択が変更された際の処理
         */
        $('#cliptransferview_preset').on('change', { controller: this }, (event) => {
            event.data.controller._onChangeClipTransferViewPreset(event.currentTarget.value);
        });

        /**
         * ストリーミング開始設定のプリセット選択が変更された際の処理
         */
        $('#streamingstart_view_preset').on('change', { controller: this }, (event) => {
            event.data.controller._onChangeStreamingStartViewPreset(event.currentTarget.value);
        });

        /**
         * ストリーミング受信開始設定のプリセット選択が変更された際の処理
         */
        $('#streamingreceivestart_view_preset').on('change', { controller: this }, (event) => {
            event.data.controller._onChangeStreamingReceiveStartViewPreset(event.currentTarget.value);
        });

        /**
         * Clip Transfer View のOKボタンを押下した際の処理
         */
        $('#cliptransferview_submit').on('click', { controller: this }, (event) => {
            event.data.controller._onClickClipTransferViewSubmit(event.data.controller);
        })

        /**
         * Clip Inspection View の展開/折り畳み ボタン押下時の処理
         */
        $('#collapse_clip_inspection_view_button').on('click', { controller: this }, (event) => {
            const show = event.data.controller.model.toggleClipInspectionView();
            const elements = $('#collapse_clip_inspection_view_button');
            if (elements) {
                this._showClipInspectionView(show);
            }
        });

        $('#collapse_recorder_planmeta_inspection_view_button').on('click', { controller: this }, (event) => {
            const show = event.data.controller.model.togglePlanningMetadataInspectionView();
            const elements = $('#collapse_recorder_planmeta_inspection_view_button');
            if (elements) {
                this._showPlanningMetadataInspectionView(show);
            }
        });

        /**
         * クリップ一覧画面の Reload ボタン押下処理
         */
        $('#cliplist_reload_button').on('click', { controller: this }, (event) => {
            event.data.controller._mediaChanged();
        });

        /**
         * ジョブ一覧画面の Reload ボタン押下処理
         */
        $('#joblist_reload_button').on('click', { controller: this }, (event) => {
            event.data.controller._getJobList();
        });

        /**
         * PlanningMetadata一覧画面の GetMetadata ボタン押下処理
         */
        $('#recorder_planmeta_get_current_metadata').on('click', { controller: this }, (event) => {
            event.data.controller._selectionClearPlanningMetadata();
            event.data.controller._getCurrentPlanningMetadata();
        });

        /**
         * PlanningMetadata一覧画面の Reload ボタン押下処理
         */
        $('#recorder_planmeta_reload_button').on('click', { controller: this }, (event) => {
            event.data.controller._getPlanningMetadataList();
        });

        /**
         * ジョブ登録画面の UseMcbチェックボタン のチェック状態変更時の処理
         */
        $('#isUseMcb').on('change', { controller: this }, (event) => {
            if (event.currentTarget.checked) {
                event.data.controller._showIsUseMcbConfig(true);
            } else {
                event.data.controller._showIsUseMcbConfig(false);
            }
        });

        /**
         * ストリーミング停止ボタン押下処理
         */
        $('#streaming_stop_button').on('click', { controller: this }, (event) => {
            event.data.controller._onClickStreamingStop();
        });

        $('#streaming_receive_stop_button').on('click', { controller: this }, (event) => {
            event.data.controller._onClickStreamingReceiveStop();
        });

        /**
         * ストリーミング開始設定用モーダル画面表示処理
         */
        $('#streamingstart_view').on('show.bs.modal', { controller: this }, (event) => {
            const networkClient = event.data.controller.model.getSelectedTxNetworkClient();
            event.data.controller._setStreamingStartVideoFormatOption(networkClient.VideoFormatCapability)
            event.data.controller._setStreamingStartQosInterfacesOption(networkClient.NetworkInterfaces)
            event.data.controller._setStreamingStartMetaInterfacesOption(networkClient.NetworkInterfaces)

            const valPreset = $('#streamingstart_view_preset').val();
            event.data.controller._onChangeStreamingStartViewPreset(valPreset);
        });

        /**
         * ストリーミング開始設定用モーダル画面表示処理
         */
        $('#streamingreceivestart_view').on('show.bs.modal', { controller: this }, (event) => {
            const networkClient = event.data.controller.model.getSelectedTxNetworkClient();
            event.data.controller._setStreamingReceiveStartVideoFormatOption(networkClient.VideoFormatCapability)
            event.data.controller._setStreamingReceiveStartQosInterfacesOption(networkClient.NetworkInterfaces)
            event.data.controller._setStreamingReceiveStartMetaInterfacesOption(networkClient.NetworkInterfaces)

            const valPreset = $('#streamingreceivestart_view_preset').val();
            event.data.controller._onChangeStreamingReceiveStartViewPreset(valPreset);
        });

        /**
         * VoIP開始設定用モーダル画面表示処理
         */
        $('#voipstart_view').on('show.bs.modal', { controller: this }, (event) => {
            const valPreset = $('#voipstart_view_preset').val();
            event.data.controller._onChangeVoIPStartViewPreset(valPreset);
        });

        /**
         * VoIP停止ボタン押下処理
         */
        $('#VoIP_stop_button').on('click', { controller: this }, (event) => {
            event.data.controller._onClickVoIPStop();
        });

        /**
         * ストリーミング開始設定の VideoFormat 設定変更時の処理
         */
        $('#streamingstart_form_videoformat').on('change', { controller: this }, (event) => {
            const value = $('#streamingstart_form_videoformat').val();
            event.data.controller._setStreamingStartBitrateOption(value);
        });

        /**
         * ストリーミング開始モーダルの開始ボタン押下処理
         */
        $('#streamingstart_view_submit').on('click', { controller: this }, (event) => {
            event.data.controller._onClickStreamingStart();
        });

        $('#streamingreceivestart_view_submit').on('click', { controller: this }, (event) => {
            event.data.controller._onClickStreamingReceiveStart();
        });

        /**
         * RecoderタブのDriveの選択が変更された際の処理
         */
        $('#recorder_media').on('change', { controller: this }, (event) => {
            event.data.controller._updateDriveStatus();
        });


        /**
         * PlanningMetadata登録用モーダル画面で、参照ファイルを変更したときの処理
         */
        $('#recorder-plameta-putfile-form-file').on('change', { controller: this }, (event) => {
            event.data.controller._onChangePutPlanningMetadataFile(event.target.files);
        })

        /**
         * PlanningMetadata登録用モーダル画面で、リセットボタンを押下したときの処理
         */
        $('#recorder-plameta-putfile-form-file_reset').on('click', { controller: this }, (event) => {
            event.data.controller._onCancelRecorderPlametaRegisterfileForm();
        })

        /**
         * PlanningMetadata登録用モーダル画面を閉じたときの処理
         */
        $('#recorder_plameta_put_view').on('hidden.bs.modal', { controller: this }, (event) => {
            event.data.controller._onCancelRecorderPlametaRegisterfileForm();
        })

        /**
         * PlanningMetadata登録用モーダル画面で、登録ボタンを押下したときの処理
         */
        $('#recorder_plameta_put_view_submit').on('click', { controller: this }, (event) => {
            event.data.controller._onClickPutPlametaViewSubmit();
        });

        /**
         * VoIP開始設定のプリセット選択が変更された際の処理
         */
        $('#voipstart_view_preset').on('change', { controller: this }, (event) => {
            event.data.controller._onChangeVoIPStartViewPreset(event.currentTarget.value);
        });

        /**
         * VoIP開始モーダルの開始ボタン押下処理
         */
        $('#voipstart_view_submit').on('click', { controller: this }, (event) => {
            event.data.controller._onClickVoIPStart();
        });

    }

    /* --- 初期化処理 (End) --- */

    /* --- Operation Menu View タブ切り替え (start) ---*/

    /**
     * NetworkClientの機能によって、タブの有効/無効の切り替え
     */
    _navInitiazlie() {
        const networkClient = this.model.getSelectedTxNetworkClient();
        if (networkClient.StreamingFunction === true) {
            $('#nav-main-streaming').removeClass('disabled');
            $('#nav-main-streaming').tab('show');
            $('#streaming_main_view_image').empty();
        } else {
            $('#nav-main-streaming').addClass('disabled');
        }

		const VoIPState = this.model.getSelectedTxVoIPState();
        if (VoIPState.Status === "00") {
            $('#nav-main-voip').addClass('disabled');
        } else {
            $('#nav-main-voip').removeClass('disabled');
            $('#nav-main-voip').tab('show');
        }	

			
        if (networkClient.FileTransferFunction === true) {
            $('#nav-main-cliplist').removeClass('disabled');
            $('#nav-main-joblist').removeClass('disabled');
            if (networkClient.StreamingFunction === false) {
                $('#nav-main-cliplist').tab('show');
            }
        } else {
            $('#nav-main-cliplist').addClass('disabled');
            $('#nav-main-joblist').addClass('disabled');
        }

        const networkClientId = this.model.getSelectedTxNetworkClientId();
        const recorderControlStatus = this.model.getRecorderControlStatusData(networkClientId);
        if (recorderControlStatus &&
            ((recorderControlStatus.low_resolution === true) ||
                (recorderControlStatus.high_resolution === true))) {
            this._navDisableRecorderTab(false);
        } else {
            this._navDisableRecorderTab(true);
        }
    }

    /**
     * Operation Menu View タブ切り替え処理
     */
    _navChanged() {
        const activeTab = $('#operation_menu_view_tab').find('.active');
        if (activeTab && activeTab[0]) {
            const tab = activeTab[0];
            console.log(`ActiveTab : ${tab.text}`);
            this._navChangedCommon();
            if (tab.id === 'nav-main-streaming') {
                this._navChangedStreaming();
            } else if (tab.id === 'nav-main-voip') {
                this._navChangedVoIP();
            } else if (tab.id === 'nav-main-cliplist') {
                this._navChangedCliplist();
            } else if (tab.id === 'nav-main-joblist') {
                this._navChangedJoblist();
            } else if (tab.id === 'nav-main-control') {
                this._navChangedControl();
            } else if (tab.id === 'nav-main-recorder') {
                this._navChangedRecorder();
            }
        }
    }

    /**
     * Operation Menu View タブ切り替え時の共通処理
     */
    _navChangedCommon() {
        this.model.hideClipInspectionView();
        this.model.hidePlanningMetadataInspectionView();
        this._showClipInspectionView(false);
        this._showPlanningMetadataInspectionView(false);
        this._clearClipList();
        this._clearUploadList();
    }
    /**
     * Operation Menu View タブ切り替え時のStreaming画面初期化処理
     */
    _navChangedStreaming() {
        // Streamingステータスによって、ボタンの有効/無効の切り替え
        this._changeEnableStreamingStartButton();
        this._changeEnableStreamingStopButton();
    }
    /**
     * Operation Menu View タブ切り替え時のVoIP画面初期化処理
     */
    _navChangedVoIP() {
        // VoIPステータスによって、ボタンの有効/無効の切り替え
        this._changeEnableVoIPStartButton();
        this._changeEnableVoIPStopButton();
		
        const networkClient = this.model.getSelectedTxNetworkClient();
        var i=0;
        for (const data of this.model.VoIPStateList) {
 			if(data.id===networkClient.id)
			{
				this._updateVoIPStatus(data,i);
				break;
 			} 
 			i++;
        }
    }
    /**
     * Operation Menu View タブ切り替え時のクリップ一覧画面初期化処理
     */
    _navChangedCliplist() {
        this._initializeClipListTab();
        this._initializeClipInspectionViewInfo();
        this._updateClipListMedia();
        this._mediaChanged();
        this._changeClipListSort();
    }
    /**
     * Operation Menu View タブ切り替え時のジョブ一覧一覧画面初期化処理
     */
    _navChangedJoblist() {
        const networkClientId = this.model.getSelectedTxNetworkClientId();
        const cmdData = {
            ID: networkClientId,
        };
        this.model.addExecutedCommandRequest(networkClientId, 'GetUploadList', cmdData);
        this.ccm.cmdGetUploadList(cmdData);
    }
    /**
     * Operation Menu View タブ切り替え時のControl画面初期化処理
     */
    _navChangedControl() {
    }

    /**
     * Recorderタブ表示処理
     */
    _navChangedRecorder() {
        const show = this.model.togglePlanningMetadataInspectionView();
        this._showPlanningMetadataInspectionView(show);

        this._getPlanningMetadataList();
        this._updateDriveStatus();
        this._updateClipRecorderStatus();
        this._updatePlanningMetadataControlStatus();
    }

    /**
     * Recorderタブの有効/無効切り替え
     * @param {*} disable 無効にする場合に true を設定
     */
    _navDisableRecorderTab(disable) {
        if (disable) {
            $('#nav-main-recorder').addClass('disabled');
        } else {
            $('#nav-main-recorder').removeClass('disabled');
        }
    }

    /* --- Operation Menu View タブ切り替え (end) ---*/

    /* --- McbNetworkInfoチャート (Start) --- */

    /**
     * McbNetworkInfoチャートの初期化
     */
    _initializeMcbNetworkInfoChart() {
        const option = {
            animation: false,
            scaleOverride: true,
            scaleStartValue: 100,
            scaleShowGridLines: false,
            scaleGridLineColor: "rgba(0,0,0,.05)",
            scaleGridLineWidth: 1,
            bezierCurve: true,
            bezierCurveTension: 0.4,
            pointDot: false,
            datasetStroke: false,
            datasetStrokeWidth: 1,
            fill: false,
            responsive: true,
            maintainAspectRatio: false,
            elements: { point: { radius: 0 } },
            legend: {
                display: true,
                labels: { boxWidth: 20 }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        autoSkip: true
                    }
                }],
                xAxes: [{
                    type: 'time',
                    time: {
                        displayFormats: { second: 'mm:ss' }
                    },
                    ticks: {
                        autoSkip: true,
                        maxRotation: 0,
                        minRotation: 0
                    }
                }]
            }
        }

        /// Loss用のグラフ
        const ctxloss = document.getElementById('mcbnetworkinfochart-loss').getContext('2d');
        ctxloss.canvas.height = 100;
        this.lossMcbNetworkInfoChart = new Chart(ctxloss, {
            type: 'line',
            data: {
                labels: this.dataMcbNetworkInfoChart.timestamp,
                datasets: [{
                    label: 'NIC #1',
                    data: this.dataMcbNetworkInfoChart.loss.nic1,
                    borderColor: 'rgba(255,99,132,1)',
                    backgroundColor: 'rgba(0,0,0,0)',
                    borderWidth: 1
                }, {
                    label: 'NIC #2',
                    data: this.dataMcbNetworkInfoChart.loss.nic2,
                    borderColor: 'rgba(188,199,100,1)',
                    backgroundColor: 'rgba(0,0,0,0)',
                    borderWidth: 1
                }, {
                    label: 'NIC #3',
                    data: this.dataMcbNetworkInfoChart.loss.nic3,
                    borderColor: 'rgba(88,199,100,1)',
                    backgroundColor: 'rgba(0,0,0,0)',
                    borderWidth: 1
                }, {
                    label: 'NIC #4',
                    data: this.dataMcbNetworkInfoChart.loss.nic4,
                    borderColor: 'rgba(0,162,232,1)',
                    backgroundColor: 'rgba(0,0,0,0)',
                    borderWidth: 1
                }]
            },
            options: option
        });

        // Bitrateグラフ
        const ctxbitrate = document.getElementById('mcbnetworkinfochart-bitrate').getContext('2d');
        ctxbitrate.canvas.height = 100;
        this.bitrateMcbNetworkInfoChart = new Chart(ctxbitrate, {
            type: 'line',
            data: {
                labels: this.dataMcbNetworkInfoChart.timestamp,
                datasets: [{
                    label: 'NIC #1',
                    data: this.dataMcbNetworkInfoChart.bitrate.nic1,
                    borderColor: 'rgba(255,99,132,1)',
                    backgroundColor: 'rgba(0,0,0,0)',
                    borderWidth: 1
                }, {
                    label: 'NIC #2',
                    data: this.dataMcbNetworkInfoChart.bitrate.nic2,
                    borderColor: 'rgba(188,199,100,1)',
                    backgroundColor: 'rgba(0,0,0,0)',
                    borderWidth: 1
                }, {
                    label: 'NIC #3',
                    data: this.dataMcbNetworkInfoChart.bitrate.nic3,
                    borderColor: 'rgba(88,199,100,1)',
                    backgroundColor: 'rgba(0,0,0,0)',
                    borderWidth: 1
                }, {
                    label: 'NIC #4',
                    data: this.dataMcbNetworkInfoChart.bitrate.nic4,
                    borderColor: 'rgba(0,162,232,1)',
                    backgroundColor: 'rgba(0,0,0,0)',
                    borderWidth: 1
                }, {
                    label: 'Total',
                    data: this.dataMcbNetworkInfoChart.bitrate.total,
                    borderColor: 'rgba(255,0,255,1)',
                    backgroundColor: 'rgba(0,0,0,0)',
                    borderWidth: 1
                }]
            },
            options: option
        });

        // rttグラフ
        const ctxrtt = document.getElementById('mcbnetworkinfochart-rtt').getContext('2d');
        ctxrtt.canvas.height = 100;
        this.rttMcbNetworkInfoChart = new Chart(ctxrtt, {
            type: 'line',
            data: {
                labels: this.dataMcbNetworkInfoChart.timestamp,
                datasets: [{
                    label: 'NIC #1',
                    data: this.dataMcbNetworkInfoChart.rtt.nic1,
                    borderColor: 'rgba(255,99,132,1)',
                    backgroundColor: 'rgba(0,0,0,0)',
                    borderWidth: 1
                }, {
                    label: 'NIC #2',
                    data: this.dataMcbNetworkInfoChart.rtt.nic2,
                    borderColor: 'rgba(188,199,100,1)',
                    backgroundColor: 'rgba(0,0,0,0)',
                    borderWidth: 1
                }, {
                    label: 'NIC #3',
                    data: this.dataMcbNetworkInfoChart.rtt.nic3,
                    borderColor: 'rgba(88,199,100,1)',
                    backgroundColor: 'rgba(0,0,0,0)',
                    borderWidth: 1
                }, {
                    label: 'NIC #4',
                    data: this.dataMcbNetworkInfoChart.rtt.nic4,
                    borderColor: 'rgba(0,162,232,1)',
                    backgroundColor: 'rgba(0,0,0,0)',
                    borderWidth: 1
                }]
            },
            options: option
        });
    }

    /**
     * McbNetworkInfoチャートの更新
     */
    _updateMcbNetworkInfoChart() {
        // 最新のデータに更新
        const networkClientId = this.model.getSelectedTxNetworkClientId();
        if (networkClientId.length > 0) {
            this.dataMcbNetworkInfoChart = this.model.getMcbNetworkInfoData(networkClientId);
            // チャート表示
            if (this.lossMcbNetworkInfoChart != undefined) {
                this.lossMcbNetworkInfoChart.data.labels = this.dataMcbNetworkInfoChart.timestamp;
                this.lossMcbNetworkInfoChart.data.datasets[0].data = this.dataMcbNetworkInfoChart.loss.nic1;
                this.lossMcbNetworkInfoChart.data.datasets[1].data = this.dataMcbNetworkInfoChart.loss.nic2;
                this.lossMcbNetworkInfoChart.data.datasets[2].data = this.dataMcbNetworkInfoChart.loss.nic3;
                this.lossMcbNetworkInfoChart.data.datasets[3].data = this.dataMcbNetworkInfoChart.loss.nic4;
                this.lossMcbNetworkInfoChart.update();
            }
            if (this.bitrateMcbNetworkInfoChart != undefined) {
                this.bitrateMcbNetworkInfoChart.data.labels = this.dataMcbNetworkInfoChart.timestamp;
                this.bitrateMcbNetworkInfoChart.data.datasets[0].data = this.dataMcbNetworkInfoChart.bitrate.nic1;
                this.bitrateMcbNetworkInfoChart.data.datasets[1].data = this.dataMcbNetworkInfoChart.bitrate.nic2;
                this.bitrateMcbNetworkInfoChart.data.datasets[2].data = this.dataMcbNetworkInfoChart.bitrate.nic3;
                this.bitrateMcbNetworkInfoChart.data.datasets[3].data = this.dataMcbNetworkInfoChart.bitrate.nic4;
                this.bitrateMcbNetworkInfoChart.update();
            }
            if (this.rttMcbNetworkInfoChart != undefined) {
                this.rttMcbNetworkInfoChart.data.labels = this.dataMcbNetworkInfoChart.timestamp;
                this.rttMcbNetworkInfoChart.data.datasets[0].data = this.dataMcbNetworkInfoChart.rtt.nic1;
                this.rttMcbNetworkInfoChart.data.datasets[1].data = this.dataMcbNetworkInfoChart.rtt.nic2;
                this.rttMcbNetworkInfoChart.data.datasets[2].data = this.dataMcbNetworkInfoChart.rtt.nic3;
                this.rttMcbNetworkInfoChart.data.datasets[3].data = this.dataMcbNetworkInfoChart.rtt.nic4;
                this.rttMcbNetworkInfoChart.update();
            }
        }
    }

    /* --- McbNetworkInfoチャート (End) --- */

    /* --- Responseコマンド受信処理 (Start) --- */

    _onResponseGetNetworkClient(data) {
        this.model.setNetworkClient(data);
        this._updateTxNetworkClient(data);
    }

    _onResponseGetNetworkClientList(data) {
        // NetworkClient一覧の取得結果のため、Model内のNetworkClient一覧を初期化
        this.model.clearNetworkClientList();
        this.model.setNetworkClientList(data);

        this._clearNetworkClientList();
        this._showNetworkClientList(this.model.getNetworkClientList());
    }

    _onResponseGetClipList(data) {
        if (data.length > 0) {
            this.model.setClipList(data[0].ID, data);
            this._showClip();
        }
    }

    _onResponseAddUploadFiles(data) {
        if (data.length > 0) {
            this.model.addExecutedCommandResponse(data[0].ID, 'AddUploadFiles', data.sequenceid);
            $('#cliptransferview_form').addClass('d-none');
            $('#cliptransferview_preset').addClass('d-none');
            $('#cliptransferview_result').removeClass('d-none');
            $('#cliptransferview_cancel').addClass('d-none');
            $('#cliptransferview_close').addClass('d-none');
            $('table#ClipTransferViewResultTable tbody *').remove();
            $('#cliptransferview_result_label').text('');
            $('#cliptransferview_result_itemcount').text(``);
        }
    }

    _onResponseGetUploadList(data) {
        if (data.length > 0) {
            this.model.addExecutedCommandResponse(data.ID, 'GetUploadList', data.sequenceid);
            this.model.setUploadList(data[0].ID, data);
            this._showUploadList();
        }
    }

    /**
     * ストリーミング開始要求結果受信処理
     * @param {*} data 受信データ
     */
    _onResponseStartQosStreamingSend(data) {
        if (data) {
            const networkClient = this.model.getSelectedTxNetworkClient();
            if (networkClient && networkClient.ID === data.ID) {
                if (data.error.code === 0) {
                    this._disableStreamingStartButton(true);
                }
                else {
                    this._disableStreamingStartButton(false);
                }
            }
        }
    }

    _onResponseStartQosStreamingReceive(data) {
        if (data) {
            const networkClient = this.model.getSelectedTxNetworkClient();
            if (networkClient && networkClient.ID === data.ID) {
                if (data.error.code === 0) {
                    this._disableStreamingReceiveStartButton(true);
                }
                else {
                    this._disableStreamingReceiveStartButton(false);
                }
            }
        }
    }

    /**
     * ストリーミング停止要求結果受信処理
     * @param {*} data 受信データ
     */
    _onResponseStopStreamingSend(data) {
        if (data) {
            const networkClient = this.model.getSelectedTxNetworkClient();
            if (networkClient && networkClient.ID === data.ID) {
                if (data.error.code !== 0) {
                    this._disableStreamingStopButton(true);
                }
            }
        }
    }

    _onResponseStopStreamingReceive(data) {
        if (data) {
            const networkClient = this.model.getSelectedTxNetworkClient();
            if (networkClient && networkClient.ID === data.ID) {
                if (data.error.code !== 0) {
                    this._disableStreamingReceiveStopButton(true);
                }
            }
        }
    }

    _onResponseGetPlanningMetadataList(data) {
        if (data && data.PlanningMetadata) {
            if (data.PlanningMetadata.length > 0) {
                this.model.setPlanningMetadataList(data);
                this._showPlanningMetadataList();
            }
        }
    }

    _onResponseGetCurrentPlanningMetadata(data) {
        $('#recorder_plameta_inspection_view_name').val('Current');
        $('#recorder_plameta_inspection_view_metadata').val(data.PlanningMetadata);
    }

    _onResponseGetPlanningMetadata(data) {
        $('#recorder_plameta_inspection_view_name').val(data.Name);
        $('#recorder_plameta_inspection_view_metadata').val(data.PlanningMetadata);
    }

    /**
     * PlanningMetadataのセット結果処理
     */
    _onResponseSetPlanningMetadata(data) {
        const networkClientId = this.model.getSelectedTxNetworkClientId();
        if (networkClientId === data.ID) {
            if (data.error.code === 0) {
                const msg = `PlanningMetadataをセットしました。\nName : ${data.Name}`;
                alert(msg);
            }
            else {
                const msg = `PlanningMetadataのセットに失敗しました。\nName : ${data.Name}\nError : ${data.error.message}, InternalError: ${data.InternalError}`;
                alert(msg);
            }
        }
    }

    /**
     * PlanningMetadataの登録結果処理
     * @param {Object} data 
     */
    _onResponsePutPlanningMetadata(data) {
        const networkClientId = this.model.getSelectedTxNetworkClientId();
        if (networkClientId === data.ID) {
            if (data.error.code === 0) {
                const msg = `登録に成功しました`;
                $('#recorder_plameta_put_view_form_result_text').text(msg);
                $('#recorder_plameta_put_view_form_result_text').removeClass('alert-danger');
                $('#recorder_plameta_put_view_form_result_text').addClass('alert-primary');
            }
            else {
                const msg = `登録に失敗しました。(Error: ${data.error.message}, InternalError: ${data.InternalError})`;
                $('#recorder_plameta_put_view_form_result_text').text(msg);
                $('#recorder_plameta_put_view_form_result_text').removeClass('alert-primary');
                $('#recorder_plameta_put_view_form_result_text').addClass('alert-danger');
            }

            $('#recorder_plameta_put_view_form_result').collapse('show');
            $('#recorder_plameta_put_view_close').removeClass('d-none');
            $('#recorder_plameta_put_view_cancel').removeClass('d-none');
            $('#recorder_plameta_put_view_close').prop('disabled', false);
            $('#recorder_plameta_put_view_cancel').prop('disabled', false);
            $('#recorder_plameta_put_view_submit_spinner').addClass('d-none');
        }
    }

    /* --- Responseコマンド受信処理 (End) --- */

    /* --- Resultコマンド受信処理 (Start) --- */

    _onResultAddUploadFiles(data) {
        $('#cliptransferview_submit_spinner').toggleClass('d-none');

        let isExecutedResult = false;
        for (const command of data) {
            isExecutedResult = this.model.addExecutedCommandResult(command.ID, 'AddUploadFiles', command.sequenceid, command.result);
        }

        if (isExecutedResult) {
            this._showClipTransferViewResult();
        }
    }

    /* --- Resultコマンド受信処理 (End) --- */

    /* --- Notifyコマンド受信処理 (Start) --- */

    _onNotifyNetworkClientConnect(data) {
        if (data.Status === 'connect') {
            this.model.setNetworkClientStatus(data.ID, true);
            this.ccm.cmdGetNetworkClient({
                ID: data.ID
            });
        }
        else {
            this.model.setNetworkClientStatus(data.ID, false);
            this._updateTxNetworkClient(data);
        }
    }

    _onNotifyStreamingStatus(data) {
        for (const status of data) {
            this.model.setNetworkClientStreamingStatus(status.ID, status.Status);
            this._updateTxNetworkClientStreamingStatus(status);
        }
        this._changeEnableStreamingStartButton();
        this._changeEnableStreamingStopButton();
    }

    _onNotifyVoipStatus(data) {
        for (const status of data) {
			this.model.setVoIPState(status);
			//ここではtab表示してないので、呼ばない
			//this._updateVoIPStatus(status);
        }
        this._changeEnableVoIPStartButton();
        this._changeEnableVoIPStopButton();
    }
	
    _onNotifyStreamingDynamicProperty(data) {
        this.model.setStreamingDynamicProperty(data);
    }

    /**
     * サムネイルの受信
     * @param {*} data データ
     */
    _onNotifyReceiveThumbnail(data) {
        const dataThumbnail = `data:${data.mime};base64,${data.thumbnail_base64}`;
        this._base64ToImage(`img_${data.ID}`, dataThumbnail);

        // 受信したサムネイル画像が選択している NetworkClient のものであれば、画像を表示
        const networkClientId = this.model.getSelectedTxNetworkClientId();
        if (networkClientId === data.ID) {
            // 表示するための要素が作成されていない場合は作成
            let elemThumbnail = $(`#streaming_thumbnail_${data.ID}`);
            if (elemThumbnail.length === 0) {
                elemThumbnail = $('<img>', {
                    id: `streaming_thumbnail_${data.ID}`,
                    class: 'border border-0 streaming_thumbnail'
                });
                $('#streaming_main_view_image').append(elemThumbnail);
            }
            this._base64ToImage(`streaming_thumbnail_${data.ID}`, dataThumbnail);
        }
    }

    /**
     * McbNetworkInfoの受信
     * @param {*} data データ
     */
    _onNotifyMcbNetworkInfo(data) {
        this.model.setMcbNetworkInfo(data);
        this._updateMcbNetworkInfoChart();
    }

    /**
     * ネットワーク情報変更通知の受信
     * @param {*} data データ
     */
    _onNotifyNetworkInfo(data) {
        this.model.setNetworkInfo(data);
        this._updateNetworkInfo();
    }

    /**
     * 位置情報変更通知の受信
     * @param {*} data データ
     */
    _onNotifyGPSInfo(data) {
        this.model.setGPSInfo(data);
        this._updateGPSInfo();
    }

    /**
     * Recorderの制御機能状態の変更通知の受信
     * @param {*} data 
     */
    _onNotifyRecorderControlStatus(data) {
        this.model.setRecorderControlStatus(data);
        this._updateRecorderControlStatus(data.ID);
    }

    /**
     * RecorderのRec状態の変更通知の受信
     * @param {*} data 
     */
    _onNotifyClipRecorderStatus(data) {
        this.model.setClipRecorderStatus(data);
        this._updateClipRecorderStatus();
    }

    /**
     * Recorderのクリップ保存メディアステータスの変更通知の受信
     * @param {*} data 
     */
    _onNotifyDriveStatus(data) {
        this.model.setDriveStatus(data);
        this._updateDriveStatus();
        this._updateClipListMedia();
        this._mediaChanged();
        this._changeClipListSort();
    }

    /**
     * PlanningMetadata機能有効・無効の変更通知
     * @param {*} data 
     */
    _onNotifyPlanningMetadataControlStatus(data) {
        this.model.setPlanningMetadataControlStatus(data);
        this._updatePlanningMetadataControlStatus();
    }

    /* --- Notifyコマンド受信処理 (End) --- */

    /* --- ストリーミング開始/停止ボタン (Start) --- */

    /**
     * ストリーミング開始ボタンの有効/無効 切り替え処理
     * @param {*} isDisable ストリーミング開始ボタンを無効にする場合は true を設定する
     */
    _disableStreamingStartButton(isDisable) {
        $('#streaming_start_button').prop('disabled', isDisable);
    }
    /**
     * ストリーミング停止ボタンの有効/無効 切り替え処理
     * @param {*} isDisable ストリーミング停止ボタンを無効にする場合は true を設定する
     */
    _disableStreamingStopButton(isDisable) {
        $('#streaming_stop_button').prop('disabled', isDisable);

        if (isDisable === true) {
            this._hideStreamingDynamicPropertyChart();
            // チャートが非表示になったらサムネイルを表示
            this._disableStreamingMainViewImage(false);
        }
        else {
            this._showStreamingDynamicPropertyChart();
            // チャートが表示になったらサムネイルを非表示
            this._disableStreamingMainViewImage(true);
        }
    }

    _disableStreamingReceiveStartButton(isDisable) {
        $('#streaming_receive_start_button').prop('disabled', isDisable);

    }
    _disableStreamingReceiveStopButton(isDisable) {
        $('#streaming_receive_stop_button').prop('disabled', isDisable);
    }

    /**
     * ストリーミング開始ボタンの有効/無効の切り替え
     */
    _changeEnableStreamingStartButton() {
        const networkClient = this.model.getSelectedTxNetworkClient();
        if (networkClient) {
            const status = this._getStringStreamingStatus(networkClient.StreamingStatus);
            if (status === 'Disabled') {
                this._disableStreamingStartButton(true);
            } else if (status === 'Stopped') {
                this._disableStreamingStartButton(false);
            } else if (status === 'Started') {
                this._disableStreamingStartButton(true);
            }
        }
    }
    _changeEnableStreamingStopButton() {
        const networkClient = this.model.getSelectedTxNetworkClient();
        if (networkClient) {
            const status = this._getStringStreamingStatus(networkClient.StreamingStatus);
            if (status === 'Disabled') {
                this._disableStreamingStopButton(true);
            } else if (status === 'Stopped') {
                this._disableStreamingStopButton(true);
            } else if (status === 'Started') {
                this._disableStreamingStopButton(false);
            }
        }
    }

    /* --- ストリーミング開始/停止ボタン (End) --- */

    /**
     * Voip開始ボタンの有効/無効の切り替え
     */
    /**
     * VoIP開始ボタンの有効/無効 切り替え処理
     * @param {*} isDisable VoIP開始ボタンを無効にする場合は true を設定する
     */
    _disableVoIPStartButton(isDisable) {
        $('#VoIP_start_button').prop('disabled', isDisable);
    }
    /**
     * VoIP停止ボタンの有効/無効 切り替え処理
     * @param {*} isDisable VoIP停止ボタンを無効にする場合は true を設定する
     */
    _disableVoIPStopButton(isDisable) {
        $('#VoIP_stop_button').prop('disabled', isDisable);
    }

    _changeEnableVoIPStartButton() {
        const networkClient = this.model.getSelectedTxNetworkClient();
        if (networkClient) {
	        for (const data of this.model.VoIPStateList) {
	 			if(data.id===networkClient.id)
				{
		            const status = this._getStringVoIPStatus(data.Status);
		            if (status === 'Disabled') {
		                this._disableVoIPStartButton(true);
		            } else if (status === 'Enabled') {
		                this._disableVoIPStartButton(false);
		            } else if (status === 'Started') {
		                this._disableVoIPStartButton(false);
		            }
					break;
	 			} 
	        }
        }
    }
    _changeEnableVoIPStopButton() {
        const networkClient = this.model.getSelectedTxNetworkClient();
        if (networkClient) {
	        for (const data of this.model.VoIPStateList) {
	 			if(data.id===networkClient.id)
				{
		            const status = this._getStringVoIPStatus(data.Status);
		            if (status === 'Disabled') {
		                this._disableVoIPStopButton(true);
		            } else if (status === 'Enabled') {
		                this._disableVoIPStopButton(false);
		            } else if (status === 'Started') {
		                this._disableVoIPStopButton(false);
		            }
					break;
	 			} 
	        }
        }
    }

    /* --- Voip開始/停止ボタン (End) --- */

    /* ----- 整理済み ここまで ----- */




    _setUseMcbSetting(useMcbDefaultState) {
        // 設定された初期値を設定
        $('#isUseMcb').prop('checked', useMcbDefaultState);
        // NetworkClient で、MCB機能が存在しない場合、
        // "Use MCB"の状態を OFF にして無効化
        // 存在する場合は、チェック状態は変更せずに有効化
        const networkClient = this.model.getSelectedTxNetworkClient();
        if (networkClient) {
            if ((networkClient.McbEnabled === undefined) ||
                (networkClient.McbEnabled === false)) {
                $('#isUseMcb').prop('checked', false);
                $("#isUseMcb").prop('disabled', true);
            } else {
                $("#isUseMcb").prop('disabled', false);
            }
        }
        this._showIsUseMcbConfig();
    }

    _showIsUseMcbConfig() {
        const isShow = $('#isUseMcb').prop('checked');
        if (isShow) {
            $("#inputMcbHostName").prop('disabled', false);
            $("#inputMcbPort").prop('disabled', false);
            $("#inputMcbUser").prop('disabled', false);
            $("#inputMcbPassword").prop('disabled', false);
        } else {
            $("#inputMcbHostName").prop('disabled', true);
            $("#inputMcbPort").prop('disabled', true);
            $("#inputMcbUser").prop('disabled', true);
            $("#inputMcbPassword").prop('disabled', true);
        }
    }

    _showClipInspectionView(isShow) {
        const elements = $('#collapse_clip_inspection_view_button');
        if (elements) {
            if (isShow) {
                $('#clip_inspection_view').removeClass('d-none');
                elements.attr('src', 'img/arrow_right.png');
                elements.removeClass('fa-chevron-circle-left');
                elements.addClass('fa-chevron-circle-right');
            }
            else {
                $('#clip_inspection_view').addClass('d-none');
                elements.attr('src', 'img/arrow_left.png');
                elements.removeClass('fa-chevron-circle-right');
                elements.addClass('fa-chevron-circle-left');
            }
        }
    }

    _showPlanningMetadataInspectionView(isShow) {
        const elements = $('#collapse_recorder_planmeta_inspection_view_button');
        if (elements) {
            if (isShow) {
                $('#recorder_plameta_inspection_view').removeClass('d-none');
                elements.attr('src', 'img/arrow_right.png');
                elements.removeClass('fa-chevron-circle-left');
                elements.addClass('fa-chevron-circle-right');
            }
            else {
                $('#recorder_plameta_inspection_view').addClass('d-none');
                elements.attr('src', 'img/arrow_left.png');
                elements.removeClass('fa-chevron-circle-right');
                elements.addClass('fa-chevron-circle-left');
            }
        }
    }

    _clearNetworkClientList() {
        // Tx一覧を初期化
        $('#transmitter_list').empty();
        // Rx一覧を初期化
        $('#receiver_list').empty();
        // TransmitterInspectionViewの初期化
        // ReceiverInspectionViewの初期化
        // Operation Menu Viewの初期化
        // Operation Main Viewの初期化
    }
    /**
     * 接続済み NetworkClient一覧を画面に表示
     * @param {*} networkClients NetworkClient一覧
     */
    _showNetworkClientList(networkClients) {
        networkClients.forEach(element => {
            this._showNetworkClient4Tx(element);
        });
    }
    _getStringStreamingStatus(streamingStatus) {
        let status = 'Disabled';
        if (streamingStatus === 'IDLE') {
            status = 'Stopped';
        }
        else if (streamingStatus === 'START') {
            status = 'Started';
        }
        return status;
    }

    _getStringVoIPStatus(VoIPStatus) {
        let status = 'Disabled';
        if (VoIPStatus === '00') {
            status = 'Disabled';
        }
        else if (VoIPStatus === '11') {
            status = 'Enabled';
        }
        else if (VoIPStatus === '22') {
            status = 'Enabled';
        }
        else if (VoIPStatus === '32') {
            status = 'Enabled';
        }
        else if (VoIPStatus === '33') {
            status = 'Started';
        }
        return status;
    }

    _getInsertTxNetworkClientElement(nc) {
        const transmitters = $('#transmitter_list li.nav-item');
        const len = transmitters.length;
        let result;

        for (let i = 0; i < len; ++i) {
            const hasAttr = transmitters[i].hasAttribute('ccm_disp_name');
            if (hasAttr) {
                const dispname = transmitters[i].attributes['ccm_disp_name'].value;
                if (dispname > nc.DisplayName) {
                    result = transmitters[i];
                    break;
                }
            }
        }

        return result;
    }

    /**
     * Tx一覧画面に指定された NetworkClient情報を表示
     * @param {*} nc NetworkClient情報
     */
    _showNetworkClient4Tx(nc) {
        const transmitter_list = $('#transmitter_list');
        const beforeElem = this._getInsertTxNetworkClientElement(nc);

        let statusStreaming = 'Disabled';
        let statusTransfer = 'Disabled';
        let statusControl = 'Disabled';
        let statusRecorderLow = 'Disabled';
        let statusRecorderHigh = 'Disabled';
        if (nc.StreamingFunction === true) {
            statusStreaming = this._getStringStreamingStatus(nc.StreamingStatus);
        }
        if (nc.FileTransferFunction === true) {
            statusTransfer = 'Enabled';
        }

        const elemRootItem = $('<li>', {
            class: 'nav-item',
            ccm_disp_name: `${nc.DisplayName}`
        });
        if (beforeElem === undefined) {
            transmitter_list.append(elemRootItem);
        } else {
			elemRootItem.insertBefore(beforeElem);
        }

        const elemInput = $('<input>', {
            type: 'radio',
            name: 'tx',
            value: nc.ID,
            id: nc.ID
        });
        elemInput.on('change', { controller: this }, this._onRadioSelectionChangedTx);
        elemRootItem.append(elemInput);

        const elemLabel = $('<label>', {
            for: nc.ID
        });
        elemRootItem.append(elemLabel);

        const elemDivCard = $('<div>', {
            class: 'card ccm_card'
        });
        elemLabel.append(elemDivCard);

        const elemDivCardRow = $('<div>', {
            class: 'row no-gutters'
        });
        elemDivCard.append(elemDivCardRow);

        const elemDivCardRowImage = $('<div>', {
            class: 'col-md-2 ccm_card_image'
        });
        elemDivCardRow.append(elemDivCardRowImage);

        const elemImageCard = $('<img>', {
            class: 'card-img align-middle',
            src: 'img/connect.png',
            id: `img_${nc.ID}`
        });
        elemDivCardRowImage.append(elemImageCard);

        const elemDivCardRowDetail = $('<div>', {
            class: 'col-md-10 ccm_card_detail'
        });
        elemDivCardRow.append(elemDivCardRowDetail);

        const elemDivCardRowDetailBody = $('<div>', {
            class: 'card-body ccm_card_body'
        });
        elemDivCardRowDetail.append(elemDivCardRowDetailBody);

        const elemDivCardRowDetailBodyText = $('<div>', {
            class: 'card-text'
        });
        elemDivCardRowDetailBody.append(elemDivCardRowDetailBodyText);

        const elemCardDetailTitleSmall = $('<small>');
        elemDivCardRowDetailBodyText.append(elemCardDetailTitleSmall);

        const elemCardDetailTitleStrong = $('<strong>');
        elemCardDetailTitleSmall.append(elemCardDetailTitleStrong);
        elemCardDetailTitleStrong.text(`Name: ${nc.DisplayName}`);

        const elemDivCardRowDetailBodyStatus = $('<div>');
        elemDivCardRowDetailBodyText.append(elemDivCardRowDetailBodyStatus);

        const elemListStatus = $('<ul>', {
            class: 'list_style_none'
        });
        elemDivCardRowDetailBodyStatus.append(elemListStatus);

        // Streaming
        const elemStreaming = $('<li>', {
            id: `status_streaming_${nc.ID}`
        });
        elemListStatus.append(elemStreaming);
        if ((nc.StreamingFunction === false) || (nc.StreamingStatus === 'INACTIVE')) {
            elemTransfer.addClass('text-muted');
        }
        const elemStreamingText = $('<small>').text(`Streaming: ${statusStreaming}`);
        elemStreaming.append(elemStreamingText);

		//VoIP
		const elemVoIP = $('<li>', {
			id: `status_voip_${nc.ID}`
		});
		elemListStatus.append(elemVoIP);
		var elemVoIPText2="";
        for (const data of this.model.VoIPStateList) {
			if(data.id===nc.id)
			{
				elemVoIPText2=this._getStringVoIPStatus(data.Status);
				break;
			}
        }

		const elemVoIPText = $('<small>').text(`VoIP: ${elemVoIPText2}`);
        if (elemVoIPText2 === 'Disabled') {
            elemVoIPText.addClass('text-muted');
        }
		elemVoIP.append(elemVoIPText);
		
        // File Transfer
        const elemTransfer = $('<li>', {
            id: `status_transfer_${nc.ID}`
        });
        elemListStatus.append(elemTransfer);
        if (nc.FileTransferFunction === false) {
            elemTransfer.addClass('text-muted');
        }
        const elemTransferText = $('<small>').text(`Transfer: ${statusTransfer}`);
        elemTransfer.append(elemTransferText);

        // Control
        const elemControl = $('<li>', {
            id: `status_control_${nc.ID}`,
            class: 'text-muted'
        });
        elemListStatus.append(elemControl);
        const elemControlText = $('<small>').text(`Control: ${statusControl}`);
        elemControl.append(elemControlText);

        // Recorder
        const elemRecorder = $('<li>');
        elemListStatus.append(elemRecorder);
        const elemRecorderText = $('<small>', {
            id: `status_recorder_${nc.ID}`
        });
        elemRecorderText.text(`Recorder:`);
        elemRecorder.append(elemRecorderText);
        // Recorder List
        const elemRecorderList = $('<ul>', {
            class: 'list_style_none ml-3'
        });
        elemRecorder.append(elemRecorderList);
        // Recorder List - Low Resolution
        const elemRecorderListLow = $('<li>');
        elemRecorderList.append(elemRecorderListLow);
        const elemRecorderListLowText = $('<small>', {
            id: `status_recorder_${nc.ID}_lowresolution`
        });
        elemRecorderListLowText.text(`Low: ${statusRecorderLow}`);
        elemRecorderListLow.append(elemRecorderListLowText);
        if (statusRecorderLow === 'Disabled') {
            elemRecorderListLowText.addClass('text-muted');
        }
        // Recorder List - High Resolution
        const elemRecorderListHigh = $('<li>');
        elemRecorderList.append(elemRecorderListHigh);
        const elemRecorderListHighText = $('<small>', {
            id: `status_recorder_${nc.ID}_highresolution`
        });
        elemRecorderListHighText.text(`High: ${statusRecorderHigh}`);
        elemRecorderListHigh.append(elemRecorderListHighText);
        if (statusRecorderHigh === 'Disabled') {
            elemRecorderListHighText.addClass('text-muted');
        }
        if ((statusRecorderHigh === 'Disabled') && (statusRecorderLow === 'Disabled')) {
            elemRecorderText.addClass('text-muted');
        }

        this._updateRecorderControlStatus(nc.ID);
    }

    _showClip() {
        // 表示するクリップ一覧の取得
        const id = this.model.getSelectedTxNetworkClientId();
        const clips = this.model.getClipList(id);

        // すべてのクリップ情報の削除
        this._clearClipList();

        if (clips) {
            for (const clip of clips) {
                this._addClip(clip);
            }
        }
    }
    _clearClipList() {
        $('table#cliplistview_table tbody *').remove();
    }
    _addClip(clip) {
        const tbody = $('table#cliplistview_table tbody');
        const clipName = CcmModel.getClipName(clip.uri);

        const elemTr = $('<tr>');
        // --- Checkbox
        const elemTdCheck = $('<td>');
        const elemCheck = $('<input>', {
            type: 'checkbox',
            id: `clip_${clipName}`
        });
        elemCheck.on('change', { controller: this }, this._onClipChanged);
        elemTdCheck.append(elemCheck);
        elemTr.append(elemTdCheck);
        // --- Image
        const elemTdImage = $('<td>');
        const elemImg = $('<img>', {
            src: 'img/no_image.png'
        });
        elemTdImage.append(elemImg);
        elemTr.append(elemTdImage);
        // --- Name
        const elemTdName = $('<td>');
        elemTdName.append(clipName);
        elemTr.append(elemTdName);
        // --- duration
        const elemTdDuration = $('<td>');
        elemTdDuration.append(clip.duration);
        elemTr.append(elemTdDuration);
        // --- frameRate
        const elemTdFrameRate = $('<td>');
        elemTdFrameRate.append(clip.frameRate);
        elemTr.append(elemTdFrameRate);
        // --- videoType
        const elemTdVideoType = $('<td>');
        elemTdVideoType.append(clip.videoType);
        elemTr.append(elemTdVideoType);

        // Click Handler
        elemTr.on('click', { controller: this }, (e) => {
            // すべての行の選択状態を解除
            const rows = $('table#cliplistview_table tbody tr');
            for (const row of rows) {
                row.classList.remove('table-primary');
            }
            e.currentTarget.classList.add('table-primary');
            console.log(e);

            e.data.controller._showClipInspectionViewInfo(e.currentTarget);
        });

        tbody.append(elemTr);
    }
    _onClipChanged(event) {
        const model = event.data.controller.model;
        const networkClientId = model.getSelectedTxNetworkClientId();
        const clipName = event.currentTarget.id.replace('clip_', '');
        const isChecked = event.currentTarget.checked;

        model.clipSelectionChanged(networkClientId, clipName, isChecked);

        if (model.isClipSelect(networkClientId)) {
            $('#cliptransfer_button').prop('disabled', false);
        } else {
            $('#cliptransfer_button').prop('disabled', true);
        }
    }
    _showClipInspectionViewInfo(element) {
        const networkClientId = this.model.getSelectedTxNetworkClientId();
        const clipName = element.childNodes[2].innerText;
        const clipInfo = this.model.getClipInfo(networkClientId, clipName);

        $('#clip_inspection_view_filename').val(clipName);
        $('#clip_inspection_view_type').val(clipInfo.clip.type);
        $('#clip_inspection_view_status').val(clipInfo.clip.status);
        $('#clip_inspection_view_duration').val(clipInfo.clip.duration);
        $('#clip_inspection_view_offset').val(clipInfo.clip.offset);
        $('#clip_inspection_view_framerate').val(clipInfo.clip.frameRate);
        $('#clip_inspection_view_aspectrate').val(clipInfo.clip.aspectRatio);
        $('#clip_inspection_view_videoformat').val(clipInfo.clip.videoType);
        $('#clip_inspection_view_audioformat').val(clipInfo.clip.audioType);
        $('#clip_inspection_view_audiochannel').val(clipInfo.clip.audioChannel);
        $('#clip_inspection_view_partialupload').val(clipInfo.clip.isPartialUploadSupported ? 'Possible' : 'Impossible');
        $('#clip_inspection_view_uri').val(clipInfo.clip.uri);
        $('#clip_inspection_view_thumbnailuri').val(clipInfo.clip.thumbnailUri);
        $('#clip_inspection_view_nrturi').val(clipInfo.clip.nrtMetadataUri);
    }

    _initializeClipInspectionViewInfo() {
        $('#clip_inspection_view_filename').val('');
        $('#clip_inspection_view_type').val('');
        $('#clip_inspection_view_status').val('');
        $('#clip_inspection_view_duration').val('');
        $('#clip_inspection_view_offset').val('');
        $('#clip_inspection_view_framerate').val('');
        $('#clip_inspection_view_aspectrate').val('');
        $('#clip_inspection_view_videoformat').val('');
        $('#clip_inspection_view_audioformat').val('');
        $('#clip_inspection_view_audiochannel').val('');
        $('#clip_inspection_view_partialupload').val('Impossible');
        $('#clip_inspection_view_uri').val('');
        $('#clip_inspection_view_thumbnailuri').val('');
        $('#clip_inspection_view_nrturi').val('');
    }
    _initializeClipListTab() {
        $('#cliptransfer_button').prop('disabled', true);
    }

    _initializeRecorderInspectionViewInfo() {
        $('#recorder_plameta_inspection_view_name').val('');
        $('#recorder_plameta_inspection_view_metadata').val('');
    }

    _clearUploadList() {
        $('table#joblistview_table tbody *').remove();
    }

    _showUploadList() {
        // 表示するジョブ一覧の取得
        const id = this.model.getSelectedTxNetworkClientId();
        const upload_files = this.model.getUploadList(id);

        // すべてのジョブ情報の削除
        this._clearUploadList();

        for (const upload_file of upload_files) {
            this._addUpload(upload_file);
        }
    }

    _addUpload(upload_file) {
        const tbody = $('table#joblistview_table tbody');

        const elemTr = $('<tr>');
        // --- ID
        const elemID = $('<td>');
        elemID.append(upload_file.id);
        elemTr.append(elemID);
        // --- Clip Name
        const elemTdName = $('<td>');
        elemTdName.append(upload_file.files[0]);
        elemTr.append(elemTdName);
        // --- Drive
        const elemTdDrive = $('<td>');
        elemTdDrive.append(upload_file.drive);
        elemTr.append(elemTdDrive);
        // --- Status
        const elemTdStatus = $('<td>');
        elemTdStatus.append(upload_file.status);
        elemTr.append(elemTdStatus);
        // --- Percentage
        const elemTdPercentage = $('<td>');
        elemTdPercentage.append(upload_file.percentage);
        elemTr.append(elemTdPercentage);
        // --- Transferred
        const elemTdTransferred = $('<td>');
        elemTdTransferred.append(upload_file.transferred);
        elemTr.append(elemTdTransferred);
        // --- Total
        const elemTdTotal = $('<td>');
        elemTdTotal.append(upload_file.total);
        elemTr.append(elemTdTotal);

        tbody.append(elemTr);
    }

    _showClipTransferViewResult() {
        const r = this.model.getAddUploadFilesCommandResult();

        $('#cliptransferview_result_label').removeClass('cliptransferview_result_label_allsuccess');
        $('#cliptransferview_result_label').removeClass('cliptransferview_result_label_failed');

        if (r.failed === 0) {
            $('#cliptransferview_result_label').text('All Success ');
            $('#cliptransferview_result_label').addClass('cliptransferview_result_label_allsuccess');
            $('#cliptransferview_result_itemcount').text(`(${r.success})`);
        }
        else {
            $('#cliptransferview_result_label').text('Failed ');
            $('#cliptransferview_result_label').addClass('cliptransferview_result_label_failed');
            $('#cliptransferview_result_itemcount').text(`(${r.failed}/${r.success + r.failed})`);
        }

        const tbody = $('table#ClipTransferViewResultTable tbody');
        let index = 0;
        for (const result of r.result) {
            const elemTr = $('<tr>');

            // --- No
            const elemTdNo = $('<td>');
            elemTdNo.append(index + 1);
            elemTr.append(elemTdNo);
            // --- File
            const elemTdFile = $('<td>');
            if (typeof (result) !== "undefined") {
                elemTdFile.append(result.file);

            } else {
                elemTdFile.append("Unknown");
            }
            elemTr.append(elemTdFile);
            // --- Result
            const elemTdResult = $('<td>');
            const elemDivInline = $('<div>', {
                class: 'd-inline'
            });
            elemTdResult.append(elemDivInline);
            if (result.error) {
                if (result.error.code === 0) {
                    const elemLabelSuccess = $('<label>', {
                        class: 'col-form-label cliptransferview_resultitem_label_success'
                    });
                    elemLabelSuccess.text(`Success`);
                    elemDivInline.append(elemLabelSuccess);
                }
                else {
                    const elemLabelError = $('<label>', {
                        class: 'col-form-label cliptransferview_resultitem_label_error'
                    });
                    elemLabelError.text(`Error `);
                    elemDivInline.append(elemLabelError);
                    const elemLabelErrorDetail = $('<label>', {
                        class: 'col-form-label'
                    });
                    elemLabelErrorDetail.text(`: ${result.error.message}`);
                    elemDivInline.append(elemLabelErrorDetail);
                }
            } else {
                // elemTdResult.append(`Error : unknown error`);
                const elemLabelError = $('<label>', {
                    class: 'col-form-label cliptransferview_resultitem_label_error'
                });
                elemLabelError.text(`Error `);
                elemDivInline.append(elemLabelError);
                const elemLabelErrorDetail = $('<label>', {
                    class: 'col-form-label'
                });
                elemLabelErrorDetail.text(`: unknown error`);
                elemDivInline.append(elemLabelErrorDetail);
            }

            elemTr.append(elemTdResult);

            tbody.append(elemTr);
            ++index
        }
    }

    // _onResultGetUploadList(data) {
    //     let isExecutedResult = false;
    //     if(data.length > 0) {
    //         isExecutedResult = this.model.addExecutedCommandResult(data[0].ID, 'GetUploadList', data[0].sequenceid, data);
    //     }

    //     if (isExecutedResult) {
    //         let index = 0;
    //         const tbody = $('table#joblistview_table tbody');
    //         const resultList = this.model.getGetUploadListCommandResult();
    //         for (const result of resultList) {
    //             const elemTr = $('<tr>');

    //             // --- No
    //             const elemTdNo = $('<td>');
    //             elemTdNo.append(index + 1);
    //             elemTr.append(elemTdNo);
    //             // --- Clip Name
    //             const elemTdClipName = $('<td>');
    //             elemTdClipName.append(result.clip_name);
    //             elemTr.append(elemTdClipName);
    //             // --- Drive
    //             const elemTdDrive = $('<td>');
    //             elemTdDrive.append(result.drive);
    //             elemTr.append(elemTdDrive);
    //             // --- status
    //             const elemTdStatus = $('<td>');
    //             elemTdStatus.append(result.status);
    //             elemTr.append(elemTdStatus);
    //             // --- percentage
    //             const elemTdPercentage = $('<td>');
    //             elemTdPercentage.append(result.percentage);
    //             elemTr.append(elemTdPercentage);
    //             // --- transferred
    //             const elemTdTransferred = $('<td>');
    //             elemTdTransferred.append(result.transferred);
    //             elemTr.append(elemTdTransferred);
    //             // --- total
    //             const elemTdTotal = $('<td>');
    //             elemTdTotal.append(result.total);
    //             elemTr.append(elemTdTotal);

    //             tbody.append(elemTr);
    //             ++index;
    //         }
    //     }
    // }

    /**
     * Tx一覧内の NetworkClient情報を更新
     * @param {*} networkClient NetworkClient情報
     */
    _updateTxNetworkClient(networkClient) {
        const element = $(`#${networkClient.ID}`);
        if (element && element.length > 0) {
            if (networkClient.Status === 'connect') {
                element.prop('disabled', false);
                $(`#img_${networkClient.ID}`).attr('src', 'img/connect.png');
                if (networkClient.StreamingFunction === true) {
                    $(`#status_streaming_${networkClient.ID}`).removeClass('text-muted');
                    const statusStreaming = this._getStringStreamingStatus(networkClient.StreamingStatus);
                    $(`#status_streaming_${networkClient.ID}`)[0].childNodes[0].innerText = `Streaming: ${statusStreaming}`;
                }
                else {
                    $(`#status_streaming_${networkClient.ID}`).addClass('text-muted');
                    $(`#status_streaming_${networkClient.ID}`)[0].childNodes[0].innerText = `Streaming: Disabled`;
                }
                if (networkClient.FileTransferFunction === true) {
                    $(`#status_transfer_${networkClient.ID}`).removeClass('text-muted');
                    $(`#status_transfer_${networkClient.ID}`)[0].childNodes[0].innerText = `Transfer: Enabled`;
                }
                else {
                    $(`#status_transfer_${networkClient.ID}`).addClass('text-muted');
                    $(`#status_transfer_${networkClient.ID}`)[0].childNodes[0].innerText = `Transfer: Disabled`;
                }
            } else {
                element.prop('disabled', true);
                $(`#img_${networkClient.ID}`).attr('src', 'img/disconnect.png');
                $(`#status_streaming_${networkClient.ID}`).addClass('text-muted');
                $(`#status_voip_${networkClient.ID}`).addClass('text-muted');
                $(`#status_transfer_${networkClient.ID}`).addClass('text-muted');
                $(`#status_control_${networkClient.ID}`).addClass('text-muted');
                $(`#status_streaming_${networkClient.ID}`)[0].childNodes[0].innerText = `Streaming: Disabled`;
                $(`#status_transfer_${networkClient.ID}`)[0].childNodes[0].innerText = `Transfer: Disabled`;
                for (const elem of element) {
                    elem.checked = false;
                }
                this._onRadioSelectionChangedTx({ data: { controller: this } });
            }
        } else {
            if (networkClient.Status === 'connect') {
                this._showNetworkClient4Tx(networkClient);
            }
        }
    }

    _updateVoIPStatus(VoIPState, no) {
        $(`#VoIP_Status`)[no].innerText=VoIPState.Status;
/*         const statusVoIP = this._getStringVoIPStatus(VoIPState.Status);
        const element = $(`#status_VoIP_${VoIPState.ID}`);
        if (element.length > 0) {
            $(`#status_VoIP_${VoIPState.ID}`)[0].childNodes[0].innerText = `VoIP: ${VoIPState}`;
            if (statusVoIP === 'Disabled') {
                $(`#status_VoIP_${VoIPState.ID}`).addClass('text-muted');
            } else {
                $(`#status_VoIP_${VoIPState.ID}`).removeClass('text-muted');
            }
        } */
    }
	
    _updateTxNetworkClientStreamingStatus(networkClient) {
        const statusStreaming = this._getStringStreamingStatus(networkClient.Status);
        const element = $(`#status_streaming_${networkClient.ID}`);
        if (element.length > 0) {
            $(`#status_streaming_${networkClient.ID}`)[0].childNodes[0].innerText = `Streaming: ${statusStreaming}`;
            if (statusStreaming === 'Disabled') {
                $(`#status_streaming_${networkClient.ID}`).addClass('text-muted');
            } else {
                $(`#status_streaming_${networkClient.ID}`).removeClass('text-muted');
            }
        }
    }


    _initializeStreamingDynamicPropertyChart() {
        /// graph設定
        const option = {
            animation: false,
            scaleOverride: true,
            scaleStartValue: 100,
            scaleShowGridLines: false,
            scaleGridLineColor: "rgba(0,0,0,.05)",
            scaleGridLineWidth: 1,
            bezierCurve: true,
            bezierCurveTension: 0.4,
            pointDot: false,
            datasetStroke: false,
            datasetStrokeWidth: 1,
            fill: false,
            responsive: true,
            maintainAspectRatio: false,
            elements: { point: { radius: 0 } },
            legend: {
                display: true,
                labels: {
                    boxWidth: 20
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        autoSkip: true
                    }
                }],
                xAxes: [{
                    type: 'time',
                    time: {
                        displayFormats: {
                            second: 'mm:ss'
                        }
                    },
                    ticks: {
                        autoSkip: true,
                        maxRotation: 0,
                        minRotation: 0
                    }
                }]
            }
        }
        /// Loss用のグラフ
        const ctxloss = document.getElementById("streamingchart-loss").getContext('2d');
        ctxloss.canvas.height = 100;
        this.lossStreamingChart = new Chart(ctxloss, {
            type: 'line',
            data: {
                labels: this.dataStreamingChart.timestamp,
                datasets: [{
                    label: 'NIC #1',
                    data: this.dataStreamingChart.loss.nic1,
                    borderColor: 'rgba(255,99,132,1)',
                    backgroundColor: 'rgba(0,0,0,0)',
                    borderWidth: 1
                }, {
                    label: 'NIC #2',
                    data: this.dataStreamingChart.loss.nic2,
                    borderColor: 'rgba(188,199,100,1)',
                    backgroundColor: 'rgba(0,0,0,0)',
                    borderWidth: 1
                }, {
                    label: 'NIC #3',
                    data: this.dataStreamingChart.loss.nic3,
                    borderColor: 'rgba(88,199,100,1)',
                    backgroundColor: 'rgba(0,0,0,0)',
                    borderWidth: 1
                }, {
                    label: 'NIC #4',
                    data: this.dataStreamingChart.loss.nic4,
                    borderColor: 'rgba(0,162,232,1)',
                    backgroundColor: 'rgba(0,0,0,0)',
                    borderWidth: 1
                }]
            },
            options: option
        });

        // Bitrateグラフ
        const ctxbitrate = document.getElementById("streamingchart-bitrate").getContext('2d');
        ctxbitrate.canvas.height = 100;
        this.bitrateStreamingChart = new Chart(ctxbitrate, {
            type: 'line',
            data: {
                labels: this.dataStreamingChart.timestamp,
                datasets: [{
                    label: 'NIC #1',
                    data: this.dataStreamingChart.bitrate.nic1,
                    borderColor: 'rgba(255,99,132,1)',
                    backgroundColor: 'rgba(0,0,0,0)',
                    borderWidth: 1
                }, {
                    label: 'NIC #2',
                    data: this.dataStreamingChart.bitrate.nic2,
                    borderColor: 'rgba(188,199,100,1)',
                    backgroundColor: 'rgba(0,0,0,0)',
                    borderWidth: 1
                }, {
                    label: 'NIC #3',
                    data: this.dataStreamingChart.bitrate.nic3,
                    borderColor: 'rgba(88,199,100,1)',
                    backgroundColor: 'rgba(0,0,0,0)',
                    borderWidth: 1
                }, {
                    label: 'NIC #4',
                    data: this.dataStreamingChart.bitrate.nic4,
                    borderColor: 'rgba(0,162,232,1)',
                    backgroundColor: 'rgba(0,0,0,0)',
                    borderWidth: 1
                }, {
                    label: 'Total',
                    data: this.dataStreamingChart.bitrate.total,
                    borderColor: 'rgba(255,0,255,1)',
                    backgroundColor: 'rgba(0,0,0,0)',
                    borderWidth: 1
                }]
            },
            options: option
        });

        // Delayグラフ
        const ctxDelay = document.getElementById("streamingchart-delay").getContext('2d');
        ctxDelay.canvas.height = 100;
        this.delayStreamingChart = new Chart(ctxDelay, {
            type: 'line',
            data: {
                labels: this.dataStreamingChart.timestamp,
                datasets: [{
                    label: 'NIC #1',
                    data: this.dataStreamingChart.delay.nic1,
                    borderColor: 'rgba(255,99,132,1)',
                    backgroundColor: 'rgba(0,0,0,0)',
                    borderWidth: 1
                }, {
                    label: 'NIC #2',
                    data: this.dataStreamingChart.delay.nic2,
                    borderColor: 'rgba(188,199,100,1)',
                    backgroundColor: 'rgba(0,0,0,0)',
                    borderWidth: 1
                }, {
                    label: 'NIC #3',
                    data: this.dataStreamingChart.delay.nic3,
                    borderColor: 'rgba(88,199,100,1)',
                    backgroundColor: 'rgba(0,0,0,0)',
                    borderWidth: 1
                }, {
                    label: 'NIC #4',
                    data: this.dataStreamingChart.delay.nic4,
                    borderColor: 'rgba(0,162,232,1)',
                    backgroundColor: 'rgba(0,0,0,0)',
                    borderWidth: 1
                }]
            },
            options: option
        });

        // rttグラフ
        const ctxrtt = document.getElementById("streamingchart-rtt").getContext('2d');
        ctxrtt.canvas.height = 100;
        this.rttStreamingChart = new Chart(ctxrtt, {
            type: 'line',
            data: {
                labels: this.dataStreamingChart.timestamp,
                datasets: [{
                    label: 'NIC #1',
                    data: this.dataStreamingChart.rtt.nic1,
                    borderColor: 'rgba(255,99,132,1)',
                    backgroundColor: 'rgba(0,0,0,0)',
                    borderWidth: 1
                }, {
                    label: 'NIC #2',
                    data: this.dataStreamingChart.rtt.nic2,
                    borderColor: 'rgba(188,199,100,1)',
                    backgroundColor: 'rgba(0,0,0,0)',
                    borderWidth: 1
                }, {
                    label: 'NIC #3',
                    data: this.dataStreamingChart.rtt.nic3,
                    borderColor: 'rgba(88,199,100,1)',
                    backgroundColor: 'rgba(0,0,0,0)',
                    borderWidth: 1
                }, {
                    label: 'NIC #4',
                    data: this.dataStreamingChart.rtt.nic4,
                    borderColor: 'rgba(0,162,232,1)',
                    backgroundColor: 'rgba(0,0,0,0)',
                    borderWidth: 1
                }]
            },
            options: option
        });
    }

    _initializeClipTransferPreset() {
        const elemPreser = $('#cliptransferview_preset');

        let index = 0;
        for (const preset of clip_transfer_preset) {
            const elemPre = $('<option>', {
                value: index
            });
            elemPre.append(`Preset #${index + 1}`);
            elemPreser.append(elemPre);
            ++index;
        }
    }

    _initializeStreamingSendStartPreset() {
        const elemPreset = $('#streamingstart_view_preset');

        let index = 0;
        for (const preset of streaming_send_start_preset) {
            const elemPre = $('<option>', {
                value: index
            });
            elemPre.append(`Preset #${index + 1}`);
            elemPreset.append(elemPre);
            ++index;
        }
    }

    _initializeStreamingReceiveStartPreset() {
        const elemPreset = $('#streamingreceivestart_view_preset');

        let index = 0;
        for (const preset of streaming_receive_start_preset) {
            const elemPre = $('<option>', {
                value: index
            });
            elemPre.append(`Preset #${index + 1}`);
            elemPreset.append(elemPre);
            ++index;
        }
    }

    _initializeVoIPStartPreset() {
        const elemPreset = $('#voipstart_view_preset');

        let index = 0;
        for (const preset of voip_send_start_preset) {
            const elemPre = $('<option>', {
                value: index
            });
            elemPre.append(preset.type);
            elemPreset.append(elemPre);
            ++index;
        }
    }

    /**
     * ストリーミング開始中チャートの表示
     */
    _showStreamingDynamicPropertyChart() {
        $('#streaming_main_view_chart').removeClass('d-none');
        this.timerStreamingChart = setInterval(() => {
            this.dataStreamingChart = this.model.getStreamingDynamicPropertyData();
            if (this.lossStreamingChart != undefined) {
                this.lossStreamingChart.data.labels = this.dataStreamingChart.timestamp;
                this.lossStreamingChart.data.datasets[0].data = this.dataStreamingChart.loss.nic1;
                this.lossStreamingChart.data.datasets[1].data = this.dataStreamingChart.loss.nic2;
                this.lossStreamingChart.data.datasets[2].data = this.dataStreamingChart.loss.nic3;
                this.lossStreamingChart.data.datasets[3].data = this.dataStreamingChart.loss.nic4;
                this.lossStreamingChart.update();
            }
            if (this.bitrateStreamingChart != undefined) {
                this.bitrateStreamingChart.data.labels = this.dataStreamingChart.timestamp;
                this.bitrateStreamingChart.data.datasets[0].data = this.dataStreamingChart.bitrate.nic1;
                this.bitrateStreamingChart.data.datasets[1].data = this.dataStreamingChart.bitrate.nic2;
                this.bitrateStreamingChart.data.datasets[2].data = this.dataStreamingChart.bitrate.nic3;
                this.bitrateStreamingChart.data.datasets[3].data = this.dataStreamingChart.bitrate.nic4;
                this.bitrateStreamingChart.update();
            }
            if (this.delayStreamingChart != undefined) {
                this.delayStreamingChart.data.labels = this.dataStreamingChart.timestamp;
                this.delayStreamingChart.data.datasets[0].data = this.dataStreamingChart.delay.nic1;
                this.delayStreamingChart.data.datasets[1].data = this.dataStreamingChart.delay.nic2;
                this.delayStreamingChart.data.datasets[2].data = this.dataStreamingChart.delay.nic3;
                this.delayStreamingChart.data.datasets[3].data = this.dataStreamingChart.delay.nic4;
                this.delayStreamingChart.update();
            }
            if (this.rttStreamingChart != undefined) {
                this.rttStreamingChart.data.labels = this.dataStreamingChart.timestamp;
                this.rttStreamingChart.data.datasets[0].data = this.dataStreamingChart.rtt.nic1;
                this.rttStreamingChart.data.datasets[1].data = this.dataStreamingChart.rtt.nic2;
                this.rttStreamingChart.data.datasets[2].data = this.dataStreamingChart.rtt.nic3;
                this.rttStreamingChart.data.datasets[3].data = this.dataStreamingChart.rtt.nic4;
                this.rttStreamingChart.update();
            }
        }, 1000);
    }

    /**
     * ストリーミング開始中チャートの非表示
     */
    _hideStreamingDynamicPropertyChart() {
        $('#streaming_main_view_chart').addClass('d-none');
        if (this.timerStreamingChart !== undefined) {
            clearInterval(this.timerStreamingChart);
            this.timerStreamingChart = undefined;
        }
    }

    _disableStreamingMainViewImage(isDisable) {
        if (isDisable === true) {
            $('#streaming_main_view_image').addClass('d-none');
        } else {
            $('#streaming_main_view_image').removeClass('d-none');
        }

    }


    /**
     * Base64 の画像データを指定された要素で表示
     * @param {*} id 表示要素のID
     * @param {*} base64String Base64形式の画像データ
     */
    _base64ToImage(id, base64String) {
        $(`#${id}`).attr('src', base64String);
    }

    _onClickClipTransferViewSubmit(controller) {
        const showform = $('#cliptransferview_form').hasClass('d-none');
        if (showform === false) {
            const networkClientID = controller.model.getSelectedTxNetworkClientId();
            const media = $('#cliplist_media').val();
            const clips = controller.model.getSelectedClipList(networkClientID);
            const cmdData = controller._getClipTransferViewData(networkClientID, media, clips);
            controller.model.addExecutedCommandRequest(networkClientID, 'AddUploadFiles', cmdData);
            controller.ccm.cmdAddUploadFiles(cmdData);
            $('#cliptransferview_submit_spinner').toggleClass('d-none');
        }
        else {
            $('#cliptransfer_view').modal('hide');
            // $('#cliptransferview_form').removeClass('d-none');
            // $('#cliptransferview_preset').removeClass('d-none');
            // $('#cliptransferview_result').addClass('d-none');
            // $('#cliptransferview_cancel').removeClass('d-none');
            // $('#cliptransferview_close').removeClass('d-none');
        }
    }
    _getClipTransferViewData(networkClientID, media, clips) {
        let data = {};
        data.ID = networkClientID;
        data.media = media;
        data.setting_id = "2";
        // files
        data.files = [];
        for (const clip of clips) {
            data.files.push(clip.uri);
        }
        // destination
        data.destination = {};
        data.destination.type = "FTP";
        data.destination.disp_name = $('#inputDisplayName').val();
        data.destination.ftp_host_name = $('#inputFtpHostName').val();
        data.destination.ftp_port = Number($('#inputFtpPort').val());
        data.destination.ftp_user_name = $('#inputFtpUser').val();
        data.destination.ftp_password = $('#inputFtpPassword').val();
        data.destination.ftp_use_ftpes = $('#isUseFtpes').prop('checked');
        data.destination.ftp_use_pasv = $('#isUsePassive').prop('checked');
        data.destination.ftp_check_cert = $('#isVerifyCertification').prop('checked');
        data.destination.ftp_upload_dir = $('#inputUploadDirectory').val();
        data.destination.ftp_use_mcb = $('#isUseMcb').prop('checked');
        if (data.destination.ftp_use_mcb === true) {
            data.destination.mcb_host = $('#inputMcbHostName').val();
            data.destination.mcb_port = Number($('#inputMcbPort').val());
            data.destination.mcb_username = $('#inputMcbUser').val();
            data.destination.mcb_password = $('#inputMcbPassword').val();
        }

        return data;
    }

    // clip_transfer_preset は、ccmconfig.js で定義しておくこと
    _onChangeClipTransferViewPreset(preset) {
        const index = Number(preset);
        const setting = clip_transfer_preset[index];
        this._setClipTransferViewPreset(setting);
    }

    _setClipTransferViewPreset(preset) {
        $('#inputDisplayName').val(preset.display_name);
        $('#inputFtpHostName').val(preset.ftp_host_name);
        $('#inputFtpPort').val(preset.ftp_port);
        $('#inputFtpUser').val(preset.ftp_user);
        $('#inputFtpPassword').val(preset.ftp_password);
        $('#inputUploadDirectory').val(preset.upload_directory);
        $('#isUseFtpes').prop('checked', preset.is_use_ftpes);
        $('#isUsePassive').prop('checked', preset.is_use_passive);
        $('#isVerifyCertification').prop('checked', preset.is_verify_certification);
        this._setUseMcbSetting(preset.is_use_mcb_setting);
        $('#inputMcbHostName').val(preset.mcb_host_name);
        $('#inputMcbPort').val(preset.mcb_port);
        $('#inputMcbUser').val(preset.mcb_user);
        $('#inputMcbPassword').val(preset.mcb_password);
        this._showIsUseMcbConfig();
    }

    _onChangeStreamingStartViewPreset(preset) {
        const index = Number(preset);
        const setting = streaming_send_start_preset[index];
        this._setStreamingSendStartViewPreset(setting);
    }
    _setStreamingSendStartViewPreset(preset) {
        $('#streamingstart_form_address').val(preset.address);
        $('#streamingstart_form_port').val(preset.port);
        $('#streamingstart_form_videoformat').prop('selectedIndex', preset.videoformat_index);
        $('#streamingstart_form_gop').val(preset.gop);
        $('#streamingstart_form_buffertime').val(preset.buffertime);
        $('#streamingstart_form_chiperkey').val(preset.chiperkey);
        $('#streamingstart_form_ssrc').val(preset.ssrc);

        const elemQosinterfaces = $('#streamingstart_form_qosinterfaces');
        if (elemQosinterfaces !== undefined) {
            for (const elem of elemQosinterfaces) {
                const qosinterfaces_length = elem.length;
                for (const e of elem) {
                    e.selected = false;
                }
                for (const i of preset.qosinterfaces_index) {
                    if (i < qosinterfaces_length) {
                        elem[i].selected = true;
                    }
                }
            }
        }
        const elemMetainterfaces = $('#streamingstart_form_metainterfaces');
        if (elemMetainterfaces !== undefined) {
            for (const elem of elemMetainterfaces) {
                const metainterfaces_length = elem.length;
                for (const e of elem) {
                    e.selected = false;
                }
                for (const i of preset.metainterfaces_index) {
                    if (i < metainterfaces_length) {
                        elem[i].selected = true;
                    }
                }
            }
        }
        const value = $('#streamingstart_form_videoformat').val();
        this._setStreamingStartBitrateOption(value);
    }

    _onChangeStreamingReceiveStartViewPreset(preset) {
        const index = Number(preset);
        const settings = streaming_receive_start_preset[index];
        this._setStreamingReceiveStartViewPreset(settings);
    }
    _setStreamingReceiveStartViewPreset(preset) {
        $('#streamingreceivestart_form_id').val(preset.id);
        $('#streamingreceivestart_form_port').val(preset.port);
        $('#streamingreceivestart_form_videoformat').prop('selectedIndex', preset.videoformat_index);
        $('#streamingreceivestart_form_gop').val(preset.gop);
        $('#streamingreceivestart_form_buffertime').val(preset.buffertime);
        $('#streamingreceivestart_form_chiperkey').val(preset.chiperkey);
        $('#streamingreceivestart_form_ssrc').val(preset.ssrc);

        const elemQosinterfaces = $('#streamingreceivestart_form_qosinterfaces');
        if (elemQosinterfaces !== undefined) {
            for (const elem of elemQosinterfaces) {
                const qosinterfaces_length = elem.length;
                for (const e of elem) {
                    e.selected = false;
                }
                for (const i of preset.qosinterfaces_index) {
                    if (i < qosinterfaces_length) {
                        elem[i].selected = true;
                    }
                }
            }
        }
        const elemMetainterfaces = $('#streamingreceivestart_form_metainterfaces');
        if (elemMetainterfaces !== undefined) {
            for (const elem of elemMetainterfaces) {
                const metainterfaces_length = elem.length;
                for (const e of elem) {
                    e.selected = false;
                }
                for (const i of preset.metainterfaces_index) {
                    if (i < metainterfaces_length) {
                        elem[i].selected = true;
                    }
                }
            }
        }
    }

    _onChangeVoIPStartViewPreset(preset) {
        const index = Number(preset);
        const setting = voip_send_start_preset[index];
        this._setVoIPSendStartViewPreset(setting);
    }
    _setVoIPSendStartViewPreset(preset) {
        $('#voipstart_form_port_d').val(preset.destination_port);
        $('#voipstart_form_port_r').val(preset.receiving_port);
        $('#voipstart_form_framelen_t').val(preset.frame_length_IntercomTx);
        $('#voipstart_form_framelen_r').val(preset.frame_length_IntercomRx);
        $('#voipstart_form_chiperkey_t').val(preset.cipher_key_IntercomTx);
        $('#voipstart_form_chiperkey_r').val(preset.cipher_key_IntercomRx);
        $('#voipstart_form_ssrc_t').val(preset.ssrc_IntercomTx);
        $('#voipstart_form_ssrc_r').val(preset.ssrc_IntercomRx);
    }

    /**
     * Tx一覧画面に表示している NetworkClient の選択を変更したときの処理
     * @param {*} event 
     */
    _onRadioSelectionChangedTx(event) {
        const controller = event.data.controller;
        const elements = $('*[name=tx]');

        let selectedElement = null;
        const len = elements.length;
        for (let i = 0; i < len; ++i) {
            if (elements[i].checked) {
                selectedElement = elements[i];
                break;
            }
        }

        controller._clearTransmitterInspectionView(controller);
        if (selectedElement) {
            const model = controller.model;
            model.setSelectedTxNetworkClientId(selectedElement.value);
            controller._showTransmitterInspectionView(controller);
            controller._navInitiazlie();
            controller._navChanged();
        }
    }
    /**
     * Transmitter Inspection View に NetworkClient情報を表示
     * @param {*} view 
     */
    _showTransmitterInspectionView(controller) {
        const networkClient = controller.model.getSelectedTxNetworkClient();
        $("#transmitterinspectionview_dispname").text(`Name: ${networkClient.DisplayName}`);
        $("#transmitterinspectionview_id").text(networkClient.ID);
        $("#tx_networkclient_detail_modelname").text(networkClient.ModelName);
        $("#tx_networkclient_detail_serialnumber").text(networkClient.SerialNumber);
        $("#tx_networkclient_detail_platformtype").text(networkClient.PlatformType);
        $("#tx_networkclient_detail_softwareversion").text(networkClient.SoftwareVersion);
        // ---
        const detailNetworkInfo = $('#tx_networkclient_detail_networkinfo');
        detailNetworkInfo.empty();
        const len = networkClient.NetworkInterfaces.length;
        for (let i = 0; i < len; ++i) {
            const nic = networkClient.NetworkInterfaces[i];
            const nicId = this._createTransmitterInspectionViewNetworkInterfaceElement(i, 'ID', nic.ID);
            detailNetworkInfo.append(nicId);
            const nicType = this._createTransmitterInspectionViewNetworkInterfaceElementMargin(i, 'Type', nic.Type);
            detailNetworkInfo.append(nicType);
            const nicPhysicalInterfaceName = this._createTransmitterInspectionViewNetworkInterfaceElementMargin(i, 'PhysicalInterfaceName', nic.PhysicalInterfaceName);
            detailNetworkInfo.append(nicPhysicalInterfaceName);
            const nicIPv4Address = this._createTransmitterInspectionViewNetworkInterfaceElementMargin(i, 'IPv4Address', nic.IPv4Address);
            detailNetworkInfo.append(nicIPv4Address);
            const nicAirInterface = this._createTransmitterInspectionViewNetworkInterfaceElementMargin(i, 'AirInterface', nic.CellularInfo.AirInterface);
            detailNetworkInfo.append(nicAirInterface);
            const nicBand = this._createTransmitterInspectionViewNetworkInterfaceElementMargin(i, 'Band', nic.CellularInfo.Band);
            detailNetworkInfo.append(nicBand);
            const nicCellID = this._createTransmitterInspectionViewNetworkInterfaceElementMargin(i, 'CellID', nic.CellularInfo.CellID);
            detailNetworkInfo.append(nicCellID);
            const nicIMEI = this._createTransmitterInspectionViewNetworkInterfaceElementMargin(i, 'IMEI', nic.CellularInfo.IMEI);
            detailNetworkInfo.append(nicIMEI);
            const nicManufacture = this._createTransmitterInspectionViewNetworkInterfaceElementMargin(i, 'Manufacture', nic.CellularInfo.Manufacture);
            detailNetworkInfo.append(nicManufacture);
            const nicModelName = this._createTransmitterInspectionViewNetworkInterfaceElementMargin(i, 'ModelName', nic.CellularInfo.ModelName);
            detailNetworkInfo.append(nicModelName);
            const nicOperator = this._createTransmitterInspectionViewNetworkInterfaceElementMargin(i, 'Operator', nic.CellularInfo.Operator);
            detailNetworkInfo.append(nicOperator);
            const nicPLMN = this._createTransmitterInspectionViewNetworkInterfaceElementMargin(i, 'PLMN', nic.CellularInfo.PLMN);
            detailNetworkInfo.append(nicPLMN);
            const nicSignalStrength = this._createTransmitterInspectionViewNetworkInterfaceElementMargin(i, 'SignalStrength', nic.CellularInfo.SignalStrength);
            detailNetworkInfo.append(nicSignalStrength);
            const nicSignalStrengthUnit = this._createTransmitterInspectionViewNetworkInterfaceElementMargin(i, 'SignalStrengthUnit', nic.CellularInfo.SignalStrengthUnit);
            detailNetworkInfo.append(nicSignalStrengthUnit);
        }
        // ---
        $('#transmitter_inspection_view_area').removeClass('d-none');
        $('#operation_view').removeClass('d-none');
        // tx_networkclient_detail
        this._updateGPSInfo();
    }
    _createTransmitterInspectionViewNetworkInterfaceElement(index, label, value) {
        const rootRowElement = $('<div>', {
            class: 'row'
        });
        const rootColElement = $('<div>', {
            class: 'col'
        });
        rootRowElement.append(rootColElement);
        const smallElement = $('<small>');
        rootColElement.append(smallElement);
        const labelElement = $('<strong>', {
            class: 'tx_networkclient_detail_label'
        });
        smallElement.append(labelElement);
        labelElement.append(`${label} : `);
        //
        const lower = label.toLowerCase();
        const valueElement = $('<small>', {
            id: `tx_networkclient_detail_networkinfo${index + 1}_${lower}`
        });
        rootColElement.append(valueElement);
        valueElement.append(`'${value}'`);

        return rootRowElement;
    }
    _createTransmitterInspectionViewNetworkInterfaceElementMargin(index, label, value) {
        const rootRowElement = $('<div>', {
            class: 'row ml-2'
        });
        const rootColElement = $('<div>', {
            class: 'col'
        });
        rootRowElement.append(rootColElement);
        const labelElement = $('<small>');
        rootColElement.append(labelElement);
        labelElement.append(`${label} : `);
        //
        const lower = label.toLowerCase();
        const valueElement = $('<small>', {
            id: `tx_networkclient_detail_networkinfo${index + 1}_${lower}`
        });
        rootColElement.append(valueElement);
        valueElement.append(`'${value}'`);

        return rootRowElement;
    }
    _clearTransmitterInspectionView(controller) {
        $("#transmitterinspectionview_dispname").text('');
        $("#transmitterinspectionview_id").text('');
        //
        $('#transmitter_inspection_view_area').addClass('d-none');
        $('#operation_view').addClass('d-none');
        //
        this._showClipInspectionView(false);
        this._showPlanningMetadataInspectionView(false);
        // tx_networkclient_detail
        $("#tx_networkclient_detail_gps_latitude").text('');
        $("#tx_networkclient_detail_gps_longitude").text('');
    }


    _mediaChanged() {
        const networkClientId = this.model.getSelectedTxNetworkClientId();
        const media = $('#cliplist_media').val();

        if (networkClientId.length > 0) {
            const cmdData = {
                ID: networkClientId,
                media: media
            };
            this.model.addExecutedCommandRequest(networkClientId, 'GetClipList', cmdData);
            this.model.clearClipList(networkClientId);
            this.ccm.cmdGetClipList(cmdData);
            this._initializeClipInspectionViewInfo();
            this._clearClipList();
        }
    }

    _getJobList() {
        const networkClientId = this.model.getSelectedTxNetworkClientId();
        const cmdData = {
            ID: networkClientId,
        };
        this.model.addExecutedCommandRequest(networkClientId, 'GetUploadList', cmdData);
        this.ccm.cmdGetUploadList(cmdData);
        this._clearUploadList();
    }

    _onClickStreamingStart() {
        const networkClientId = this.model.getSelectedTxNetworkClientId();
        const cmdData = {
            ID: networkClientId,
            address: $('#streamingstart_form_address').val(),
            port: Number($('#streamingstart_form_port').val()),
            video_format: $('#streamingstart_form_videoformat').val(),
            bitrate: Number($('#streamingstart_form_bitrate').val()),
            gop: Number($('#streamingstart_form_gop').val()),
            buffer_time: Number($('#streamingstart_form_buffertime').val()),
            chiper_key: $('#streamingstart_form_chiperkey').val(),
            ssrc: Number($('#streamingstart_form_ssrc').val()),
            qos_interfaces: $('#streamingstart_form_qosinterfaces').val(),
            meta_interfaces: $('#streamingstart_form_metainterfaces').val()
        };
        this.model.addExecutedCommandRequest(networkClientId, 'StartQosStreamingSend', cmdData);
        this.ccm.cmdStartQosStreamingSend(cmdData);

        $('#streamingstart_view').modal('hide');
    }

    _onClickStreamingReceiveStart() {
        const networkClientId = $('#streamingreceivestart_form_id').val();
        const cmdData = {
            ID: networkClientId,
            port: Number($('#streamingreceivestart_form_port').val()),
            video_format: $('#streamingreceivestart_form_videoformat').val(),
            gop: Number($('#streamingreceivestart_form_gop').val()),
            buffer_time: Number($('#streamingreceivestart_form_buffertime').val()),
            chiper_key: $('#streamingreceivestart_form_chiperkey').val(),
            ssrc: Number($('#streamingreceivestart_form_ssrc').val()),
            qos_interfaces: $('#streamingreceivestart_form_qosinterfaces').val(),
            meta_interfaces: $('#streamingreceivestart_form_metainterfaces').val()
        };
        this.model.addExecutedCommandRequest(networkClientId, 'StartQosStreamingReceive', cmdData);
        this.ccm.cmdStartQosStreamingReceive(cmdData);

        $('#streamingreceivestart_view').modal('hide');
    }

    _onClickStreamingStop() {
        const networkClientId = this.model.getSelectedTxNetworkClientId();
        const cmdData = {
            ID: networkClientId
        };
        this.model.addExecutedCommandRequest(networkClientId, 'StopStreamingSend', cmdData);
        this.ccm.cmdStopStreamingSend(cmdData);
    }

    _onClickStreamingReceiveStop() {
        const networkClientId = this.model.getSelectedTxNetworkClientId();
        const cmdData = {
            ID: networkClientId
        };
        this.model.addExecutedCommandRequest(networkClientId, 'StopStreamingReceive', cmdData);
        this.ccm.cmdStopStreamingReceive(cmdData);
    }

    _onClickVoIPStart() {
        const networkClientId = this.model.getSelectedTxNetworkClientId();
        const cmdData = {
            ID: networkClientId,
            address: $('#voipstart_form_address').val(),
            port_d: Number($('#voipstart_form_port_d').val()),
            port_r: Number($('#voipstart_form_port_r').val()),
            framelen_t: Number($('#voipstart_form_framelen_t').val()),
            framelen_r: Number($('#voipstart_form_framelen_r').val()),
            chiperkey_t: $('#voipstart_form_chiperkey_t').val(),
            chiperkey_r: $('#voipstart_form_chiperkey_r').val(),
            ssrc_t: $('#voipstart_form_ssrc_t').val(),
            ssrc_r: $('#voipstart_form_ssrc_r').val(),
        };
        this.model.addExecutedCommandRequest(networkClientId, 'StartVoIP', cmdData);
        this.ccm.cmdStartVoIP(cmdData);

        $('#voipstart_view').modal('hide');
    }

    _onClickVoIPStop() {
        const networkClientId = this.model.getSelectedTxNetworkClientId();
        const cmdData = {
            ID: networkClientId
        };
        this.model.addExecutedCommandRequest(networkClientId, 'StopVoIP', cmdData);
        this.ccm.cmdStopVoIP(cmdData);
    }

    _setStreamingStartVideoFormatOption(videoFormatCapability) {
        $('#streamingstart_form_videoformat *').remove();

        const len = videoFormatCapability.length;
        for (let i = 0; i < len; ++i) {
            const elemOption = $('<option>', {
                value: videoFormatCapability[i].ID
            });
            elemOption.append(videoFormatCapability[i].ID);
            $('#streamingstart_form_videoformat').append(elemOption);
        }

        this._setStreamingStartBitrateOption(videoFormatCapability[0].ID);
    }
    _setStreamingStartBitrateOption(id) {
        $('#streamingstart_form_bitrate *').remove();
        //
        const networkClient = this.model.getSelectedTxNetworkClient();
        const capability = networkClient.VideoFormatCapability.find(item => item.ID === id);

        if (capability.BitrateStep === 0) {
            const elemOption = $('<option>', {
                value: 0
            });
            elemOption.append('0');
            $('#streamingstart_form_bitrate').append(elemOption);
        } else {
            for (let bitrate = capability.MinBitrate; bitrate <= capability.MaxBitrate; bitrate = bitrate + capability.BitrateStep) {
                const elemOption = $('<option>', {
                    value: bitrate
                });
                elemOption.append(bitrate);
                $('#streamingstart_form_bitrate').append(elemOption);
            }
        }
    }

    _setStreamingStartQosInterfacesOption(networkInterfaces) {
        $('#streamingstart_form_qosinterfaces *').remove();
        //
        const len = networkInterfaces.length;
        for (let i = 0; i < len; ++i) {
            const elemOption = $('<option>', {
                value: networkInterfaces[i].ID
            });
            elemOption.append(networkInterfaces[i].ID);
            $('#streamingstart_form_qosinterfaces').append(elemOption);
        }
    }

    _setStreamingStartMetaInterfacesOption(networkInterfaces) {
        $('#streamingstart_form_metainterfaces *').remove();
        //
        const len = networkInterfaces.length;
        for (let i = 0; i < len; ++i) {
            const elemOption = $('<option>', {
                value: networkInterfaces[i].ID
            });
            elemOption.append(networkInterfaces[i].ID);
            $('#streamingstart_form_metainterfaces').append(elemOption);
        }
    }

    _setStreamingReceiveStartVideoFormatOption(videoFormatCapability) {
        $('#streamingreceivestart_form_videoformat *').remove();

        const len = videoFormatCapability.length;
        for (let i = 0; i < len; ++i) {
            const elemOption = $('<option>', {
                value: videoFormatCapability[i].ID
            });
            elemOption.append(videoFormatCapability[i].ID);
            $('#streamingreceivestart_form_videoformat').append(elemOption);
        }
    }

    _setStreamingReceiveStartQosInterfacesOption(networkInterfaces) {
        $('#streamingreceivestart_form_qosinterfaces *').remove();
        //
        const len = networkInterfaces.length;
        for (let i = 0; i < len; ++i) {
            const elemOption = $('<option>', {
                value: networkInterfaces[i].ID
            });
            elemOption.append(networkInterfaces[i].ID);
            $('#streamingreceivestart_form_qosinterfaces').append(elemOption);
        }
    }

    _setStreamingReceiveStartMetaInterfacesOption(networkInterfaces) {
        $('#streamingreceivestart_form_metainterfaces *').remove();
        //
        const len = networkInterfaces.length;
        for (let i = 0; i < len; ++i) {
            const elemOption = $('<option>', {
                value: networkInterfaces[i].ID
            });
            elemOption.append(networkInterfaces[i].ID);
            $('#streamingreceivestart_form_metainterfaces').append(elemOption);
        }
    }

    _changeClipListSort() {
        const sortType = $('#cliplist_sort').val();
        this.model.setClipListOrder(sortType);
        this._showClip();
    }

    /**
     * ネットワーク情報の更新
     */
    _updateNetworkInfo() {
        const networkClientId = this.model.getSelectedTxNetworkClientId();
        if (networkClientId.length > 0) {
            const networkInfo = this.model.getNetworkInfoData(networkClientId);
            if (networkInfo !== undefined) {
                const len = networkInfo.CellularInfo.length;
                for (let i = 0; i < len; ++i) {
                    const cellularInfo = networkInfo.CellularInfo[i];
                    this._updateTransmitterInspectionViewNetworkInterfaceElement(i, 'AirInterface', cellularInfo.AirInterface);
                    this._updateTransmitterInspectionViewNetworkInterfaceElement(i, 'Band', cellularInfo.Band);
                    this._updateTransmitterInspectionViewNetworkInterfaceElement(i, 'CellID', cellularInfo.CellID);
                    this._updateTransmitterInspectionViewNetworkInterfaceElement(i, 'IMEI', cellularInfo.IMEI);
                    this._updateTransmitterInspectionViewNetworkInterfaceElement(i, 'Manufacture', cellularInfo.Manufacture);
                    this._updateTransmitterInspectionViewNetworkInterfaceElement(i, 'ModelName', cellularInfo.ModelName);
                    this._updateTransmitterInspectionViewNetworkInterfaceElement(i, 'Operator', cellularInfo.Operator);
                    this._updateTransmitterInspectionViewNetworkInterfaceElement(i, 'PLMN', cellularInfo.PLMN);
                    this._updateTransmitterInspectionViewNetworkInterfaceElement(i, 'SignalStrength', cellularInfo.SignalStrength);
                    this._updateTransmitterInspectionViewNetworkInterfaceElement(i, 'SignalStrengthUnit', cellularInfo.SignalStrengthUnit);
                }
            }
        }
    }
    _updateTransmitterInspectionViewNetworkInterfaceElement(index, label, value) {
        const lower = label.toLowerCase();
        const elem = $(`#tx_networkclient_detail_networkinfo${index + 1}_${lower}`);
        elem.empty();
        elem.append(`'${value}'`);
    }

    /**
     * 位置情報の更新
     */
    _updateGPSInfo() {
        const networkClientId = this.model.getSelectedTxNetworkClientId();
        if (networkClientId.length > 0) {
            const gpsInfo = this.model.getGpsInfoData(networkClientId);
            if (gpsInfo !== undefined) {
                $('#tx_networkclient_detail_gps_latitude').text(gpsInfo.latitude);
                $('#tx_networkclient_detail_gps_longitude').text(gpsInfo.longitude);
            }
        }
    }

    /**
     * Recorderの制御機能状態の更新
     */
    _updateRecorderControlStatus(networkClientId) {
        const recorderControlStatus = this.model.getRecorderControlStatusData(networkClientId);
        if (recorderControlStatus !== undefined) {
            const elemRecorder = $(`#status_recorder_${networkClientId}`);
            const elemLowResolution = $(`#status_recorder_${networkClientId}_lowresolution`);
            const elemHighResolution = $(`#status_recorder_${networkClientId}_highresolution`);
            // 表示文字列
            const strLowResolution = recorderControlStatus.low_resolution === false ? 'False' : 'True';
            const strHighResolution = recorderControlStatus.high_resolution === false ? 'False' : 'True';
            elemLowResolution.text(`Low: ${strLowResolution}`);
            elemHighResolution.text(`High: ${strHighResolution}`);
            // 有効/無効
            if (recorderControlStatus.low_resolution === true) {
                elemLowResolution.removeClass('text-muted');
            }
            else {
                elemLowResolution.addClass('text-muted');
            }
            if (recorderControlStatus.high_resolution === true) {
                elemHighResolution.removeClass('text-muted');
            }
            else {
                elemHighResolution.addClass('text-muted');
            }
            if ((recorderControlStatus.low_resolution === true) ||
                (recorderControlStatus.high_resolution === true)) {
                elemRecorder.removeClass('text-muted');
                this._navDisableRecorderTab(false);
            }
            else {
                elemRecorder.addClass('text-muted');
                this._navDisableRecorderTab(true);
            }
        }
    }

    /**
     * RecorderのRec状態の更新
     */
    _updateClipRecorderStatus() {
        const networkClientId = this.model.getSelectedTxNetworkClientId();
        if (networkClientId.length > 0) {
            const statusList = this.model.getClipRecorderStatusData(networkClientId);
            if ((statusList !== undefined) && (statusList.result != undefined)) {
                for (const status of statusList.result) {
                    const rec = status.recorder.toLowerCase();
                    $(`#recorder_clip_${rec}`).val(status.status);
                }
            }
            else {
                $('#recorder_clip_lowresolution').val('INACTIVE');
                $('#recorder_clip_highresolution').val('INACTIVE');
            }
        }
    }

    /**
     * Recorderのクリップ保存メディアステータスの更新
     */
    _updateDriveStatus() {
        const networkClientId = this.model.getSelectedTxNetworkClientId();
        if (networkClientId.length > 0) {
            const driveStatusList = this.model.getDriveStatusData(networkClientId);
            if ((driveStatusList !== undefined) &&
                (driveStatusList.Media !== undefined)) {
                const selectDrive = $('#recorder_media').val();
                for (const driveStatus of driveStatusList.Media) {
                    if (driveStatus.id === selectDrive) {
                        $('#recorder_status').val(driveStatus.status);
                        $('#recorder_capacity').val(driveStatus.capacity);
                        $('#recorder_available').val(driveStatus.available);
                    }
                }
            }
            else {
                $('#recorder_status').val('-');
                $('#recorder_capacity').val('-');
                $('#recorder_available').val('-');
            }
        }
    }

    /**
     * Recordeのクリップ保存メディアステータス更新
     */
    _updateClipListMedia() {
        const networkClientId = this.model.getSelectedTxNetworkClientId();
        if (networkClientId.length > 0) {
            const driveStatusList = this.model.getDriveStatusData(networkClientId);
            if ((driveStatusList !== undefined) && (driveStatusList.Media)) {
                for (const driveStatus of driveStatusList.Media) {
                    const elemId = driveStatus.id.replace('.', '_');
                    const elem = $(`#cliplist_${elemId}`);
                    if (driveStatus.status !== 'None') {
                        elem.prop('disabled', false);
                    }
                    else {
                        elem.prop('disabled', true);
                    }
                }
            }
        }
    }

    _getCurrentPlanningMetadata() {
        const networkClientId = this.model.getSelectedTxNetworkClientId();
        const cmdData = {
            ID: networkClientId,
        };
        this.model.addExecutedCommandRequest(networkClientId, 'GetCurrentPlanningMetadata', cmdData);
        this.ccm.cmdGetCurrentPlanningMetadata(cmdData);
    }

    _getPlanningMetadata(target) {
        const networkClientId = this.model.getSelectedTxNetworkClientId();
        const cmdData = {
            ID: networkClientId,
            Name: target.cells[0].textContent,
            Uri: target.cells[1].textContent
        };
        this.model.addExecutedCommandRequest(networkClientId, 'GetPlanningMetadata', cmdData);
        this.ccm.cmdGetPlanningMetadata(cmdData);

    }

    _getPlanningMetadataList() {
        const networkClientId = this.model.getSelectedTxNetworkClientId();
        if (networkClientId.length > 0) {
            const statusPlanningMetadataControl = this.model.getPlanningMetadataControlStatus(networkClientId);
            if ((statusPlanningMetadataControl !== undefined) &&
                (statusPlanningMetadataControl.Status === true)) {
                const cmdData = {
                    ID: networkClientId,
                };
                this.model.addExecutedCommandRequest(networkClientId, 'GetPlanningMetadataList', cmdData);
                this.ccm.cmdGetPlanningMetadataList(cmdData);
                this._clearPlanningMetadataList();
                this._initializeRecorderInspectionViewInfo();
            }
        }
    }

    _clearPlanningMetadataList() {
        $('table#recorder_planmetalistview_table tbody *').remove();
    }

    _showPlanningMetadataList() {
        // 表示するPlanningMetadata一覧の取得
        const id = this.model.getSelectedTxNetworkClientId();
        const list = this.model.getPlanningMetadataList(id);

        // 表示中のすべての PlanningMetadata一覧を削除
        this._clearPlanningMetadataList();

        for (const metadata of list.PlanningMetadata) {
            this._addPlanningMetadata(metadata);
        }
    }

    _addPlanningMetadata(metadata) {
        const tbody = $('table#recorder_planmetalistview_table tbody');

        const elemTr = $('<tr>');
        // --- Name
        const elemTdName = $('<td>');
        elemTdName.append(metadata.name);
        elemTr.append(elemTdName);
        // --- Uri
        const elemTdUri = $('<td>', {
            class: 'd-none'
        });
        elemTdUri.append(metadata.uri);
        elemTr.append(elemTdUri);
        // --- Date
        const elemTdDate = $('<td>');
        elemTdDate.append(metadata.date);
        elemTr.append(elemTdDate);
        // --- Size
        const elemTdSize = $('<td>');
        elemTdSize.append(metadata.filesize);
        elemTr.append(elemTdSize);
        // Set
        const elemTdSet = $('<td>');
        const elemButtonSet = $('<input>', {
            type: 'button',
            id: `recorder_plameta_set_${metadata.name}`,
            class: 'btn btn-primary',
            value: 'セット'
        });
        elemButtonSet.on('click', { controller: this }, (e) => {
            const controller = e.data.controller;
            const name = e.target.id.replace('recorder_plameta_set_', '');
            controller._onRecorderSetPlanningMetadata(name);
        });
        elemTdSet.append(elemButtonSet);
        elemTr.append(elemTdSet);

        // Click Handler
        elemTr.on('click', { controller: this }, (e) => {
            // すべての行の選択状態を解除
            e.data.controller._selectionClearPlanningMetadata();
            e.currentTarget.classList.add('table-primary');
            console.log(e);

            e.data.controller._getPlanningMetadata(e.currentTarget);
        });
        tbody.append(elemTr);
    }

    _selectionClearPlanningMetadata() {
        // すべての行の選択状態を解除
        const rows = $('table#recorder_planmetalistview_table tbody tr');
        for (const row of rows) {
            row.classList.remove('table-primary');
        }
    }

    _updatePlanningMetadataControlStatus() {
        const networkClientId = this.model.getSelectedTxNetworkClientId();
        if (networkClientId.length > 0) {
            const statusPlanningMetadataControl = this.model.getPlanningMetadataControlStatus(networkClientId);
            if ((statusPlanningMetadataControl !== undefined) &&
                (statusPlanningMetadataControl.Status === true)) {
                $('#recorder_planning_metadata_title').removeClass('text-muted');
                $('#recorder_planning_metadata_detail').removeClass('text-muted');
                $('#recorder_planmeta_get_current_metadata').prop('disabled', false);
                $('#recorder_planmeta_register_button').prop('disabled', false);
                $('#recorder_planmeta_reload_button').prop('disabled', false);
            }
            else {
                $('#recorder_planning_metadata_title').addClass('text-muted');
                $('#recorder_planning_metadata_detail').addClass('text-muted');
                $('#recorder_planmeta_get_current_metadata').prop('disabled', true);
                $('#recorder_planmeta_register_button').prop('disabled', true);
                $('#recorder_planmeta_reload_button').prop('disabled', true);
            }
        }
        else {
            $('#recorder_planning_metadata_title').addClass('text-muted');
            $('#recorder_planning_metadata_detail').addClass('text-muted');
            $('#recorder_planmeta_get_current_metadata').prop('disabled', true);
            $('#recorder_planmeta_register_button').prop('disabled', true);
            $('#recorder_planmeta_reload_button').prop('disabled', true);
        }
    }

    _onRecorderSetPlanningMetadata(name) {
        const networkClientId = this.model.getSelectedTxNetworkClientId();
        if (networkClientId.length > 0) {
            const item = this.model.getPlanningMetadataInfo(networkClientId, name);
            if (item !== undefined) {
                const cmdData = {
                    ID: networkClientId,
                    Uri: item.uri,
                    Name: item.name
                };
                this.model.addExecutedCommandRequest(networkClientId, 'SetPlanningMetadata', cmdData);
                this.ccm.cmdSetPlanningMetadata(cmdData);
            }
        }
    }

    /**
     * Recorder制御のPlanningMetadata登録実行処理
     */
    _onClickPutPlametaViewSubmit() {
        const networkClientId = this.model.getSelectedTxNetworkClientId();
        if (networkClientId.length > 0) {
            const files = $('#recorder-plameta-putfile-form-file').prop('files');
            if (files.length > 0) {
                const fileName = files[0].name;
                const metaData = $('#recorder_plameta_putfile_form_detail').val();
                const cmdData = {
                    ID: networkClientId,
                    Name: fileName,
                    PlanningMetadata: metaData,
                    Override: false
                };
                this.model.addExecutedCommandRequest(networkClientId, 'PutPlanningMetadata', cmdData);
                this.ccm.cmdPutPlanningMetadata(cmdData);
                $('#recorder_plameta_put_view_close').addClass('d-none');
                $('#recorder_plameta_put_view_cancel').addClass('d-none');
                $('#recorder_plameta_put_view_close').prop('disabled', true);
                $('#recorder_plameta_put_view_cancel').prop('disabled', true);
                $('#recorder_plameta_put_view_submit_spinner').removeClass('d-none');
            }
        }
    }

    _onCancelRecorderPlametaRegisterfileForm() {
        $('#recorder_plameta_put_view_submit').prop('disabled', true);
        $('#recorder-plameta-putfile-form-file').val('');
        $('#recorder-plameta-putfile-form-file-label').html('ファイル選択...');
        $('#recorder_plameta_putfile_form_detail').val('');
        $('#recorder_plameta_put_view_form_result').collapse('hide');
    }

    /**
     * 登録するPlanningMetadataファイルを変更したときの処理
     * @param {Array} files ファイル一覧
     */
    _onChangePutPlanningMetadataFile(files) {
        if (files.length > 0) {
            // 選択可能なファイルは先頭の1つのみとする
            const file = files[0];

            // ファイル名の表示
            $('#recorder-plameta-putfile-form-file-label').html(file.name);

            // ファイルの中身の表示
            const reader = new FileReader();
            reader.onload = (res) => {
                $('#recorder_plameta_put_view_submit').prop('disabled', false);
                $('#recorder_plameta_putfile_form_detail').val(res.target.result);
            }
            reader.readAsText(file);
        }
        $('#recorder_plameta_put_view_form_result').collapse('hide');
    }
}

/**
 * HTML表示完了後の処理
 */
$(window).on('load', () => {
    const model = new CcmModel();
    model.initialize();

    const controller = new Controller(model);
    controller.initialize();
});
