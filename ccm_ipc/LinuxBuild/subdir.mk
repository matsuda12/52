################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../base64.cpp \
../ccm_gui.cpp \
../ccm_ipc.cpp \
../ccm_json_parser.cpp \
../command.cpp \
../setting.cpp 

OBJS += \
./base64.o \
./ccm_gui.o \
./ccm_ipc.o \
./ccm_json_parser.o \
./command.o \
./setting.o 

CPP_DEPS += \
./base64.d \
./ccm_gui.d \
./ccm_ipc.d \
./ccm_json_parser.d \
./command.d \
./setting.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++0x -I../../include -O2 -g -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


