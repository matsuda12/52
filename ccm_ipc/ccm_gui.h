/**
* @file ccm_cui.h
**/

#ifndef CCM_CUI_H_
#define CCM_CUI_H_

#include "../ccmsdk/connection_control_manager.h"
#include "../include/log_manager.h"
#include "ccm_ipc.h"

#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <stdio.h>

#include <sstream> // std::stringstream
#include <istream> // std::getline

#include <picojson.h>

#ifdef _WIN32
#include <Windows.h>
#include <locale.h>
#else
#include <unistd.h>
#endif

namespace ccm_cui {
  struct args {
    ccm::ConnectionControlManager *connection_control_manager = nullptr;
    std::string command;
    std::string rootpath;
    std::map<std::string, std::string> config;
    std::map<std::string, std::string> names;
    std::map<std::string, std::map<std::uint32_t, ccm::Upload>> job_list;
    std::map<std::string, std::vector<ccm::Clip>> clip_list;
  };

  static ccm::CcmIpc *ccmipc_ = nullptr;
  static ccm::ConnectionControlManager *connection_control_manager_ = nullptr;
  static std::string rootpath_;
  static std::string separator_ = "\\";
  static std::string  authUsername_;
  static std::string  authPassword_;
  static picojson::value validate_;
  static std::map<std::string, std::string> config_;
  static std::map<std::string, std::string> names_;

  static std::map<std::string, std::map<std::uint32_t, ccm::Upload>> job_list_;
  static std::map<std::string, std::vector<ccm::Clip>> clip_list_;
}

namespace ccm_setting {
  std::string readConfigFile(const char* path);
  bool getSystemConfigData(const std::string json, std::map < std::string, std::string >& config);
  bool getNamemapData(const std::string json, std::map<std::string, std::string>& names);
  std::map<std::string, picojson::object> getPropertyMap(const std::string PropartyName, const std::string json);
  void getFileTransferConfig(
    std::vector<ccm::UploadFiles> *add_files,
    ccm::UploadFiles *destination,
    const std::string config_name,
    ccm::UploadDestinationConfig *config,
    std::vector<std::string> *upload_cert,
    std::vector<std::string> *mcb_cert,
    const std::string json);

  ccm::ErrorCode parseFileTransferConfig(
    const std::string json,
    ccm::UploadDestinationConfig *config,
    std::vector<std::string> *upload_cert,
    std::vector<std::string> *mcb_cert
  );
  ccm::QosStreamingSendConfig getQosStreamingSendConfigFromFile(const std::string PropartyName, const std::string json);
  ccm::QosStreamingRecieveConfig getQosStreamingRecieveConfigFromFile(const std::string PropartyName, const std::string json);
}

namespace ccm_cui_commands {
  static std::map<std::string, std::function<void(const ccm_cui::args)>>		on_command_func_;	/// OnMessage(REQUEST)受信時に呼び出す関数マップ

  std::vector<std::string> commandArgs(std::string cmd);
  void GetClipList(ccm_cui::args);
  void GetMediaList(ccm_cui::args);
  void GetUploadList(ccm_cui::args);
  void RemoveUploadList(ccm_cui::args);
  void SuspendUpload(ccm_cui::args);
  void ResumeUpload(ccm_cui::args);
  void AddUpload(ccm_cui::args);
  void GetClient(ccm_cui::args);
  void GetList(ccm_cui::args);
  void GetProperty(ccm_cui::args);
  void SetIntercomProperty(ccm_cui::args);
  void SetProperty(ccm_cui::args);
  void StartVoIP(ccm_cui::args);
  void StopVoIP(ccm_cui::args);
  void StartStreaming(ccm_cui::args);
  void StopStreaming(ccm_cui::args);
  void StartStreamingRecieve(ccm_cui::args);
  void StartThumbnail(ccm_cui::args);
  void StopThumbnail(ccm_cui::args);
  void SetUploadServerCert(ccm_cui::args);
  void SetMCBServerCert(ccm_cui::args);
  void SetUploadDestinationConfig(ccm_cui::args);
  void GetPlanningMetadataList(ccm_cui::args);
  void GetPlanningMetadataDetail(ccm_cui::args);
  void GetCurrentPlanningMetadata(ccm_cui::args);
  void SetPlanningMetadata(ccm_cui::args);
  void PutPlanningMetadata(ccm_cui::args);
}

const void McbNetworkInfoHandler(const ccm::NetworkClientID id, const std::vector<ccm::McbNetworkInfo> infos);
const void RecieveThumbnail(const ccm::NetworkClientID id, const ccm::Thumbnail thumbnail);
const void StreamingDynamicPropertyHandler(const ccm::NetworkClientID id, const std::vector<ccm::StreamingDynamicProperty> params);
const void RecieveNetworkInfo(const ccm::NetworkClientID, const ccm::NetworkInfo);
const void RecieveGPSInfo(const ccm::NetworkClientID, const ccm::GPSInfo);
const void ReceiveRecorderControlStatus(const ccm::NetworkClientID id, const ccm::RecorderControlStatus result);
const void ReceiveClipRecorderStatus(const ccm::NetworkClientID id, const std::map<std::string, ccm::ClipRecorderStatus> result);
const void OnReceiveDriveStatus(const ccm::NetworkClientID id, const std::vector<ccm::Drive> result);
const void OnPlanningMetadataControlStatus(const ccm::NetworkClientID id, const bool status);


#endif // CCM_CUI_H_