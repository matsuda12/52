﻿#include "ccm_gui.h"
#include "ccm_json_parser.h"
#include "base64.h"


class JobList {
public:
	JobList() {};
	ccm::ErrorCode Add(std::string nc, ccm::Upload params) {
		try {
			std::map<std::uint32_t, ccm::Upload> list = job_list_.find(nc)->second;
			if (job_list_.find(nc) != job_list_.end()) {

			}
		}
		catch (...) {

		}
	};
	ccm::ErrorCode Change(std::string nc, ccm::Upload params) {

	};
	ccm::ErrorCode Progress(std::string nc, ccm::Upload params) {

	};
	ccm::ErrorCode Remove(std::string nc, std::uint32_t id) {

	};

private:
	std::map<std::string, std::map<std::uint32_t, ccm::Upload>> job_list_;

};

const bool DeviceRegistredHandler(const char* model_name, const char* serial_number) {
	CCM_LOG_INFO("CCMCUI", "Callback DeviceRegistredHandler [modelname:%s/serialnumber:%s]", model_name, serial_number);
	printf("\nCallback DeviceRegistred ID:%s_%s\nCCM> ", model_name, serial_number);
	return true;
};

const struct ccm::AuthenticationHandlerResult AuthenticationHandler(const char* username, const char* password) {
	CCM_LOG_INFO("CCMCUI", "Callback AuthenticationHandler [username:%s/password:%s]", username, password);
	printf("\nCallback Authentication USER:%s PASSWORD:%s\nCCM> ", username, password);
	const struct ccm::AuthenticationHandlerResult res = { 0,"" };
	return res;
};

/**
* Virtual Function VoIPStatusHandler()
* @param [in] char VoIP Status Code
*/
const void VoIPStatusHandler(const ccm::NetworkClientID id, const char* status) {
	std::string strResponse;
	CCM_LOG_INFO("CCMCUI", "Callback VoIPStatusHandler [NetworkClientID:%s/Status:%s]", id.GetID().c_str(), status);
	strResponse = ccm::CcmJsonParser::CreateVoIPStatusNotify(id, status);
	CCM_LOG_INFO("CCMCUI", "Callback VoIPStatusHandler [NetworkClientID:%s/Status:%s/Response:%s]", id.GetID().c_str(), status, strResponse.c_str());
	printf("\nCallback VoIPStatus ID:%s Status:%s\nCCM> ", id.GetID().c_str(), status);

	// For IPC
	if (ccm_cui::ccmipc_ != nullptr) {
		//printf("ccm::CcmJsonParser[%s]\n", strResponse.c_str());
		ccm_cui::ccmipc_->Send(strResponse);
	}
	return;
};

/**
* Virtual Function VoIPStatusHandler()
* @param [in] char VoIP Status Code
*/
const void StreamingStatusHandler(const ccm::NetworkClientID id, const ccm::StreamingSendStatus status) {
	std::string strResponse;
	switch (status) {
	case ccm::StreamingSendStatus::SEND_INACTIVE:
		CCM_LOG_INFO("CCMCUI", "Callback StreamingStatusHandler [NetworkClientID:%s/StreamingSendStatus:SEND_INACTIVE]", id.GetID().c_str());
		strResponse = ccm::CcmJsonParser::CreateStreamingStatusNotify(id, "INACTIVE");
		CCM_LOG_INFO("CCMCUI", "Callback StreamingStatusHandler [NetworkClientID:%s/StreamingSendStatus:SEND_INACTIVE/Response:%s]", id.GetID().c_str(), strResponse.c_str());
		printf("\nCallback StreamingStatus ID:%s Status:SEND_INACTIVE\nCCM> ", id.GetID().c_str());
		break;

	case ccm::StreamingSendStatus::SEND_IDLE:
		CCM_LOG_INFO("CCMCUI", "Callback StreamingStatusHandler [NetworkClientID:%s/StreamingSendStatus:SEND_IDLE]", id.GetID().c_str());
		strResponse = ccm::CcmJsonParser::CreateStreamingStatusNotify(id, "IDLE");
		CCM_LOG_INFO("CCMCUI", "Callback StreamingStatusHandler [NetworkClientID:%s/StreamingSendStatus:SEND_IDLE/Response:%s]", id.GetID().c_str(), strResponse.c_str());
		printf("\nCallback StreamingStatus ID:%s Status:SEND_IDLE\nCCM> ", id.GetID().c_str());
		break;

	case ccm::StreamingSendStatus::SEND_START:
		CCM_LOG_INFO("CCMCUI", "Callback StreamingStatusHandler [NetworkClientID:%s/StreamingSendStatus:SEND_START]", id.GetID().c_str());
		strResponse = ccm::CcmJsonParser::CreateStreamingStatusNotify(id, "START");
		CCM_LOG_INFO("CCMCUI", "Callback StreamingStatusHandler [NetworkClientID:%s/StreamingSendStatus:SEND_START/Response:%s]", id.GetID().c_str(), strResponse.c_str());
		printf("\nCallback StreamingStatus ID:%s Status:SEND_START\nCCM> ", id.GetID().c_str());
		break;

	default:
		CCM_LOG_INFO("CCMCUI", "Callback StreamingStatusHandler [NetworkClientID:%s/StreamingSendStatus:UNKNOWN]", id.GetID().c_str());
		strResponse = ccm::CcmJsonParser::CreateStreamingStatusNotify(id, "UNKNOWN");
		CCM_LOG_INFO("CCMCUI", "Callback StreamingStatusHandler [NetworkClientID:%s/StreamingSendStatus:UNKNOWN/Response:%s]", id.GetID().c_str(), strResponse.c_str());
		printf("\nCallback StreamingStatus ID:%s Status:UNKNOWN\nCCM> ", id.GetID().c_str());
		break;
	}

	// For IPC
	if (ccm_cui::ccmipc_ != nullptr) {
		//printf("ccm::CcmJsonParser[%s]\n", strResponse.c_str());
		ccm_cui::ccmipc_->Send(strResponse);
	}
	return;
};

/**
* Virtual Function ConnectHandler()
* @param [in] NetworkClientID
* @param [in] int ErrorCode
*/
const void ConnectHandler(const ccm::NetworkClientID id, const ccm::ErrorCode error_code) {
	CCM_LOG_INFO("CCMCUI", "Callback ConnectHandler [NetworkClientID:%s/ErrorCode:%s]", id.GetID().c_str(), ccm::GetErrorMessage(error_code).c_str());
	printf("\nCallback Connect ID:%s Message:%s\nCCM> ", id.GetID().c_str(), ccm::GetErrorMessage(error_code).c_str());

	//std::vector<ccm::shared_ptr<ccm::NetworkClient>> clients = ccm_cui::connection_control_manager_->GetNetworkClientList();
	std::string strResponse;
	if (error_code == ccm::CCM_OK) {
		strResponse = ccm::CcmJsonParser::CreateNetworkClientConnectNotify(id, true);
	}
	else {
		strResponse = ccm::CcmJsonParser::CreateNetworkClientConnectNotify(id, false);
	}
	CCM_LOG_INFO("CCMCUI", "Callback ConnectHandler [NetworkClientID:%s/ErrorCode:%s/Response:%s]", id.GetID().c_str(), ccm::GetErrorMessage(error_code).c_str(), strResponse.c_str());

	// For IPC
	if (ccm_cui::ccmipc_ != nullptr) {
		//printf("ccm::CcmJsonParser[%s]\n", strResponse.c_str());
		ccm_cui::ccmipc_->Send(strResponse);
	}
	return;
};

/**
* Virtual Function StreamingPropertyHandler()
* @param [in] NetworkClientID
* @param [in] StreamingDynamicProperty
*/
const void StreamingDynamicPropertyHandler(const ccm::NetworkClientID id, const std::vector<ccm::StreamingDynamicProperty> params) {
	CCM_LOG_INFO("CCMCUI", "Callback StreamingDynamicPropertyHandler [NetworkClientID:%s]", id.GetID().c_str());

	for (int i = 0; i < std::end(params) - std::begin(params); ++i) {
		printf("\nCallback StreamingDynamicProperty ID:%s Message:%s %d %d %d %d",
			id.GetID().c_str(),
			params[i].nic_name.c_str(),
			params[i].rate,
			params[i].loss_rate,
			params[i].rtt,
			params[i].delay
		);
		CCM_LOG_INFO("CCMCUI", "Callback StreamingDynamicPropertyHandler [Message:%s %d %d %d %d]",
			params[i].nic_name.c_str(),
			params[i].rate,
			params[i].loss_rate,
			params[i].rtt,
			params[i].delay
		);
	}
	printf("\nCCM> ");

	std::string strResponse;
	strResponse = ccm::CcmJsonParser::CreateStreamingDynamicPropertyNotify(id, params);
	CCM_LOG_INFO("CCMCUI", "Callback StreamingDynamicPropertyHandler [NetworkClientID:%s/Response:%s]", id.GetID().c_str(), strResponse.c_str());

	// For IPC
	if (ccm_cui::ccmipc_ != nullptr) {
		//printf("ccm::CcmJsonParser[%s]\n", strResponse.c_str());
		ccm_cui::ccmipc_->Send(strResponse);
	}

	return;
};

/*!
*  Callback Set Upload Server Cert Handler
*/
const void SetUploadServerCertHandler(const ccm::NetworkClientID id, const std::vector<ccm::AsyncResult> results) {
	CCM_LOG_INFO("CCMCUI", "Callback SetUploadServerCertHandler [NetworkClientID:%s]", id.GetID().c_str());
	std::string res_str = "Callback SetUploadServerCertHandler [NetworkClientID:" + id.GetID();
	printf("\nCallback SetUploadServerCertHandler [ID:%s", id.GetID().c_str());

	for (ccm::AsyncResult result : results) {
		res_str += "/ID:" + std::to_string(result.procces_id) + ",ErrorMessage:" + ccm::GetErrorMessage(result.error_code);
		printf("/ID:%d Message:%s", result.procces_id, ccm::GetErrorMessage(result.error_code).c_str());
	}
	printf("]");
	res_str += "]";
	CCM_LOG_INFO("CCMCUI", "Callback SetUploadServerCertHandler [res_str:%s]", res_str.c_str());
};

/*!
*  Callback Set Upload Server Cert Handler
*/
const void SetDefaultUploadServerConfigHandler(const ccm::NetworkClientID id, const std::vector<ccm::AsyncResult> results) {
	CCM_LOG_INFO("CCMCUI", "Callback SetDefaultUploadServerConfigHandler [NetworkClientID:%s]", id.GetID().c_str());
	std::string res_str = "Callback SetDefaultUploadServerConfigHandler [NetworkClientID:" + id.GetID();
	for (ccm::AsyncResult result : results) {
		res_str += "/ID:" + std::to_string(result.procces_id) + ",ErrorMessage:" + ccm::GetErrorMessage(result.error_code);
		printf("\nCallback SetDefaultUploadServerConfig ID:%s ProccesID:%d Message:%s", id.GetID().c_str(), result.procces_id, ccm::GetErrorMessage(result.error_code).c_str());
	}
	res_str += "]";
	CCM_LOG_INFO("CCMCUI", "Callback SetDefaultUploadServerConfigHandler [res_str:%s]", res_str.c_str());
};

/*!
*  Callback Set Mcb Server Cert Handler
*/
const void SetMcbServerCertHandler(const ccm::NetworkClientID id, const std::vector<ccm::AsyncResult> results) {
	CCM_LOG_INFO("CCMCUI", "Callback SetMcbServerCertHandler [NetworkClientID:%s]", id.GetID().c_str());
	std::string res_str = "Callback SetMcbServerCertHandler [NetworkClientID:" + id.GetID();
	printf("\nCallback SetMcbServerCert [ID:%s", id.GetID().c_str());
	for (ccm::AsyncResult result : results) {
		res_str += "/ID:" + std::to_string(result.procces_id) + ",ErrorMessage:" + ccm::GetErrorMessage(result.error_code);
		printf("/ID:%d Message:%s", result.procces_id, ccm::GetErrorMessage(result.error_code).c_str());
	}
	printf("]");
	res_str += "]";
	CCM_LOG_INFO("CCMCUI", "Callback SetMcbServerCertHandler [res_str:%s]", res_str.c_str());
};

/*!
*  Callback Add Upload Files Handler
*/
const void AddUploadFilesHandler(const ccm::NetworkClientID id, const std::vector<ccm::AsyncResult> results) {
	try {
		CCM_LOG_INFO("CCMCUI", "Callback AddUploadFilesHandler [NetworkClientID:%s]", id.GetID().c_str());

		std::string res_str = "Callback AddUploadFilesHandler [NetworkClientID:" + id.GetID();
		printf("\nCallback AddUploadFiles ID:%s", id.GetID().c_str());
		for (ccm::AsyncResult result : results) {
			res_str += "/" + std::to_string(result.procces_id) + "" + ccm::GetErrorMessage(result.error_code);
			printf("\n  -> JobID:%d Message:%s", result.procces_id, ccm::GetErrorMessage(result.error_code).c_str());
		}
		printf("\nCCM> ");
		res_str += "]";
		CCM_LOG_INFO("CCMCUI", "%s", res_str.c_str());

		// For IPC
		if (ccm_cui::ccmipc_ != nullptr) {
			CCM_LOG_INFO("CCMCUI", "%s", "Callback AddUploadFilesHandler CreateAddUploadFilesResult");
			std::string strResponse = ccm::CcmJsonParser::CreateAddUploadFilesResult(id, results);
			//printf("ccm::CcmJsonParser[%s]\n", strResponse.c_str());
			CCM_LOG_INFO("CCMCUI", "Callback AddUploadFilesHandler response:%s", strResponse.c_str());
			ccm_cui::ccmipc_->Send(strResponse);
		}
		CCM_LOG_INFO("CCMCUI", "%s", "Callback AddUploadFilesHandler end");
	}
	catch (...) {
		CCM_LOG_INFO("CCMCUI", "%s", "Callback AddUploadFilesHandler Exception\n");

	}
};

/**
* Callback Function OnRemoveUploadListHandler()
* @param [in] NetworkClientID
* @param [in] int32 process id
* @param [in] int32 error code
*/
const void RemoveUploadList(const ccm::NetworkClientID id, const std::vector<ccm::AsyncResult> results) {
	CCM_LOG_INFO("CCMCUI", "Callback RemoveUploadList [NetworkClientID:%s]", id.GetID().c_str());
	std::string res_str = "Callback RemoveUploadList [NetworkClientID:" + id.GetID();
	printf("\nCallback RemoveUploadList ID:%s", id.GetID().c_str());
	for (ccm::AsyncResult result : results) {
		res_str += "/" + std::to_string(result.procces_id) + "" + ccm::GetErrorMessage(result.error_code);
		printf("\n  -> JobID:%d Message:%s", result.procces_id, ccm::GetErrorMessage(result.error_code).c_str());
	}
	printf("\nCCM> ");
	res_str += "]";
	CCM_LOG_INFO("CCMCUI", "Callback RemoveUploadList [res_str:%s]", res_str.c_str());
	return;
};

/**
* Callback Function OnRemoveUploadListHandler()
* @param [in] NetworkClientID
* @param [in] int32 process id
* @param [in] int32 error code
*/
const void SuspendUploadList(const ccm::NetworkClientID id, const std::vector<ccm::AsyncResult> results) {
	CCM_LOG_INFO("CCMCUI", "Callback SuspendUploadList [NetworkClientID:%s]", id.GetID().c_str());
	std::string res_str = "Callback SuspendUploadList [NetworkClientID:" + id.GetID();
	printf("\nCallback SuspendUploadList ID:%s", id.GetID().c_str());
	for (ccm::AsyncResult result : results) {
		res_str += "/" + std::to_string(result.procces_id) + "" + ccm::GetErrorMessage(result.error_code);
		printf("\n  -> JobID:%d Message:%s", result.procces_id, ccm::GetErrorMessage(result.error_code).c_str());
	}
	printf("\nCCM> ");
	res_str += "]";
	CCM_LOG_INFO("CCMCUI", "Callback SuspendUploadList [res_str:%s]", res_str.c_str());
	return;
};

/**
* Callback Function OnRemoveUploadListHandler()
* @param [in] NetworkClientID
* @param [in] int32 process id
* @param [in] int32 error code
*/
const void ResumeUploadList(const ccm::NetworkClientID id, const std::vector<ccm::AsyncResult> results) {
	CCM_LOG_INFO("CCMCUI", "Callback ResumeUploadList [NetworkClientID:%s]", id.GetID().c_str());
	std::string res_str = "Callback ResumeUploadList [NetworkClientID:" + id.GetID();
	printf("\nCallback ResumeUploadList ID:%s", id.GetID().c_str());
	for (ccm::AsyncResult result : results) {
		res_str += "/" + std::to_string(result.procces_id) + "" + ccm::GetErrorMessage(result.error_code);
		printf("\n  -> JobID:%d Message:%s", result.procces_id, ccm::GetErrorMessage(result.error_code).c_str());
	}
	printf("\nCCM> ");
	res_str += "]";
	CCM_LOG_INFO("CCMCUI", "Callback ResumeUploadList [res_str:%s]", res_str.c_str());
	return;
};

/*!
*  Callback Mcb Network Info Handler
*/
const void McbNetworkInfoHandler(const ccm::NetworkClientID id, const std::vector<ccm::McbNetworkInfo> infos) {
	CCM_LOG_INFO("CCMCUI", "Callback McbNetworkInfoHandler [NetworkClientID:%s]", id.GetID().c_str());
	//printf("\nCallback McbNetworkInfoHandler ID:%s", id.GetID().c_str());
	std::string log;
	for (ccm::McbNetworkInfo info : infos) {

		log += (log.length() > 0 ? "|\"" : "\"") + info.id + "\"" + " " + std::to_string((int)info.rtt) + " " + std::to_string((int)info.rate) + " " + std::to_string((double)info.loss);

		//std::_Floating_to_string("%.3f", (double)info.loss);
		//std::to_string((int)info.rate)
		//printf("\nCallback OnMcbNetworkInfoHandler %s %d %d %lf", info.id.c_str(), (int)info.rtt, (int)info.rate, (double)info.loss);
	}
	CCM_LOG_INFO("CCMCUI", "Callback McbNetworkInfoHandler [NetworkClientID:%s/params:%s]", id.GetID().c_str(), log.c_str());
	//CCM_LOG_INFO(id.GetID() + "_NetworkInfo", "%s", log.c_str());

	// For IPC
	if (ccm_cui::ccmipc_ != nullptr) {
		std::string strResponse = ccm::CcmJsonParser::CreateMcbNetworkInfoNotify(id, infos);
		CCM_LOG_INFO("CCMCUI", "Callback McbNetworkInfoHandler [NetworkClientID:%s/Response:%s]", id.GetID().c_str(), strResponse.c_str());
		//printf("ccm::CcmJsonParser[%s]\n", strResponse.c_str());
		ccm_cui::ccmipc_->Send(strResponse);
	}
};


/**
* Callback Function OnAddedUpload
* @param [in] NetworkClientID
* @param [in] Upload[]
*/
const void AddedUpload(const ccm::NetworkClientID id, const std::vector<ccm::Upload> results) {
	CCM_LOG_INFO("CCMCUI", "Callback AddedUpload [NetworkClientID:%s]", id.GetID().c_str());
	std::map<std::uint32_t, ccm::Upload> job_list;
	if (ccm_cui::job_list_.find(id.GetID()) != ccm_cui::job_list_.end()) {
		job_list = ccm_cui::job_list_.find(id.GetID())->second;
	}
	else {
		ccm_cui::job_list_.insert(std::make_pair(id.GetID(), job_list));
	}
	std::string res_str = "Callback AddedUpload [NetworkClientID:" + id.GetID() + "/Upload:";
	std::uint32_t count = 0;
	bool first_param = true;
	printf("Added Upload: ");
	for (ccm::Upload result : results) {
		if (!first_param) {
			res_str += ",";
			printf(", ");
		}
		res_str += std::to_string(result.id);
		printf("%d", result.id);

		//printf("\nCallback SDK OnAddedUpload\n");

		/*printf("[%d] id                        :%d\n", count, result.id);
		printf("[%d] setting_id                :%d\n", count, result.setting_id);
		printf("[%d] setting_name              :%s\n", count, result.setting_name.c_str());
		printf("[%d] clip_name                 :%s\n", count, result.clip_name.c_str());
		printf("[%d] files                     :Size:%d\n", count, result.files.size());
		for (std::string file_name : result.files) {
		printf("  %s\n", file_name.c_str());
		}

		printf("[%d] drive                     :%s\n", count, result.drive.c_str());
		printf("[%d] total                     :%d\n", count, result.total);
		printf("[%d] status                    :%s\n", count, result.status.c_str());
		printf("[%d] percentage                :%d\n", count, result.percentage);
		printf("[%d] transferred               :%d\n", count, result.transferred);
		printf("[%d] destination.type          :%s\n", count, result.destination.type.c_str());
		printf("[%d] destination.disp_name     :%s\n", count, result.destination.disp_name.c_str());
		printf("[%d] destination.ftp_host_name :%s\n", count, result.destination.ftp_host_name.c_str());
		printf("[%d] destination.ftp_port      :%d\n", count, result.destination.ftp_port);
		printf("[%d] destination.ftp_user_name :%s\n", count, result.destination.ftp_user_name.c_str());
		printf("[%d] destination.ftp_password  :%s\n", count, result.destination.ftp_password.c_str());
		printf("[%d] destination.ftp_use_ftpes :%d\n", count, result.destination.ftp_use_ftpes);
		printf("[%d] destination.ftp_use_pasv  :%d\n", count, result.destination.ftp_use_pasv);
		printf("[%d] destination.ftp_check_cert:%d\n", count, result.destination.ftp_check_cert);
		printf("[%d] destination.ftp_upload_dir:%s\n", count, result.destination.ftp_upload_dir.c_str());
		printf("[%d] destination.ftp_use_mcb   :%d\n", count, result.destination.ftp_use_mcb);
		printf("[%d] destination.mcb_host_name :%s\n", count, result.destination.mcb_host_name.c_str());
		printf("[%d] destination.mcb_port      :%d\n", count, result.destination.mcb_port);
		printf("[%d] destination.mcb_user_name :%s\n", count, result.destination.mcb_user_name.c_str());
		printf("[%d] destination.mcb_password  :%s\n", count, result.destination.mcb_password.c_str());
		*/



		if (job_list.find(result.id) != job_list.end()) {
			job_list.find(result.id)->second = result;
			//CCM_LOG_INFO("CCMCUI", "AddedUpload arleady inserted. Added:%s/%ld", id.GetID().c_str(), result.id);
			CCM_LOG_INFO("CCMCUI", "UploadList Update: NC:%s ID:%d Status:%d Percentage:%d Transferred:%d", id.GetID().c_str(), result.id, result.status, result.percentage, result.transferred);
		}
		else {
			job_list.insert(std::make_pair(result.id, result));
			CCM_LOG_INFO("CCMCUI", "UploadList Insert: NC:%s ID:%d Status:%d Percentage:%d Transferred:%d", id.GetID().c_str(), result.id, result.status, result.percentage, result.transferred);
			//CCM_LOG_INFO("CCMCUI", "AddedUpload Added:%s/%ld", id.GetID().c_str(), result.id);
		}

		count++;
		if (first_param) first_param = false;
	}
	CCM_LOG_INFO("CCMCUI", "Callback AddedUpload [res_str:%s]", res_str.c_str());
	printf("\n");

	try {
		ccm_cui::job_list_.find(id.GetID())->second = job_list;
	}
	catch (...) {

	}
	return;
}

/**
* Callback Function OnAddedUpload
* @param [in] NetworkClientID
* @param [in] UploadProgress[]
*/
const void ChangedUpload(const ccm::NetworkClientID id, const std::vector<ccm::UploadChange> results) {
	CCM_LOG_INFO("CCMCUI", "Callback ChangedUpload [NetworkClientID:%s]", id.GetID().c_str());
	std::string res_str = "Callback ChangedUpload [NetworkClientID:" + id.GetID();
	std::map<std::uint32_t, ccm::Upload> job_list;
	if (ccm_cui::job_list_.find(id.GetID()) != ccm_cui::job_list_.end()) {
		job_list = ccm_cui::job_list_.find(id.GetID())->second;
	}
	else {
		ccm_cui::job_list_.insert(std::make_pair(id.GetID(), job_list));
	}

	for (ccm::UploadChange result : results) {
		res_str += "/ID:" + std::to_string(result.id) + "," + result.display_name + "," + std::to_string(result.status) + "," + std::to_string(result.total);
		printf("Changed Upload: %d: %s status %d, total %lld\n", result.id, result.display_name.c_str(), result.status, result.total);

		if (job_list.find(result.id) != job_list.end()) {
			job_list.find(result.id)->second.status = result.status;
			job_list.find(result.id)->second.total = result.total;
			job_list.find(result.id)->second.destination.display_name = result.display_name;
			//CCM_LOG_INFO("CCMCUI", "ChangedUpload Changed:%s/%ld", id.GetID().c_str(), result.id);
			CCM_LOG_INFO("CCMCUI", "UploadList Update: NC:%s ID:%d Status:%d Total:%d DisplayName:%s", id.GetID().c_str(), result.id, result.status, result.total, result.display_name.c_str());
		}
		else {
			ccm::Upload tmp_upload;
			tmp_upload.status = result.status;
			tmp_upload.total = result.total;
			tmp_upload.destination.display_name = result.display_name;
			job_list.insert(std::make_pair(result.id, tmp_upload));
			//CCM_LOG_INFO("CCMCUI", "ChangedUpload Not Found. Changed:%s/%ld", id.GetID().c_str(), result.id);
			CCM_LOG_INFO("CCMCUI", "UploadList Add: NC:%s ID:%d Status:%d Total:%d DisplayName:%s", id.GetID().c_str(), result.id, result.status, result.total, result.display_name.c_str());
		}
	}

	try {
		ccm_cui::job_list_.find(id.GetID())->second = job_list;
	}
	catch (...) {

	}
	res_str += "]";
	CCM_LOG_INFO("CCMCUI", "Callback ChangedUpload [res_str:%s]", res_str.c_str());
	return;
}

/**
* Callback Function OnAddedUpload
* @param [in] NetworkClientID
* @param [in] UploadProgress[]
*/
const void ProgressedUpload(const ccm::NetworkClientID id, const std::vector<ccm::UploadProgress> results) {
	CCM_LOG_INFO("CCMCUI", "Callback ProgressedUpload [NetworkClientID:%s]", id.GetID().c_str());

	std::map<std::uint32_t, ccm::Upload> job_list;
	if (ccm_cui::job_list_.find(id.GetID()) != ccm_cui::job_list_.end()) {
		job_list = ccm_cui::job_list_.find(id.GetID())->second;
	}
	else {
		ccm_cui::job_list_.insert(std::make_pair(id.GetID(), job_list));
	}

	for (ccm::UploadProgress result : results) {
		printf("Progressed Upload: %d: transferring %d, %d\n", result.id, result.percentage, result.transferred);
		if (job_list.find(result.id) != job_list.end()) {
			job_list.find(result.id)->second.percentage = result.percentage;
			job_list.find(result.id)->second.transferred = result.transferred;
			//CCM_LOG_INFO("CCMCUI", "ProgressedUpload Progressed:%s/%ld", id.GetID().c_str(), result.id);
			CCM_LOG_INFO("CCMCUI", "UploadList Update: NC:%s ID:%d Percentage:%d Transferred:%d", id.GetID().c_str(), result.id, result.percentage, result.transferred);
		}
		else {
			ccm::Upload tmp_upload;
			tmp_upload.percentage = result.percentage;
			tmp_upload.transferred = result.transferred;
			job_list.insert(std::make_pair(result.id, tmp_upload));
			//CCM_LOG_INFO("CCMCUI", "ProgressedUpload Not Found. Progressed:%s", id.GetID().c_str(), result.id);
			CCM_LOG_INFO("CCMCUI", "UploadList Add: NC:%s ID:%d Percentage:%d Transferred:%d", id.GetID().c_str(), result.id, result.percentage, result.transferred);
		}
	}

	try {
		ccm_cui::job_list_.find(id.GetID())->second = job_list;
	}
	catch (...) {

	}
	return;
}

/**
* Callback Function OnAddedUpload
* @param [in] NetworkClientID
* @param [in] int32[]
*/
const void RemovedUpload(const ccm::NetworkClientID id, const std::vector<std::uint32_t> results) {
	CCM_LOG_INFO("CCMCUI", "Callback RemovedUpload [NetworkClientID:%s]", id.GetID().c_str());
	bool first_param = true;
	printf("Deleted Upload: ");


	std::map<std::uint32_t, ccm::Upload> job_list;
	if (ccm_cui::job_list_.find(id.GetID()) != ccm_cui::job_list_.end()) {
		job_list = ccm_cui::job_list_.find(id.GetID())->second;
	}
	else {
		ccm_cui::job_list_.insert(std::make_pair(id.GetID(), job_list));
	}

	for (std::uint32_t result : results) {
		//printf("\nCallback SDK OnRemovedUpload ID:%d", result);
		if (!first_param) printf(", ");
		printf("%d", result);
		if (first_param) first_param = false;

		if (job_list.find(result) != job_list.end()) {
			job_list.erase(result);
			//CCM_LOG_INFO("CCMCUI", "RemovedUpload Removed:%s/%ld", id.GetID().c_str(), result);
			CCM_LOG_INFO("CCMCUI", "UploadList Remove: NC:%s ID:%d", id.GetID().c_str(), result);
		}
		else {
			CCM_LOG_INFO("CCMCUI", "UploadList arleady Remove: NC:%s ID:%d", id.GetID().c_str(), result);
		}
	}
	printf("\n");

	try {
		ccm_cui::job_list_.find(id.GetID())->second = job_list;
	}
	catch (...) {

	}
	return;
}

/**
* Callback Function OnGetClipList
* @param [in] NetworkClientID
* @param [in] Clip[]
*/
const void GetClipList(const ccm::NetworkClientID id, const std::vector<ccm::Clip> results) {
	CCM_LOG_INFO("CCMCUI", "Callback GetClipList [NetworkClientID:%s]", id.GetID().c_str());

	if (ccm_cui::clip_list_.find(id.GetID()) != ccm_cui::clip_list_.end()) {
		ccm_cui::clip_list_.find(id.GetID())->second = results;
	}
	else {
		ccm_cui::clip_list_.insert(std::make_pair(id.GetID(), results));
	}



	//printf("\nCallback GetClipList Result Size:%d", results.size());
	// bool first_param = true;

	int count = 0;
	for (ccm::Clip result : results) {
		printf("%d: Clip List URI:%s\n", count + 1, result.uri.c_str());
		/*
		printf("\n [%d] -> uri             :%s", count, result.uri.c_str());
		printf("\n [%d] -> type            :%s", count, result.type.c_str());
		printf("\n [%d] -> status          :%s", count, result.status.c_str());
		printf("\n [%d] -> duration        :%d", count, result.duration);
		printf("\n [%d] -> offset          :%d", count, result.offset);
		printf("\n [%d] -> frame_rate      :%s", count, result.frame_rate.c_str());
		printf("\n [%d] -> aspect_ratio    :%s", count, result.aspect_ratio.c_str());
		printf("\n [%d] -> video_type      :%s", count, result.video_type.c_str());
		printf("\n [%d] -> audio_type      :%s", count, result.audio_type.c_str());
		printf("\n [%d] -> aduio_channel   :%d", count, result.aduio_channel);
		printf("\n [%d] -> nrt_metadata_uri:%s", count, result.nrt_metadata_uri.c_str());
		printf("\n [%d] -> thumbnail_uri   :%s", count, result.thumbnail_uri.c_str());
		printf("\n [%d] -> partial_upload  :%s", count, (result.is_partial_upload_supported?"TRUE":"FALSE"));
		printf("\n");
		*/
		count++;
	}




	// For IPC
	if (ccm_cui::ccmipc_ != nullptr) {
		std::string strResponse = ccm::CcmJsonParser::CreateGetClipListResponse(id, results);
		CCM_LOG_INFO("CCMCUI", "Callback GetClipList [NetworkClientID:%s/Response:%s]", id.GetID().c_str(), strResponse.c_str());
		//printf("ccm::CcmJsonParser[%s]\n", strResponse.c_str());
		ccm_cui::ccmipc_->Send(strResponse);
	}
	return;
}

/**
* Callback Function GetUploadList
* @param [in] NetworkClientID
* @param [in] Upload[]
*/
const void GetUploadList(const ccm::NetworkClientID id, const std::vector<ccm::Upload> results) {
	CCM_LOG_INFO("CCMCUI", "Callback GetUploadList [NetworkClientID:%s]", id.GetID().c_str());
	//printf("\nCallback GetUploadList Result Size:%d", results.size());
	// bool first_param = true;
	std::map<std::uint32_t, ccm::Upload> job_list;
	if (ccm_cui::job_list_.find(id.GetID()) != ccm_cui::job_list_.end()) {
		job_list = ccm_cui::job_list_.find(id.GetID())->second;
	}
	else {
		ccm_cui::job_list_.insert(std::make_pair(id.GetID(), job_list));
	}

	int count = 0;
	for (ccm::Upload result : results) {
		printf("Recieved Upload List ID:%d Status:%d Percentage:%d Transferred:%lld\n", result.id, result.status, result.percentage, result.transferred);
		//CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] Params Not Found", cmd.c_str());
		if (job_list.find(result.id) != job_list.end()) {
			job_list.find(result.id)->second = result;
			CCM_LOG_INFO("CCMCUI", "UploadList Update: NC:%s ID:%d Status:%d Percentage:%d Transferred:%d", id.GetID().c_str(), result.id, result.status, result.percentage, result.transferred);
		}
		else {
			job_list.insert(std::make_pair(result.id, result));
			CCM_LOG_INFO("CCMCUI", "UploadList Insert: NC:%s ID:%d Status:%d Percentage:%d Transferred:%d", id.GetID().c_str(), result.id, result.status, result.percentage, result.transferred);
		}
		count++;
	}

	if (ccm_cui::job_list_.find(id.GetID()) != ccm_cui::job_list_.end()) {
		ccm_cui::job_list_.find(id.GetID())->second = job_list;
	}
	else {
		ccm_cui::job_list_.insert(std::make_pair(id.GetID(), job_list));
	}

	// For IPC
	if (ccm_cui::ccmipc_ != nullptr) {
		std::string strResponse = ccm::CcmJsonParser::CreateGetUploadListResponse(id, results);
		CCM_LOG_INFO("CCMCUI", "Callback GetUploadList [NetworkClientID:%s/Response:%s]", id.GetID().c_str(), strResponse.c_str());
		//printf("ccm::CcmJsonParser[%s]\n", strResponse.c_str());
		ccm_cui::ccmipc_->Send(strResponse);
	}
	return;
}

/**
* Callback Function StartQosStreamingSendRequest
* @param [in] NetworkClientID
*/
const ccm::QosStreamingSendConfig StartQosStreamingSendRequest(const ccm::NetworkClientID id) {
	CCM_LOG_INFO("CCMCUI", "Callback StartQosStreamingSendRequest [NetworkClientID:%s]", id.GetID().c_str());
	ccm::QosStreamingSendConfig streaming_params;

	std::string fpath = ccm_cui::rootpath_.substr(0, ccm_cui::rootpath_.find_last_of(ccm_cui::separator_) + 1);
	fpath += "streaming.cfg";

	std::string name;
	if (!ccm_cui::names_[id.GetID().c_str()].empty()) {
		name = ccm_cui::names_[id.GetID().c_str()];
	}
	else {
		name = id.GetID().c_str();
	}

	std::string configStr = ccm_setting::readConfigFile(fpath.c_str());

	streaming_params = ccm_setting::getQosStreamingSendConfigFromFile(name.c_str(), configStr);

	return streaming_params;
}

const void RecieveThumbnail(const ccm::NetworkClientID id, const ccm::Thumbnail thumbnail) {
	CCM_LOG_INFO("CCMCUI", "Callback RecieveThumbnail [NetworkClientID:%s ErrorCode:%d]", id.GetID().c_str(), thumbnail.error_code);
	printf("Recieved Thumbnail ErrorCode:%d\n", thumbnail.error_code);
	//	printf("Recieved Thumbnail:%s ErrorCode:%d\n", thumbnail.thumbnail.c_str(), thumbnail.error_code);
	//	CCM_LOG_INFO("CCMCUI", "Recieved Thumbnail: NC:%s Thumbnail:%s ErrorCode:%d", id.GetID().c_str(), thumbnail.thumbnail.c_str(), thumbnail.error_code);

		// For IPC
	if (ccm_cui::ccmipc_ != nullptr) {
		if (thumbnail.thumbnail.size() > 0) {
			std::string base64enc = ccm::Base64::base64_encode((unsigned char*)thumbnail.thumbnail.data(), (unsigned int)thumbnail.thumbnail.size());
			std::string strResponse = ccm::CcmJsonParser::CreateRecieveThumbnailNotify(id, thumbnail, "image/jpeg", base64enc.c_str());
			CCM_LOG_INFO("CCMCUI", "Callback RecieveThumbnail [NetworkClientID:%s/ErrorCode:%d/Response:%s]", id.GetID().c_str(), thumbnail.error_code, strResponse.c_str());
			ccm_cui::ccmipc_->Send(strResponse);
		}
	}
}

const void RecieveNetworkInfo(const ccm::NetworkClientID id, const ccm::NetworkInfo networkInfo) {
	CCM_LOG_INFO("CCMCUI", "Callback RecieveNetworkInfo [NetworkClientID:%s]", id.GetID().c_str());
	printf("Recieved NetworkInfo: ID = %s\n", id.GetID().c_str());

	//printf("RecieveNetworkInfo\n");

	/*for (auto cellular_info : result.cellular_info) {
	printf("\n");
	printf("   GetSignalStrength     : %s\n", cellular_info->GetSignalStrength()->c_str());
	printf("   GetSignalStrengthUnit : %s\n", cellular_info->GetSignalStrengthUnit()->c_str());
	printf("   GetOperator           : %s\n", cellular_info->GetOperator()->c_str());
	printf("   GetPLMN               : %s\n", cellular_info->GetPLMN()->c_str());
	printf("   GetBand               : %s\n", cellular_info->GetBand()->c_str());
	printf("   GetCellID             : %s\n", cellular_info->GetCellID()->c_str());
	printf("   GetAirInterface       : %s\n", cellular_info->GetAirInterface()->c_str());
	printf("   GetManufacture        : %s\n", cellular_info->GetManufacture()->c_str());
	printf("   GetModelName          : %s\n", cellular_info->GetModelName()->c_str());
	printf("   GetIMEI               : %s\n", cellular_info->GetIMEI()->c_str());
	}
	for (auto qos_info : result.qos_interface_type) {
	printf("   QoS Interface         : %s %s\n", qos_info.first.c_str(), qos_info.second.c_str());
	}*/


	// For IPC
	if (ccm_cui::ccmipc_ != nullptr) {
		std::string strResponse = ccm::CcmJsonParser::CreateNetworkInfoNotify(id, networkInfo);
		CCM_LOG_INFO("CCMCUI", "Callback RecieveNetworkInfo [NetworkClientID:%s/Response:%s]", id.GetID().c_str(), strResponse.c_str());
		ccm_cui::ccmipc_->Send(strResponse);
	}
}
const void RecieveGPSInfo(const ccm::NetworkClientID id, const ccm::GPSInfo gpsInfo) {
	CCM_LOG_INFO("CCMCUI", "Callback RecieveGPSInfo [NetworkClientID:%s]", id.GetID().c_str());
	printf("Recieved GPSInfo: ID = %s\n", id.GetID().c_str());

	// For IPC
	if (ccm_cui::ccmipc_ != nullptr) {
		std::string strResponse = ccm::CcmJsonParser::CreateGPSInfoNotify(id, gpsInfo);
		CCM_LOG_INFO("CCMCUI", "Callback RecieveGPSInfo [NetworkClientID:%s/Response:%s]", id.GetID().c_str(), strResponse.c_str());
		ccm_cui::ccmipc_->Send(strResponse);
	}
}

const void ReceiveRecorderControlStatus(const ccm::NetworkClientID id, const ccm::RecorderControlStatus result) {
	CCM_LOG_INFO("CCMCUI", "Callback ReceiveRecorderControlStatus [NetworkClientID:%s]", id.GetID().c_str());
	printf("Recieved ReceiveRecorderControlStatus: ID = %s\n", id.GetID().c_str());

	// For IPC
	if (ccm_cui::ccmipc_ != nullptr) {
		std::string strResponse = ccm::CcmJsonParser::CreateRecorderControlStatusNotify(id, result);
		CCM_LOG_INFO("CCMCUI", "Callback ReceiveRecorderControlStatus [NetworkClientID:%s/Response:%s]", id.GetID().c_str(), strResponse.c_str());
		ccm_cui::ccmipc_->Send(strResponse);
	}
}
const void ReceiveClipRecorderStatus(const ccm::NetworkClientID id, const std::map<std::string, ccm::ClipRecorderStatus> result) {
	CCM_LOG_INFO("CCMCUI", "Callback ReceiveClipRecorderStatus [NetworkClientID:%s]", id.GetID().c_str());
	printf("Recieved ReceiveClipRecorderStatus: ID = %s\n", id.GetID().c_str());

	// For IPC
	if (ccm_cui::ccmipc_ != nullptr) {
		std::string strResponse = ccm::CcmJsonParser::CreateClipRecorderStatusNotify(id, result);
		CCM_LOG_INFO("CCMCUI", "Callback ReceiveClipRecorderStatus [NetworkClientID:%s/Response:%s]", id.GetID().c_str(), strResponse.c_str());
		ccm_cui::ccmipc_->Send(strResponse);
	}
}
const void OnReceiveDriveStatus(const ccm::NetworkClientID id, const std::vector<ccm::Drive> result) {
	CCM_LOG_INFO("CCMCUI", "Callback OnReceiveDriveStatus [NetworkClientID:%s]", id.GetID().c_str());
	printf("Recieved OnReceiveDriveStatus: ID = %s\n", id.GetID().c_str());

	// For IPC
	if (ccm_cui::ccmipc_ != nullptr) {
		std::string strResponse = ccm::CcmJsonParser::CreateDriveStatusNotify(id, result);
		CCM_LOG_INFO("CCMCUI", "Callback OnReceiveDriveStatus [NetworkClientID:%s/Response:%s]", id.GetID().c_str(), strResponse.c_str());
		ccm_cui::ccmipc_->Send(strResponse);
	}
}

const void OnPlanningMetadataControlStatus(const ccm::NetworkClientID id, const bool status) {
	CCM_LOG_INFO("CCMCUI", "Callback OnPlanningMetadataControlStatus [NetworkClientID:%s, Status:%d]", id.GetID().c_str(), status);
	printf("Recieved OnPlanningMetadataControlStatus: ID = %s, Status = %d\n", id.GetID().c_str(), status);

	// For IPC
	if (ccm_cui::ccmipc_ != nullptr) {
		std::string strResponse = ccm::CcmJsonParser::CreatePlanningMetadataControlStatusNotify(id, status);
		CCM_LOG_INFO("CCMCUI", "Callback OnPlanningMetadataControlStatus [NetworkClientID:%s/Response:%s]", id.GetID().c_str(), strResponse.c_str());
		ccm_cui::ccmipc_->Send(strResponse);
	}
}



int main() {


	std::string cmd;
	std::cout << "---- CCM CUI START ----" << std::endl;

	//std::string rootpath;
	int rtn = 0;
	int httpServerport;
	int sslServerport;
	int ncSessionPort;
	std::string sslCertFile;
	std::string sslPrivateKey;
	std::string sslCaFile;
	std::string logOutputPath;
	std::string ccmRootPath;


#ifdef _WIN32
	WCHAR szPath[_MAX_PATH];
	char path[_MAX_PATH];
	size_t rc;
	GetModuleFileName(NULL, szPath, _MAX_PATH);
	wcstombs_s(&rc, path, _MAX_PATH, szPath, _MAX_PATH);
	ccm_cui::rootpath_ = path;
	ccmRootPath = ccm_cui::rootpath_.substr(0, ccm_cui::rootpath_.find_last_of("\\") + 1);
	std::cout << "CcmCui Rootpath :" << ccmRootPath.c_str() << std::endl;
	std::string systemconf = ccmRootPath + "ccm.cfg";
#else
	ccm_cui::separator_ = "/";
	static char buf[1024] = {};
	readlink("/proc/self/exe", buf, sizeof(buf) - 1);
	ccm_cui::rootpath_ = buf;
	ccmRootPath = ccm_cui::rootpath_.substr(0, ccm_cui::rootpath_.find_last_of("/") + 1);
	std::cout << "CcmCui Rootpath :" << ccmRootPath.c_str() << std::endl;
	std::string systemconf = ccmRootPath + "ccm.cfg";
#endif

	std::string configStr = ccm_setting::readConfigFile(systemconf.c_str());

	if (configStr.length() > 0) {
		ccm_setting::getSystemConfigData(configStr, ccm_cui::config_);
		ccm_setting::getNamemapData(configStr, ccm_cui::names_);
	}
	else {
		printf("configStr error\n");
	}

	//printf("ccm_cui::names:%d\n",ccm_cui::names_.size());

	auto itr = ccm_cui::config_.find("HTTPServerport");
	if (itr != ccm_cui::config_.end()) { httpServerport = atoi(itr->second.c_str()); }
	else { httpServerport = 8080; }

	itr = ccm_cui::config_.find("SavonaServerUsername");
	if (itr != ccm_cui::config_.end()) { ccm_cui::authUsername_ = itr->second.c_str(); }
	else { ccm_cui::authUsername_ = ""; }

	itr = ccm_cui::config_.find("SavonaServerPassword");
	if (itr != ccm_cui::config_.end()) { ccm_cui::authPassword_ = itr->second.c_str(); }
	else { ccm_cui::authPassword_ = ""; }

	itr = ccm_cui::config_.find("SSLServerport");
	if (itr != ccm_cui::config_.end()) { sslServerport = atoi(itr->second.c_str()); }
	else { sslServerport = 11500; }

	itr = ccm_cui::config_.find("SSLCertFile");
	if (itr != ccm_cui::config_.end()) { sslCertFile = itr->second; }

	itr = ccm_cui::config_.find("SSLPrivateKey");
	if (itr != ccm_cui::config_.end()) { sslPrivateKey = itr->second; }

	itr = ccm_cui::config_.find("SSLCaFile");
	if (itr != ccm_cui::config_.end()) { sslCaFile = itr->second; }

	itr = ccm_cui::config_.find("LogOutputPath");
	if (itr != ccm_cui::config_.end()) { logOutputPath = itr->second; }
	else { logOutputPath = ccm_cui::rootpath_.substr(0, ccm_cui::rootpath_.find_last_of(ccm_cui::separator_) + 1) + "log" + ccm_cui::separator_; }

	itr = ccm_cui::config_.find("NCSessionPort");
	if (itr != ccm_cui::config_.end()) { ncSessionPort = atoi(itr->second.c_str()); }
	else { ncSessionPort = 11500; }


	CCM_LOG_INITIALIZE(logOutputPath);
	CCM_LOG_INFO("CCMCUI", "%s", "CCM CUI Start");

	if ((int)((const char)*"あ" & 0xFF) == (int)0x82) {
		CCM_LOG_INFO("CCMCUI", "%s", "Command Line Character Code:SJIS");
	}
	else if ((int)((const char)*"あ" & 0xFF) == (int)0x30) {
		CCM_LOG_INFO("CCMCUI", "%s", "Command Line Character Code:UTF16");
	}
	else if ((int)((const char)*"あ" & 0xFF) == (int)0xa4) {
		CCM_LOG_INFO("CCMCUI", "%s", "Command Line Character Code:EUC");
	}
	else if ((int)((const char)*"あ" & 0xFF) == (int)0xe3) {
		CCM_LOG_INFO("CCMCUI", "%s", "Command Line Character Code:UTF8");
	}
	else {
		CCM_LOG_INFO("CCMCUI", "%s", "Command Line Character Code:Unknown");
	}


	ccm::UploadDestinationConfig config;
	std::vector<std::string> upload_cert;
	std::vector<std::string> mcb_cert;

	std::string file_transfer_conf = ccm_cui::rootpath_.substr(0, ccm_cui::rootpath_.find_last_of(ccm_cui::separator_) + 1) + "filetransfer.cfg";
	std::string file_transfer_config_str = ccm_setting::readConfigFile(file_transfer_conf.c_str());
	ccm_setting::getFileTransferConfig(nullptr, nullptr, "", &config, &upload_cert, &mcb_cert, file_transfer_config_str);

	// ログパスの設定
	CCM_LOG_INITIALIZE(logOutputPath);
	//CCM_LOG_ADD("CCMSDK");
	CCM_LOG_INFO("CCMCUI", "%s", "---- CCM CUI START ----");
	CCM_LOG_INFO("CCMCUI", "CcmCui Rootpath : %s", ccmRootPath.c_str());

	ccm_cui::connection_control_manager_ = new ccm::ConnectionControlManager();
	ccm_cui::connection_control_manager_->RegistAuthenticationHandler(AuthenticationHandler);
	ccm_cui::connection_control_manager_->RegistNetworkClientConnectHandler(ConnectHandler);
	ccm_cui::connection_control_manager_->RegistVoIPStatusChangedHandler(VoIPStatusHandler);
	ccm_cui::connection_control_manager_->RegistStreamingStatusChangedHandler(StreamingStatusHandler);
	ccm_cui::connection_control_manager_->RegistDeviceRegisterdHandler(DeviceRegistredHandler);
	ccm_cui::connection_control_manager_->RegistStreamingDynamicPropertyHandler(StreamingDynamicPropertyHandler);
	ccm_cui::connection_control_manager_->RegistUploadServerCertHandler(SetUploadServerCertHandler);
	ccm_cui::connection_control_manager_->RegistMCBServerCertHandler(SetMcbServerCertHandler);
	ccm_cui::connection_control_manager_->RegistDefaultUploadServerConfigHandler(SetDefaultUploadServerConfigHandler);
	ccm_cui::connection_control_manager_->RegistAddUploadFilesHandler(AddUploadFilesHandler);
	ccm_cui::connection_control_manager_->RegistRemoveUploadListHandler(RemoveUploadList);
	ccm_cui::connection_control_manager_->RegistResumeUploadListHandler(ResumeUploadList);
	ccm_cui::connection_control_manager_->RegistSuspendUploadListHandler(SuspendUploadList);
	ccm_cui::connection_control_manager_->RegistMcbNetworkInfoHandler(McbNetworkInfoHandler);
	ccm_cui::connection_control_manager_->RegistAddedUploadHandler(AddedUpload);
	ccm_cui::connection_control_manager_->RegistChangedUploadHandler(ChangedUpload);
	ccm_cui::connection_control_manager_->RegistProgressedUploadHandler(ProgressedUpload);
	ccm_cui::connection_control_manager_->RegistRemovedUploadHandler(RemovedUpload);
	ccm_cui::connection_control_manager_->RegistGetClipListHandler(GetClipList);
	ccm_cui::connection_control_manager_->RegistGetUploadListHandler(GetUploadList);
	ccm_cui::connection_control_manager_->RegistStartQosStreamingRequestHandler(StartQosStreamingSendRequest);
	ccm_cui::connection_control_manager_->RegistRecieveThumbnailHandler(RecieveThumbnail);
	ccm_cui::connection_control_manager_->RegistRecieveNetworkInfoHandler(RecieveNetworkInfo);
	ccm_cui::connection_control_manager_->RegistRecieveGPSInfoHandler(RecieveGPSInfo);
	ccm_cui::connection_control_manager_->RegistReceiveRecorderControlStatusHandler(ReceiveRecorderControlStatus);
	ccm_cui::connection_control_manager_->RegistReceiveClipRecorderStatusHandler(ReceiveClipRecorderStatus);
	ccm_cui::connection_control_manager_->RegistOnReceiveDriveStatusHandler(OnReceiveDriveStatus);
	ccm_cui::connection_control_manager_->RegistOnPlanningMetadataControlStatusHandler(OnPlanningMetadataControlStatus);

	//登録系
	ccm_cui::connection_control_manager_->RegistUploadServerCert(upload_cert);
	ccm_cui::connection_control_manager_->RegistMCBServerCert(mcb_cert);
	ccm_cui::connection_control_manager_->RegistUploadDestinationConfig(config);

	// CCM IPC
	ccm_cui::ccmipc_ = new ccm::CcmIpc(ccm_cui::connection_control_manager_);


	// HTTPサーバ起動時に、接続先のNCが使用するSavonaの制御用ポートを指定する
	rtn = ccm_cui::connection_control_manager_->StartHTTPServer(httpServerport, ncSessionPort);
	if (rtn != ccm::CCM_OK) {
		std::cout << "---- HTTP Server Starting Error :" << ccm::GetErrorMessage(ccm::ErrorCode(rtn)) << " ----" << std::endl;
		CCM_LOG_INFO("CCMCUI", "---- HTTP Server Starting Error Code:%d ----", rtn);
		std::getline(std::cin, cmd);
		return 0;
	}
	else {
		std::cout << "---- HTTP Server Started ----" << std::endl;
		CCM_LOG_INFO("CCMCUI", "%s", "---- HTTP Server Started ----");
	}

	rtn = ccm_cui::connection_control_manager_->StartServer(
		sslCertFile,
		sslPrivateKey,
		sslCaFile,
		sslServerport,
		3000);

	if (rtn != ccm::CCM_OK) {
		std::cout << "---- Server Starting Error :" << ccm::GetErrorMessage(ccm::ErrorCode(rtn)) << " ----" << std::endl;
		CCM_LOG_INFO("CCMCUI", "---- Server Starting Error Code:%d ----", rtn);
		std::getline(std::cin, cmd);
		return 0;
	}
	else {
		std::cout << "---- Server Started ----" << std::endl;
		CCM_LOG_INFO("CCMCUI", "%s", "---- Server Started ----");
	}

	ccm_cui_commands::on_command_func_ = {
	  { "GetClipList",			      ccm_cui_commands::GetClipList },
	  { "GetMediaList",			      ccm_cui_commands::GetMediaList },
	  { "GetUploadList",		      ccm_cui_commands::GetUploadList },
	  { "RemoveUploadList",		      ccm_cui_commands::RemoveUploadList },
	  { "SuspendUpload",		      ccm_cui_commands::SuspendUpload },
	  { "ResumeUpload",			      ccm_cui_commands::ResumeUpload },
	  { "AddUpload",			      ccm_cui_commands::AddUpload },
	  { "getclient",			      ccm_cui_commands::GetClient },
	  { "getlist",			          ccm_cui_commands::GetList },
	  { "pget",			              ccm_cui_commands::GetProperty },
	  { "SetIntercomProperty",	      ccm_cui_commands::SetIntercomProperty },
	  { "SetProperty",			      ccm_cui_commands::SetProperty },
	  { "StartVoIP",			      ccm_cui_commands::StartVoIP },
	  { "StopVoIP",			          ccm_cui_commands::StopVoIP },
	  { "StartStreaming",		      ccm_cui_commands::StartStreaming },
	  { "StartStreamingRecieve",      ccm_cui_commands::StartStreamingRecieve },
	  { "StopStreaming",		      ccm_cui_commands::StopStreaming },
	  { "StartThumbnail",		      ccm_cui_commands::StartThumbnail },
	  { "StopThumbnail",		      ccm_cui_commands::StopThumbnail },
	  { "SetUploadServerCert",        ccm_cui_commands::SetUploadServerCert },
	  { "SetMCBServerCert",		      ccm_cui_commands::SetMCBServerCert },
	  { "SetUploadDestinationConfig", ccm_cui_commands::SetUploadDestinationConfig },
	  { "GetPlanningMetadataList",    ccm_cui_commands::GetPlanningMetadataList },
	  { "GetPlanningMetadataDetail",  ccm_cui_commands::GetPlanningMetadataDetail },
	  { "GetCurrentPlanningMetadata", ccm_cui_commands::GetCurrentPlanningMetadata },
	  { "SetPlanningMetadata",        ccm_cui_commands::SetPlanningMetadata },
	  { "PutPlanningMetadata",        ccm_cui_commands::PutPlanningMetadata }
	};

	ccm_cui::args api_args;
	api_args.connection_control_manager = ccm_cui::connection_control_manager_;
	api_args.rootpath = ccm_cui::rootpath_;
	api_args.names = ccm_cui::names_;
	api_args.config = ccm_cui::config_;
	api_args.job_list = ccm_cui::job_list_;
	api_args.clip_list = ccm_cui::clip_list_;

	//printf("CCM> ");
	std::getline(std::cin, cmd);
	bool not_command = true;
	while (true) {

		std::vector<std::string> args = ccm_cui_commands::commandArgs(cmd);
		if (args.size() > 0) {
			if (ccm_cui_commands::on_command_func_.find(args[0]) != ccm_cui_commands::on_command_func_.end()) {
				api_args.command = cmd;
				ccm_cui_commands::on_command_func_[args[0]](api_args);
			}
			else if (cmd == "Q" || cmd == "E") {
				ccm_cui::connection_control_manager_->StopHTTPServer();
				ccm_cui::connection_control_manager_->StopServer();
				not_command = false;
				break;
			}
			else {
				printf("command '%s' not found\n\n", cmd.c_str());
			}
		}

		//printf("CCM> ");
		cmd.clear();
		std::getline(std::cin, cmd);
	}
	return 0;
};
