#include "ccm_gui.h"

namespace {
#ifdef _WIN32
//  const char *SJIStoUTF8(const char *szSJIS, int size) {
  void SJIStoUTF8(const char *szSJIS, int size, std::string& utf8) {
    if (size > 1024 * 32) {
		utf8 = "";
      //return "";
    }
    wchar_t bufUnicode[1024 * 32];
    char bufUTF8[1024 * 32];
    int lenUnicode = MultiByteToWideChar(CP_ACP, 0, szSJIS, (int)(strlen(szSJIS) + 1), bufUnicode, size);
    WideCharToMultiByte(CP_UTF8, 0, bufUnicode, lenUnicode, bufUTF8, size, NULL, NULL);
	utf8 = bufUTF8;
    //return bufUTF8;
  }

  char *UTF8toSJIS(const char *szUTF8, char *bufSJIS, int size) {
    if (size > 1024 * 32) {
      return "";
    }
    wchar_t bufUnicode[1024 * 32];
    int lenUnicode = MultiByteToWideChar(CP_UTF8, 0, szUTF8, (int)(strlen(szUTF8) + 1), bufUnicode, 1024);
    WideCharToMultiByte(CP_ACP, 0, bufUnicode, lenUnicode, bufSJIS, size, NULL, NULL);
    return bufSJIS;
  }
#endif
}
std::vector<std::string> ccm_cui_commands::commandArgs(std::string cmd) {
  std::vector<std::string> v;
  //std::stringstream ss{ cmd };
  //std::string buf;
  //std::string strBuf;
  bool strFlg1 = false;
  bool strFlg2 = false;


  //printf("commandArgs\n");
  std::string buffer;
  buffer.clear();
  for (std::string::const_iterator it = cmd.begin(); it != cmd.end(); ++it) {
    if (*it == ' ' && strFlg1 == false && strFlg2 == false && buffer.length() > 0) {
      v.push_back(buffer);
      //printf("CMD : [%s]\n", buffer.c_str());
      buffer.clear();
    } else if (*it == *"\r" || *it == *"\n") {

    } else if (*it == *"'" && strFlg2 == false && strFlg1 == false) {
      strFlg2 = true;
    } else if (*it == *"'" && strFlg2 == true) {
      strFlg2 = false;
      v.push_back(buffer);
      //printf("CMD : [%s]\n", buffer.c_str());
      buffer.clear();
    } else if (*it == '"' && strFlg2 == false && strFlg1 == false) {
      strFlg1 = true;
    } else if (*it == '"' && strFlg1 == true) {
      strFlg1 = false;
      v.push_back(buffer);
      //printf("CMD : [%s]\n", buffer.c_str());
      buffer.clear();
    } else {
      buffer += *it;
    }
  }

  if (buffer.length() > 0) {
    v.push_back(buffer);
  }

  /*while (std::getline(ss, buf, ' ')) {
  	
    if (buf.find("'") != std::string::npos && strFlg2 == false) {
      if (buf.find("'", buf.find("'") + 1) == std::string::npos) {
        printf("case 1 [%s]\n", buf.c_str());
        //strBuf += std::regex_replace(buf, std::regex("'"), "") + " ";
        strBuf += " ";
        strFlg2 = true;
      } else {
        printf("case 2 [%s]\n", buf.c_str());
        strBuf += std::regex_replace(buf, std::regex("'"), "") + "";
        v.push_back(strBuf);
        strBuf.clear();
      }
      //strBuf += std::regex_replace(buf, std::regex("'"), "") + " ";
    } else if (buf.find("'") == std::string::npos && strFlg2 == true) {
      printf("case 3 [%s]\n", buf.c_str());
      strBuf += std::regex_replace(buf, std::regex("'"), "") + " ";
    } else if (buf.find("'") != std::string::npos && strFlg2 == true) {
      printf("case 4 [%s]\n", buf.c_str());
      strBuf += std::regex_replace(buf, std::regex("'"), "");
      v.push_back(strBuf);
      strBuf.clear();
      strFlg2 = false;
    } else if (std::count(buf.begin(), buf.end(), '"') == 1 && strFlg1 == false) {
      printf("case 5 [%s]\n", buf.c_str());
      strBuf += std::regex_replace(buf, std::regex("\""), "") + " ";
      strFlg1 = true;
    } else if (std::count(buf.begin(), buf.end(), '"') == 0 && strFlg1 == true) {
      printf("case 6 [%s]\n", buf.c_str());
      strBuf += std::regex_replace(buf, std::regex("\""), "") + " ";
    } else if (std::count(buf.begin(), buf.end(), '"') == 1 && strFlg1 == true) {
      printf("case 7 [%s]\n", buf.c_str());
      strBuf += std::regex_replace(buf, std::regex("\""), "");
      v.push_back(strBuf);
      strBuf.clear();
      strFlg1 = false;
    } else {
      printf("case 8 [%s]\n", buf.c_str());
      v.push_back(std::regex_replace(buf, std::regex("\""), ""));
    }
  }

  ss.str("");
  ss.clear(std::stringstream::goodbit);*/

  return v;
}

void ccm_cui_commands::GetClipList(ccm_cui::args cui_args) {
  std::vector<std::string> args = commandArgs(cui_args.command);
  if (args.size() > 1) {
    std::string nc_name;
    std::string conf_name;

    auto itr = cui_args.names.find(args[1].c_str());
    if (itr != cui_args.names.end()) {
      nc_name = itr->second;
    } else {
      nc_name = args[1];
    }

    CCM_LOG_INFO("CCMCUI", "[IN] Command:[%s]", cui_args.command.c_str());

    if (nc_name.find('_') != std::string::npos) {
      std::string network_client_model_name = nc_name.substr(0, nc_name.find('_'));
      std::string network_client_serial_number = nc_name.substr(nc_name.find('_') + 1);
      ccm::NetworkClientID id = ccm::NetworkClientID(&network_client_model_name, &network_client_serial_number);

      ccm::shared_ptr<ccm::NetworkClient> client = cui_args.connection_control_manager->GetNetworkClient(id);
      if (client) {
        ccm::shared_ptr<ccm::FileTransferFunction> file_transfer_function = cui_args.connection_control_manager->GetNetworkClient(id)->GetFileTransferFunction();

        if (file_transfer_function) {
          //CCM_LOG_INFO("CCMCUI", "[IN] Command:GetClipList / NetworkClient:%s / DriveNumber:%s", nc_name.c_str(), v[2].c_str());
          //printf("GetClipList : %d\n", stoi(v[2]));
          if (args.size() > 2) {
            ccm::ErrorCode err = file_transfer_function->GetClipList(args[2]);
            if (err == ccm::CCM_OK) {
              printf("GetClipList OK\n");
              CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] OK", cui_args.command.c_str());
            } else {
              printf("GetClipList ERROR\n");
              CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] ERROR [%s]", cui_args.command.c_str(), ccm::GetErrorMessage(err).c_str());
            }
          } else {
            printf("param error\n");
            CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] Params Not Found", cui_args.command.c_str());
          }
        } else {
          printf("File Transfer Function is NULL\n");
          CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] File Transfer Function is NULL", cui_args.command.c_str());
        }
      } else {
        printf("Client [%s] is Not Online\n", nc_name.c_str());
        CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] Client [%s] is Not Online", cui_args.command.c_str(), nc_name.c_str());
      }
    } else {
      printf("Params Not Found\n");
      CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] Params Not Found", cui_args.command.c_str());
    }
  } else {
    printf("Params Not Found\n");
    CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] Params Not Found", cui_args.command.c_str());
  }
}

void ccm_cui_commands::GetMediaList(ccm_cui::args cui_args) {
  std::vector<std::string> v = commandArgs(cui_args.command);
  if (v.size() > 1) {
    std::string nc_name;
    std::string conf_name;

    auto itr = cui_args.names.find(v[1].c_str());
    if (itr != cui_args.names.end()) {
      nc_name = itr->second;
    } else {
      nc_name = v[1];
    }

    CCM_LOG_INFO("CCMCUI", "[IN] Command:[%s]", cui_args.command.c_str());

    if (nc_name.find('_') != std::string::npos) {
      std::string network_client_model_name = nc_name.substr(0, nc_name.find('_'));
      std::string network_client_serial_number = nc_name.substr(nc_name.find('_') + 1);
      ccm::NetworkClientID id = ccm::NetworkClientID(&network_client_model_name, &network_client_serial_number);

      ccm::shared_ptr<ccm::NetworkClient> client = cui_args.connection_control_manager->GetNetworkClient(id);
      if (client) {
        ccm::shared_ptr<ccm::FileTransferFunction> file_transfer_function = cui_args.connection_control_manager->GetNetworkClient(id)->GetFileTransferFunction();

        if (file_transfer_function) {
          std::vector<ccm::Drive> drives = file_transfer_function->GetMediaList();
          if (drives.size() > 0) {
            printf("GetMediaList OK\n");
            for (ccm::Drive drive : drives) {
              printf(" -> %s %s %llu %llu\n", drive.id.c_str(), drive.status.c_str(), drive.capacity, drive.available);
            }
          } else {
            printf("GetMediaList ERROR\n");
          }
        } else {
          printf("File Transfer Function is NULL\n");
          CCM_LOG_INFO("CCMCUI", "%s", "[OUT]Command:GetMediaList File Transfer Function is NULL");
        }
      } else {
        printf("Client [%s] is Not Online\n", nc_name.c_str());
        CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] Client [%s] is Not Online", cui_args.command.c_str(), nc_name.c_str());
      }
    } else {
      printf("Params Not Found\n");
      CCM_LOG_INFO("CCMCUI", "%s", "[OUT]Command:GetMediaList Params Not Found");
    }
  } else {
    printf("NC Error\n");
    CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] NC Error", cui_args.command.c_str());
  }
};
void ccm_cui_commands::GetUploadList(ccm_cui::args cui_args) {
  std::vector<std::string> v = commandArgs(cui_args.command);
  if (v.size() > 1) {
    std::string nc_name;
    std::string conf_name;

    auto itr = cui_args.names.find(v[1].c_str());
    if (itr != cui_args.names.end()) {
      nc_name = itr->second;
    } else {
      nc_name = v[1];
    }

    CCM_LOG_INFO("CCMCUI", "[IN] Command:[%s]", cui_args.command.c_str());

    if (nc_name.find('_') != std::string::npos) {
      std::string network_client_model_name = nc_name.substr(0, nc_name.find('_'));
      std::string network_client_serial_number = nc_name.substr(nc_name.find('_') + 1);
      ccm::NetworkClientID id = ccm::NetworkClientID(&network_client_model_name, &network_client_serial_number);

      ccm::shared_ptr<ccm::NetworkClient> client = cui_args.connection_control_manager->GetNetworkClient(id);
      if (client) {
        ccm::shared_ptr<ccm::FileTransferFunction> file_transfer_function = cui_args.connection_control_manager->GetNetworkClient(id)->GetFileTransferFunction();

        if (file_transfer_function) {
          if (cui_args.job_list.find(id.GetID()) != cui_args.job_list.end()) {
            std::map<std::uint32_t, ccm::Upload> job_list = cui_args.job_list.find(id.GetID())->second;
            if (job_list.size() > 0) {
              printf("Local Upload List Count:%zd\n", job_list.size());
              CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] Local Upload List Count[%d]", cui_args.command.c_str(), job_list.size());
              for (std::pair<std::uint32_t, ccm::Upload> upload : job_list) {
                printf("Local Upload List ID:%d Status:%d Percentage:%d Transferred:%llu\n", upload.second.id, upload.second.status, upload.second.percentage, upload.second.transferred);
                CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] Local Upload List ID:%d Status:%d Percentage:%d Transferred:%d", cui_args.command.c_str(), upload.second.id, upload.second.status, upload.second.percentage, upload.second.transferred);
              }
            } else {
              printf("Local Upload List is Empty\n");
              CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] Local Upload List is Empt", cui_args.command.c_str());
            }
          }
          ccm::ErrorCode err = file_transfer_function->GetUploadList(100);
          if (err == ccm::CCM_OK) {
            printf("GetUploadList OK\n");
            CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] OK", cui_args.command.c_str());
          } else {
            printf("GetUploadList ERROR\n");
            CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] ERROR", cui_args.command.c_str());
          }
        } else {
          printf("File Transfer Function is NULL\n");
          CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] File Transfer Function is NULL", cui_args.command.c_str());
        }
      } else {
        printf("Client [%s] is Not Online\n", nc_name.c_str());
        CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] Client [%s] is Not Online", cui_args.command.c_str(), nc_name.c_str());
      }
    } else {
      printf("Params Not Found\n");
      CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] Params Not Found", cui_args.command.c_str());
    }
  } else {
    printf("NC Error\n");
    CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] NC Error", cui_args.command.c_str());
  }
};
void ccm_cui_commands::RemoveUploadList(ccm_cui::args cui_args) {
  std::vector<std::string> v = commandArgs(cui_args.command);
  if (v.size() > 1) {
    std::string nc_name;
    std::string conf_name;

    auto itr = cui_args.names.find(v[1].c_str());
    if (itr != cui_args.names.end()) {
      nc_name = itr->second;
    } else {
      nc_name = v[1];
    }

    try {
      CCM_LOG_INFO("CCMCUI", "[IN] Command:[%s]", cui_args.command.c_str());

      if (nc_name.find('_') != std::string::npos) {
        std::string network_client_model_name = nc_name.substr(0, nc_name.find('_'));
        std::string network_client_serial_number = nc_name.substr(nc_name.find('_') + 1);
        ccm::NetworkClientID id = ccm::NetworkClientID(&network_client_model_name, &network_client_serial_number);

        //ccm::NetworkClientID id = ccm::NetworkClientID(&v[1], &v[2]);
        ccm::shared_ptr<ccm::NetworkClient> client = cui_args.connection_control_manager->GetNetworkClient(id);
        if (client) {
          ccm::shared_ptr<ccm::FileTransferFunction> file_transfer_function = cui_args.connection_control_manager->GetNetworkClient(id)->GetFileTransferFunction();

          if (file_transfer_function) {
            std::vector<std::uint32_t> job_list;

            if (v.size() > 2) {
              for (int a = 2; a < v.size(); a = a + 1) {
                job_list.push_back(stoi(v[a]));
              }
            }

            if (file_transfer_function->RemoveUploadList(job_list) == ccm::CCM_OK) {
              printf("RemoveUploadList OK\n");
              CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] OK", cui_args.command.c_str());
            } else {
              printf("RemoveUploadList ERROR\n");
              CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] ERROR", cui_args.command.c_str());
            }
          } else {
            printf("File Transfer Function is NULL\n");
            CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] File Transfer Function is NULL", cui_args.command.c_str());
          }
        } else {
          printf("Client [%s] is Not Online\n", nc_name.c_str());
          CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] Client [%s] is Not Online", cui_args.command.c_str(), nc_name.c_str());
        }
      } else {
        printf("NC Error\n");
        CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] NC Error", cui_args.command.c_str());
      }
    } catch (...) {
      printf("param error\n");
      CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] Params Error", cui_args.command.c_str());
    }
  } else {
    printf("param error\n");
    CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] Params Error", cui_args.command.c_str());
  }
};
void ccm_cui_commands::SuspendUpload(ccm_cui::args cui_args) {
  std::vector<std::string> v = commandArgs(cui_args.command);
  if (v.size() > 1) {
    std::string nc_name;
    std::string conf_name;

    auto itr = cui_args.names.find(v[1].c_str());
    if (itr != cui_args.names.end()) {
      nc_name = itr->second;
    } else {
      nc_name = v[1];
    }

    try {
      CCM_LOG_INFO("CCMCUI", "[IN] Command:[%s]", cui_args.command.c_str());

      if (nc_name.find('_') != std::string::npos) {
        std::string network_client_model_name = nc_name.substr(0, nc_name.find('_'));
        std::string network_client_serial_number = nc_name.substr(nc_name.find('_') + 1);
        ccm::NetworkClientID id = ccm::NetworkClientID(&network_client_model_name, &network_client_serial_number);

        //ccm::NetworkClientID id = ccm::NetworkClientID(&v[1], &v[2]);
        ccm::shared_ptr<ccm::NetworkClient> client = cui_args.connection_control_manager->GetNetworkClient(id);
        if (client) {
          ccm::shared_ptr<ccm::FileTransferFunction> file_transfer_function = cui_args.connection_control_manager->GetNetworkClient(id)->GetFileTransferFunction();

          if (file_transfer_function) {
            std::vector<std::uint32_t> job_list;

            if (v.size() > 2) {
              for (int a = 2; a < v.size(); a = a + 1) {
                job_list.push_back(stoi(v[a]));
              }
            }

            if (file_transfer_function->SuspendUploadList(job_list) == ccm::CCM_OK) {
              printf("SuspendUploadList OK\n");
              CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] OK", cui_args.command.c_str());
            } else {
              printf("SuspendUploadList ERROR\n");
              CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] ERROR", cui_args.command.c_str());
            }
          } else {
            printf("File Transfer Function is NULL\n");
            CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] File Transfer Function is NULL", cui_args.command.c_str());
          }
        } else {
          printf("Client [%s] is Not Online\n", nc_name.c_str());
          CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] Client [%s] is Not Online", cui_args.command.c_str(), nc_name.c_str());
        }
      } else {
        printf("NC Error\n");
        CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] NC Error", cui_args.command.c_str());
      }
    } catch (...) {
      printf("param error\n");
      CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] Params Error", cui_args.command.c_str());
    }
  } else {
    printf("param error\n");
    CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] Params Error", cui_args.command.c_str());
  }
};
void ccm_cui_commands::ResumeUpload(ccm_cui::args cui_args) {
  std::vector<std::string> v = commandArgs(cui_args.command);
  if (v.size() > 1) {
    std::string nc_name;
    std::string conf_name;

    auto itr = cui_args.names.find(v[1].c_str());
    if (itr != cui_args.names.end()) {
      nc_name = itr->second;
    } else {
      nc_name = v[1];
    }

    try {
      CCM_LOG_INFO("CCMCUI", "[IN] Command:[%s]", cui_args.command.c_str());
      if (nc_name.find('_') != std::string::npos) {
        std::string network_client_model_name = nc_name.substr(0, nc_name.find('_'));
        std::string network_client_serial_number = nc_name.substr(nc_name.find('_') + 1);
        ccm::NetworkClientID id = ccm::NetworkClientID(&network_client_model_name, &network_client_serial_number);

        //ccm::NetworkClientID id = ccm::NetworkClientID(&v[1], &v[2]);
        ccm::shared_ptr<ccm::NetworkClient> client = cui_args.connection_control_manager->GetNetworkClient(id);
        if (client) {
          ccm::shared_ptr<ccm::FileTransferFunction> file_transfer_function = cui_args.connection_control_manager->GetNetworkClient(id)->GetFileTransferFunction();

          if (file_transfer_function) {
            std::vector<std::uint32_t> job_list;

            if (v.size() > 2) {
              for (int a = 2; a < v.size(); a = a + 1) {
                job_list.push_back(stoi(v[a]));
              }
            }

            if (file_transfer_function->ResumeUploadList(job_list) == ccm::CCM_OK) {
              printf("ResumeUploadList OK\n");
              CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] OK", cui_args.command.c_str());
            } else {
              printf("ResumeUploadList ERROR\n");
              CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] ERROR", cui_args.command.c_str());
            }
          } else {
            printf("File Transfer Function is NULL\n");
            CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] File Transfer Function is NULL", cui_args.command.c_str());
          }
        } else {
          printf("Client [%s] is Not Online\n", nc_name.c_str());
          CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] Client [%s] is Not Online", cui_args.command.c_str(), nc_name.c_str());
        }
      } else {
        printf("NC Error\n");
        CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] NC Error", cui_args.command.c_str());
      }
    } catch (...) {
      printf("param error\n");
      CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] Params Error", cui_args.command.c_str());
    }
  } else {
    printf("param error\n");
    CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] Params Error", cui_args.command.c_str());
  }
};
void ccm_cui_commands::AddUpload(ccm_cui::args cui_args) {
  std::vector<std::string> v = commandArgs(cui_args.command);
  if (v.size() > 1) {
    std::string nc_name;
    std::string conf_name;
    std::string clip_number;

    auto itr = cui_args.names.find(v[1].c_str());
    if (itr != cui_args.names.end()) {
      nc_name = itr->second;
    } else {
      nc_name = v[1];
    }

    if (v.size() > 2) {
      if (v[2].length() <= 2) {
        clip_number = v[2];
        conf_name = "Destination";
      } else {
        conf_name = v[2];
      }
    }

    if (v.size() > 3) {
      if (v[3].length() > 0) {
        conf_name = v[3];
      }
    }

    ccm::UploadDestinationConfig config;
    std::vector<std::string> upload_cert;
    std::vector<std::string> mcb_cert;

    if (nc_name.find('_') != std::string::npos) {
      std::string file_transfer_conf = cui_args.rootpath.substr(0, cui_args.rootpath.find_last_of(ccm_cui::separator_) + 1) + "filetransfer.cfg";
      std::string file_transfer_config_str = ccm_setting::readConfigFile(file_transfer_conf.c_str());
      ccm_setting::getFileTransferConfig(nullptr, nullptr, "", &config, &upload_cert, &mcb_cert, file_transfer_config_str);

      std::string network_client_model_name = nc_name.substr(0, nc_name.find('_'));
      std::string network_client_serial_number = nc_name.substr(nc_name.find('_') + 1);
      ccm::NetworkClientID id = ccm::NetworkClientID(&network_client_model_name, &network_client_serial_number);


      ccm::UploadFiles destination;
      std::vector<ccm::UploadFiles> files;



      //printf("%s:%s:%s\n", nc_name.c_str(), clip_number.c_str(), conf_name.c_str());

      try {
        // クリップ番号の指定があるとき

        CCM_LOG_INFO("CCMCUI", "[IN] Command:[%s]", cui_args.command.c_str());
        if (clip_number.length() > 0) {
          if (cui_args.clip_list.find(nc_name) != cui_args.clip_list.end()) {
            std::vector<ccm::Clip> clip_list = cui_args.clip_list.find(nc_name)->second;
            // クリップ番号が範囲内の時
            if (clip_list.size() >= stoi(clip_number) - 1) {
              // クリップ情報から生成※I/Fの都合上一つのみ？
              ccm_setting::getFileTransferConfig(nullptr, &destination, conf_name, nullptr, nullptr, nullptr, ccm_setting::readConfigFile(file_transfer_conf.c_str()));

              ccm::UploadFiles upload_files;
              upload_files.drive = clip_list[stoi(clip_number) - 1].drive;
              upload_files.files.push_back(clip_list[stoi(clip_number) - 1].uri);
              upload_files.setting_id = destination.setting_id;
              upload_files.destination = destination.destination;

              files.push_back(upload_files);
              //clip_list[0]
              //files[0].destination = destination_config.destination;
            }
          } else {
            CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] ClipList is Empty", cui_args.command.c_str());
          }
        } else {
          // 設定ファイルから読み込み（複数可能）
          // iperfはこちら側
          ccm_setting::getFileTransferConfig(&files, nullptr, conf_name, nullptr, nullptr, nullptr, ccm_setting::readConfigFile(file_transfer_conf.c_str()));
        }
      } catch (...) {
        CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] Param Error", cui_args.command.c_str())
      }
      if (files.size() > 0) {
        ccm::shared_ptr<ccm::NetworkClient> client = cui_args.connection_control_manager->GetNetworkClient(id);
        if (client) {
          ccm::shared_ptr<ccm::FileTransferFunction> file_transfer_function = cui_args.connection_control_manager->GetNetworkClient(id)->GetFileTransferFunction();
          if (file_transfer_function) {
            ccm::ErrorCode err = file_transfer_function->AddUploadFiles(files);
            if (err == ccm::CCM_OK) {
              printf("AddUpload OK\n");
              CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] OK", cui_args.command.c_str())
            } else {
              printf("AddUpload ERROR\n");
              CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] ERROR [%s]", cui_args.command.c_str(), ccm::GetErrorMessage(err).c_str());
            }
          } else {
            printf("File Transfer Function is NULL\n");
            CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] File Transfer Function is NULL", cui_args.command.c_str());
          }
        } else {
          printf("Client [%s] is Not Online\n", nc_name.c_str());
          CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] Client [%s] is Not Online", cui_args.command.c_str(), nc_name.c_str());
        }
      } else {
        printf("Params Not Found\n");
        CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] Params Not Found", cui_args.command.c_str());
      }
    } else {
      printf("NC Error\n");
      CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] NC Error", cui_args.command.c_str());
    }
  } else {
    printf("param error\n");
    CCM_LOG_INFO("CCMCUI", "[OUT]Command:[%s] Params Error", cui_args.command.c_str());
  }
};
void ccm_cui_commands::GetClient(ccm_cui::args cui_args) {
  std::vector<std::string> v = commandArgs(cui_args.command);
  if (v.size() > 1) {
    ccm::NetworkClientID id = ccm::NetworkClientID(&v[1], &v[2]);
    ccm::shared_ptr<ccm::NetworkClient> nc = cui_args.connection_control_manager->GetNetworkClient(id);

    if (nc) {
      auto *dn = nc->GetDisplayName();
      auto *mn = nc->GetModelName();
      auto *sn = nc->GetSerialNumber();
      auto *sv = nc->GetSoftwareVersion();

      printf("nc->GetDisplayName() = %s\n", (dn != nullptr ? dn->c_str() : ""));
      printf("nc->GetModelName() = %s\n", (mn != nullptr ? mn->c_str() : ""));
      printf("nc->GetSerialNumber() = %s\n", (sn != nullptr ? sn->c_str() : ""));
      printf("nc->GetSoftwareVersion() = %s\n", (sv != nullptr ? sv->c_str() : ""));
    }
  }
};
void ccm_cui_commands::GetList(ccm_cui::args cui_args) {
  std::vector<ccm::shared_ptr<ccm::NetworkClient>> nclist = cui_args.connection_control_manager->GetNetworkClientList();
  CCM_LOG_INFO("CCMCUI", "[IN] Command:[%s]", cui_args.command.c_str());

  for (auto nc : nclist) {
    //std::cout << x << "\n";
    if (nc) {


      auto *dn = nc->GetDisplayName();
      auto *mn = nc->GetModelName();
      auto *sn = nc->GetSerialNumber();
      auto *sv = nc->GetSoftwareVersion();

      printf("Client DisplayName:%s ModelName:%s SerialNumber:%s Version:%s\n",
        (dn != nullptr ? dn->c_str() : ""),
             (mn != nullptr ? mn->c_str() : ""),
             (sn != nullptr ? sn->c_str() : ""),
             (sv != nullptr ? sv->c_str() : "")
      );

      for (auto network_interface : nc->GetNetworkInterfaces()) {
        if (network_interface) {
          printf("##################################\n");
          printf("# GetNetworkInterfaces()\n");
          printf("GetNetworkClientID       : %s\n", network_interface->GetID()->c_str());
          printf("GetPhysicalInterfaceName : %s\n", network_interface->GetPhysicalInterfaceName()->c_str());
          printf("GetType                  : %s\n", network_interface->GetType()->c_str());
          printf("GetIPv4Address           : %s\n", network_interface->GetIPv4Address()->c_str());
          printf("-> CellularInfo\n");
          printf("   GetSignalStrength     : %s\n", network_interface->GetCellularInfo()->GetSignalStrength()->c_str());
          printf("   GetSignalStrengthUnit : %s\n", network_interface->GetCellularInfo()->GetSignalStrengthUnit()->c_str());
          printf("   GetOperator           : %s\n", network_interface->GetCellularInfo()->GetOperator()->c_str());
          printf("   GetPLMN               : %s\n", network_interface->GetCellularInfo()->GetPLMN()->c_str());
          printf("   GetBand               : %s\n", network_interface->GetCellularInfo()->GetBand()->c_str());
          printf("   GetCellID             : %s\n", network_interface->GetCellularInfo()->GetCellID()->c_str());
          printf("   GetAirInterface       : %s\n", network_interface->GetCellularInfo()->GetAirInterface()->c_str());
          printf("   GetManufacture        : %s\n", network_interface->GetCellularInfo()->GetManufacture()->c_str());
          printf("   GetModelName          : %s\n", network_interface->GetCellularInfo()->GetModelName()->c_str());
          printf("   GetIMEI               : %s\n", network_interface->GetCellularInfo()->GetIMEI()->c_str());
        } else {
          printf("NetworkInterfaces Empty\n");
        }
      }

      //for (auto streaming_interface : nc->GetStreamingFunction()->GetStreamingInterface()) {
      //  printf("##################################\n");
      //  printf("# GetStreamingInterface()\n");
      //  printf("Interface                : %s\n", streaming_interface.first.c_str());
      //}
      ccm::shared_ptr<ccm::StreamingFunction> streaming_function = nc->GetStreamingFunction();
      if (streaming_function) {
        for (auto video_capability : streaming_function->GetVideoFormatCapability()->GetAll()) {
          if (video_capability) {
            printf("##################################\n");
            printf("# GetVideoFormatCapability()->GetAll()\n");
            printf("GetID                    : %s\n", video_capability->GetID()->c_str());
            printf("GetMinBitrate            : %ld\n", *video_capability->GetMinBitrate());
            printf("GetMaxBitrate            : %ld\n", *video_capability->GetMaxBitrate());
            printf("GetBitrateStep           : %ld\n", *video_capability->GetBitrateStep());
          }
        }
      } else {
        printf("Streaming Not Support\n");
      }


      //nc->GetStreamingFunction()->StartQosStreamingSend(nc->GetNetworkClientID(), testparam);


      //printf("nc->GetNetworkInterfaces()[0]->id->c_str() = %s\n", nc->GetNetworkInterfaces()[0]->id->c_str());
    }
  }
};
void ccm_cui_commands::GetProperty(ccm_cui::args cui_args) {
  std::vector<std::string> v = commandArgs(cui_args.command);
  CCM_LOG_INFO("CCMCUI", "[IN] Command:[%s]", cui_args.command.c_str());

  std::string NC;
  std::string KEY;
  std::string VAL;
  std::string name;

  if (v.size() < 2) {

  } else if (v.size() == 2) {
    NC = v[1];
    KEY = "";
    VAL = "";
  } else if (v.size() == 3) {
    NC = v[1];
    KEY = v[2];
    VAL = "*";
  } else if (v.size() == 4) {
    NC = v[1];
    KEY = v[2];
    VAL = v[3];
  } else {

  }

  auto itr = cui_args.names.find(NC.c_str());
  if (itr != cui_args.names.end()) {
    name = itr->second;
  } else {
    name = NC;
  }

  std::string network_client_model_name = name.substr(0, name.find('_'));
  std::string network_client_serial_number = name.substr(name.find('_') + 1);
  ccm::NetworkClientID id = ccm::NetworkClientID(&network_client_model_name, &network_client_serial_number);

  std::string prp = cui_args.connection_control_manager->GetPropery(id, KEY, VAL);

  printf("GetProperty Response:%s\n", prp.c_str());
};

void ccm_cui_commands::SetProperty(ccm_cui::args cui_args) {
  //printf("startsetproperty");
  std::vector<std::string> v = commandArgs(cui_args.command);
  CCM_LOG_INFO("CCMCUI", "[IN] Command:[%s]", cui_args.command.c_str());


  std::string NC;
  std::string PROPERTY;
  std::string PNC_MS;
  std::string str_value;

  //bool optFromPrpFlg = false;
  //bool optToPrpFlg = false;

  //for (auto args : v ) {
  //  printf("Args:%s\n", args.c_str());
  //}

  if (v.size() < 2) {

  } else if (v.size() == 2) {
    NC = v[1];
    PROPERTY = v[1];
  } else if (v.size() == 3) {
    NC = v[1];
    PROPERTY = v[1];
    str_value = v[2];
  } else {

  }

  auto itr = cui_args.names.find(NC.c_str());
  if (itr != cui_args.names.end()) {
    PNC_MS = itr->second;
  } else {
    PNC_MS = NC;
  }

  if (str_value.length() > 0) {

    //ccm::NetworkClientID id = ccm::NetworkClientID ( &PNC_MS.substr(0, PNC_MS.find('_')), &PNC_MS.substr(PNC_MS.find('_') + 1) );
    std::string network_client_model_name = PNC_MS.substr(0, PNC_MS.find('_'));
    std::string network_client_serial_number = PNC_MS.substr(PNC_MS.find('_') + 1);
    ccm::NetworkClientID id = ccm::NetworkClientID(&network_client_model_name, &network_client_serial_number);
    //printf("DEBUG NetworkClientID = %s\n", id.GetID().c_str());

    if (cui_args.connection_control_manager->SetProperty(id, str_value.c_str()) != ccm::CCM_OK) {
      printf("SetProperty Error\n");
    } else {
      printf("SetProperty Success\n");
    }
    //printf("PicoJson TEST = %s", picojson::value(map["Json"][PROPERTY.c_str()]).serialize().c_str());
  }
};

void ccm_cui_commands::SetIntercomProperty(ccm_cui::args cui_args) {
  //printf("startsetproperty");
  std::vector<std::string> v = commandArgs(cui_args.command);
  CCM_LOG_INFO("CCMCUI", "[IN] Command:[%s]", cui_args.command.c_str());

  bool optNCFlg = false;
  bool optConfigFlg = false;

  std::string NC;
  std::string PROPERTY;
  std::string PNC_MS;
  std::string CONFFILE = "ifb.cfg";

  //bool optFromPrpFlg = false;
  //bool optToPrpFlg = false;

  if (v.size() < 2) {

  } else if (v.size() == 2) {
    NC = v[1];
    PROPERTY = v[1];
  } else if (v.size() == 3) {
    NC = v[1];
    PROPERTY = v[1];
    CONFFILE = v[2];
  } else {

  }
  std::map<std::string, picojson::object> map;

  // コンフィグファイルが指定されているとき
  if (CONFFILE.length() > 0) {
    std::string fpath = cui_args.rootpath.substr(0, cui_args.rootpath.find_last_of(ccm_cui::separator_) + 1);
    fpath += CONFFILE;

    std::string configStr = ccm_setting::readConfigFile(fpath.c_str());
    if (configStr.length() > 0) {
      //printf("DEBUG = Loadconfig\n");
      ccm_setting::getNamemapData(configStr, cui_args.names);
      map = ccm_setting::getPropertyMap(PROPERTY.c_str(), configStr);
    }

    auto itr = cui_args.names.find(NC.c_str());
    if (itr != cui_args.names.end()) {
      PNC_MS = itr->second;
    } else {
      PNC_MS = NC;
    }

    if (map.size() > 0) {

      //ccm::NetworkClientID id = ccm::NetworkClientID ( &PNC_MS.substr(0, PNC_MS.find('_')), &PNC_MS.substr(PNC_MS.find('_') + 1) );
      std::string network_client_model_name = PNC_MS.substr(0, PNC_MS.find('_'));
      std::string network_client_serial_number = PNC_MS.substr(PNC_MS.find('_') + 1);
      ccm::NetworkClientID id = ccm::NetworkClientID(&network_client_model_name, &network_client_serial_number);
      //printf("DEBUG NetworkClientID = %s\n", id.GetID().c_str());

      if (cui_args.connection_control_manager->SetProperty(id, picojson::value(map["Json"][PROPERTY.c_str()]).serialize().c_str()) != ccm::CCM_OK) {
        printf("SetProperty Error\n");
      } else {
        printf("SetProperty Success\n");
      }
      //printf("PicoJson TEST = %s", picojson::value(map["Json"][PROPERTY.c_str()]).serialize().c_str());
    }
  }
};
void ccm_cui_commands::StartVoIP(ccm_cui::args cui_args) {
  std::vector<std::string> v = commandArgs(cui_args.command);
  CCM_LOG_INFO("CCMCUI", "[IN] Command:[%s]", cui_args.command.c_str());

  bool optFromFlg = false;
  bool optToFlg = false;
  bool optConfigFlg = false;

  std::string PNC = "";
  std::string PNCCNF = "";
  std::string PNC_MS = "";

  std::string SNC = "";
  std::string SNCCNF = "";
  std::string SNC_MS = "";
  std::string CONFFILE = "";

  if (v.size() < 3) {

  } else if (v.size() == 3) {
    PNC = v[1];
    PNCCNF = v[1];
    SNC = v[2];
    SNCCNF = v[2];
    CONFFILE = "ifb.cfg";
  } else if (v.size() == 4) {
    PNC = v[1];
    PNCCNF = v[1];
    SNC = v[2];
    SNCCNF = v[2];
    CONFFILE = v[3];
  } else {

  }

  if (PNC.empty() || PNCCNF.empty() || SNC.empty() || SNCCNF.empty() || CONFFILE.empty()) {
    printf("option error\n");
  } else {

    std::map<std::string, picojson::object> map1;
    std::map<std::string, picojson::object> map2;

    // コンフィグファイルが指定されているとき
    if (CONFFILE.length() > 0) {
      std::string fpath = cui_args.rootpath.substr(0, cui_args.rootpath.find_last_of(ccm_cui::separator_) + 1);
      fpath += CONFFILE;

      std::string configStr = ccm_setting::readConfigFile(fpath.c_str());
      if (configStr.length() > 0) {
        //sdk->getSystemConfigData(configStr, config);
        //sdk->SetConfigData(configStr);
        ccm_setting::getNamemapData(configStr, cui_args.names);

        map1 = ccm_setting::getPropertyMap(PNCCNF.c_str(), configStr);
        map2 = ccm_setting::getPropertyMap(SNCCNF.c_str(), configStr);

        if (map1.size() < 1) {
          printf("[%s] NC Property Data not found : %s\n", CONFFILE.c_str(), PNCCNF.c_str());
        }
        if (map2.size() < 1) {
          printf("[%s] NC Property Data not found : %s\n", CONFFILE.c_str(), SNCCNF.c_str());
        }
      }
    }
    //const std::string NC,//モデル名_シリアル番号
    //	const std::string clientType,//Slave,Master
    //	const std::string address,//clientTypeによって自動判別※Masterのみ
    //	const std::string port//clientTypeによって自動判別※Slave,Masterどちらも使用



    auto itr = cui_args.names.find(PNC.c_str());
    if (itr != cui_args.names.end()) {
      PNC_MS = itr->second;
    } else {
      PNC_MS = PNC;
    }
    itr = cui_args.names.find(SNC.c_str());
    if (itr != cui_args.names.end()) {
      SNC_MS = itr->second;
    } else {
      SNC_MS = SNC;
    }

    if (!PNC_MS.empty() && !SNC_MS.empty()) {

      std::string clienttype;
      std::string destinationaddress;
      std::uint32_t destinationport;
      std::uint32_t receivingport;
      std::map<std::string, std::uint32_t> framelength;
      std::map<std::string, std::string> cipherKey;
      std::map<std::string, std::string> ssrc;
      //std::map<std::string, bool> codecopusfec;
      //std::map<std::string, bool> codecopusdtx;

      std::string network_client_model_name = "";
      std::string network_client_serial_number = "";

      // Rxを先に処理
      clienttype = (map2.find("Network.Service.Intercom.Control.ClientType") != map2.end() ? map2["Network.Service.Intercom.Control.ClientType"]["Intercom"].get<std::string>() : "");
      destinationaddress = (map2.find("Network.Service.Intercom.Destination.Address") != map2.end() ? map2["Network.Service.Intercom.Destination.Address"]["IntercomTx"].get<std::string>() : "");
      destinationport = (int)(map2.find("Network.Service.Intercom.Destination.Port") != map2.end() ? map2["Network.Service.Intercom.Destination.Port"]["IntercomTx"].get<std::double_t>() : 0);
      receivingport = (int)(map2.find("Network.Service.Intercom.Receiving.Port") != map2.end() ? map2["Network.Service.Intercom.Receiving.Port"]["IntercomRx"].get<std::double_t>() : 0);
      framelength = {
        { "IntercomTx",(int)(map2.find("Network.Service.Intercom.Audio.FrameLength") != map2.end() ? map2["Network.Service.Intercom.Audio.FrameLength"]["IntercomTx"].get<std::double_t>() : 0) },
        { "IntercomRx",(int)(map2.find("Network.Service.Intercom.Audio.FrameLength") != map2.end() ? map2["Network.Service.Intercom.Audio.FrameLength"]["IntercomRx"].get<std::double_t>() : 0) }
      };
      cipherKey = {
        { "IntercomTx",(map2.find("Network.Service.Intercom.Security.CipherKey") != map2.end() ? map2["Network.Service.Intercom.Security.CipherKey"]["IntercomTx"].get<std::string>() : "") },
        { "IntercomRx",(map2.find("Network.Service.Intercom.Security.CipherKey") != map2.end() ? map2["Network.Service.Intercom.Security.CipherKey"]["IntercomRx"].get<std::string>() : "") }
      };
      ssrc = {
        { "IntercomTx",(map2.find("Network.Service.Intercom.Security.SSRC") != map2.end() ? map2["Network.Service.Intercom.Security.SSRC"]["IntercomTx"].get<std::string>() : "") },
        { "IntercomRx",(map2.find("Network.Service.Intercom.Security.SSRC") != map2.end() ? map2["Network.Service.Intercom.Security.SSRC"]["IntercomRx"].get<std::string>() : "") }
      };

      //codecopusfec = { { "Encode",(map2.find("Network.Service.Intercom.Codec.Opus.FEC") != map2.end() ? map2["Network.Service.Intercom.Codec.Opus.FEC"]["Encode"].get<bool>() : false) } };
      //codecopusdtx = { { "Encode",(map2.find("Network.Service.Intercom.Codec.Opus.DTX") != map2.end() ? map2["Network.Service.Intercom.Codec.Opus.DTX"]["Encode"].get<bool>() : false) },
      //                 { "Decode",(map2.find("Network.Service.Intercom.Codec.Opus.DTX") != map2.end() ? map2["Network.Service.Intercom.Codec.Opus.DTX"]["Decode"].get<bool>() : false) } };

      network_client_model_name = SNC_MS.substr(0, SNC_MS.find('_'));
      network_client_serial_number = SNC_MS.substr(SNC_MS.find('_') + 1);
      ccm::NetworkClientID id = ccm::NetworkClientID(&network_client_model_name, &network_client_serial_number);
      struct ccm::VoIPConfig cnf = {
        clienttype,
        destinationaddress,
        destinationport,
        receivingport,
        framelength,
        cipherKey,
        ssrc
        //codecopusfec,
        //codecopusdtx
      };
      //printf("DEBUG NetworkClientID = %s\n", id.GetID().c_str());

      if (cui_args.connection_control_manager->StartVoIP(
        id,
        cnf
        ) == ccm::CCM_OK) {
        printf("Secondary StartVoIP Success\n");
      } else {
        printf("Secondary StartVoIP Error\n");
      }

      // Txは後から
      //rtp / rtcp
      //bool enabled = (map1.find("Network.Service.Intercom.Control.Enabled") != map1.end() ? map1["Network.Service.Intercom.Control.Enabled"]["Intercom"].get<bool>() : true);
      clienttype = (map1.find("Network.Service.Intercom.Control.ClientType") != map1.end() ? map1["Network.Service.Intercom.Control.ClientType"]["Intercom"].get<std::string>() : "");
      destinationaddress = (map1.find("Network.Service.Intercom.Destination.Address") != map1.end() ? map1["Network.Service.Intercom.Destination.Address"]["IntercomTx"].get<std::string>() : "");
      destinationport = (int)(map1.find("Network.Service.Intercom.Destination.Port") != map1.end() ? map1["Network.Service.Intercom.Destination.Port"]["IntercomTx"].get<std::double_t>() : 0);
      receivingport = (int)(map1.find("Network.Service.Intercom.Receiving.Port") != map1.end() ? map1["Network.Service.Intercom.Receiving.Port"]["IntercomRx"].get<std::double_t>() : 0);
      framelength = {
        { "IntercomTx",(int)(map1.find("Network.Service.Intercom.Audio.FrameLength") != map1.end() ? map1["Network.Service.Intercom.Audio.FrameLength"]["IntercomTx"].get<std::double_t>() : 0) },
        { "IntercomRx",(int)(map1.find("Network.Service.Intercom.Audio.FrameLength") != map1.end() ? map1["Network.Service.Intercom.Audio.FrameLength"]["IntercomRx"].get<std::double_t>() : 0) }
      };
      cipherKey = {
        { "IntercomTx",(map1.find("Network.Service.Intercom.Security.CipherKey") != map1.end() ? map1["Network.Service.Intercom.Security.CipherKey"]["IntercomTx"].get<std::string>() : "") },
        { "IntercomRx",(map1.find("Network.Service.Intercom.Security.CipherKey") != map1.end() ? map1["Network.Service.Intercom.Security.CipherKey"]["IntercomRx"].get<std::string>() : "") }
      };
      ssrc = {
        { "IntercomTx",(map1.find("Network.Service.Intercom.Security.SSRC") != map1.end() ? map1["Network.Service.Intercom.Security.SSRC"]["IntercomTx"].get<std::string>() : "") },
        { "IntercomRx",(map1.find("Network.Service.Intercom.Security.SSRC") != map1.end() ? map1["Network.Service.Intercom.Security.SSRC"]["IntercomRx"].get<std::string>() : "") }
      };

      //codecopusfec = { { "Encode",(map1.find("Network.Service.Intercom.Codec.Opus.FEC") != map1.end() ? map1["Network.Service.Intercom.Codec.Opus.FEC"]["Encode"].get<bool>() : false) } };
      //codecopusdtx = { { "Encode",(map1.find("Network.Service.Intercom.Codec.Opus.DTX") != map1.end() ? map1["Network.Service.Intercom.Codec.Opus.DTX"]["Encode"].get<bool>() : false) },
      //                 { "Decode",(map1.find("Network.Service.Intercom.Codec.Opus.DTX") != map1.end() ? map1["Network.Service.Intercom.Codec.Opus.DTX"]["Decode"].get<bool>() : false) } };


      network_client_model_name = PNC_MS.substr(0, PNC_MS.find('_'));
      network_client_serial_number = PNC_MS.substr(PNC_MS.find('_') + 1);
      ccm::NetworkClientID pid = ccm::NetworkClientID(&network_client_model_name, &network_client_serial_number);
      struct ccm::VoIPConfig pcnf = {
        clienttype,
        destinationaddress,
        destinationport,
        receivingport,
        framelength,
        cipherKey,
        ssrc
        //codecopusfec,
        //codecopusdtx
      };
      if (cui_args.connection_control_manager->StartVoIP(
        pid,
        pcnf
        ) == ccm::CCM_OK) {
        printf("Primary StartVoIP Success\n");
      } else {
        printf("primary StartVoIP Error\n");
      }

    }
  }
};
void ccm_cui_commands::StopVoIP(ccm_cui::args cui_args) {
  std::vector<std::string> v = commandArgs(cui_args.command);
  CCM_LOG_INFO("CCMCUI", "[IN] Command:[%s]", cui_args.command.c_str());
  std::string name;

  if (!cui_args.names[v[1].c_str()].empty()) {
    name = cui_args.names[v[1].c_str()];

  } else {
    name = v[1].c_str();
  }

  std::string network_client_model_name = name.substr(0, name.find('_'));
  std::string network_client_serial_number = name.substr(name.find('_') + 1);
  ccm::NetworkClientID id = ccm::NetworkClientID(&network_client_model_name, &network_client_serial_number);

  if (cui_args.connection_control_manager->StopVoIP(id) != ccm::CCM_OK) {
    printf("StopVoIP Error\n");
  } else {
    printf("StopVoIP Success\n");
  }
};
void ccm_cui_commands::StartStreaming(ccm_cui::args cui_args) {
  //printf("StartStreaming\n");
  std::vector<std::string> v = commandArgs(cui_args.command);
  CCM_LOG_INFO("CCMCUI", "[IN] Command:[%s]", cui_args.command.c_str());

  std::string PNC = "";
  std::string PNCCNF = "";
  std::string PNC_MS = "";

  std::string SNC = "";
  std::string SNCCNF = "";
  std::string SNC_MS = "";
  std::string CONFFILE = "streaming.cfg";
  //printf("v.size() = %d",v.size());
  if (v.size() == 2) {
    PNC = v[1];
    PNCCNF = v[1];
  } else if (v.size() == 3) {
    PNC = v[1];
    PNCCNF = v[1];
    CONFFILE = v[2];
  } else if (v.size() == 4) {
    PNC = v[1];
    PNCCNF = v[1];
    SNC = v[2];
    SNCCNF = v[2];
    CONFFILE = v[3];
  } else {
    //continue;
  }



  std::string fpath = cui_args.rootpath.substr(0, cui_args.rootpath.find_last_of(ccm_cui::separator_) + 1);
  fpath += CONFFILE;

  std::string configStr = ccm_setting::readConfigFile(fpath.c_str());

  ccm::QosStreamingSendConfig streaming_params = ccm_setting::getQosStreamingSendConfigFromFile(PNCCNF.c_str(), configStr);

 // printf("cui_args.names.size:%d\n", cui_args.names.size());
  //printf("%s\n", fpath.c_str());

  std::string name;
  if (!cui_args.names[PNC.c_str()].empty()) {
    name = cui_args.names[PNC.c_str()];
  } else {
    name = PNC.c_str();
  }
  std::string model_name = name.substr(0, name.find('_'));
  std::string serial_number = name.substr(name.find('_') + 1);
  ccm::NetworkClientID id = ccm::NetworkClientID(&model_name, &serial_number);

  //printf("%s\n", id.GetID().c_str());

  //Get Netowork Client
  ccm::shared_ptr<ccm::NetworkClient> network_client = cui_args.connection_control_manager->GetNetworkClient(id);
  if (network_client) {
    //Get Streaming Function
    ccm::shared_ptr<ccm::StreamingFunction> streaming_function = network_client->GetStreamingFunction();
    
    // 設定ファイルが最優先
    if (streaming_params.qos_interfaces.size() == 0 || streaming_params.meta_interfaces.size() == 0) {

      // NCから通知されるパラメータを使用
      streaming_params.qos_interfaces = streaming_function->GetStreamingInterface();
      streaming_params.meta_interfaces = streaming_function->GetStreamingInterface();

      // 何も指定が無い場合、WLANになる可能性の高いNIC2を指定する
      if (streaming_params.qos_interfaces.size() == 0 || streaming_params.meta_interfaces.size() == 0) {
        streaming_params.qos_interfaces.push_back(*network_client->GetNetworkInterfaces()[1]->GetID());
        streaming_params.meta_interfaces.push_back(*network_client->GetNetworkInterfaces()[1]->GetID());
      }
    }

    

    if (streaming_function) {
      if (streaming_function->StartQosStreamingSend(streaming_params) == ccm::CCM_OK) {
        printf("StartStreaming Success\n");
      } else {
        printf("StartStreaming Error\n");
      }
    } else {
      printf("StartStreaming Error. StreamingFunction NotFound\n");
    }
  } else {
    printf("StartStreaming Error. NetworkClient NotFound\n");
  }
};

void ccm_cui_commands::StartStreamingRecieve(ccm_cui::args cui_args) {
  std::vector<std::string> v = commandArgs(cui_args.command);
  CCM_LOG_INFO("CCMCUI", "[IN] Command:[%s]", cui_args.command.c_str());

  std::string PNC = "";
  std::string PNCCNF = "";
  std::string PNC_MS = "";

  std::string SNC = "";
  std::string SNCCNF = "";
  std::string SNC_MS = "";
  std::string CONFFILE = "streaming.cfg";
  //printf("v.size() = %d",v.size());
  if (v.size() == 2) {
    PNC = v[1];
    PNCCNF = v[1];
  } else if (v.size() == 3) {
    PNC = v[1];
    PNCCNF = v[1];
    CONFFILE = v[2];
  } else if (v.size() == 4) {
    PNC = v[1];
    PNCCNF = v[1];
    SNC = v[2];
    SNCCNF = v[2];
    CONFFILE = v[3];
  } else {
    //continue;
  }

  std::string fpath = cui_args.rootpath.substr(0, cui_args.rootpath.find_last_of(ccm_cui::separator_) + 1);
  fpath += CONFFILE;

  std::string configStr = ccm_setting::readConfigFile(fpath.c_str());

  ccm::QosStreamingRecieveConfig streaming_params = ccm_setting::getQosStreamingRecieveConfigFromFile(PNCCNF.c_str(), configStr);

  // printf("cui_args.names.size:%d\n", cui_args.names.size());
  //printf("%s\n", fpath.c_str());

  std::string name;
  if (!cui_args.names[PNC.c_str()].empty()) {
    name = cui_args.names[PNC.c_str()];
  } else {
    name = PNC.c_str();
  }
  std::string model_name = name.substr(0, name.find('_'));
  std::string serial_number = name.substr(name.find('_') + 1);
  ccm::NetworkClientID id = ccm::NetworkClientID(&model_name, &serial_number);

  //printf("%s\n", id.GetID().c_str());

  //Get Netowork Client
  ccm::shared_ptr<ccm::NetworkClient> network_client = cui_args.connection_control_manager->GetNetworkClient(id);
  if (network_client) {
    //Get Streaming Function
    ccm::shared_ptr<ccm::StreamingFunction> streaming_function = network_client->GetStreamingFunction();

    // 設定ファイルが最優先
    if (streaming_params.qos_interfaces.size() == 0 || streaming_params.meta_interfaces.size() == 0) {

      // NCから通知されるパラメータを使用
      streaming_params.qos_interfaces = streaming_function->GetStreamingInterface();
      streaming_params.meta_interfaces = streaming_function->GetStreamingInterface();

      // 何も指定が無い場合、WLANになる可能性の高いNIC2を指定する
      if (streaming_params.qos_interfaces.size() == 0 || streaming_params.meta_interfaces.size() == 0) {
        streaming_params.qos_interfaces.push_back(*network_client->GetNetworkInterfaces()[1]->GetID());
        streaming_params.meta_interfaces.push_back(*network_client->GetNetworkInterfaces()[1]->GetID());
      }
    }

    if (streaming_function) {
      if (streaming_function->StartQosStreamingRecieve(streaming_params) == ccm::CCM_OK) {
        printf("StartStreaming Success\n");
      } else {
        printf("StartStreaming Error\n");
      }
    } else {
      printf("StartStreaming Error. StreamingFunction NotFound\n");
    }
  } else {
    printf("StartStreaming Error. NetworkClient NotFound\n");
  }
};
void ccm_cui_commands::StopStreaming(ccm_cui::args cui_args) {
  CCM_LOG_INFO("CCMCUI", "[IN] Command:[%s]", cui_args.command.c_str());
  std::vector<std::string> v = commandArgs(cui_args.command);
  std::string name;

  //cout << "OPT \: " << v[1].c_str() << endl;

  if (!cui_args.names[v[1].c_str()].empty()) {
    name = cui_args.names[v[1].c_str()];
  } else {
    name = v[1].c_str();
  }

  std::string model_name = name.substr(0, name.find('_'));
  std::string serial_number = name.substr(name.find('_') + 1);
  ccm::NetworkClientID id = ccm::NetworkClientID(&model_name, &serial_number);

  //Get Netowork Client
  ccm::shared_ptr<ccm::NetworkClient> network_client = cui_args.connection_control_manager->GetNetworkClient(id);

  if (network_client) {
    //Get Streaming Function
    ccm::shared_ptr<ccm::StreamingFunction> streaming_function = network_client->GetStreamingFunction();
    if (streaming_function) {
      if (streaming_function->StopStreamingSend() == ccm::CCM_OK) {
        printf("StopStreaming Success\n");
      } else {
        printf("StopStreaming Error\n");
      }
    }
  } else {
    printf("StartStreaming Error. NetworkClient NotFound\n");
  }
};

void ccm_cui_commands::StartThumbnail(ccm_cui::args cui_args) {
  std::vector<std::string> v = commandArgs(cui_args.command);
  CCM_LOG_INFO("CCMCUI", "[IN] Command:[%s]", cui_args.command.c_str());

  std::string name;
  if (!cui_args.names[v[1].c_str()].empty()) {
    name = cui_args.names[v[1].c_str()];
  } else {
    name = v[1].c_str();
  }
  std::string model_name = name.substr(0, name.find('_'));
  std::string serial_number = name.substr(name.find('_') + 1);
  ccm::NetworkClientID id = ccm::NetworkClientID(&model_name, &serial_number);

  //Get Netowork Client
  ccm::shared_ptr<ccm::NetworkClient> network_client = cui_args.connection_control_manager->GetNetworkClient(id);

  if (network_client) {
    //Get Streaming Function
    ccm::shared_ptr<ccm::StreamingFunction> streaming_function = network_client->GetStreamingFunction();
    if (streaming_function) {
      if (streaming_function->StartThumbnail() == ccm::CCM_OK) {
        printf("StartThumbnail Success\n");
      } else {
        printf("StartThumbnail Error\n");
      }
    } else {
      printf("StartThumbnail Error. Not Support\n");
    }
  } else {
    printf("StartStreaming Error. NetworkClient NotFound\n");
  }
}

void ccm_cui_commands::StopThumbnail(ccm_cui::args cui_args) {
  std::vector<std::string> v = commandArgs(cui_args.command);
  CCM_LOG_INFO("CCMCUI", "[IN] Command:[%s]", cui_args.command.c_str());

  std::string name;
  if (!cui_args.names[v[1].c_str()].empty()) {
    name = cui_args.names[v[1].c_str()];
  } else {
    name = v[1].c_str();
  }
  std::string model_name = name.substr(0, name.find('_'));
  std::string serial_number = name.substr(name.find('_') + 1);
  ccm::NetworkClientID id = ccm::NetworkClientID(&model_name, &serial_number);

  //Get Netowork Client
  ccm::shared_ptr<ccm::NetworkClient> network_client = cui_args.connection_control_manager->GetNetworkClient(id);

  if (network_client) {
    //Get Streaming Function
    ccm::shared_ptr<ccm::StreamingFunction> streaming_function = network_client->GetStreamingFunction();
    if (streaming_function) {
      if (streaming_function->StopThumbnail() == ccm::CCM_OK) {
        printf("StopThumbnail Success\n");
      } else {
        printf("StopThumbnail Error\n");
      }
    } else {
      printf("StartThumbnail Error. Not Support\n");
    }
  } else {
    printf("StartStreaming Error. NetworkClient NotFound\n");
  }
}

void ccm_cui_commands::SetUploadServerCert(ccm_cui::args cui_args) {
  std::vector<std::string> v = commandArgs(cui_args.command);
  CCM_LOG_INFO("CCMCUI", "[IN] Command:[%s]", cui_args.command.c_str());

  if (v.size() > 1) {
    std::vector<std::string> upload_cert;
    if (ccm_setting::parseFileTransferConfig(v[1].c_str(), nullptr, &upload_cert, nullptr) == ccm::CCM_OK) {
      if (ccm_cui::connection_control_manager_->RegistUploadServerCert(upload_cert) == ccm::CCM_OK) {
        printf("SetUploadServerCert Success\n");
      } else {
        printf("SetUploadServerCert API Error\n");
      }
    } else {
      printf("SetUploadDestinationConfig Params Error\n");
    }
  } else {
    printf("SetUploadServerCert Params Error\n");
  }
}
void ccm_cui_commands::SetMCBServerCert(ccm_cui::args cui_args) {
  std::vector<std::string> v = commandArgs(cui_args.command);
  CCM_LOG_INFO("CCMCUI", "[IN] Command:[%s]", cui_args.command.c_str());

  if (v.size() > 1) {
    std::vector<std::string> mcb_cert;
    if (ccm_setting::parseFileTransferConfig(v[1].c_str(), nullptr, nullptr, &mcb_cert) == ccm::CCM_OK) {
      if (ccm_cui::connection_control_manager_->RegistMCBServerCert(mcb_cert) == ccm::CCM_OK) {
        printf("SetMCBServerCert Success\n");
      } else {
        printf("SetMCBServerCert API Error\n");
      }
    } else {
      printf("SetUploadDestinationConfig Params Error\n");
    }
  } else {
    printf("SetMCBServerCert Params Error\n");
  }
}
void ccm_cui_commands::SetUploadDestinationConfig(ccm_cui::args cui_args) {
  std::vector<std::string> v = commandArgs(cui_args.command);
  CCM_LOG_INFO("CCMCUI", "[IN] Command:[%s]", cui_args.command.c_str());

  if (v.size() > 1) {
    ccm::UploadDestinationConfig config;
    if (ccm_setting::parseFileTransferConfig(v[1].c_str(), &config, nullptr, nullptr) == ccm::CCM_OK) {
      if (ccm_cui::connection_control_manager_->RegistUploadDestinationConfig(config) == ccm::CCM_OK) {
        printf("SetUploadDestinationConfig Success\n");
      } else {
        printf("SetUploadDestinationConfig API Error\n");
      }
    } else {
      printf("SetUploadDestinationConfig Params Error\n");
    }
  } else {
    printf("SetUploadDestinationConfig Params Error\n");
  }
}

void ccm_cui_commands::GetPlanningMetadataList(ccm_cui::args cui_args) {
  std::vector<std::string> v = commandArgs(cui_args.command);
  CCM_LOG_INFO("CCMCUI", "[IN] Command:[%s]", cui_args.command.c_str());

  if (v.size() > 1) {
    std::string name;
    if (!cui_args.names[v[1].c_str()].empty()) {
      name = cui_args.names[v[1].c_str()];
    } else {
      name = v[1].c_str();
    }
    std::string model_name = name.substr(0, name.find('_'));
    std::string serial_number = name.substr(name.find('_') + 1);
    ccm::NetworkClientID id = ccm::NetworkClientID(&model_name, &serial_number);

    //Get Netowork Client
    ccm::shared_ptr<ccm::NetworkClient> network_client = cui_args.connection_control_manager->GetNetworkClient(id);
    if (network_client) {
      //Get Streaming Function
      ccm::shared_ptr<ccm::RecorderFunction> recorder_function = network_client->GetRecorderFunction();
      if (recorder_function) {
        //printf("GetPlanningMetadataList Calling...\n");
        std::vector<ccm::PlanningMetadataFile> res_data;
        if (recorder_function->GetPlanningMetadataList(&res_data) == ccm::CCM_OK) {
          printf("GetPlanningMetadataList OK. Datasize:%zu\n", res_data.size());
          for (ccm::PlanningMetadataFile data : res_data) {
#ifdef _WIN32
            if ((int)((const char)*"あ" & 0xFF) == (int)0x82) {
              char file_name[256];
              char file_uri[256];
              UTF8toSJIS(data.name.c_str(), file_name, 256);
              UTF8toSJIS(data.uri.c_str(), file_uri, 256);

              printf("%s:%s\n", file_name, file_uri);
            } else {
              printf("%s:%s\n", data.name.c_str(), data.uri.c_str());
            }
#else
            printf("%s:%s\n", data.name.c_str(), data.uri.c_str());
#endif

          }
        } else {
          printf("GetPlanningMetadataList Http Request Error\n");
        }
      } else {
        printf("GetPlanningMetadataList Error. Not Support\n");
      }
    } else {
      printf("GetPlanningMetadataList NC Not Found\n");
    }
  } else {
    printf("GetPlanningMetadataList Params Error\n");
  }
}

void ccm_cui_commands::GetPlanningMetadataDetail(ccm_cui::args cui_args) {
  std::vector<std::string> v = commandArgs(cui_args.command);
  CCM_LOG_INFO("CCMCUI", "[IN] Command:[%s]", cui_args.command.c_str());

  if (v.size() > 2) {
    std::string name;
    std::string uri = v[2];

    #ifdef _WIN32
    if ((int)((const char)*"あ" & 0xFF) == (int)0x82) {
      CCM_LOG_INFO("CCMCUI", "%s", "Character Code:SJIS");
      //uri = SJIStoUTF8(uri.c_str(), 256);
	  std::string uri_tmp;
	  SJIStoUTF8(uri.c_str(), 256, uri_tmp);
	  uri = uri_tmp;
	} else {
      CCM_LOG_INFO("CCMCUI", "%s", "Character Code:Unknown");
    }
    #else

    #endif

    if (!cui_args.names[v[1].c_str()].empty()) {
      name = cui_args.names[v[1].c_str()];
    } else {
      name = v[1].c_str();
    }

    std::string model_name = name.substr(0, name.find('_'));
    std::string serial_number = name.substr(name.find('_') + 1);
    ccm::NetworkClientID id = ccm::NetworkClientID(&model_name, &serial_number);

    //Get Netowork Client
    ccm::shared_ptr<ccm::NetworkClient> network_client = cui_args.connection_control_manager->GetNetworkClient(id);
    if (network_client) {
      //Get Streaming Function
      ccm::shared_ptr<ccm::RecorderFunction> recorder_function = network_client->GetRecorderFunction();
      if (recorder_function) {
        //printf("GetPlanningMetadataDetail Calling...\n");
        std::string res_data;
        if (recorder_function->GetPlanningMetadataDetail(uri, &res_data) == ccm::CCM_OK) {
          //printf("%s\n", res_data.c_str());
          printf("GetPlanningMetadataDetaiAuthenticationHandlerResultl OK. Datasize:%zu\n", res_data.size());
        } else {
          printf("GetPlanningMetadataDetail Http Request Error\n");
        }
      } else {
        printf("GetPlanningMetadataDetail Error. Not Support\n");
      }
    } else {
      printf("GetPlanningMetadataList NC Not Found\n");
    }
  } else {
    printf("GetPlanningMetadataDetail Params Error\n");
  }
}

void ccm_cui_commands::GetCurrentPlanningMetadata(ccm_cui::args cui_args) {
  std::vector<std::string> v = commandArgs(cui_args.command);
  CCM_LOG_INFO("CCMCUI", "[IN] Command:[%s]", cui_args.command.c_str());

  if (v.size() > 1) {
    std::string name;
    if (!cui_args.names[v[1].c_str()].empty()) {
      name = cui_args.names[v[1].c_str()];
    } else {
      name = v[1].c_str();
    }
    std::string model_name = name.substr(0, name.find('_'));
    std::string serial_number = name.substr(name.find('_') + 1);
    ccm::NetworkClientID id = ccm::NetworkClientID(&model_name, &serial_number);

    //Get Netowork Client
    ccm::shared_ptr<ccm::NetworkClient> network_client = cui_args.connection_control_manager->GetNetworkClient(id);
    if (network_client) {
      //Get Streaming Function
      ccm::shared_ptr<ccm::RecorderFunction> recorder_function = network_client->GetRecorderFunction();
      if (recorder_function) {
        //printf("GetCurrentPlanningMetadata Calling...\n");
        std::string res_data;
        if (recorder_function->GetCurrentPlanningMetadata(&res_data) == ccm::CCM_OK) {
          if (res_data.length() > 0) {
            printf("GetCurrentPlanningMetadata OK. Datasize:%zu\n", res_data.size());
          } else {
            printf("GetCurrentPlanningMetadata Http Request Error\n");
          }
        } else {
          printf("GetCurrentPlanningMetadata Http Request Error\n");
        }
      } else {
        printf("GetCurrentPlanningMetadata Error. Not Support\n");
      }
    } else {
      printf("GetPlanningMetadataList NC Not Found\n");
    }
  } else {
    printf("GetCurrentPlanningMetadata Params Error\n");
  }
}

void ccm_cui_commands::SetPlanningMetadata(ccm_cui::args cui_args) {
  std::vector<std::string> v = commandArgs(cui_args.command);
  CCM_LOG_INFO("CCMCUI", "[IN] Command:[%s]", cui_args.command.c_str());

  if (v.size() > 2) {
    std::string name;
    if (!cui_args.names[v[1].c_str()].empty()) {
      name = cui_args.names[v[1].c_str()];
    } else {
      name = v[1].c_str();
    }
    std::string uri = v[2];

#ifdef _WIN32
    if ((int)((const char)*"あ" & 0xFF) == (int)0x82) {
      CCM_LOG_INFO("CCMCUI", "%s", "Character Code:SJIS");
      //uri = SJIStoUTF8(uri.c_str(), 256);
	  std::string uri_tmp;
	  SJIStoUTF8(uri.c_str(), 256, uri_tmp);
	  uri = uri_tmp;
	} else {
      CCM_LOG_INFO("CCMCUI", "%s", "Character Code:Unknown");
    }
#else

#endif

    std::string model_name = name.substr(0, name.find('_'));
    std::string serial_number = name.substr(name.find('_') + 1);
    ccm::NetworkClientID id = ccm::NetworkClientID(&model_name, &serial_number);

    //Get Netowork Client
    ccm::shared_ptr<ccm::NetworkClient> network_client = cui_args.connection_control_manager->GetNetworkClient(id);
    if (network_client) {
      //Get Streaming Function
      ccm::shared_ptr<ccm::RecorderFunction> recorder_function = network_client->GetRecorderFunction();
      if (recorder_function) {
        //printf("SetPlanningMetadata Calling...\n");
        std::string res_data;
        if (recorder_function->SetPlanningMetadata(&res_data, uri) == ccm::CCM_OK) {
          printf("SetPlanningMetadata OK.\n");
        } else {
          if (res_data.length() > 0) {
            printf("SetPlanningMetadata Http Request Error [%s]\n", res_data.c_str());
          } else {
            printf("SetPlanningMetadata Http Request Error\n");
          }
        }
      } else {
        printf("SetPlanningMetadata Error. Not Support\n");
      }
    } else {
      printf("SetPlanningMetadata NC Not Found\n");
    }
  } else {
    printf("SetPlanningMetadata Params Error\n");
  }
}

void ccm_cui_commands::PutPlanningMetadata(ccm_cui::args cui_args) {
  std::vector<std::string> v = commandArgs(cui_args.command);
  CCM_LOG_INFO("CCMCUI", "[IN] Command:[%s]", cui_args.command.c_str());

  if (v.size() > 3) {
    std::string name;
    if (!cui_args.names[v[1].c_str()].empty()) {
      name = cui_args.names[v[1].c_str()];
    } else {
      name = v[1].c_str();
    }
    std::string file_name = v[2];
    std::string xml_data = v[3];

#ifdef _WIN32
    if ((int)((const char)*"あ" & 0xFF) == (int)0x82) {
      CCM_LOG_INFO("CCMCUI", "%s", "Character Code:SJIS");
      //file_name = SJIStoUTF8(file_name.c_str(), 1024);
	  std::string file_name_tmp;
	  SJIStoUTF8(file_name.c_str(), 1024, file_name_tmp);
	  file_name = file_name_tmp;
	  //xml_data = SJIStoUTF8(xml_data.c_str(), 1024);
	  std::string xml_data_tmp;
	  SJIStoUTF8(xml_data.c_str(), 1024, xml_data_tmp);
	  xml_data = xml_data_tmp;
	} else {
      CCM_LOG_INFO("CCMCUI", "%s", "Character Code:Unknown");
    }
#else

#endif

    std::string model_name = name.substr(0, name.find('_'));
    std::string serial_number = name.substr(name.find('_') + 1);
    ccm::NetworkClientID id = ccm::NetworkClientID(&model_name, &serial_number);

    //Get Netowork Client
    ccm::shared_ptr<ccm::NetworkClient> network_client = cui_args.connection_control_manager->GetNetworkClient(id);
    if (network_client) {
      //Get Streaming Function
      ccm::shared_ptr<ccm::RecorderFunction> recorder_function = network_client->GetRecorderFunction();
      if (recorder_function) {
        //printf("SetPlanningMetadata Calling...\n");
        std::string res_data;
        ccm::PutPlanningMetadataFile pmfile{ file_name.c_str(),xml_data,"",false };
        //file.name = file_name;
        //file.xml = xml_data;
        if (recorder_function->PutPlanningMetadata(&res_data, pmfile) == ccm::CCM_OK) {
          printf("PutPlanningMetadata OK\n");
        } else {
          if (res_data.length() > 0) {
            printf("PutPlanningMetadata Http Request Error [%s]\n", res_data.c_str());
          } else {
            printf("PutPlanningMetadata Http Request Error\n");
          }
        }
      } else {
        printf("PutPlanningMetadata Error. Not Support\n");
      }
    } else {
      printf("PutPlanningMetadata NC Not Found\n");
    }
  } else {
    printf("PutPlanningMetadata Params Error\n");
  }
}
