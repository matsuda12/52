#pragma once

#include <string>
#include <picojson.h>
//
#include "../ccmsdk/connection_control_manager.h"

namespace ccm {

	class CcmJsonParser
	{
	public:
		CcmJsonParser();
		~CcmJsonParser();

        /**
        * Create Notify Json
        */
        static std::string CreateNetworkClientConnectNotify(const ccm::NetworkClientID& id, const bool& connect);
        static std::string CreateVoIPStatusNotify(const ccm::NetworkClientID& id, const std::string& status);
        static std::string CreateStreamingStatusNotify(const ccm::NetworkClientID& id, const std::string& status);
		static std::string CreateStreamingDynamicPropertyNotify(const ccm::NetworkClientID& id, const std::vector<ccm::StreamingDynamicProperty>& params);
		static std::string CreateRecieveThumbnailNotify(const ccm::NetworkClientID& id, const ccm::Thumbnail& thumbnail, const char* const mime, const char* const base64);
		static std::string CreateMcbNetworkInfoNotify(const ccm::NetworkClientID& id, const std::vector<ccm::McbNetworkInfo>& infos);
		static std::string CreateNetworkInfoNotify(const ccm::NetworkClientID& id, const ccm::NetworkInfo& networkInfo);
		static std::string CreateGPSInfoNotify(const ccm::NetworkClientID& id, const ccm::GPSInfo& gpsInfo);
		static std::string CreateRecorderControlStatusNotify(const ccm::NetworkClientID& id, const ccm::RecorderControlStatus& gpsInfo);
		static std::string CreateClipRecorderStatusNotify(const ccm::NetworkClientID& id, const std::map<std::string, ccm::ClipRecorderStatus>& result);
		static std::string CreateDriveStatusNotify(const ccm::NetworkClientID& id, const std::vector<ccm::Drive>& result);
		static std::string CreatePlanningMetadataControlStatusNotify(const ccm::NetworkClientID& id, const bool& status);

        /**
        * Create Response Json
        */
		static std::string CreateGetNetworkClientResponse(const NetworkClientID& id, const ccm::shared_ptr<NetworkClient>& nc);
		static std::string CreateGetNetworkClientListResponse(const std::vector<ccm::shared_ptr<NetworkClient>>& list);
		static std::string CreateGetClipListResponse(const NetworkClientID& id, const std::vector<Clip>& clips);
        static std::string CreateGetUploadListResponse(const NetworkClientID& id, const std::vector<Upload>& files);
        static std::string CreateAddUploadFilesResponse(const NetworkClientID& id, const ccm::ErrorCode& error);
		static std::string CreateStartQosStreamingSendResponse(const NetworkClientID& id, const ccm::ErrorCode& error);
		static std::string CreateStopStreamingSendResponse(const NetworkClientID& id, const ccm::ErrorCode& error);
		static std::string CreateStartQosStreamingReceiveResponse(const NetworkClientID& id, const ccm::ErrorCode& error);
		static std::string CreateStopStreamingRecieveResponse(const NetworkClientID& id, const ccm::ErrorCode& error);
		static std::string CreateGetPlanningMetadataListResponse(const NetworkClientID& id, const std::vector<ccm::PlanningMetadataFile>& list, const ccm::ErrorCode& error);
		static std::string CreateGetCurrentPlanningMetadataResponse(const NetworkClientID& id, const std::string& metadata, const ccm::ErrorCode& error);
		static std::string CreateGetPlanningMetadataResponse(const NetworkClientID& id, const std::string& name, const std::string& uri, const std::string& metadata, const ccm::ErrorCode& error);
		static std::string CreateSetPlanningMetadataResponse(const NetworkClientID& id, const ccm::ErrorCode& error, const std::string& internalError, const std::string& fileName, const std::string& uri);
		static std::string CreatePutPlanningMetadataResponse(const NetworkClientID& id, const ccm::ErrorCode& error, const std::string& internalError, const std::string& fileName);
		static std::string CreateStartVoIPResponse(const NetworkClientID& id, const ccm::ErrorCode& error);
		static std::string CreateStopVoIPResponse(const NetworkClientID& id, const ccm::ErrorCode& error);

        /**
        * Create Result Json
        */
        static std::string CreateAddUploadFilesResult(const NetworkClientID& id, const std::vector<ccm::AsyncResult>& results);

	private:
		static picojson::object createNetworkClientResponse(const NetworkClientID& id, const ccm::shared_ptr<NetworkClient>& nc);
	};
}
