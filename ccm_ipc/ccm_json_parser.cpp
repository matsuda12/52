#include "ccm_json_parser.h"
#include <ctime>
#include <time.h>
#include "../include/log_manager.h"

namespace ccm {

	CcmJsonParser::CcmJsonParser()
	{
	}

	CcmJsonParser::~CcmJsonParser()
	{
	}

	std::string CcmJsonParser::CreateNetworkClientConnectNotify(const ccm::NetworkClientID& id, const bool& connect)
	{
		CCM_LOG_INFO("CCMCUI", "[json] CreateNetworkClientConnectNotify [NetworkClientID:%s]", id.GetID().c_str());

		picojson::object jobj;
		jobj.emplace(std::make_pair("command", picojson::value("NetworkClientConnect")));
		jobj.emplace(std::make_pair("type", picojson::value("notify")));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateNetworkClientConnectNotify create jobjData");
		picojson::object jobjData;
		{
			jobjData.emplace(std::make_pair("ID", picojson::value(id.GetID())));
			jobjData.emplace(std::make_pair("Status", picojson::value((connect ? "connect" : "disconnect"))));
		}
		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateNetworkClientConnectNotify add data : data");
		jobj.emplace(std::make_pair("data", picojson::value(jobjData)));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateNetworkClientConnectNotify create json value - valResponse");
		picojson::value valResponse(jobj);
		CCM_LOG_INFO("CCMCUI", "[json] CreateNetworkClientConnectNotify return json value : %s", valResponse.serialize().c_str());
		return valResponse.serialize();
	}

	std::string CcmJsonParser::CreateVoIPStatusNotify(const ccm::NetworkClientID& id, const std::string& status)
	{
		CCM_LOG_INFO("CCMCUI", "[json] CreateVoIPStatusNotify [NetworkClientID:%s]", id.GetID().c_str());

		picojson::object jobj;
		jobj.emplace(std::make_pair("command", picojson::value("VoIPStatus")));
		jobj.emplace(std::make_pair("type", picojson::value("notify")));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateVoIPStatusNotify create dataArray");
		picojson::array dataArray;
		{
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateVoIPStatusNotify create jobjStatus");
			picojson::object jobjStatus;
			jobjStatus.emplace(std::make_pair("ID", picojson::value(id.GetID())));
			jobjStatus.emplace(std::make_pair("Status", picojson::value(status)));
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateVoIPStatusNotify add data : dataArray");
			dataArray.push_back(picojson::value(jobjStatus));
		}
		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateVoIPStatusNotify add data : data");
		jobj.emplace(std::make_pair("data", picojson::value(dataArray)));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateVoIPStatusNotify create json value - valResponse");
		picojson::value valResponse(jobj);
		CCM_LOG_INFO("CCMCUI", "[json] CreateVoIPStatusNotify return json value : %s", valResponse.serialize().c_str());
		return valResponse.serialize();
	}

	std::string CcmJsonParser::CreateStreamingStatusNotify(const ccm::NetworkClientID& id, const std::string& status)
	{
		CCM_LOG_INFO("CCMCUI", "[json] CreateStreamingStatusNotify [NetworkClientID:%s]", id.GetID().c_str());

		picojson::object jobj;
		jobj.emplace(std::make_pair("command", picojson::value("StreamingStatus")));
		jobj.emplace(std::make_pair("type", picojson::value("notify")));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStreamingStatusNotify create dataArray");
		picojson::array dataArray;
		{
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStreamingStatusNotify create jobjStatus");
			picojson::object jobjStatus;
			jobjStatus.emplace(std::make_pair("ID", picojson::value(id.GetID())));
			jobjStatus.emplace(std::make_pair("Status", picojson::value(status)));
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStreamingStatusNotify add data : dataArray");
			dataArray.push_back(picojson::value(jobjStatus));
		}
		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStreamingStatusNotify add data : data");
		jobj.emplace(std::make_pair("data", picojson::value(dataArray)));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStreamingStatusNotify create json value - valResponse");
		picojson::value valResponse(jobj);
		CCM_LOG_INFO("CCMCUI", "[json] CreateStreamingStatusNotify return json value : %s", valResponse.serialize().c_str());
		return valResponse.serialize();
	}

	std::string CcmJsonParser::CreateStreamingDynamicPropertyNotify(const ccm::NetworkClientID& id, const std::vector<ccm::StreamingDynamicProperty>& params)
	{
		CCM_LOG_INFO("CCMCUI", "[json] CreateStreamingDynamicPropertyNotify [NetworkClientID:%s]", id.GetID().c_str());

		picojson::object jobj;
		jobj.emplace(std::make_pair("command", picojson::value("StreamingDynamicProperty")));
		jobj.emplace(std::make_pair("type", picojson::value("notify")));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStreamingDynamicPropertyNotify create dataArray");
		picojson::array dataArray;
		{
			for (const auto& param : params) {
				CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStreamingDynamicPropertyNotify create jobjProperty");
				picojson::object jobjProperty;
				jobjProperty.emplace(std::make_pair("ID", picojson::value(id.GetID())));
				jobjProperty.emplace(std::make_pair("nic_name", picojson::value(param.nic_name)));
				jobjProperty.emplace(std::make_pair("rate", picojson::value((double)param.rate)));
				jobjProperty.emplace(std::make_pair("loss_rate", picojson::value((double)param.loss_rate)));
				jobjProperty.emplace(std::make_pair("rtt", picojson::value((double)param.rtt)));
				jobjProperty.emplace(std::make_pair("delay", picojson::value((double)param.delay)));
				CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStreamingDynamicPropertyNotify add data : dataArray");
				dataArray.push_back(picojson::value(jobjProperty));
			}
		}
		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStreamingDynamicPropertyNotify add data : data");
		jobj.emplace(std::make_pair("data", picojson::value(dataArray)));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStreamingDynamicPropertyNotify create json value - valResponse");
		picojson::value valResponse(jobj);
		CCM_LOG_INFO("CCMCUI", "[json] CreateStreamingDynamicPropertyNotify return json value : %s", valResponse.serialize().c_str());
		return valResponse.serialize();
	}

	std::string CcmJsonParser::CreateRecieveThumbnailNotify(const ccm::NetworkClientID& id, const ccm::Thumbnail& thumbnail, const char* const mime, const char* const base64)
	{
		CCM_LOG_INFO("CCMCUI", "[json] CreateRecieveThumbnailNotify [NetworkClientID:%s]", id.GetID().c_str());

		picojson::object jobj;
		jobj.emplace(std::make_pair("command", picojson::value("ReceiveThumbnail")));
		jobj.emplace(std::make_pair("type", picojson::value("notify")));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateRecieveThumbnailNotify create jobjData");
		picojson::object jobjData;
		{
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateRecieveThumbnailNotify add data : ID");
			jobjData.emplace(std::make_pair("ID", picojson::value(id.GetID())));
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateRecieveThumbnailNotify add data : thumbnail");
			jobjData.emplace(std::make_pair("thumbnail", picojson::value(thumbnail.thumbnail)));
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateRecieveThumbnailNotify add data : thumbnail_base64");
			jobjData.emplace(std::make_pair("thumbnail_base64", picojson::value(base64)));
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateRecieveThumbnailNotify add data : mime");
			jobjData.emplace(std::make_pair("mime", picojson::value(mime)));
		}
		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateRecieveThumbnailNotify add data : data");
		jobj.emplace(std::make_pair("data", picojson::value(jobjData)));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateRecieveThumbnailNotify create json value - valResponse");
		picojson::value valResponse(jobj);
		CCM_LOG_INFO("CCMCUI", "[json] CreateRecieveThumbnailNotify return json value : %s", valResponse.serialize().c_str());
		return valResponse.serialize();
	}

	std::string CcmJsonParser::CreateMcbNetworkInfoNotify(const ccm::NetworkClientID& id, const std::vector<ccm::McbNetworkInfo>& infos)
	{
		CCM_LOG_INFO("CCMCUI", "[json] CreateMcbNetworkInfoNotify [NetworkClientID:%s]", id.GetID().c_str());

		picojson::object jobj;
		jobj.emplace(std::make_pair("command", picojson::value("McbNetworkInfo")));
		jobj.emplace(std::make_pair("type", picojson::value("notify")));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateMcbNetworkInfoNotify create jobjData");
		picojson::object jobjData;
		{
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateMcbNetworkInfoNotify create dataArrayMcbNetworkInfo");
			picojson::array dataArrayMcbNetworkInfo;
			std::vector<ccm::McbNetworkInfo>::const_iterator itBegin = infos.begin();
			std::vector<ccm::McbNetworkInfo>::const_iterator itEnd = infos.end();
			for (; itBegin < itEnd; ++itBegin) {
				picojson::object jobjMcbNetworkInfo;
				double rate = itBegin->rate;
				double loss = itBegin->loss;
				double rtt = itBegin->rtt;
				jobjMcbNetworkInfo.emplace(std::make_pair("ID", picojson::value(itBegin->id)));
				jobjMcbNetworkInfo.emplace(std::make_pair("rate", picojson::value(rate)));
				jobjMcbNetworkInfo.emplace(std::make_pair("loss", picojson::value(loss)));
				jobjMcbNetworkInfo.emplace(std::make_pair("rtt", picojson::value(rtt)));
				dataArrayMcbNetworkInfo.push_back(picojson::value(jobjMcbNetworkInfo));
			}

			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateMcbNetworkInfoNotify add data : ID");
			jobjData.emplace(std::make_pair("ID", picojson::value(id.GetID())));
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateMcbNetworkInfoNotify add data : McbNetworkInfo");
			jobjData.emplace(std::make_pair("McbNetworkInfo", picojson::value(dataArrayMcbNetworkInfo)));
		}
		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateMcbNetworkInfoNotify add data : data");
		jobj.emplace(std::make_pair("data", picojson::value(jobjData)));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateMcbNetworkInfoNotify create json value - valResponse");
		picojson::value valResponse(jobj);
		CCM_LOG_INFO("CCMCUI", "[json] CreateMcbNetworkInfoNotify return json value : %s", valResponse.serialize().c_str());
		return valResponse.serialize();
	}

	std::string CcmJsonParser::CreateNetworkInfoNotify(const ccm::NetworkClientID& id, const ccm::NetworkInfo& networkInfo)
	{
		CCM_LOG_INFO("CCMCUI", "[json] CreateNetworkInfoNotify [NetworkClientID:%s]", id.GetID().c_str());

		picojson::object jobj;
		jobj.emplace(std::make_pair("command", picojson::value("NetworkInfo")));
		jobj.emplace(std::make_pair("type", picojson::value("notify")));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateNetworkInfoNotify create jobjData");
		picojson::object jobjData;
		{
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateNetworkInfoNotify create jobjCellularArray");
			picojson::array jobjCellularArray;
			{
				std::vector<ccm::shared_ptr<ccm::CellularInfo>>::const_iterator itr = networkInfo.cellular_info.begin();
				std::vector<ccm::shared_ptr<ccm::CellularInfo>>::const_iterator itrEnd = networkInfo.cellular_info.end();
				for (; itr != itrEnd; ++itr) {
					CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateNetworkInfoNotify create jobjCellular");
					picojson::object jobjCellular;

					std::string* pSignalStrength = (*itr)->GetSignalStrength();
					std::string* pSignalStrengthUnit = (*itr)->GetSignalStrengthUnit();
					std::string* pOperator = (*itr)->GetOperator();
					std::string* pPLMN = (*itr)->GetPLMN();
					std::string* pBand = (*itr)->GetBand();
					std::string* pCellID = (*itr)->GetCellID();
					std::string* pAirInterface = (*itr)->GetAirInterface();
					std::string* pManufacture = (*itr)->GetManufacture();
					std::string* pModelName = (*itr)->GetModelName();
					std::string* pIMEI = (*itr)->GetIMEI();
					jobjCellular.emplace(std::make_pair("SignalStrength", picojson::value(*pSignalStrength)));
					jobjCellular.emplace(std::make_pair("SignalStrengthUnit", picojson::value(*pSignalStrengthUnit)));
					jobjCellular.emplace(std::make_pair("Operator", picojson::value(*pOperator)));
					jobjCellular.emplace(std::make_pair("PLMN", picojson::value(*pPLMN)));
					jobjCellular.emplace(std::make_pair("Band", picojson::value(*pBand)));
					jobjCellular.emplace(std::make_pair("CellID", picojson::value(*pCellID)));
					jobjCellular.emplace(std::make_pair("AirInterface", picojson::value(*pAirInterface)));
					jobjCellular.emplace(std::make_pair("Manufacture", picojson::value(*pManufacture)));
					jobjCellular.emplace(std::make_pair("ModelName", picojson::value(*pModelName)));
					jobjCellular.emplace(std::make_pair("IMEI", picojson::value(*pIMEI)));

					CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateNetworkInfoNotify add data : jobjCellularArray");
					jobjCellularArray.push_back(picojson::value(jobjCellular));
				}
			}
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateNetworkInfoNotify add data : CellularInfo");
			jobjData.emplace(std::make_pair("CellularInfo", picojson::value(jobjCellularArray)));

			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateNetworkInfoNotify create jobjQosInterfaceType");
			picojson::object jobjQosInterfaceType;
			{
				std::map<std::string, std::string>::const_iterator itr = networkInfo.qos_interface_type.begin();
				std::map<std::string, std::string>::const_iterator itrEnd = networkInfo.qos_interface_type.end();
				for (; itr != itrEnd; ++itr) {
					jobjQosInterfaceType.emplace(std::make_pair(itr->first, picojson::value(itr->second)));
				}
			}
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateNetworkInfoNotify add data : QosInterfaceType");
			jobjData.emplace(std::make_pair("QosInterfaceType", picojson::value(jobjQosInterfaceType)));
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateNetworkInfoNotify add data : ID");
			jobjData.emplace(std::make_pair("ID", picojson::value(id.GetID())));
		}
		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateNetworkInfoNotify add data : data");
		jobj.emplace(std::make_pair("data", picojson::value(jobjData)));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateNetworkInfoNotify create json value - valResponse");
		picojson::value valResponse(jobj);
		CCM_LOG_INFO("CCMCUI", "[json] CreateNetworkInfoNotify return json value : %s", valResponse.serialize().c_str());
		return valResponse.serialize();
	}

	std::string CcmJsonParser::CreateGPSInfoNotify(const ccm::NetworkClientID& id, const ccm::GPSInfo& gpsInfo)
	{
		CCM_LOG_INFO("CCMCUI", "[json] CreateGPSInfoNotify [NetworkClientID:%s]", id.GetID().c_str());

		picojson::object jobj;
		jobj.emplace(std::make_pair("command", picojson::value("GPSInfo")));
		jobj.emplace(std::make_pair("type", picojson::value("notify")));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGPSInfoNotify create jobjData");
		picojson::object jobjData;
		{
#ifdef _WIN32
			struct tm tmData;
			struct tm* timeData = &tmData;
			gmtime_s(timeData, &gpsInfo.update_time);
#else
			struct tm* timeData = gmtime(&gpsInfo.update_time);
#endif
			char timeBuf[24];
			std::strftime(timeBuf, sizeof(timeBuf), "%FT%TZ", timeData);
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGPSInfoNotify add data : update_time");
			jobjData.emplace(std::make_pair("update_time", picojson::value(timeBuf)));
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGPSInfoNotify add data : latitude");
			jobjData.emplace(std::make_pair("latitude", picojson::value(gpsInfo.latitude)));
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGPSInfoNotify add data : longitude");
			jobjData.emplace(std::make_pair("longitude", picojson::value(gpsInfo.longitude)));
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGPSInfoNotify add data : ID");
			jobjData.emplace(std::make_pair("ID", picojson::value(id.GetID())));
		}
		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGPSInfoNotify add data : data");
		jobj.emplace(std::make_pair("data", picojson::value(jobjData)));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGPSInfoNotify create json value - valResponse");
		picojson::value valResponse(jobj);
		CCM_LOG_INFO("CCMCUI", "[json] CreateGPSInfoNotify return json value : %s", valResponse.serialize().c_str());
		return valResponse.serialize();
	}

	std::string CcmJsonParser::CreateRecorderControlStatusNotify(const ccm::NetworkClientID& id, const ccm::RecorderControlStatus& gpsInfo)
	{
		CCM_LOG_INFO("CCMCUI", "[json] CreateRecorderControlStatusNotify [NetworkClientID:%s]", id.GetID().c_str());

		picojson::object jobj;
		jobj.emplace(std::make_pair("command", picojson::value("RecorderControlStatus")));
		jobj.emplace(std::make_pair("type", picojson::value("notify")));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateRecorderControlStatusNotify create jobjData");
		picojson::object jobjData;
		{
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateRecorderControlStatusNotify add data : ID");
			jobjData.emplace(std::make_pair("ID", picojson::value(id.GetID())));
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateRecorderControlStatusNotify add data : low_resolution");
			jobjData.emplace(std::make_pair("low_resolution", picojson::value(gpsInfo.low_resolution)));
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateRecorderControlStatusNotify add data : high_resolution");
			jobjData.emplace(std::make_pair("high_resolution", picojson::value(gpsInfo.high_resolution)));
		}
		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateRecorderControlStatusNotify add data : data");
		jobj.emplace(std::make_pair("data", picojson::value(jobjData)));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateRecorderControlStatusNotify create json value - valResponse");
		picojson::value valResponse(jobj);
		CCM_LOG_INFO("CCMCUI", "[json] CreateRecorderControlStatusNotify return json value : %s", valResponse.serialize().c_str());
		return valResponse.serialize();
	}

	std::string CcmJsonParser::CreateClipRecorderStatusNotify(const ccm::NetworkClientID& id, const std::map<std::string, ccm::ClipRecorderStatus>& result)
	{
		CCM_LOG_INFO("CCMCUI", "[json] CreateClipRecorderStatusNotify [NetworkClientID:%s]", id.GetID().c_str());

		picojson::object jobj;
		jobj.emplace(std::make_pair("command", picojson::value("ClipRecorderStatus")));
		jobj.emplace(std::make_pair("type", picojson::value("notify")));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateClipRecorderStatusNotify create jobjData");
		picojson::object jobjData;
		{
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateClipRecorderStatusNotify create jobjResultArray");
			picojson::array jobjResultArray;
			std::map<std::string, ccm::ClipRecorderStatus>::const_iterator itBegin = result.begin();
			std::map<std::string, ccm::ClipRecorderStatus>::const_iterator itEnd = result.end();
			for (; itBegin != itEnd; ++itBegin) {
				std::string recorderStatus;
				switch (itBegin->second)
				{
				case ClipRecorderStatus::REC_ACTIVE:
					recorderStatus = "ACTIVE";
					break;
				case ClipRecorderStatus::REC_RECORDING:
					recorderStatus = "RECORDING";
					break;
				case ClipRecorderStatus::REC_INACTIVE:
				default:
					recorderStatus = "INACTIVE";
					break;
				};
				CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateClipRecorderStatusNotify create jobjStatus");
				picojson::object jobjStatus;
				jobjStatus.emplace(std::make_pair("recorder", picojson::value(itBegin->first)));
				jobjStatus.emplace(std::make_pair("status", picojson::value(recorderStatus)));
				//
				CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateClipRecorderStatusNotify add data : jobjResultArray");
				jobjResultArray.push_back(picojson::value(jobjStatus));
			}
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateClipRecorderStatusNotify add data : ID");
			jobjData.emplace(std::make_pair("ID", picojson::value(id.GetID())));
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateClipRecorderStatusNotify add data : result");
			jobjData.emplace(std::make_pair("result", picojson::value(jobjResultArray)));
		}
		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateClipRecorderStatusNotify add data : data");
		jobj.emplace(std::make_pair("data", picojson::value(jobjData)));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateClipRecorderStatusNotify create json value - valResponse");
		picojson::value valResponse(jobj);
		CCM_LOG_INFO("CCMCUI", "[json] CreateClipRecorderStatusNotify return json value : %s", valResponse.serialize().c_str());
		return valResponse.serialize();
	}

	std::string CcmJsonParser::CreateDriveStatusNotify(const ccm::NetworkClientID& id, const std::vector<ccm::Drive>& result)
	{
		CCM_LOG_INFO("CCMCUI", "[json] CreateDriveStatusNotify [NetworkClientID:%s]", id.GetID().c_str());

		picojson::object jobj;
		jobj.emplace(std::make_pair("command", picojson::value("DriveStatus")));
		jobj.emplace(std::make_pair("type", picojson::value("notify")));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateDriveStatusNotify create jobjData");
		picojson::object jobjData;
		{
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateDriveStatusNotify create jobjDriveArray");
			picojson::array jobjDriveArray;
			{
				std::vector<ccm::Drive>::const_iterator itBegin = result.begin();
				std::vector<ccm::Drive>::const_iterator itEnd = result.end();
				for (; itBegin != itEnd; ++itBegin) {
					CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateDriveStatusNotify create jobjDriveData");
					picojson::object jobjDriveData;
					jobjDriveData.emplace(std::make_pair("id", picojson::value(itBegin->id)));
					jobjDriveData.emplace(std::make_pair("status", picojson::value(itBegin->status)));
					jobjDriveData.emplace(std::make_pair("capacity", picojson::value((double)itBegin->capacity)));
					jobjDriveData.emplace(std::make_pair("available", picojson::value((double)itBegin->available)));
					CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateDriveStatusNotify add data : jobjDriveArray");
					jobjDriveArray.push_back(picojson::value(jobjDriveData));
				}
			}
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateDriveStatusNotify add data : ID");
			jobjData.emplace(std::make_pair("ID", picojson::value(id.GetID())));
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateDriveStatusNotify add data : drives");
			jobjData.emplace(std::make_pair("drives", picojson::value(jobjDriveArray)));
		}
		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateDriveStatusNotify add data : data");
		jobj.emplace(std::make_pair("data", picojson::value(jobjData)));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateDriveStatusNotify create json value - valResponse");
		picojson::value valResponse(jobj);
		CCM_LOG_INFO("CCMCUI", "[json] CreateDriveStatusNotify return json value : %s", valResponse.serialize().c_str());
		return valResponse.serialize();
	}

	std::string CcmJsonParser::CreatePlanningMetadataControlStatusNotify(const ccm::NetworkClientID& id, const bool& status)
	{
		CCM_LOG_INFO("CCMCUI", "[json] CreatePlanningMetadataControlStatusNotify [NetworkClientID:%s / status:%d]", id.GetID().c_str(), status);

		picojson::object jobj;
		jobj.emplace(std::make_pair("command", picojson::value("PlanningMetadataControlStatus")));
		jobj.emplace(std::make_pair("type", picojson::value("notify")));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreatePlanningMetadataControlStatusNotify create jobjData");
		picojson::object jobjData;
		{
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreatePlanningMetadataControlStatusNotify add data : ID");
			jobjData.emplace(std::make_pair("ID", picojson::value(id.GetID())));
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreatePlanningMetadataControlStatusNotify add data : PlanningMetadata");
			jobjData.emplace(std::make_pair("PlanningMetadata", picojson::value(status)));
		}
		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreatePlanningMetadataControlStatusNotify add data : data");
		jobj.emplace(std::make_pair("data", picojson::value(jobjData)));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreatePlanningMetadataControlStatusNotify create json value - valResponse");
		picojson::value valResponse(jobj);
		CCM_LOG_INFO("CCMCUI", "[json] CreatePlanningMetadataControlStatusNotify return json value : %s", valResponse.serialize().c_str());
		return valResponse.serialize();
	}

	picojson::object CcmJsonParser::createNetworkClientResponse(const NetworkClientID& id, const ccm::shared_ptr<NetworkClient>& nc)
	{
		CCM_LOG_INFO("CCMCUI", "[json] createNetworkClientResponse [NetworkClientID:%s]", id.GetID().c_str());

		CCM_LOG_INFO("CCMCUI", "%s", "[json] createNetworkClientResponse create jobjNC");
		picojson::object jobjNC;
		if (nc) {
			CCM_LOG_INFO("CCMCUI", "%s", "[json] createNetworkClientResponse [NetworkClient : connect]");

			jobjNC.emplace(std::make_pair("Status", picojson::value("connect")));
			jobjNC.emplace(std::make_pair("ID", picojson::value(nc->GetNetworkClientID().GetID())));
			jobjNC.emplace(std::make_pair("DisplayName", picojson::value(*(nc->GetDisplayName()))));
			jobjNC.emplace(std::make_pair("ModelName", picojson::value(*(nc->GetModelName()))));
			jobjNC.emplace(std::make_pair("SerialNumber", picojson::value(*(nc->GetSerialNumber()))));
			jobjNC.emplace(std::make_pair("SoftwareVersion", picojson::value(*(nc->GetSoftwareVersion()))));
			jobjNC.emplace(std::make_pair("PlatformType", picojson::value(*(nc->GetPlatformType()))));

			// Streaming
			CCM_LOG_INFO("CCMCUI", "%s", "[json] createNetworkClientResponse - Streaming");
			ccm::StreamingFunction* streamingFunc = (nc->GetStreamingFunction()).get();
			bool streamingFunction = false;
			std::string streamingStatus = "INACTIVE";
			picojson::array dataArrayVideoFormatCapability;
			bool fileTransferFunction = false;
			bool recorderFunction = false;
			if (streamingFunc) {
				// Streaming Function
				streamingFunction = true;
				// Streaming Status
				StreamingSendStatus status = streamingFunc->GetStreamingSendStatus();
				if (status == StreamingSendStatus::SEND_IDLE) {
					streamingStatus = "IDLE";
				}
				else if (status == StreamingSendStatus::SEND_START) {
					streamingStatus = "START";
				}
				// Streaming - Video Format Capability
				{
					ccm::shared_ptr<VideoFormatCapability> vformatCapability = streamingFunc->GetVideoFormatCapability();
					if (vformatCapability) {
						std::vector<ccm::shared_ptr<VideoFormat>> videoFormatList = vformatCapability->GetAll();
						auto itBeginVideoFormat = videoFormatList.begin();
						auto itEndVideoFormat = videoFormatList.end();
						for (; itBeginVideoFormat < itEndVideoFormat; ++itBeginVideoFormat) {
							picojson::object jobjVFC;
							int32_t maxBitrate = *((*itBeginVideoFormat)->GetMaxBitrate());
							int32_t minBitrate = *((*itBeginVideoFormat)->GetMinBitrate());
							int32_t bitrateStep = *((*itBeginVideoFormat)->GetBitrateStep());
							jobjVFC.emplace(std::make_pair("ID", picojson::value(*((*itBeginVideoFormat)->GetID()))));
							jobjVFC.emplace(std::make_pair("MaxBitrate", picojson::value((double)maxBitrate)));
							jobjVFC.emplace(std::make_pair("MinBitrate", picojson::value((double)minBitrate)));
							jobjVFC.emplace(std::make_pair("BitrateStep", picojson::value((double)bitrateStep)));
							dataArrayVideoFormatCapability.push_back(picojson::value(jobjVFC));
						}
					}
				}
			}
			// File Transfer
			CCM_LOG_INFO("CCMCUI", "%s", "[json] createNetworkClientResponse - File Transfer");
			ccm::FileTransferFunction* filetransferFunc = (nc->GetFileTransferFunction()).get();
			if (filetransferFunc) {
				fileTransferFunction = true;
			}
			jobjNC.emplace(std::make_pair("StreamingFunction", picojson::value(streamingFunction)));
			jobjNC.emplace(std::make_pair("StreamingStatus", picojson::value(streamingStatus)));
			jobjNC.emplace(std::make_pair("VideoFormatCapability", picojson::value(dataArrayVideoFormatCapability)));
			jobjNC.emplace(std::make_pair("FileTransferFunction", picojson::value(fileTransferFunction)));
			if (filetransferFunc) {
				// Media
				std::vector<ccm::Drive> driveList = filetransferFunc->GetMediaList();
				picojson::array listMedia;
				{
					std::vector<ccm::Drive>::const_iterator itBegin = driveList.begin();
					std::vector<ccm::Drive>::const_iterator itEnd = driveList.end();

					for (; itBegin != itEnd; ++itBegin) {
						picojson::object jobjMedia;
						jobjMedia.emplace(std::make_pair("id", picojson::value(itBegin->id)));
						jobjMedia.emplace(std::make_pair("status", picojson::value(itBegin->status)));
						jobjMedia.emplace(std::make_pair("capacity", picojson::value((double)itBegin->capacity)));
						jobjMedia.emplace(std::make_pair("available", picojson::value((double)itBegin->available)));
						listMedia.push_back(picojson::value(jobjMedia));
					}
					jobjNC.emplace(std::make_pair("Media", picojson::value(listMedia)));
				}
				//
				bool isMcbEnabled = filetransferFunc->McbEnabled();
				std::string isMcbEnabledString = (isMcbEnabled == true) ? "True" : "False";
				CCM_LOG_INFO("CCMCUI", "[json] createNetworkClientResponse - File Transfer [McbEnabled:%s]", isMcbEnabledString.c_str());
				jobjNC.emplace(std::make_pair("McbEnabled", picojson::value(isMcbEnabled)));
			}

			// Recorder
			ccm::shared_ptr<ccm::RecorderFunction> recorderFunc = nc->GetRecorderFunction();
			if (recorderFunc) {
				recorderFunction = true;
			}
			jobjNC.emplace(std::make_pair("RecorderFunction", picojson::value(recorderFunction)));


			CCM_LOG_INFO("CCMCUI", "%s", "[json] createNetworkClientResponse - Network Interface");
			picojson::array listNic;
			{
				std::vector<ccm::shared_ptr<ccm::NetworkInterface>> arrayNic = nc->GetNetworkInterfaces();
				std::vector<ccm::shared_ptr<ccm::NetworkInterface>>::iterator itNic = arrayNic.begin();
				std::vector<ccm::shared_ptr<ccm::NetworkInterface>>::iterator iteNic = arrayNic.end();
				for (; itNic != iteNic; ++itNic) {
					picojson::object jobjNic;
					ccm::NetworkInterface* nic = itNic->get();
					jobjNic.emplace(std::make_pair("ID", picojson::value(*(nic->GetID()))));
					jobjNic.emplace(std::make_pair("PhysicalInterfaceName", picojson::value(*(nic->GetPhysicalInterfaceName()))));
					jobjNic.emplace(std::make_pair("Type", picojson::value(*(nic->GetType()))));
					jobjNic.emplace(std::make_pair("IPv4Address", picojson::value(*(nic->GetIPv4Address()))));
					{
						picojson::object jobjCellular;
						ccm::CellularInfo* cellular = nic->GetCellularInfo().get();
						jobjCellular.emplace(std::make_pair("SignalStrength", picojson::value(*(cellular->GetSignalStrength()))));
						jobjCellular.emplace(std::make_pair("SignalStrengthUnit", picojson::value(*(cellular->GetSignalStrengthUnit()))));
						jobjCellular.emplace(std::make_pair("Operator", picojson::value(*(cellular->GetOperator()))));
						jobjCellular.emplace(std::make_pair("PLMN", picojson::value(*(cellular->GetPLMN()))));
						jobjCellular.emplace(std::make_pair("Band", picojson::value(*(cellular->GetBand()))));
						jobjCellular.emplace(std::make_pair("CellID", picojson::value(*(cellular->GetCellID()))));
						jobjCellular.emplace(std::make_pair("AirInterface", picojson::value(*(cellular->GetAirInterface()))));
						jobjCellular.emplace(std::make_pair("Manufacture", picojson::value(*(cellular->GetManufacture()))));
						jobjCellular.emplace(std::make_pair("ModelName", picojson::value(*(cellular->GetModelName()))));
						jobjCellular.emplace(std::make_pair("IMEI", picojson::value(*(cellular->GetIMEI()))));

						jobjNic.emplace(std::make_pair("CellularInfo", picojson::value(jobjCellular)));
					}
					listNic.push_back(picojson::value(jobjNic));
				}
				jobjNC.emplace(std::make_pair("NetworkInterfaces", picojson::value(listNic)));
			}
		}
		else {
			CCM_LOG_INFO("CCMCUI", "%s", "[json] createNetworkClientResponse [NetworkClient : disconnect]");
			jobjNC.emplace(std::make_pair("Status", picojson::value("disconnect")));
			jobjNC.emplace(std::make_pair("ID", picojson::value(id.GetID())));
		}

		CCM_LOG_INFO("CCMCUI", "%s", "[json] createNetworkClientResponse end");
		return jobjNC;
	}

	std::string CcmJsonParser::CreateGetNetworkClientResponse(const NetworkClientID& id, const ccm::shared_ptr<NetworkClient>& nc)
	{
		CCM_LOG_INFO("CCMCUI", "[json] CreateGetNetworkClientResponse [NetworkClientID:%s]", id.GetID().c_str());

		picojson::object jobjResponse;
		jobjResponse.emplace(std::make_pair("command", picojson::value("GetNetworkClient")));
		jobjResponse.emplace(std::make_pair("type", picojson::value("response")));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetNetworkClientResponse create jobjNC");
		picojson::object jobjNC = createNetworkClientResponse(id, nc);
		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetNetworkClientResponse add data : data");
		jobjResponse.emplace(std::make_pair("data", picojson::value(jobjNC)));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetNetworkClientResponse create json value - valResponse");
		picojson::value valResponse(jobjResponse);
		CCM_LOG_INFO("CCMCUI", "[json] CreateGetNetworkClientResponse return json value : %s", valResponse.serialize().c_str());
		return valResponse.serialize();
	}

	std::string CcmJsonParser::CreateGetNetworkClientListResponse(const std::vector<ccm::shared_ptr<NetworkClient>>& ncArray)
	{
		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetNetworkClientListResponse");

		picojson::object jobj;
		jobj.emplace(std::make_pair("command", picojson::value("GetNetworkClientList")));
		jobj.emplace(std::make_pair("type", picojson::value("response")));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetNetworkClientListResponse create dataArray");
		picojson::array dataArray;
		{
			std::vector<ccm::shared_ptr<ccm::NetworkClient>>::const_iterator itNc = ncArray.begin();
			std::vector<ccm::shared_ptr<ccm::NetworkClient>>::const_iterator iteNc = ncArray.end();
			for (; itNc != iteNc; ++itNc) {
				CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetNetworkClientListResponse create jobjNC");
				picojson::object jobjNC = createNetworkClientResponse((*itNc)->GetNetworkClientID(), *itNc);
				CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetNetworkClientListResponse add data : dataArray");
				dataArray.push_back(picojson::value(jobjNC));
			}
		}
		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetNetworkClientListResponse add data : data");
		jobj.emplace(std::make_pair("data", picojson::value(dataArray)));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetNetworkClientListResponse create json value - valResponse");
		picojson::value valResponse(jobj);
		CCM_LOG_INFO("CCMCUI", "[json] CreateGetNetworkClientListResponse return json value : %s", valResponse.serialize().c_str());
		return valResponse.serialize();
	}

	std::string CcmJsonParser::CreateGetClipListResponse(const NetworkClientID& id, const std::vector<Clip>& clips)
	{
		CCM_LOG_INFO("CCMCUI", "[json] CreateGetClipListResponse [NetworkClientID:%s]", id.GetID().c_str());

		picojson::object jobj;
		jobj.emplace(std::make_pair("command", picojson::value("GetClipList")));
		jobj.emplace(std::make_pair("type", picojson::value("response")));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetClipListResponse create dataArray");
		picojson::array dataArray;
		{
			std::vector<Clip>::const_iterator itClip = clips.begin();
			std::vector<Clip>::const_iterator iteClip = clips.end();
			for (; itClip != iteClip; ++itClip) {
				CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetClipListResponse create jobjClip");
				picojson::object jobjClip;
				jobjClip.emplace(std::make_pair("ID", picojson::value(id.GetID())));
				jobjClip.emplace(std::make_pair("media", picojson::value(itClip->drive)));
				jobjClip.emplace(std::make_pair("uri", picojson::value(itClip->uri)));
				jobjClip.emplace(std::make_pair("type", picojson::value(itClip->type)));
				jobjClip.emplace(std::make_pair("status", picojson::value(itClip->status)));
				jobjClip.emplace(std::make_pair("duration", picojson::value((double)itClip->duration)));
				jobjClip.emplace(std::make_pair("offset", picojson::value((double)itClip->offset)));
				jobjClip.emplace(std::make_pair("frameRate", picojson::value(itClip->frame_rate)));
				jobjClip.emplace(std::make_pair("aspectRatio", picojson::value(itClip->aspect_ratio)));
				jobjClip.emplace(std::make_pair("videoType", picojson::value((itClip->video_type))));
				jobjClip.emplace(std::make_pair("audioType", picojson::value((itClip->audio_type))));
				jobjClip.emplace(std::make_pair("audioChannel", picojson::value((double)itClip->aduio_channel)));
				jobjClip.emplace(std::make_pair("thumbnailUri", picojson::value(itClip->thumbnail_uri)));
				jobjClip.emplace(std::make_pair("nrtMetadataUri", picojson::value(itClip->nrt_metadata_uri)));
				jobjClip.emplace(std::make_pair("isPartialUploadSupported", picojson::value(itClip->is_partial_upload_supported)));

				CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetClipListResponse add data : dataArray");
				dataArray.push_back(picojson::value(jobjClip));
			}
		}
		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetClipListResponse add data : data");
		jobj.emplace(std::make_pair("data", picojson::value(dataArray)));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetClipListResponse create json value - valResponse");
		picojson::value valResponse(jobj);
		CCM_LOG_INFO("CCMCUI", "[json] CreateGetClipListResponse return json value : %s", valResponse.serialize().c_str());
		return valResponse.serialize();
	}

	std::string CcmJsonParser::CreateGetUploadListResponse(const NetworkClientID& id, const std::vector<Upload>& files)
	{
		CCM_LOG_INFO("CCMCUI", "[json] CreateGetUploadListResponse [NetworkClientID:%s]", id.GetID().c_str());

		picojson::object jobj;
		jobj.emplace(std::make_pair("command", picojson::value("GetUploadList")));
		jobj.emplace(std::make_pair("type", picojson::value("response")));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetUploadListResponse create dataArray");
		picojson::array dataArray;
		{
			for (ccm::Upload file : files) {
				CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetUploadListResponse create uploadFile");
				picojson::object uploadFile;
				uploadFile.emplace(std::make_pair("ID", picojson::value(id.GetID())));
				uploadFile.emplace(std::make_pair("id", picojson::value((double)file.id)));
				uploadFile.emplace(std::make_pair("setting_id", picojson::value(file.setting_id)));
				uploadFile.emplace(std::make_pair("clip_name", picojson::value(file.clip_name)));
				uploadFile.emplace(std::make_pair("drive", picojson::value(file.drive)));
				uploadFile.emplace(std::make_pair("total", picojson::value((double)file.total)));
				uploadFile.emplace(std::make_pair("status", picojson::value((double)file.status)));
				uploadFile.emplace(std::make_pair("percentage", picojson::value((double)file.percentage)));
				uploadFile.emplace(std::make_pair("transferred", picojson::value((double)file.transferred)));

				CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetUploadListResponse create upload_files");
				picojson::array upload_files;
				for (std::string file_name : file.files) {
					upload_files.push_back(picojson::value(file_name));
				}
				uploadFile.emplace(std::make_pair("files", picojson::value(upload_files)));

				CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetUploadListResponse create upload_destination");
				picojson::object upload_destination;
				upload_destination.emplace(std::make_pair("type", picojson::value(file.destination.type)));
				upload_destination.emplace(std::make_pair("display_name", picojson::value(file.destination.display_name)));
				upload_destination.emplace(std::make_pair("ftp_host_name", picojson::value(file.destination.ftp_host_name)));
				upload_destination.emplace(std::make_pair("ftp_port", picojson::value((double)file.destination.ftp_port)));
				upload_destination.emplace(std::make_pair("ftp_user_name", picojson::value(file.destination.ftp_user_name)));
				upload_destination.emplace(std::make_pair("ftp_password", picojson::value(file.destination.ftp_password)));
				upload_destination.emplace(std::make_pair("ftp_use_ftpes", picojson::value(file.destination.ftp_use_ftpes)));
				upload_destination.emplace(std::make_pair("ftp_use_pasv", picojson::value(file.destination.ftp_use_pasv)));
				upload_destination.emplace(std::make_pair("ftp_check_cert", picojson::value(file.destination.ftp_check_cert)));
				upload_destination.emplace(std::make_pair("ftp_upload_dir", picojson::value(file.destination.ftp_upload_dir)));
				CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetUploadListResponse add data : ftp_use_mcb");
				upload_destination.emplace(std::make_pair("ftp_use_mcb", picojson::value(file.destination.ftp_use_mcb)));
				CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetUploadListResponse add data : mcb_host_name");
				upload_destination.emplace(std::make_pair("mcb_host_name", picojson::value(file.destination.mcb_host_name)));
				CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetUploadListResponse add data : mcb_port");
				upload_destination.emplace(std::make_pair("mcb_port", picojson::value((double)file.destination.mcb_port)));
				upload_destination.emplace(std::make_pair("mcb_user_name", picojson::value(file.destination.mcb_user_name)));
				upload_destination.emplace(std::make_pair("mcb_password", picojson::value(file.destination.mcb_password)));

				CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetUploadListResponse add data : destination");
				uploadFile.emplace(std::make_pair("destination", picojson::value(upload_destination)));
				CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetUploadListResponse add data : dataArray");
				dataArray.push_back(picojson::value(uploadFile));
			}
		}
		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetUploadListResponse add data : data");
		jobj.emplace(std::make_pair("data", picojson::value(dataArray)));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetUploadListResponse create json value - valResponse");
		picojson::value valResponse(jobj);
		CCM_LOG_INFO("CCMCUI", "[json] CreateGetUploadListResponse return json value : %s", valResponse.serialize().c_str());
		return valResponse.serialize();
	}

	std::string CcmJsonParser::CreateAddUploadFilesResponse(const NetworkClientID& id, const ccm::ErrorCode& error)
	{
		CCM_LOG_INFO("CCMCUI", "[json] CreateAddUploadFilesResponse [NetworkClientID:%s]", id.GetID().c_str());

		picojson::object jobj;
		jobj.emplace(std::make_pair("command", picojson::value("AddUploadFiles")));
		jobj.emplace(std::make_pair("type", picojson::value("response")));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateAddUploadFilesResponse create dataArray");
		picojson::array dataArray;
		{
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateAddUploadFilesResponse create uploadResult");
			picojson::object uploadResult;
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateAddUploadFilesResponse add data : ID");
			uploadResult.emplace(std::make_pair("ID", picojson::value(id.GetID())));
			//uploadResult.emplace(std::make_pair("error", (double)error));
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateAddUploadFilesResponse add data : dataArray");
			dataArray.push_back(picojson::value(uploadResult));
		}
		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateAddUploadFilesResponse add data : data");
		jobj.emplace(std::make_pair("data", picojson::value(dataArray)));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateAddUploadFilesResponse create json value - valResponse");
		picojson::value valResponse(jobj);
		CCM_LOG_INFO("CCMCUI", "[json] CreateAddUploadFilesResponse return json value : %s", valResponse.serialize().c_str());
		return valResponse.serialize();
	}

	std::string CcmJsonParser::CreateStartQosStreamingSendResponse(const NetworkClientID& id, const ccm::ErrorCode& error)
	{
		CCM_LOG_INFO("CCMCUI", "[json] CreateStartQosStreamingSendResponse [NetworkClientID:%s]", id.GetID().c_str());

		picojson::object jobj;
		jobj.emplace(std::make_pair("command", picojson::value("StartQosStreamingSend")));
		jobj.emplace(std::make_pair("type", picojson::value("response")));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStartQosStreamingSendResponse create jobjData");
		picojson::object jobjData;
		{
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStartQosStreamingSendResponse create jobjError");
			picojson::object jobjError;
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStartQosStreamingSendResponse add data : code");
			jobjError.emplace(std::make_pair("code", picojson::value((double)error)));
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStartQosStreamingSendResponse add data : message");
			jobjError.emplace(std::make_pair("message", picojson::value(ccm::GetErrorMessage(error))));

			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStartQosStreamingSendResponse add data : ID");
			jobjData.emplace(std::make_pair("ID", picojson::value(id.GetID())));
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStartQosStreamingSendResponse add data : error");
			jobjData.emplace(std::make_pair("error", picojson::value(jobjError)));
		}
		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStartQosStreamingSendResponse add data : data");
		jobj.emplace(std::make_pair("data", picojson::value(jobjData)));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStartQosStreamingSendResponse create json value - valResponse");
		picojson::value valResponse(jobj);
		CCM_LOG_INFO("CCMCUI", "[json] CreateStartQosStreamingSendResponse return json value : %s", valResponse.serialize().c_str());
		return valResponse.serialize();
	}

	std::string CcmJsonParser::CreateStopStreamingSendResponse(const NetworkClientID& id, const ccm::ErrorCode& error)
	{
		CCM_LOG_INFO("CCMCUI", "[json] CreateStopStreamingSendResponse [NetworkClientID:%s]", id.GetID().c_str());

		picojson::object jobj;
		jobj.emplace(std::make_pair("command", picojson::value("StopStreamingSend")));
		jobj.emplace(std::make_pair("type", picojson::value("response")));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStopStreamingSendResponse create jobjData");
		picojson::object jobjData;
		{
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStopStreamingSendResponse create jobjError");
			picojson::object jobjError;
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStopStreamingSendResponse add data : code");
			jobjError.emplace(std::make_pair("code", picojson::value((double)error)));
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStopStreamingSendResponse add data : message");
			jobjError.emplace(std::make_pair("message", picojson::value(ccm::GetErrorMessage(error))));

			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStopStreamingSendResponse add data : ID");
			jobjData.emplace(std::make_pair("ID", picojson::value(id.GetID())));
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStopStreamingSendResponse add data : error");
			jobjData.emplace(std::make_pair("error", picojson::value(jobjError)));
		}
		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStopStreamingSendResponse add data : data");
		jobj.emplace(std::make_pair("data", picojson::value(jobjData)));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStopStreamingSendResponse create json value - valResponse");
		picojson::value valResponse(jobj);
		CCM_LOG_INFO("CCMCUI", "[json] CreateStopStreamingSendResponse return json value : %s", valResponse.serialize().c_str());
		return valResponse.serialize();
	}

	std::string CcmJsonParser::CreateStartQosStreamingReceiveResponse(const NetworkClientID& id, const ccm::ErrorCode& error)
	{
		CCM_LOG_INFO("CCMCUI", "[json] CreateStartQosStreamingReceiveResponse [NetworkClientID:%s]", id.GetID().c_str());

		picojson::object jobj;
		jobj.emplace(std::make_pair("command", picojson::value("StartQosStreamingReceive")));
		jobj.emplace(std::make_pair("type", picojson::value("response")));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStartQosStreamingReceiveResponse create jobjData");
		picojson::object jobjData;
		{
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStartQosStreamingReceiveResponse create jobjError");
			picojson::object jobjError;
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStartQosStreamingReceiveResponse add data : code");
			jobjError.emplace(std::make_pair("code", picojson::value((double)error)));
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStartQosStreamingReceiveResponse add data : message");
			jobjError.emplace(std::make_pair("message", picojson::value(ccm::GetErrorMessage(error))));

			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStartQosStreamingReceiveResponse add data : ID");
			jobjData.emplace(std::make_pair("ID", picojson::value(id.GetID())));
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStartQosStreamingReceiveResponse add data : error");
			jobjData.emplace(std::make_pair("error", picojson::value(jobjError)));
		}
		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStartQosStreamingReceiveResponse add data : data");
		jobj.emplace(std::make_pair("data", picojson::value(jobjData)));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStartQosStreamingReceiveResponse create json value - valResponse");
		picojson::value valResponse(jobj);
		CCM_LOG_INFO("CCMCUI", "[json] CreateStartQosStreamingReceiveResponse return json value : %s", valResponse.serialize().c_str());
		return valResponse.serialize();
	}

	std::string CcmJsonParser::CreateStopStreamingRecieveResponse(const NetworkClientID& id, const ccm::ErrorCode& error)
	{
		CCM_LOG_INFO("CCMCUI", "[json] CreateStopStreamingRecieveResponse [NetworkClientID:%s]", id.GetID().c_str());

		picojson::object jobj;
		jobj.emplace(std::make_pair("command", picojson::value("StopStreamingRecieve")));
		jobj.emplace(std::make_pair("type", picojson::value("response")));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStopStreamingRecieveResponse create jobjData");
		picojson::object jobjData;
		{
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStopStreamingRecieveResponse create jobjError");
			picojson::object jobjError;
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStopStreamingRecieveResponse add data : code");
			jobjError.emplace(std::make_pair("code", picojson::value((double)error)));
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStopStreamingRecieveResponse add data : message");
			jobjError.emplace(std::make_pair("message", picojson::value(ccm::GetErrorMessage(error))));

			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStopStreamingRecieveResponse add data : ID");
			jobjData.emplace(std::make_pair("ID", picojson::value(id.GetID())));
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStopStreamingRecieveResponse add data : error");
			jobjData.emplace(std::make_pair("error", picojson::value(jobjError)));
		}
		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStopStreamingRecieveResponse add data : data");
		jobj.emplace(std::make_pair("data", picojson::value(jobjData)));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStopStreamingRecieveResponse create json value - valResponse");
		picojson::value valResponse(jobj);
		CCM_LOG_INFO("CCMCUI", "[json] CreateStopStreamingRecieveResponse return json value : %s", valResponse.serialize().c_str());
		return valResponse.serialize();
	}

	std::string CcmJsonParser::CreateGetPlanningMetadataListResponse(const NetworkClientID& id, const std::vector<ccm::PlanningMetadataFile>& list, const ccm::ErrorCode& error)
	{
		CCM_LOG_INFO("CCMCUI", "[json] CreateGetPlanningMetadataListResponse [NetworkClientID:%s]", id.GetID().c_str());

		picojson::object jobj;
		jobj.emplace(std::make_pair("command", picojson::value("GetPlanningMetadataList")));
		jobj.emplace(std::make_pair("type", picojson::value("response")));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetPlanningMetadataListResponse create jobjData");
		picojson::object jobjData;
		{
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetPlanningMetadataListResponse add data : ID");
			jobjData.emplace(std::make_pair("ID", picojson::value(id.GetID())));

			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetPlanningMetadataListResponse create jobjPlaMetaFileArray");
			picojson::array jobjPlaMetaFileArray;
			for (PlanningMetadataFile file : list) {
				CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetPlanningMetadataListResponse create jobjPlaMetaFile");
				picojson::object jobjPlaMetaFile;
				CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetPlanningMetadataListResponse add data : name");
				jobjPlaMetaFile.emplace(std::make_pair("name", picojson::value(file.name)));
				CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetPlanningMetadataListResponse add data : uri");
				jobjPlaMetaFile.emplace(std::make_pair("uri", picojson::value(file.uri)));
				CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetPlanningMetadataListResponse add data : date");
				jobjPlaMetaFile.emplace(std::make_pair("date", picojson::value(file.date)));
				CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetPlanningMetadataListResponse add data : filesize");
				jobjPlaMetaFile.emplace(std::make_pair("filesize", picojson::value((double)file.filesize)));
				CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetPlanningMetadataListResponse add data : jobjPlaMetaFileArray");
				jobjPlaMetaFileArray.push_back(picojson::value(jobjPlaMetaFile));
			}
			jobjData.emplace(std::make_pair("PlanningMetadata", picojson::value(jobjPlaMetaFileArray)));

			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetPlanningMetadataListResponse create jobjError");
			picojson::object jobjError;
			{
				std::string errorMsg = ccm::GetErrorMessage(error);
				CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetPlanningMetadataListResponse add data : code");
				jobjError.emplace(std::make_pair("code", picojson::value((double)error)));
				CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetPlanningMetadataListResponse add data : message");
				jobjError.emplace(std::make_pair("message", picojson::value(errorMsg)));
			}
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetPlanningMetadataListResponse add data : error");
			jobjData.emplace(std::make_pair("error", picojson::value(jobjError)));
		}
		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetPlanningMetadataListResponse add data : data");
		jobj.emplace(std::make_pair("data", picojson::value(jobjData)));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetPlanningMetadataListResponse create json value - valResponse");
		picojson::value valResponse(jobj);
		CCM_LOG_INFO("CCMCUI", "[json] CreateGetPlanningMetadataListResponse return json value : %s", valResponse.serialize().c_str());
		return valResponse.serialize();
	}

	std::string CcmJsonParser::CreateGetCurrentPlanningMetadataResponse(const NetworkClientID& id, const std::string& metadata, const ccm::ErrorCode& error)
	{
		CCM_LOG_INFO("CCMCUI", "[json] CreateGetCurrentPlanningMetadataResponse [NetworkClientID:%s]", id.GetID().c_str());

		picojson::object jobj;
		jobj.emplace(std::make_pair("command", picojson::value("GetCurrentPlanningMetadata")));
		jobj.emplace(std::make_pair("type", picojson::value("response")));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetCurrentPlanningMetadataResponse create jobjData");
		picojson::object jobjData;
		{
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetCurrentPlanningMetadataResponse add data : ID");
			jobjData.emplace(std::make_pair("ID", picojson::value(id.GetID())));
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetCurrentPlanningMetadataResponse add data : PlanningMetadata");
			jobjData.emplace(std::make_pair("PlanningMetadata", picojson::value(metadata)));

			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetCurrentPlanningMetadataResponse create jobjError");
			picojson::object jobjError;
			{
				std::string errorMsg = ccm::GetErrorMessage(error);
				CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetCurrentPlanningMetadataResponse add data : code");
				jobjError.emplace(std::make_pair("code", picojson::value((double)error)));
				CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetCurrentPlanningMetadataResponse add data : message");
				jobjError.emplace(std::make_pair("message", picojson::value(errorMsg)));
			}
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetCurrentPlanningMetadataResponse add data : error");
			jobjData.emplace(std::make_pair("error", picojson::value(jobjError)));
		}
		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetCurrentPlanningMetadataResponse add data : data");
		jobj.emplace(std::make_pair("data", picojson::value(jobjData)));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetCurrentPlanningMetadataResponse create json value - valResponse");
		picojson::value valResponse(jobj);
		CCM_LOG_INFO("CCMCUI", "[json] CreateGetCurrentPlanningMetadataResponse return json value : %s", valResponse.serialize().c_str());
		return valResponse.serialize();
	}

	std::string CcmJsonParser::CreateGetPlanningMetadataResponse(const NetworkClientID& id, const std::string& name, const std::string& uri, const std::string& metadata, const ccm::ErrorCode& error)
	{
		CCM_LOG_INFO("CCMCUI", "[json] CreateGetPlanningMetadataResponse [NetworkClientID:%s]", id.GetID().c_str());

		picojson::object jobj;
		jobj.emplace(std::make_pair("command", picojson::value("GetPlanningMetadata")));
		jobj.emplace(std::make_pair("type", picojson::value("response")));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetPlanningMetadataResponse create jobjData");
		picojson::object jobjData;
		{
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetPlanningMetadataResponse add data : ID");
			jobjData.emplace(std::make_pair("ID", picojson::value(id.GetID())));
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetPlanningMetadataResponse add data : Name");
			jobjData.emplace(std::make_pair("Name", picojson::value(name)));
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetPlanningMetadataResponse add data : Uri");
			jobjData.emplace(std::make_pair("Uri", picojson::value(uri)));
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetPlanningMetadataResponse add data : PlanningMetadata");
			jobjData.emplace(std::make_pair("PlanningMetadata", picojson::value(metadata)));

			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetPlanningMetadataResponse create jobjError");
			picojson::object jobjError;
			{
				std::string errorMsg = ccm::GetErrorMessage(error);
				CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetPlanningMetadataResponse add data : code");
				jobjError.emplace(std::make_pair("code", picojson::value((double)error)));
				CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetPlanningMetadataResponse add data : message");
				jobjError.emplace(std::make_pair("message", picojson::value(errorMsg)));
			}
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetPlanningMetadataResponse add data : error");
			jobjData.emplace(std::make_pair("error", picojson::value(jobjError)));
		}
		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetPlanningMetadataResponse add data : data");
		jobj.emplace(std::make_pair("data", picojson::value(jobjData)));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateGetPlanningMetadataResponse create json value - valResponse");
		picojson::value valResponse(jobj);
		CCM_LOG_INFO("CCMCUI", "[json] CreateGetPlanningMetadataResponse return json value : %s", valResponse.serialize().c_str());
		return valResponse.serialize();
	}

	std::string CcmJsonParser::CreateSetPlanningMetadataResponse(const NetworkClientID& id, const ccm::ErrorCode& error, const std::string& internalError, const std::string& fileName, const std::string& uri)
	{
		CCM_LOG_INFO("CCMCUI", "[json] CreateSetPlanningMetadataResponse [NetworkClientID:%s]", id.GetID().c_str());

		picojson::object jobj;
		jobj.emplace(std::make_pair("command", picojson::value("SetPlanningMetadata")));
		jobj.emplace(std::make_pair("type", picojson::value("response")));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateSetPlanningMetadataResponse create jobjData");
		picojson::object jobjData;
		{
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateSetPlanningMetadataResponse add data : ID");
			jobjData.emplace(std::make_pair("ID", picojson::value(id.GetID())));
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateSetPlanningMetadataResponse add data : Uri");
			jobjData.emplace(std::make_pair("Uri", picojson::value(uri)));
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateSetPlanningMetadataResponse add data : Name");
			jobjData.emplace(std::make_pair("Name", picojson::value(fileName)));
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateSetPlanningMetadataResponse add data : InternalError");
			jobjData.emplace(std::make_pair("InternalError", picojson::value(internalError)));

			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateSetPlanningMetadataResponse create jobjError");
			picojson::object jobjError;
			{
				std::string errorMsg = ccm::GetErrorMessage(error);
				CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateSetPlanningMetadataResponse add data : code");
				jobjError.emplace(std::make_pair("code", picojson::value((double)error)));
				CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateSetPlanningMetadataResponse add data : message");
				jobjError.emplace(std::make_pair("message", picojson::value(errorMsg)));
			}
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateSetPlanningMetadataResponse add data : error");
			jobjData.emplace(std::make_pair("error", picojson::value(jobjError)));
		}
		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateSetPlanningMetadataResponse add data : data");
		jobj.emplace(std::make_pair("data", picojson::value(jobjData)));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateSetPlanningMetadataResponse create json value - valResponse");
		picojson::value valResponse(jobj);
		CCM_LOG_INFO("CCMCUI", "[json] CreateSetPlanningMetadataResponse return json value : %s", valResponse.serialize().c_str());
		return valResponse.serialize();
	}

	std::string CcmJsonParser::CreatePutPlanningMetadataResponse(const NetworkClientID& id, const ccm::ErrorCode& error, const std::string& internalError, const std::string& fileName)
	{
		CCM_LOG_INFO("CCMCUI", "[json] CreatePutPlanningMetadataResponse [NetworkClientID:%s]", id.GetID().c_str());

		picojson::object jobj;
		jobj.emplace(std::make_pair("command", picojson::value("PutPlanningMetadata")));
		jobj.emplace(std::make_pair("type", picojson::value("response")));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreatePutPlanningMetadataResponse create jobjData");
		picojson::object jobjData;
		{
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreatePutPlanningMetadataResponse add data : ID");
			jobjData.emplace(std::make_pair("ID", picojson::value(id.GetID())));
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreatePutPlanningMetadataResponse add data : Name");
			jobjData.emplace(std::make_pair("Name", picojson::value(fileName)));
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreatePutPlanningMetadataResponse add data : InternalError");
			jobjData.emplace(std::make_pair("InternalError", picojson::value(internalError)));

			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreatePutPlanningMetadataResponse create jobjError");
			picojson::object jobjError;
			{
				std::string errorMsg = ccm::GetErrorMessage(error);
				CCM_LOG_INFO("CCMCUI", "%s", "[json] CreatePutPlanningMetadataResponse add data : code");
				jobjError.emplace(std::make_pair("code", picojson::value((double)error)));
				CCM_LOG_INFO("CCMCUI", "%s", "[json] CreatePutPlanningMetadataResponse add data : message");
				jobjError.emplace(std::make_pair("message", picojson::value(errorMsg)));
			}
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreatePutPlanningMetadataResponse add data : error");
			jobjData.emplace(std::make_pair("error", picojson::value(jobjError)));
		}
		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreatePutPlanningMetadataResponse add data : data");
		jobj.emplace(std::make_pair("data", picojson::value(jobjData)));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreatePutPlanningMetadataResponse create json value - valResponse");
		picojson::value valResponse(jobj);
		CCM_LOG_INFO("CCMCUI", "[json] CreatePutPlanningMetadataResponse return json value : %s", valResponse.serialize().c_str());
		return valResponse.serialize();
	}



	std::string CcmJsonParser::CreateAddUploadFilesResult(const NetworkClientID& id, const std::vector<ccm::AsyncResult>& results)
	{
		CCM_LOG_INFO("CCMCUI", "[json] CreateAddUploadFilesResult [NetworkClientID:%s]", id.GetID().c_str());

		picojson::object jobj;
		jobj.emplace(std::make_pair("command", picojson::value("AddUploadFiles")));
		jobj.emplace(std::make_pair("type", picojson::value("result")));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateAddUploadFilesResult create dataArray");
		picojson::array dataArray;
		{
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateAddUploadFilesResult create jobjResult");
			picojson::object jobjResult;
			jobjResult.emplace(std::make_pair("ID", picojson::value(id.GetID())));
			{
				CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateAddUploadFilesResult create jobjResultDetail");
				picojson::object jobjResultDetail;
				{
					CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateAddUploadFilesResult create jobjErrorArray");
					picojson::array jobjErrorArray;
					if (!results.empty()) {
						CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateAddUploadFilesResult - results no empty");
						for (ccm::AsyncResult result : results) {
							CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateAddUploadFilesResult create jobjError");
							picojson::object jobjError;
							{
								CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateAddUploadFilesResult create jobjErrorCode");
								picojson::object jobjErrorCode;
								double processId = (double)result.procces_id;
								CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateAddUploadFilesResult add data : processId");
								jobjError.emplace(std::make_pair("id", picojson::value(processId)));
								//
								double errorCode = (double)result.error_code;
								std::string errorMsg = ccm::GetErrorMessage(result.error_code);
								CCM_LOG_INFO("CCMCUI", "[json] CreateAddUploadFilesResult - CreateErrorObject [ProcessID:%d, Code:%d, Message:%s]", processId, errorCode, errorMsg.c_str());
								CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateAddUploadFilesResult add data : code");
								jobjErrorCode.emplace(std::make_pair("code", picojson::value(errorCode)));
								CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateAddUploadFilesResult add data : message");
								jobjErrorCode.emplace(std::make_pair("message", picojson::value(errorMsg)));
								//
								CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateAddUploadFilesResult add data : error");
								jobjError.emplace(std::make_pair("error", picojson::value(jobjErrorCode)));
							}
							CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateAddUploadFilesResult add data : jobjErrorArray");
							jobjErrorArray.push_back(picojson::value(jobjError));
						}
					}
					else {
						CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateAddUploadFilesResult - results empty");
					}
					CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateAddUploadFilesResult add data : result1");
					jobjResultDetail.emplace(std::make_pair("result", picojson::value(jobjErrorArray)));
				}
				CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateAddUploadFilesResult add data : result2");
				jobjResult.emplace(std::make_pair("result", picojson::value(jobjResultDetail)));
			}
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateAddUploadFilesResult add data : dataArray");
			dataArray.push_back(picojson::value(jobjResult));
		}
		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateAddUploadFilesResult add data : data");
		jobj.emplace(std::make_pair("data", picojson::value(dataArray)));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateAddUploadFilesResult create json value - valResponse");
		picojson::value valResponse(jobj);
		printf("%s", valResponse.serialize().c_str());
		CCM_LOG_INFO("CCMCUI", "[json] CreateAddUploadFilesResult return json value : %s", valResponse.serialize().c_str());
		return valResponse.serialize();
	}
	
	
	
	std::string CcmJsonParser::CreateStartVoIPResponse(const NetworkClientID& id, const ccm::ErrorCode& error)
	{
		CCM_LOG_INFO("CCMCUI", "[json] CreateStartVoIPResponse [NetworkClientID:%s]", id.GetID().c_str());

		picojson::object jobj;
		jobj.emplace(std::make_pair("command", picojson::value("StartVoIP")));
		jobj.emplace(std::make_pair("type", picojson::value("response")));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStartVoIPResponse create jobjData");
		picojson::object jobjData;
		{
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStartVoIPResponse create jobjError");
			picojson::object jobjError;
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStartVoIPResponse add data : code");
			jobjError.emplace(std::make_pair("code", picojson::value((double)error)));
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStartVoIPResponse add data : message");
			jobjError.emplace(std::make_pair("message", picojson::value(ccm::GetErrorMessage(error))));

			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStartVoIPResponse add data : ID");
			jobjData.emplace(std::make_pair("ID", picojson::value(id.GetID())));
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStartVoIPResponse add data : error");
			jobjData.emplace(std::make_pair("error", picojson::value(jobjError)));
		}
		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStartVoIPResponse add data : data");
		jobj.emplace(std::make_pair("data", picojson::value(jobjData)));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStartVoIPResponse create json value - valResponse");
		picojson::value valResponse(jobj);
		CCM_LOG_INFO("CCMCUI", "[json] CreateStartVoIPResponse return json value : %s", valResponse.serialize().c_str());
		return valResponse.serialize();
	}

	std::string CcmJsonParser::CreateStopVoIPResponse(const NetworkClientID& id, const ccm::ErrorCode& error)
	{
		CCM_LOG_INFO("CCMCUI", "[json] CreateStopVoIPResponse [NetworkClientID:%s]", id.GetID().c_str());

		picojson::object jobj;
		jobj.emplace(std::make_pair("command", picojson::value("StopVoIP")));
		jobj.emplace(std::make_pair("type", picojson::value("response")));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStopVoIPResponse create jobjData");
		picojson::object jobjData;
		{
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStopVoIPResponse create jobjError");
			picojson::object jobjError;
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStopVoIPResponse add data : code");
			jobjError.emplace(std::make_pair("code", picojson::value((double)error)));
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStopVoIPResponse add data : message");
			jobjError.emplace(std::make_pair("message", picojson::value(ccm::GetErrorMessage(error))));

			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStopVoIPResponse add data : ID");
			jobjData.emplace(std::make_pair("ID", picojson::value(id.GetID())));
			CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStopVoIPResponse add data : error");
			jobjData.emplace(std::make_pair("error", picojson::value(jobjError)));
		}
		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStopVoIPResponse add data : data");
		jobj.emplace(std::make_pair("data", picojson::value(jobjData)));

		CCM_LOG_INFO("CCMCUI", "%s", "[json] CreateStopVoIPResponse create json value - valResponse");
		picojson::value valResponse(jobj);
		CCM_LOG_INFO("CCMCUI", "[json] CreateStopVoIPResponse return json value : %s", valResponse.serialize().c_str());
		return valResponse.serialize();
	}

}
