#pragma once

#include <string>

namespace ccm {

	class Base64
	{
	public:
		Base64();
		~Base64();

		static std::string base64_encode(unsigned char const* s, unsigned int len);
		static std::string base64_decode(std::string const& s);

	private:
		static const std::string base64_chars;
		static inline bool is_base64(unsigned char c) {
			return (isalnum(c) || (c == '+') || (c == '/'));
		}
	};

}
