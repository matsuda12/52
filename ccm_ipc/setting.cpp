#include "ccm_gui.h"


std::string ccm_setting::readConfigFile(const char* path) {
  std::ifstream ifs(path);
  std::string configStr;
  configStr.clear();

  if (ifs.is_open()) {
    std::string str;
    if (ifs.fail()) {
      std::cerr << "Failed to open config file." << std::endl;
      return "";
    }
    while (getline(ifs, str)) {

      //if (str.find("//") != std::string::npos) {
      //  str.replace(str.find("//"), 2, "/\\/");
      //}
      if (str.find_first_of("#") == 0) {
        //std::cout << "#COMMENT# " << str << std::endl;
      } else {


        if (str.find("/*") != std::string::npos && str.find("*/") != std::string::npos) {
          configStr += str.replace(str.find("/*"), str.find("*/") + 2 - str.find("/*"), "");

          //std::cout << "#COMMENT# ORG " << str.c_str() << std::endl;
          //std::cout << "#COMMENT# AFT " << str.replace(str.find("/*"), str.find("*/") + 2 - str.find("/*"), "").c_str() << std::endl;

        } else {
          configStr.append( str.c_str() );
        }

        // コメントアウト除外処理
        //std::string r = std::regex_replace(str, std::regex("//(.+)|^#(.+)|/\\*(.+)\\*/"), "");
        //std::string r = std::regex_replace(str, std::regex("^#(.+)|/\\*(.+)\\*/"), "");

        //r = std::regex_replace(r, std::regex("////"), "");

        //std::cout << "#" << str << std::endl;



        



        //configStr += r;
      }
    }

    //printf("\n\n%s\n\n", configStr.c_str());
    return configStr;
  } else {
    //std::cout << "---- non config file ----" << std::endl;
    return configStr;
  }
}


bool ccm_setting::getSystemConfigData(const std::string json, std::map < std::string, std::string >& config) {
  picojson::value root;
  try {
    std::string err = picojson::parse(root, json.c_str());
    if (!err.empty() || !root.is<picojson::object>()) {
      throw std::invalid_argument(err.c_str());
    }
  } catch (std::exception& e) {
    std::cout << "   json parse error. " << e.what() << std::endl;
    return false;
  }
  auto value = root.get("SYSTEM");
  if (value.is<picojson::array>()) {
    picojson::array setlist = value.get<picojson::array>();
    for (picojson::array::iterator sit = setlist.begin(); sit != setlist.end(); sit++) {
      if (sit->is<picojson::object>()) {
        picojson::object& setlists = sit->get<picojson::object>();

        for (const auto& p : setlists) {
          config[p.first.c_str()] = p.second.get<std::string>();
        }
      }
    }
  }
  return true;
}

bool ccm_setting::getNamemapData(const std::string json, std::map<std::string, std::string>& names) {
  picojson::value root;
  std::string log;
  try {
    std::string err = picojson::parse(root, json.c_str());
    if (!err.empty() || !root.is<picojson::object>()) {
      throw std::invalid_argument(err.c_str());
    }
  } catch (std::exception& e) {
    //RX_LOG_ERROR("json parse error. %s", e.what());
    //logMgr.Error("CCMSDK","");
    std::cout << "   json parse error. " << e.what() << std::endl;
    return false;
  }
  auto value = root.get("NAMEMAP");
  if (value.is<picojson::array>()) {
    picojson::array setlist = value.get<picojson::array>();
    for (picojson::array::iterator sit = setlist.begin(); sit != setlist.end(); sit++) {
      if (sit->is<picojson::object>()) {
        picojson::object& setlists = sit->get<picojson::object>();
        for (const auto& p : setlists) {
          //std::cout << "   #NAMEMAP = " << p.first.c_str() << std::endl;
          // マップに追加※NC名::NCモデル＆シリアル
          names[p.first.c_str()] = p.second.get<std::string>().c_str();

          // 逆引き用も追加
          names[p.second.get<std::string>()] = p.first.c_str();
        }
      }
    }
  }
  return true;
}

std::map<std::string, picojson::object> ccm_setting::getPropertyMap(const std::string PropartyName, const std::string json) {
  std::map<std::string, picojson::object> configMap;
  picojson::value root;
  try {
    std::string err = picojson::parse(root, json.c_str());
    if (!err.empty() || !root.is<picojson::object>()) {
      throw std::invalid_argument(err.c_str());
    }
  } catch (std::exception& e) {
    //RX_LOG_ERROR("json parse error. %s", e.what());
    //std::cout << "   json parse error. " << e.what() << std::endl;
    CCM_LOG_ERROR("CCMCUI", "json parse error. [params:%s/error:%s]", json.c_str(), e.what());
    return configMap;
  }

  auto value = root.get("Intercom");

  if (value.is<picojson::null>()) {

    //std::cout << "NOt Intercom" << std::endl;
    picojson::object& ic_setting = root.get<picojson::object>();
    configMap["Json"] = ic_setting;

    for (const auto& p : ic_setting) {
      //std::cout << p.first << " = " << PropartyName << std::endl;
      if (p.first == PropartyName) {
        if (p.second.is<picojson::object>()) {
          picojson::object setlists = p.second.get<picojson::object>();
          for (const auto& p2 : setlists) {
            if (p2.second.is<picojson::object>()) {
              configMap[p2.first.c_str()] = p2.second.get<picojson::object>();
            }
            if (p2.second.is<std::double_t>()) {
              configMap[p2.first.c_str()] = picojson::object{ std::make_pair("",  picojson::value{ p2.second.get<std::double_t>() }) };
            }
            if (p2.second.is<picojson::array>()) {
              for (auto arrp : p2.second.get<picojson::array>()) {
                configMap[p2.first.c_str()] = picojson::object{ std::make_pair("",  picojson::value{ arrp }) };
              }

            }
            //std::cout << p2.first << std::endl;
          }
        } else {
          //std::cout << "not object" << std::endl;
        }
      }
    }
  } else if (value.is<picojson::array>()) {
    picojson::array& list = value.get<picojson::array>();
    picojson::array tmparr;

    for (picojson::array::iterator it = list.begin(); it != list.end(); it++) {
      picojson::object& ic_setting = it->get<picojson::object>();
      for (const auto& p : ic_setting) {
        if (p.first == PropartyName) {
          configMap["Json"] = ic_setting;
          if (p.second.is<picojson::array>()) {
            picojson::array setlist = p.second.get<picojson::array>();
            for (picojson::array::iterator sit = setlist.begin(); sit != setlist.end(); sit++) {
              if (sit->is<picojson::object>()) {
                picojson::object& setlists = sit->get<picojson::object>();
                for (const auto& p2 : setlists) {
                  //std::cout << p2.first.c_str() << " OK " << std::endl;

                  if (p2.first.find("Network.Service.Intercom.Control.Enabled") != std::string::npos) {
                    if (setlists[p2.first].is<picojson::object>()) {
                      configMap["Network.Service.Intercom.Control.Enabled"] = setlists[p2.first].get<picojson::object>();
                    }
                  }
                  if (p2.first.find("Network.Service.Intercom.Control.Type") != std::string::npos) {
                    if (setlists[p2.first].is<picojson::object>()) {
                      configMap["Network.Service.Intercom.Control.Type"] = setlists[p2.first].get<picojson::object>();
                    }
                  }
                  if (p2.first.find("Network.Service.Intercom.Control.Status") != std::string::npos) {
                    if (setlists[p2.first].is<picojson::object>()) {
                      configMap["Network.Service.Intercom.Control.Status"] = setlists[p2.first].get<picojson::object>();
                    }
                  }
                  if (p2.first.find("Network.Service.Intercom.Control.ClientType") != std::string::npos) {
                    if (setlists[p2.first].is<picojson::object>()) {
                      configMap["Network.Service.Intercom.Control.ClientType"] = setlists[p2.first].get<picojson::object>();
                    }
                  }
                  if (p2.first.find("Network.Service.Intercom.Destination.Address") != std::string::npos) {
                    if (setlists[p2.first].is<picojson::object>()) {
                      if (setlists[p2.first].get<picojson::object>()["IntercomTx"].is<std::string>()) {
                        configMap["Network.Service.Intercom.Destination.Address"] = setlists[p2.first].get<picojson::object>();
                      }
                    }
                  }
                  if (p2.first.find("Network.Service.Intercom.Destination.Port") != std::string::npos) {
                    if (setlists[p2.first].is<picojson::object>()) {
                      //if (setlists[p2.first].get<picojson::object>()["IntercomTx"].is<int>()) {
                      configMap["Network.Service.Intercom.Destination.Port"] = setlists[p2.first].get<picojson::object>();
                      //}
                    }
                  }
                  if (p2.first.find("Network.Service.Intercom.Receiving.Port") != std::string::npos) {
                    if (setlists[p2.first].is<picojson::object>()) {
                      //if (setlists[p2.first].get<picojson::object>()["IntercomRx"].is<int>()) {
                      configMap["Network.Service.Intercom.Receiving.Port"] = setlists[p2.first].get<picojson::object>();
                      //}
                    }
                  }
                  if (p2.first.find("Network.Service.Intercom.Audio.SamplingFrequency") != std::string::npos) {
                    if (setlists[p2.first].is<picojson::object>()) {
                      configMap["Network.Service.Intercom.Audio.SamplingFrequency"] = setlists[p2.first].get<picojson::object>();
                    }
                  }
                  if (p2.first.find("Network.Service.Intercom.Audio.Bits") != std::string::npos) {
                    if (setlists[p2.first].is<picojson::object>()) {
                      configMap["Network.Service.Intercom.Audio.Bits"] = setlists[p2.first].get<picojson::object>();
                    }
                  }
                  if (p2.first.find("Network.Service.Intercom.Audio.FrameLength") != std::string::npos) {
                    if (setlists[p2.first].is<picojson::object>()) {
                      configMap["Network.Service.Intercom.Audio.FrameLength"] = setlists[p2.first].get<picojson::object>();
                    }
                  }
                  if (p2.first.find("Network.Service.Intercom.Audio.BufferType") != std::string::npos) {
                    if (setlists[p2.first].is<picojson::object>()) {
                      if (setlists[p2.first].get<picojson::object>()["IntercomRx"].is<std::string>()) {
                        configMap["Network.Service.Intercom.Audio.BufferType"] = setlists[p2.first].get<picojson::object>();
                      }
                    }
                  }
                  if (p2.first.find("Network.Service.Intercom.Audio.BufferTime") != std::string::npos) {
                    if (setlists[p2.first].is<picojson::object>()) {
                      if (setlists[p2.first].get<picojson::object>()["IntercomRx"].is<std::string>()) {
                        configMap["Network.Service.Intercom.Audio.BufferTime"] = setlists[p2.first].get<picojson::object>();
                      }
                    }
                  }
                  if (p2.first.find("Network.Service.Intercom.Codec.Type") != std::string::npos) {
                    if (setlists[p2.first].is<picojson::object>()) {
                      if (setlists[p2.first].get<picojson::object>()["Encode"].is<std::string>() &&
                          setlists[p2.first].get<picojson::object>()["Decode"].is<std::string>()) {
                        configMap["Network.Service.Intercom.Codec.Type"] = setlists[p2.first].get<picojson::object>();
                      }
                    }
                  }
                  /*if (p2.first.find("Network.Service.Intercom.Codec.Version") != std::string::npos) {
                  if (setlists[p2.first].is<picojson::object>()) {
                  if (setlists[p2.first].get<picojson::object>()["Type"].is<std::string>()) {
                  preset->codec.version.type = setlists[p2.first].get<picojson::object>()["Type"].get<std::string>();
                  }
                  }
                  if (p2.second.is<std::int64_t>()) {
                  preset->codec.version.type = std::to_string(setlists[p2.first].get<std::int64_t>());
                  }
                  if (p2.second.is<std::string>()) {
                  preset->codec.version.type = std::to_string(std::stoi(setlists[p2.first].get<std::string>().c_str()));
                  }
                  }*/
                  if (p2.first.find("Network.Service.Intercom.Codec.Opus.Bitrate") != std::string::npos) {
                    if (setlists[p2.first].is<picojson::object>()) {
                      if (setlists[p2.first].get<picojson::object>()["Encode"].is<std::double_t>()) {
                        configMap["Network.Service.Intercom.Codec.Opus.Bitrate"] = setlists[p2.first].get<picojson::object>();
                      }
                    }
                  }
                  //if (p2.first.find("Network.Service.Intercom.Codec.Opus.Bandwidth") != std::string::npos) {
                  //	if (setlists[p2.first].is<picojson::object>()) {
                  //		if (setlists[p2.first].get<picojson::object>()["Encode"].is<std::int64_t>()) {
                  //			preset->codec.opus.bandwidth.encode = setlists[p2.first].get<picojson::object>()["Encode"].get<std::int64_t>();
                  //			preset->codec.opus.bandwidth.decode = setlists[p2.first].get<picojson::object>()["Decode"].get<std::int64_t>();
                  //		}
                  //	}
                  //}
                  if (p2.first.find("Network.Service.Intercom.Codec.Opus.FEC") != std::string::npos) {
                    if (setlists[p2.first].is<picojson::object>()) {
                      if (setlists[p2.first].get<picojson::object>()["Encode"].get<bool>()) {
                        configMap["Network.Service.Intercom.Codec.Opus.FEC"] = setlists[p2.first].get<picojson::object>();
                      }
                    }
                  }
                  if (p2.first.find("Network.Service.Intercom.Codec.Opus.DTX") != std::string::npos) {
                    if (setlists[p2.first].is<picojson::object>()) {
                      if (setlists[p2.first].get<picojson::object>()["Encode"].get<bool>() &&
                          setlists[p2.first].get<picojson::object>()["Decode"].get<bool>()) {
                        configMap["Network.Service.Intercom.Codec.Opus.DTX"] = setlists[p2.first].get<picojson::object>();
                      }
                    }
                  }
                  if (p2.first.find("Network.Service.Intercom.Security.CipherType") != std::string::npos) {
                    if (setlists[p2.first].is<picojson::object>()) {
                      if (setlists[p2.first].get<picojson::object>()["IntercomTx"].is<std::string>() &&
                          setlists[p2.first].get<picojson::object>()["IntercomRx"].is<std::string>()) {
                        configMap["Network.Service.Intercom.Security.CipherType"] = setlists[p2.first].get<picojson::object>();
                      }
                    }
                  }
                  if (p2.first.find("Network.Service.Intercom.Security.CipherKey") != std::string::npos) {
                    if (setlists[p2.first].is<picojson::object>()) {
                      if (setlists[p2.first].get<picojson::object>()["IntercomTx"].is<std::string>() &&
                          setlists[p2.first].get<picojson::object>()["IntercomRx"].is<std::string>()) {
                        configMap["Network.Service.Intercom.Security.CipherKey"] = setlists[p2.first].get<picojson::object>();
                      }
                    }
                  }
                  if (p2.first.find("Network.Service.Intercom.Security.SSRC") != std::string::npos) {
                    if (setlists[p2.first].is<picojson::object>()) {
                      if (setlists[p2.first].get<picojson::object>()["IntercomTx"].is<std::string>() &&
                          setlists[p2.first].get<picojson::object>()["IntercomRx"].is<std::string>()) {
                        configMap["Network.Service.Intercom.Security.SSRC"] = setlists[p2.first].get<picojson::object>();
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  return configMap;
}

ccm::ErrorCode ccm_setting::parseFileTransferConfig(const std::string json,
                                          ccm::UploadDestinationConfig *config,
                                          std::vector<std::string> *upload_cert,
                                          std::vector<std::string> *mcb_cert){
  picojson::value root;
  try {
    std::string err = picojson::parse(root, json.c_str());
    if (!err.empty()) {
      throw std::invalid_argument(err.c_str());
    }
  } catch (std::exception& e) {
    CCM_LOG_ERROR("CCMCUI", "json parse error. [params:%s/error:%s]", json.c_str(), e.what());
    return ccm::ErrorCode::CCM_UNKNOWN;
  }

  

  if (config != nullptr) {
    try {
      picojson::object& settings = root.get<picojson::object>();

      for (const auto& configs : settings) {
        //std::cout << "   -> " << configs.first << std::endl;

        if (configs.first == "DispName") {
          config->name = configs.second.get<std::string>();
          config->destination.display_name = configs.second.get<std::string>();
        };
        if (configs.first == "Type") { config->destination.type = configs.second.get<std::string>(); };
        if (configs.first == "FtpCheckCert") { config->destination.ftp_check_cert = configs.second.get<bool>(); };
        if (configs.first == "FtpUseFtpes") { config->destination.ftp_use_ftpes = configs.second.get<bool>(); };
        if (configs.first == "FtpUsePasv") { config->destination.ftp_use_pasv = configs.second.get<bool>(); };
        if (configs.first == "FtpHostName") { config->destination.ftp_host_name = configs.second.get<std::string>(); };
        if (configs.first == "FtpUserName") { config->destination.ftp_user_name = configs.second.get<std::string>(); };
        if (configs.first == "FtpPassword") { config->destination.ftp_password = configs.second.get<std::string>(); };
        if (configs.first == "FtpPort") { config->destination.ftp_port = (int)configs.second.get<std::double_t>(); };
        if (configs.first == "FtpUploadDir") { config->destination.ftp_upload_dir = configs.second.get<std::string>(); };
        if (configs.first == "FtpUseMcb") { config->destination.ftp_use_mcb = configs.second.get<bool>(); };
        if (configs.first == "McbHostName") { config->destination.mcb_host_name = configs.second.get<std::string>(); };
        if (configs.first == "McbUserName") { config->destination.mcb_user_name = configs.second.get<std::string>(); };
        if (configs.first == "McbPassword") { config->destination.mcb_password = configs.second.get<std::string>(); };
        if (configs.first == "McbPort") { config->destination.mcb_port = (int)configs.second.get<std::double_t>(); };
        //if (configs.first == "Option") { config->destination.option = configs.second.get<std::string>(); };
      }
      return ccm::ErrorCode::CCM_OK;
    } catch (std::exception& e) {
      CCM_LOG_ERROR("CCMCUI", "json parse error. [params:%s/error:%s]", json.c_str(), e.what());
      return ccm::ErrorCode::CCM_UNKNOWN;
    }
    
  } else if(upload_cert != nullptr) {
    try {
      picojson::array& settings = root.get<picojson::array>();
      for (const auto& cert : settings) {
        //std::cout << "   -> " << cert.get<std::string>() << std::endl;
        upload_cert->push_back(cert.get<std::string>());
      }
      return ccm::ErrorCode::CCM_OK;
    } catch (std::exception& e) {
      CCM_LOG_ERROR("CCMCUI", "json parse error. [params:%s/error:%s]", json.c_str(), e.what());
      return ccm::ErrorCode::CCM_UNKNOWN;
    }
  } else if (mcb_cert != nullptr) {
    try {
      picojson::array& settings = root.get<picojson::array>();
      for (const auto& cert : settings) {
        //std::cout << "   -> " << cert.get<std::string>() << std::endl;
        mcb_cert->push_back(cert.get<std::string>());
      }
      return ccm::ErrorCode::CCM_OK;
    } catch (std::exception& e) {
      CCM_LOG_ERROR("CCMCUI", "json parse error. [params:%s/error:%s]", json.c_str(), e.what());
      return ccm::ErrorCode::CCM_UNKNOWN;
    }
  } else {
    return ccm::ErrorCode::CCM_UNKNOWN;
    //CCM_LOG_ERROR("CCMCUI", "json parse error. [params:%s/error:%s]","");
  }
  return ccm::ErrorCode::CCM_OK;
}

void ccm_setting::getFileTransferConfig(
  std::vector<ccm::UploadFiles> *add_files,
  ccm::UploadFiles *destination,
  const std::string config_name,
  ccm::UploadDestinationConfig *config,
  std::vector<std::string> *upload_cert,
  std::vector<std::string> *mcb_cert,
  const std::string json) {

  picojson::value root;
  try {
    std::string err = picojson::parse(root, json.c_str());
    if (!err.empty() || !root.is<picojson::object>()) {
      throw std::invalid_argument(err.c_str());
    }
  } catch (std::exception& e) {
    CCM_LOG_ERROR("CCMCUI", "json parse error. [params:%s/error:%s]", json.c_str(), e.what());
    return;
  }

  picojson::object& filetransfer_setting = root.get<picojson::object>();
  for (const auto& p : filetransfer_setting) {
    //std::cout << p.first << std::endl;
    if (upload_cert != nullptr) {
      if (p.first == "UploadServerCert") {
        if (p.second.is<picojson::array>()) {
          //std::cout << " -> Array" << std::endl;
          picojson::array upload_certs = p.second.get<picojson::array>();
          for (const auto& cert : upload_certs) {
            upload_cert->push_back(cert.get<std::string>());
          }
        }
      }
    }

    if (mcb_cert != nullptr) {
      if (p.first == "MCBServerCert") {
        if (p.second.is<picojson::array>()) {
          //std::cout << " -> Array" << std::endl;
          picojson::array mcb_certs = p.second.get<picojson::array>();
          for (const auto& cert : mcb_certs) {
            mcb_cert->push_back(cert.get<std::string>());
          }
        }
      }
    }


    if (config != nullptr) {
      if (p.first == "DefaultUploadConfig") {
        //std::cout << " -> Map" << std::endl;
        picojson::object default_config = p.second.get<picojson::object>();
        for (const auto& configs : default_config) {
          //std::cout << "   -> " << configs.first << std::endl;
          if (configs.first == "DispName") {
            config->name = configs.second.get<std::string>();
            config->destination.display_name = configs.second.get<std::string>();
          };
          if (configs.first == "Type") { config->destination.type = configs.second.get<std::string>(); };
          if (configs.first == "FtpCheckCert") { config->destination.ftp_check_cert = configs.second.get<bool>(); };
          if (configs.first == "FtpUseFtpes") { config->destination.ftp_use_ftpes = configs.second.get<bool>(); };
          if (configs.first == "FtpUsePasv") { config->destination.ftp_use_pasv = configs.second.get<bool>(); };
          if (configs.first == "FtpHostName") { config->destination.ftp_host_name = configs.second.get<std::string>(); };
          if (configs.first == "FtpUserName") { config->destination.ftp_user_name = configs.second.get<std::string>(); };
          if (configs.first == "FtpPassword") { config->destination.ftp_password = configs.second.get<std::string>(); };
          if (configs.first == "FtpPort") { config->destination.ftp_port = (int)configs.second.get<std::double_t>(); };
          if (configs.first == "FtpUploadDir") { config->destination.ftp_upload_dir = configs.second.get<std::string>(); };
          if (configs.first == "FtpUseMcb") { config->destination.ftp_use_mcb = configs.second.get<bool>(); };
          if (configs.first == "McbHostName") { config->destination.mcb_host_name = configs.second.get<std::string>(); };
          if (configs.first == "McbUserName") { config->destination.mcb_user_name = configs.second.get<std::string>(); };
          if (configs.first == "McbPassword") { config->destination.mcb_password = configs.second.get<std::string>(); };
          if (configs.first == "McbPort") { config->destination.mcb_port = (int)configs.second.get<std::double_t>(); };
          //if (configs.first == "Option") { config->destination.option = configs.second.get<std::string>(); };
        }
      }
    }

    if (destination != nullptr) {
      //std::cout << "Add Files" << std::endl;
      std::string read_name;
      if (config_name.empty()) {
        return;
      } else {
        read_name = config_name;
      }
      if (p.first == read_name) {
        picojson::object default_config = p.second.get<picojson::object>();
        for (const auto& configs : default_config) {
          //std::cout << "   -> " << configs.first << std::endl;
          if (configs.first == "DispName") {
            destination->destination.display_name = configs.second.get<std::string>();
          };
          if (configs.first == "Type") { destination->destination.type = configs.second.get<std::string>(); };
          if (configs.first == "FtpCheckCert") { destination->destination.ftp_check_cert = configs.second.get<bool>(); };
          if (configs.first == "FtpUseFtpes") { destination->destination.ftp_use_ftpes = configs.second.get<bool>(); };
          if (configs.first == "FtpUsePasv") { destination->destination.ftp_use_pasv = configs.second.get<bool>(); };
          if (configs.first == "FtpHostName") { destination->destination.ftp_host_name = configs.second.get<std::string>(); };
          if (configs.first == "FtpUserName") { destination->destination.ftp_user_name = configs.second.get<std::string>(); };
          if (configs.first == "FtpPassword") { destination->destination.ftp_password = configs.second.get<std::string>(); };
          if (configs.first == "FtpPort") { destination->destination.ftp_port = (int)configs.second.get<std::double_t>(); };
          if (configs.first == "FtpUploadDir") { destination->destination.ftp_upload_dir = configs.second.get<std::string>(); };
          if (configs.first == "FtpUseMcb") { destination->destination.ftp_use_mcb = configs.second.get<bool>(); };
          if (configs.first == "McbHostName") { destination->destination.mcb_host_name = configs.second.get<std::string>(); };
          if (configs.first == "McbUserName") { destination->destination.mcb_user_name = configs.second.get<std::string>(); };
          if (configs.first == "McbPassword") { destination->destination.mcb_password = configs.second.get<std::string>(); };
          if (configs.first == "McbPort") { destination->destination.mcb_port = (int)configs.second.get<std::double_t>(); };
          if (configs.first == "SettingID") {
            destination->setting_id = std::to_string((int)configs.second.get<std::double_t>());
          }
        }
      }
    }


    if (add_files != nullptr) {
      //std::cout << "Add Files" << std::endl;
      std::string read_name;
      if (config_name.empty()) {
        read_name = "UploadFiles";
      } else {
        read_name = config_name;
      }
      if (p.first == read_name) {
        //std::cout << " -> Map" << std::endl;
        picojson::array upload_files = p.second.get<picojson::array>();
        for (const auto& files : upload_files) {
          //Array
          ccm::UploadFiles upload_files;

          picojson::object file_details = files.get<picojson::object>();


          for (const auto& configs : file_details) {


            //ccm::DefaultUploadConfig upload_config;

            //std::cout << "   -> " << configs.first << std::endl;

            if (configs.first == "DispName") {
              //upload_files. = configs.second.get<std::string>();
              upload_files.destination.display_name = configs.second.get<std::string>();
            };
            if (configs.first == "Type") { upload_files.destination.type = configs.second.get<std::string>(); };
            if (configs.first == "FtpCheckCert") { upload_files.destination.ftp_check_cert = configs.second.get<bool>(); };
            if (configs.first == "FtpUseFtpes") { upload_files.destination.ftp_use_ftpes = configs.second.get<bool>(); };
            if (configs.first == "FtpUsePasv") { upload_files.destination.ftp_use_pasv = configs.second.get<bool>(); };
            if (configs.first == "FtpHostName") { upload_files.destination.ftp_host_name = configs.second.get<std::string>(); };
            if (configs.first == "FtpUserName") { upload_files.destination.ftp_user_name = configs.second.get<std::string>(); };
            if (configs.first == "FtpPassword") { upload_files.destination.ftp_password = configs.second.get<std::string>(); };
            if (configs.first == "FtpPort") { upload_files.destination.ftp_port = (int)configs.second.get<std::double_t>(); };
            if (configs.first == "FtpUploadDir") { upload_files.destination.ftp_upload_dir = configs.second.get<std::string>(); };
            if (configs.first == "FtpUseMcb") { upload_files.destination.ftp_use_mcb = configs.second.get<bool>(); };
            if (configs.first == "McbHostName") { upload_files.destination.mcb_host_name = configs.second.get<std::string>(); };
            if (configs.first == "McbUserName") { upload_files.destination.mcb_user_name = configs.second.get<std::string>(); };
            if (configs.first == "McbPassword") { upload_files.destination.mcb_password = configs.second.get<std::string>(); };
            if (configs.first == "McbPort") { upload_files.destination.mcb_port = (int)configs.second.get<std::double_t>(); };
            if (configs.first == "Option") { upload_files.destination.option = configs.second.get<std::string>(); };
            if (configs.first == "SettingID") {
              upload_files.setting_id = std::to_string((int)configs.second.get<std::double_t>());
            }
            if (configs.first == "Drive") {
              upload_files.drive = configs.second.get<std::string>();
            }
            if (configs.first == "Files") {
              picojson::array file_urls = configs.second.get<picojson::array>();

              for (const auto& url : file_urls) {
                upload_files.files.push_back(url.to_str());
                //std::cout << "   -> " << url.to_str() << std::endl;
              }
            }


          }
          add_files->push_back(upload_files);
        }
      }
    }


  }
}

ccm::QosStreamingRecieveConfig ccm_setting::getQosStreamingRecieveConfigFromFile(const std::string PropartyName, const std::string json) {
  ccm::QosStreamingRecieveConfig params;
  picojson::value root;
  try {
    std::string err = picojson::parse(root, json.c_str());
    if (!err.empty() || !root.is<picojson::object>()) {
      throw std::invalid_argument(err.c_str());
    }
  } catch (std::exception& e) {
    //RX_LOG_ERROR("json parse error. %s", e.what());
    //std::cout << "   json parse error. " << e.what() << std::endl;
    CCM_LOG_ERROR("CCMCUI", "json parse error. [params:%s/error:%s]", json.c_str(), e.what());
    return params;
  }

  picojson::object& ic_setting = root.get<picojson::object>();

  if (ic_setting.find(PropartyName) != ic_setting.end()) {
    CCM_LOG_INFO("CCMCUI", "Streeaming Params Found from Setting File. [%s]", PropartyName.c_str());
    try {
      if (ic_setting.find(PropartyName)->second.is<picojson::object>()) {
        picojson::object setlists = ic_setting.find(PropartyName)->second.get<picojson::object>();
        for (const auto& config_map : setlists) {
          if (config_map.first == "Port") {
            if (config_map.second.is<std::double_t>()) {
              params.port = (int)config_map.second.get<std::double_t>();
            }
          } else if (config_map.first == "VideoFormat") {
            if (config_map.second.is<std::string>()) {
              params.video_format = config_map.second.get<std::string>();
            }
          } else if (config_map.first == "GoP") {
            if (config_map.second.is<std::double_t>()) {
              params.gop = (int)config_map.second.get<std::double_t>();
            }
          } else if (config_map.first == "BufferTime") {
            if (config_map.second.is<std::double_t>()) {
              params.buffer_time = (int)config_map.second.get<std::double_t>();
            }
          } else if (config_map.first == "ChiperKey") {
            if (config_map.second.is<std::string>()) {
              params.chiper_key = config_map.second.get<std::string>();
            }
          } else if (config_map.first == "SSRC") {
            if (config_map.second.is<std::double_t>()) {
              params.ssrc = (int)config_map.second.get<std::double_t>();
            }
          } else if (config_map.first == "QoSInterfaces") {
            if (config_map.second.is<picojson::array>()) {
              for (auto interface : config_map.second.get<picojson::array>()) {
                params.qos_interfaces.push_back(interface.get < std::string >());
              }
            }
          } else if (config_map.first == "MetaInterfaces") {
            if (config_map.second.is<picojson::array>()) {
              for (auto interface : config_map.second.get<picojson::array>()) {
                params.meta_interfaces.push_back(interface.get < std::string >());
              }
            }
          } else {
            CCM_LOG_INFO("CCMCUI", "This Streeaming Params not use. [%s]", config_map.first.c_str());
          }
        }
      } else {
        CCM_LOG_ERROR("CCMCUI", "json parse error. [params:%s/error:not object]", json.c_str());
      }
    } catch (std::exception& e) {
      CCM_LOG_ERROR("CCMCUI", "json parse error. [params:%s/error:%s]", json.c_str(), e.what());
    }
  } else {
    CCM_LOG_WARN("CCMCUI", "Streeaming Params Not Found from Setting File. [%s]", PropartyName.c_str());
  }

  return params;
}
ccm::QosStreamingSendConfig ccm_setting::getQosStreamingSendConfigFromFile(const std::string PropartyName, const std::string json) {
  ccm::QosStreamingSendConfig params;
  picojson::value root;
  try {
    std::string err = picojson::parse(root, json.c_str());
    if (!err.empty() || !root.is<picojson::object>()) {
      throw std::invalid_argument(err.c_str());
    }
  } catch (std::exception& e) {
    //RX_LOG_ERROR("json parse error. %s", e.what());
    //std::cout << "   json parse error. " << e.what() << std::endl;
    CCM_LOG_ERROR("CCMCUI", "json parse error. [params:%s/error:%s]", json.c_str(), e.what());
    return params;
  }

  picojson::object& ic_setting = root.get<picojson::object>();

  if (ic_setting.find(PropartyName) != ic_setting.end()) {
    CCM_LOG_INFO("CCMCUI", "Streeaming Params Found from Setting File. [%s]", PropartyName.c_str());
    try {
      if (ic_setting.find(PropartyName)->second.is<picojson::object>()) {
        picojson::object setlists = ic_setting.find(PropartyName)->second.get<picojson::object>();
        for (const auto& config_map : setlists) {
          if (config_map.first == "Address") {
            if (config_map.second.is<std::string>()) {
              params.address = config_map.second.get<std::string>();
            }
          } else if (config_map.first == "Port") {
            if (config_map.second.is<std::double_t>()) {
              params.port = (int)config_map.second.get<std::double_t>();
            }
          } else if (config_map.first == "VideoFormat") {
            if (config_map.second.is<std::string>()) {
              params.video_format = config_map.second.get<std::string>();
            }
          } else if (config_map.first == "BitRate") {
            if (config_map.second.is<std::double_t>()) {
              params.bitrate = (int)config_map.second.get<std::double_t>();
            }
          } else if (config_map.first == "GoP") {
            if (config_map.second.is<std::double_t>()) {
              params.gop = (int)config_map.second.get<std::double_t>();
            }
          } else if (config_map.first == "BufferTime") {
            if (config_map.second.is<std::double_t>()) {
              params.buffer_time = (int)config_map.second.get<std::double_t>();
            }
          } else if (config_map.first == "ChiperKey") {
            if (config_map.second.is<std::string>()) {
              params.chiper_key = config_map.second.get<std::string>();
            }
          } else if (config_map.first == "SSRC") {
            if (config_map.second.is<std::double_t>()) {
              params.ssrc = (int)config_map.second.get<std::double_t>();
            }
          } else if (config_map.first == "QoSInterfaces") {
            if (config_map.second.is<picojson::array>()) {
              for (auto interface : config_map.second.get<picojson::array>()) {
                params.qos_interfaces.push_back(interface.get < std::string >());
              }
            }
          } else if (config_map.first == "MetaInterfaces") {
            if (config_map.second.is<picojson::array>()) {
              for (auto interface : config_map.second.get<picojson::array>()) {
                params.meta_interfaces.push_back(interface.get < std::string >());
              }
            }
          } else {
            CCM_LOG_INFO("CCMCUI", "This Streeaming Params not use. [%s]", config_map.first.c_str());
          }
        }
      } else {
        CCM_LOG_ERROR("CCMCUI", "json parse error. [params:%s/error:not object]", json.c_str());
      }
    } catch (std::exception& e) {
      CCM_LOG_ERROR("CCMCUI", "json parse error. [params:%s/error:%s]", json.c_str(), e.what());
    }
  } else {
    CCM_LOG_WARN("CCMCUI", "Streeaming Params Not Found from Setting File. [%s]", PropartyName.c_str());
  }
  /*for (const auto& p : ic_setting) {
    //std::cout << p.first << " = " << PropartyName << std::endl;
    if (p.first == PropartyName) {
      
    }
  }*/

  return params;
}