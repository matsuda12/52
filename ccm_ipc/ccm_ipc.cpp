﻿#include "../ccmsdk/connection_control_manager.h"
#include "../include/log_manager.h"
#include "ccm_ipc.h"
#include "ccm_json_parser.h"
#include "uv.h"
#include <stdio.h>
#include <string.h>
#include <vector>
#include <thread>
#ifdef CCMDEBUG
#include "ccm_gui.h"
#endif

namespace {
  ccm::CcmIpc *self_ = NULL;
  ccm::ConnectionControlManager *ccmsdk_ = NULL;
  std::mutex				mutex_;/// 排他用mutex
  std::vector<ccm::shared_ptr<std::condition_variable>> array_cond_;
}

namespace ccm {

	void alloc_buffer(uv_handle_t* handle, size_t suggested_size, uv_buf_t* buf) {
		//printf("alloc_buffer, suggested_size=%zd\n", suggested_size);
		*buf = uv_buf_init((char*)malloc(suggested_size), (unsigned int)suggested_size);
	}

	/**
	 * 受信データの解析
	 */
	void parseReceiveData(const uv_buf_t* buf, ssize_t nread, std::vector<std::string>& vec) {
		CCM_LOG_INFO("CCMCUI", "[ipc] parseReceiveData start (data size : %d)", nread);
		uint32_t length = 0;
		char *bufData = buf->base;
		ssize_t bufLen = nread;

		while (bufLen > 0)
		{
			if (bufLen >= sizeof(length))
			{
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] parseReceiveData get length");
				memcpy(&length, bufData, sizeof(length));
				CCM_LOG_INFO("CCMCUI", "[ipc] parseReceiveData get length : %d", length);
				bufData = bufData + sizeof(length);
				bufLen = bufLen - sizeof(length);
			}
			else
			{
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] parseReceiveData get length (bufLen < sizeof(length))");
				break;
			}

			if (bufLen >= length)
			{
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] parseReceiveData get command");
				char *dataBuffer = (char *)malloc(length + 1 /*null*/);
				memset(dataBuffer, 0x00, length + 1);
				memcpy(dataBuffer, bufData, length);
				bufData = bufData + length;
				bufLen = bufLen - length;
				std::string data = std::string(dataBuffer);
				vec.push_back(data);
			}
			else
			{
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] parseReceiveData get command (bufLen < length)");
				break;
			}
		}

		CCM_LOG_INFO("CCMCUI", "[ipc] parseReceiveData end (command count : %d)", vec.size());
	}

	void callback_read(uv_stream_t* stream, ssize_t nread, const uv_buf_t* buf) {
		//printf("callback_read. nread=%zd\n", nread);
		if (nread < 0) {
			printf("IPC Disconnected\n");
			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] IPC Disconnected");
			self_->OnDisconnect();
		}
		else {
			//buf->base[nread] = 0x00;
			//printf("%s\n", buf->base);
			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] --------------");
			std::vector<std::string> vecReceiveData;
			parseReceiveData(buf, nread, vecReceiveData);

			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] recv ipc   |<---|");
			for (const auto& receiveData : vecReceiveData) {
				CCM_LOG_INFO("CCMCUI", "[ipc] recv ipc   |<---||%s", receiveData.c_str());
				self_->OnMessage(receiveData);
			}
		}
		free(buf->base);
	}

	void on_new_connection(uv_stream_t* pipe, int status) {
		//fprintf(stderr, "on_new_connection\n");

		if (status == -1) {
			// error!
			printf("on_new_connection status error");
			CCM_LOG_INFO("CCMCUI", "[ipc] IPC Connection Status Error (status:%d)", status);
			return;
		}
		else {
			printf("IPC Conneted\n");
			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] IPC Connected");
		}

		if (self_ != NULL) {
			self_->OnConnect(pipe);
		}
		return;
	}

	typedef struct {
		uv_write_t req;
		uv_buf_t buf;
	} write_req_t;
	void free_write_req(const uv_write_t* const req) {
      try {
        write_req_t *wr = (write_req_t*)req;
        free(wr->buf.base);
        free(wr);
        //printf("IPC FREE OK\n");
      } catch (...) {
        //printf("IPC FREE ERROR\n");
      }
      /*try {
        // Unlock
        mutex_.unlock();

        // Send Notify
        if (array_cond_.size() > 1) {
          (*array_cond_.begin())->notify_all();
          array_cond_.erase(array_cond_.begin());
        }
      } catch (...) {
        //printf("IPC UNLOCK ERROR\n");
      }*/
	}
	void on_uv_written(uv_write_t* req, int status) {
		free_write_req(req);
	}


	void CcmIpc::OnDisconnect() {
		uv_stop(client_loop_);
        uv_close((uv_handle_t*)&client_pipe_, NULL);
	}

	void CcmIpc::OnConnect(uv_stream_t *pipe) {
		client_loop_ = uv_default_loop();
		uv_pipe_init(client_loop_, &client_pipe_, 0);
		//printf("IPC: uv_pipe_init OK\n");

		if (uv_accept((uv_stream_t*)pipe, (uv_stream_t*)&client_pipe_) == 0) {
			uv_read_start((uv_stream_t*)&client_pipe_, alloc_buffer, callback_read);
		}
		else {
			uv_close((uv_handle_t*)&client_pipe_, NULL);
		}

		std::thread onlineThread_(uv_run, client_loop_, UV_RUN_DEFAULT);
		onlineThread_.detach();
	};

	/**
	 * NetworkClient情報の取得
	 */
	void CcmIpc::onCmdGetNetworkClient_(const picojson::object& ccm_request) {
		CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetNetworkClient: start");

		std::string client_id = ccm_request.find("data")->second.get<picojson::object>().find("ID")->second.get<std::string>();
		CCM_LOG_INFO("CCMCUI", "[ipc] GetNetworkClient client_id:%s", client_id.c_str());

		std::string client_model_name = client_id.substr(0, client_id.find("_"));
		std::string client_serial_number = client_id.substr(client_id.find("_") + 1);
		ccm::NetworkClientID id = ccm::NetworkClientID(&client_model_name, &client_serial_number);

		//printf("calling sdk -> GetNetworkClient\n");
		CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetNetworkClient calling...");
		ccm::shared_ptr<ccm::NetworkClient> client = ccmsdk_->GetNetworkClient(id);

		//printf("calling sdk -> GetNetworkClientList return : %d\n", clients.size());
		CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetNetworkClient create response");
		std::string strResponse = CcmJsonParser::CreateGetNetworkClientResponse(id, client);

		//printf("%s\n\n", strResponse.c_str());
		CCM_LOG_INFO("CCMCUI", "[ipc] GetNetworkClient response:%s", strResponse.c_str());
		Send(strResponse);

		CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetNetworkClient: end");
	}

	/**
	 * 接続されている NetworkClient一覧 の取得
	 */
	void CcmIpc::onCmdGetNetworkClientList_(const picojson::object& ccm_request) {
		CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetNetworkClientList: start");

		//printf("calling sdk -> GetNetworkClientList\n");
		CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetNetworkClientList calling...");
		std::vector<ccm::shared_ptr<ccm::NetworkClient>> clients = ccmsdk_->GetNetworkClientList();

		//printf("calling sdk -> GetNetworkClientList return : %d\n", clients.size());
		CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetNetworkClientList create response");
		std::string strResponse = CcmJsonParser::CreateGetNetworkClientListResponse(clients);

		//printf("%s\n\n", strResponse.c_str());
		CCM_LOG_INFO("CCMCUI", "[ipc] GetNetworkClientList response:%s", strResponse.c_str());
		Send(strResponse);

		CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetNetworkClientList: end");
	}

	void CcmIpc::onCmdGetClipList_(const picojson::object& ccm_request) {
		CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetClipList: start");
		std::string client_model_name;
		std::string client_serial_number;
		std::string drive_num;
		try {
			std::string client_id = ccm_request.find("data")->second.get<picojson::object>().find("ID")->second.get<std::string>();
			CCM_LOG_INFO("CCMCUI", "[ipc] GetClipList client_id:%s", client_id.c_str());
			//
			drive_num = ccm_request.find("data")->second.get<picojson::object>().find("media")->second.get<std::string>();
			CCM_LOG_INFO("CCMCUI", "[ipc] GetClipList drive_num:%s", drive_num.c_str());
			//
			client_model_name = client_id.substr(0, client_id.find("_"));
			client_serial_number = client_id.substr(client_id.find("_") + 1);
			ccm::NetworkClientID id = ccm::NetworkClientID(&client_model_name, &client_serial_number);

			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetClipList - get network client");
			ccm::shared_ptr<ccm::NetworkClient> client = ccmsdk_->GetNetworkClient(id);

			if (client) {
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetClipList - get file transfer func");
				ccm::shared_ptr<ccm::FileTransferFunction> file_transfer_function = client->GetFileTransferFunction();
				if (file_transfer_function) {
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetClipList calling...");
					file_transfer_function->GetClipList(drive_num);
				}
				else {
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetClipList - NO file transfer func");
				}
			}
			else {
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetClipList - NO network client");
			}
		}
		catch (...) {
			printf("GetClipList Request Error\n");
			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetClipList Request Error : exception");
		}
		CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetClipList: end");
	}

	void CcmIpc::onCmdGetUploadList_(const picojson::object& ccm_request) {
		CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetUploadList: start");
		std::string client_model_name;
		std::string client_serial_number;
		int interval = 100;

		try {
			std::string client_id = ccm_request.find("data")->second.get<picojson::object>().find("ID")->second.get<std::string>();
			CCM_LOG_INFO("CCMCUI", "[ipc] GetUploadList client_id:%s", client_id.c_str());

			client_model_name = client_id.substr(0, client_id.find("_"));
			client_serial_number = client_id.substr(client_id.find("_") + 1);
			ccm::NetworkClientID id = ccm::NetworkClientID(&client_model_name, &client_serial_number);

			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetUploadList - get network client");
			ccm::shared_ptr<ccm::NetworkClient> client = ccmsdk_->GetNetworkClient(id);

			if (client) {
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetUploadList - get file transfer func");
				ccm::shared_ptr<ccm::FileTransferFunction> file_transfer_function = client->GetFileTransferFunction();
				if (file_transfer_function) {
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetUploadList calling...");
					file_transfer_function->GetUploadList(interval);
				}
				else {
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetUploadList - NO file transfer func");
				}
			}
			else {
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetUploadList - NO network client");
			}
		}
		catch (...) {
			printf("GetUploadList Request Error\n");
			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetUploadList Request Error : exception");
		}
		CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetUploadList: end");
	}

	void CcmIpc::onCmdAddUploadFiles_(const picojson::object& ccm_request) {
		CCM_LOG_INFO("CCMCUI", "%s", "[ipc] AddUploadFiles: start");
		std::string client_model_name;
		std::string client_serial_number;
		ccm::UploadFiles upload_file;

		try {
			std::string client_id = ccm_request.find("data")->second.get<picojson::object>().find("ID")->second.get<std::string>();
			CCM_LOG_INFO("CCMCUI", "[ipc] AddUploadFiles client_id:%s", client_id.c_str());

			client_model_name = client_id.substr(0, client_id.find("_"));
			client_serial_number = client_id.substr(client_id.find("_") + 1);
			ccm::NetworkClientID id = ccm::NetworkClientID(&client_model_name, &client_serial_number);

			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] AddUploadFiles - get network client");
			ccm::shared_ptr<ccm::NetworkClient> client = ccmsdk_->GetNetworkClient(id);

			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] AddUploadFiles get params");
			picojson::object request_params = ccm_request.find("data")->second.get<picojson::object>();

			// Base
			upload_file.drive = request_params.find("media")->second.get<std::string>();
			CCM_LOG_INFO("CCMCUI", "[ipc] AddUploadFiles params [drive:%s]", upload_file.drive.c_str());
			upload_file.setting_id = request_params.find("setting_id")->second.get<std::string>();
			CCM_LOG_INFO("CCMCUI", "[ipc] AddUploadFiles params [setting_id:%s]", upload_file.setting_id.c_str());

			// Files
			auto fileArray = request_params.find("files")->second.get<picojson::array>();
			CCM_LOG_INFO("CCMCUI", "[ipc] AddUploadFiles params [file array size:%d]", fileArray.size());
			upload_file.files.push_back(request_params.find("files")->second.get<picojson::array>()[0].get<std::string>());
			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] AddUploadFiles params files");

			// Destination
			picojson::object request_params_dest = request_params.find("destination")->second.get<picojson::object>();
			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] AddUploadFiles params dest");
			upload_file.destination.type = request_params_dest.find("type")->second.get<std::string>();
			CCM_LOG_INFO("CCMCUI", "[ipc] AddUploadFiles params [upload_file.destination.type:%s]", upload_file.destination.type.c_str());
			upload_file.destination.display_name = request_params_dest.find("disp_name")->second.get<std::string>();
			CCM_LOG_INFO("CCMCUI", "[ipc] AddUploadFiles params [upload_file.destination.display_name:%s]", upload_file.destination.display_name.c_str());
			upload_file.destination.ftp_host_name = request_params_dest.find("ftp_host_name")->second.get<std::string>();
			CCM_LOG_INFO("CCMCUI", "[ipc] AddUploadFiles params [upload_file.destination.ftp_host_name:%s]", upload_file.destination.ftp_host_name.c_str());
			upload_file.destination.ftp_port = (int)request_params_dest.find("ftp_port")->second.get<double>();
			CCM_LOG_INFO("CCMCUI", "[ipc] AddUploadFiles params [upload_file.destination.ftp_port:%d]", upload_file.destination.ftp_port);
			upload_file.destination.ftp_user_name = request_params_dest.find("ftp_user_name")->second.get<std::string>();
			CCM_LOG_INFO("CCMCUI", "[ipc] AddUploadFiles params [upload_file.destination.ftp_user_name:%s]", upload_file.destination.ftp_user_name.c_str());
			upload_file.destination.ftp_password = request_params_dest.find("ftp_password")->second.get<std::string>();
			CCM_LOG_INFO("CCMCUI", "[ipc] AddUploadFiles params [upload_file.destination.ftp_password:%s]", upload_file.destination.ftp_password.c_str());
			upload_file.destination.ftp_use_ftpes = request_params_dest.find("ftp_use_ftpes")->second.get<bool>();
			CCM_LOG_INFO("CCMCUI", "[ipc] AddUploadFiles params [upload_file.destination.ftp_use_ftpes:%d]", upload_file.destination.ftp_use_ftpes);
			upload_file.destination.ftp_use_pasv = request_params_dest.find("ftp_use_pasv")->second.get<bool>();
			CCM_LOG_INFO("CCMCUI", "[ipc] AddUploadFiles params [upload_file.destination.ftp_use_pasv:%d]", upload_file.destination.ftp_use_pasv);
			upload_file.destination.ftp_check_cert = request_params_dest.find("ftp_check_cert")->second.get<bool>();
			CCM_LOG_INFO("CCMCUI", "[ipc] AddUploadFiles params [upload_file.destination.ftp_check_cert:%d]", upload_file.destination.ftp_check_cert);
			upload_file.destination.ftp_upload_dir = request_params_dest.find("ftp_upload_dir")->second.get<std::string>();
			CCM_LOG_INFO("CCMCUI", "[ipc] AddUploadFiles params [upload_file.destination.ftp_upload_dir:%s]", upload_file.destination.ftp_upload_dir.c_str());
			upload_file.destination.ftp_use_mcb = request_params_dest.find("ftp_use_mcb")->second.get<bool>();
			CCM_LOG_INFO("CCMCUI", "[ipc] AddUploadFiles params [upload_file.destination.ftp_use_mcb:%d]", upload_file.destination.ftp_use_mcb);
			if (upload_file.destination.ftp_use_mcb == true) {
				upload_file.destination.mcb_host_name = request_params_dest.find("mcb_host")->second.get<std::string>();
				CCM_LOG_INFO("CCMCUI", "[ipc] AddUploadFiles params [upload_file.destination.mcb_host_name:%s]", upload_file.destination.mcb_host_name.c_str());
				upload_file.destination.mcb_port = (int)request_params_dest.find("mcb_port")->second.get<double>();
				CCM_LOG_INFO("CCMCUI", "[ipc] AddUploadFiles params [upload_file.destination.mcb_port:%d]", upload_file.destination.mcb_port);
				upload_file.destination.mcb_user_name = request_params_dest.find("mcb_username")->second.get<std::string>();
				CCM_LOG_INFO("CCMCUI", "[ipc] AddUploadFiles params [upload_file.destination.mcb_user_name:%s]", upload_file.destination.mcb_user_name.c_str());
				upload_file.destination.mcb_password = request_params_dest.find("mcb_password")->second.get<std::string>();
				CCM_LOG_INFO("CCMCUI", "[ipc] AddUploadFiles params [upload_file.destination.mcb_password:%s]", upload_file.destination.mcb_password.c_str());
			}
			else {
				upload_file.destination.mcb_host_name = "";
				CCM_LOG_INFO("CCMCUI", "[ipc] AddUploadFiles params [upload_file.destination.mcb_host_name:%s]", upload_file.destination.mcb_host_name.c_str());
				upload_file.destination.mcb_port = 0;
				CCM_LOG_INFO("CCMCUI", "[ipc] AddUploadFiles params [upload_file.destination.mcb_port:%d]", upload_file.destination.mcb_port);
				upload_file.destination.mcb_user_name = "";
				CCM_LOG_INFO("CCMCUI", "[ipc] AddUploadFiles params [upload_file.destination.mcb_user_name:%s]", upload_file.destination.mcb_user_name.c_str());
				upload_file.destination.mcb_password = "";
				CCM_LOG_INFO("CCMCUI", "[ipc] AddUploadFiles params [upload_file.destination.mcb_password:%s]", upload_file.destination.mcb_password.c_str());
			}

			std::vector<ccm::UploadFiles> upload_files = { upload_file };
			CCM_LOG_INFO("CCMCUI", "[ipc] AddUploadFiles params [upload_files size:%d]", upload_files.size());

			if (client != nullptr) {
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] AddUploadFiles - get file transfer func");
				ccm::shared_ptr<ccm::FileTransferFunction> file_transfer_function = client->GetFileTransferFunction();
				if (file_transfer_function) {
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] AddUploadFiles calling...");
					ccm::ErrorCode err = file_transfer_function->AddUploadFiles(upload_files);
					CCM_LOG_INFO("CCMCUI", "[ipc] AddUploadFiles AddUploadFiles() error:%d", err);

					// For IPC
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] AddUploadFiles CreateAddUploadFilesResponse");
					std::string strResponse = ccm::CcmJsonParser::CreateAddUploadFilesResponse(id, err);
					CCM_LOG_INFO("CCMCUI", "[ipc] AddUploadFiles response:%s", strResponse.c_str());
					Send(strResponse);
				}
				else {
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] AddUploadFiles - NO file transfer func");

					// For IPC
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] AddUploadFiles CreateAddUploadFilesResponse");
					std::string strResponse = ccm::CcmJsonParser::CreateAddUploadFilesResponse(id, ccm::ErrorCode::CCM_NC_NOT_SUPPORTED);
					CCM_LOG_INFO("CCMCUI", "[ipc] AddUploadFiles response:%s", strResponse.c_str());
					Send(strResponse);
				}
			}
			else {
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] AddUploadFiles - NO network client");
				// For IPC
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] AddUploadFiles CreateAddUploadFilesResponse");
				std::string strResponse = ccm::CcmJsonParser::CreateAddUploadFilesResponse(id, ccm::ErrorCode::CCM_UNKNOWN);
				CCM_LOG_INFO("CCMCUI", "[ipc] AddUploadFiles response:%s", strResponse.c_str());
				Send(strResponse);
			}
		}
		catch (...) {
			printf("AddUploadFiles Request Error\n");
			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] AddUploadFiles Request Error : exception");
			// For IPC
			ccm::NetworkClientID id = ccm::NetworkClientID(&client_model_name, &client_serial_number);
			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] AddUploadFiles CreateAddUploadFilesResponse");
			std::string strResponse = ccm::CcmJsonParser::CreateAddUploadFilesResponse(id, ccm::ErrorCode::CCM_UNKNOWN);
			CCM_LOG_INFO("CCMCUI", "[ipc] AddUploadFiles response:%s", strResponse.c_str());
			Send(strResponse);
		}
		CCM_LOG_INFO("CCMCUI", "%s", "[ipc] AddUploadFiles: end");
	}

	void CcmIpc::onCmdStartQosStreamingSend_(const picojson::object& ccm_request) {
		CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StartQosStreamingSend: start");
		std::string client_model_name;
		std::string client_serial_number;

		try {
			std::string client_id = ccm_request.find("data")->second.get<picojson::object>().find("ID")->second.get<std::string>();
			CCM_LOG_INFO("CCMCUI", "[ipc] StartQosStreamingSend client_id:%s", client_id.c_str());

			QosStreamingSendConfig streamingParam;
			streamingParam.address = ccm_request.find("data")->second.get<picojson::object>().find("address")->second.get<std::string>();
			CCM_LOG_INFO("CCMCUI", "[ipc] StartQosStreamingSend params [streamingParam.address:%s]", streamingParam.address.c_str());
			streamingParam.port = (uint32_t)ccm_request.find("data")->second.get<picojson::object>().find("port")->second.get<std::double_t>();
			CCM_LOG_INFO("CCMCUI", "[ipc] StartQosStreamingSend params [streamingParam.port:%d]", streamingParam.port);
			streamingParam.video_format = ccm_request.find("data")->second.get<picojson::object>().find("video_format")->second.get<std::string>();
			CCM_LOG_INFO("CCMCUI", "[ipc] StartQosStreamingSend params [streamingParam.video_format:%s]", streamingParam.video_format.c_str());
			streamingParam.bitrate = (uint32_t)ccm_request.find("data")->second.get<picojson::object>().find("bitrate")->second.get<std::double_t>();
			CCM_LOG_INFO("CCMCUI", "[ipc] StartQosStreamingSend params [streamingParam.bitrate:%d]", streamingParam.bitrate);
			streamingParam.gop = (uint32_t)ccm_request.find("data")->second.get<picojson::object>().find("gop")->second.get<std::double_t>();
			CCM_LOG_INFO("CCMCUI", "[ipc] StartQosStreamingSend params [streamingParam.gop:%d]", streamingParam.gop);
			streamingParam.buffer_time = (uint32_t)ccm_request.find("data")->second.get<picojson::object>().find("buffer_time")->second.get<std::double_t>();
			CCM_LOG_INFO("CCMCUI", "[ipc] StartQosStreamingSend params [streamingParam.buffer_time:%d]", streamingParam.buffer_time);
			streamingParam.chiper_key = ccm_request.find("data")->second.get<picojson::object>().find("chiper_key")->second.get<std::string>();
			CCM_LOG_INFO("CCMCUI", "[ipc] StartQosStreamingSend params [streamingParam.chiper_key:%s]", streamingParam.chiper_key.c_str());
			streamingParam.ssrc = (uint32_t)ccm_request.find("data")->second.get<picojson::object>().find("ssrc")->second.get<std::double_t>();
			CCM_LOG_INFO("CCMCUI", "[ipc] StartQosStreamingSend params [streamingParam.ssrc:%d]", streamingParam.ssrc);
			//
			const picojson::array& qos_interfaces = ccm_request.find("data")->second.get<picojson::object>().find("qos_interfaces")->second.get<picojson::array>();
			for (const auto& interface : qos_interfaces) {
				std::string qosInterface = interface.get<std::string>();
				CCM_LOG_INFO("CCMCUI", "[ipc] StartQosStreamingSend params [streamingParam.qos_interfaces:%s]", qosInterface.c_str());
				streamingParam.qos_interfaces.push_back(qosInterface);
			}
			const picojson::array& meta_interfaces = ccm_request.find("data")->second.get<picojson::object>().find("meta_interfaces")->second.get<picojson::array>();
			for (const auto& interface : meta_interfaces) {
				std::string metaInterface = interface.get<std::string>();
				CCM_LOG_INFO("CCMCUI", "[ipc] StartQosStreamingSend params [streamingParam.meta_interfaces:%s]", metaInterface.c_str());
				streamingParam.meta_interfaces.push_back(metaInterface);
			}

			client_model_name = client_id.substr(0, client_id.find("_"));
			client_serial_number = client_id.substr(client_id.find("_") + 1);
			ccm::NetworkClientID id = ccm::NetworkClientID(&client_model_name, &client_serial_number);

			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StartQosStreamingSend - get network client");
			ccm::shared_ptr<ccm::NetworkClient> client = ccmsdk_->GetNetworkClient(id);

			if (client) {
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StartQosStreamingSend - get streaming func");
				ccm::shared_ptr<ccm::StreamingFunction> streaming_function = client->GetStreamingFunction();
				if (streaming_function) {
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StartQosStreamingSend calling...");
					ccm::ErrorCode err = streaming_function->StartQosStreamingSend(streamingParam);
					CCM_LOG_INFO("CCMCUI", "[ipc] StartQosStreamingSend StartQosStreamingSend() error:%d", err);

					// For IPC
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StartQosStreamingSend CreateStartQosStreamingSendResponse");
					std::string strResponse = ccm::CcmJsonParser::CreateStartQosStreamingSendResponse(id, err);
					CCM_LOG_INFO("CCMCUI", "[ipc] StartQosStreamingSend response:%s", strResponse.c_str());
					printf("#StartQosStreamingSend Send Response to IPC\n");
					Send(strResponse);

#ifdef CCMDEBUG_STREAMING
					if (err == ccm::ErrorCode::CCM_OK) {
						for (int i = 0; i < 60; i++) {
							std::vector<ccm::StreamingDynamicProperty> vecStreamingDynamicProperty;
							{
								StreamingDynamicProperty property1;
								property1.nic_name = "NIC1";
								property1.delay = std::rand();
								property1.loss_rate = std::rand();
								property1.rate = std::rand();
								property1.rtt = std::rand();
								vecStreamingDynamicProperty.push_back(property1);

								StreamingDynamicProperty property2;
								property2.nic_name = "NIC2";
								property2.delay = std::rand();
								property2.loss_rate = std::rand();
								property2.rate = std::rand();
								property2.rtt = std::rand();
								vecStreamingDynamicProperty.push_back(property2);

								StreamingDynamicProperty property3;
								property3.nic_name = "NIC3";
								property3.delay = std::rand();
								property3.loss_rate = std::rand();
								property3.rate = std::rand();
								property3.rtt = std::rand();
								vecStreamingDynamicProperty.push_back(property3);

								StreamingDynamicProperty property4;
								property4.nic_name = "NIC4";
								property4.delay = std::rand();
								property4.loss_rate = std::rand();
								property4.rate = std::rand();
								property4.rtt = std::rand();
								vecStreamingDynamicProperty.push_back(property4);
							}
							StreamingDynamicPropertyHandler(id, vecStreamingDynamicProperty);
							//											std::string strResponse = ccm::CcmJsonParser::CreateStreamingDynamicPropertyNotify(id, vecStreamingDynamicProperty);
							//											Send(strResponse);
							Sleep(1000);
						}
					}
#endif //CCMDEBUG_STREAMING
				}
				else {
					printf("#StartQosStreamingSend Send Response to IPC CCM_NC_NOT_SUPPORTED\n");
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StartQosStreamingSend - NO streaming func");
					// For IPC
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StartQosStreamingSend CreateStartQosStreamingSendResponse");
					std::string strResponse = ccm::CcmJsonParser::CreateStartQosStreamingSendResponse(id, ccm::ErrorCode::CCM_NC_NOT_SUPPORTED);
					CCM_LOG_INFO("CCMCUI", "[ipc] StartQosStreamingSend response:%s", strResponse.c_str());
					Send(strResponse);
				}
			}
			else {
				printf("#StartQosStreamingSend Send Response to IPC CCM_UNKNOWN 1\n");
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StartQosStreamingSend - NO network client");
				// For IPC
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StartQosStreamingSend CreateStartQosStreamingSendResponse");
				std::string strResponse = ccm::CcmJsonParser::CreateStartQosStreamingSendResponse(id, ccm::ErrorCode::CCM_UNKNOWN);
				CCM_LOG_INFO("CCMCUI", "[ipc] StartQosStreamingSend response:%s", strResponse.c_str());
				Send(strResponse);
			}
		}
		catch (...) {
			printf("#StartQosStreamingSend Send Response to IPC CCM_UNKNOWN 2\n");
			printf("Request Error\n");
			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StartQosStreamingSend Request Error : exception");
			// For IPC
			ccm::NetworkClientID id = ccm::NetworkClientID(&client_model_name, &client_serial_number);
			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StartQosStreamingSend CreateStartQosStreamingSendResponse");
			std::string strResponse = ccm::CcmJsonParser::CreateStartQosStreamingSendResponse(id, ccm::ErrorCode::CCM_UNKNOWN);
			CCM_LOG_INFO("CCMCUI", "[ipc] StartQosStreamingSend response:%s", strResponse.c_str());
			Send(strResponse);
		}
		CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StartQosStreamingSend: end");
	}

	void CcmIpc::onCmdStartQosStreamingReceive_(const picojson::object& ccm_request) {
		CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StartQosStreamingReceive: start");
		std::string client_model_name;
		std::string client_serial_number;

		try {
			std::string client_id = ccm_request.find("data")->second.get<picojson::object>().find("ID")->second.get<std::string>();
			CCM_LOG_INFO("CCMCUI", "[ipc] StartQosStreamingReceive client_id:%s", client_id.c_str());

			QosStreamingRecieveConfig streamingParam;
			streamingParam.port = (uint32_t)ccm_request.find("data")->second.get<picojson::object>().find("port")->second.get<std::double_t>();
			CCM_LOG_INFO("CCMCUI", "[ipc] StartQosStreamingReceive params [streamingParam.port:%d]", streamingParam.port);
			streamingParam.video_format = ccm_request.find("data")->second.get<picojson::object>().find("video_format")->second.get<std::string>();
			CCM_LOG_INFO("CCMCUI", "[ipc] StartQosStreamingReceive params [streamingParam.video_format:%s]", streamingParam.video_format.c_str());
			streamingParam.gop = (uint32_t)ccm_request.find("data")->second.get<picojson::object>().find("gop")->second.get<std::double_t>();
			CCM_LOG_INFO("CCMCUI", "[ipc] StartQosStreamingReceive params [streamingParam.gop:%d]", streamingParam.gop);
			streamingParam.buffer_time = (uint32_t)ccm_request.find("data")->second.get<picojson::object>().find("buffer_time")->second.get<std::double_t>();
			CCM_LOG_INFO("CCMCUI", "[ipc] StartQosStreamingReceive params [streamingParam.buffer_time:%d]", streamingParam.buffer_time);
			streamingParam.chiper_key = ccm_request.find("data")->second.get<picojson::object>().find("chiper_key")->second.get<std::string>();
			CCM_LOG_INFO("CCMCUI", "[ipc] StartQosStreamingReceive params [streamingParam.chiper_key:%s]", streamingParam.chiper_key.c_str());
			streamingParam.ssrc = (uint32_t)ccm_request.find("data")->second.get<picojson::object>().find("ssrc")->second.get<std::double_t>();
			CCM_LOG_INFO("CCMCUI", "[ipc] StartQosStreamingReceive params [streamingParam.ssrc:%d]", streamingParam.ssrc);
			//
			const picojson::array& qos_interfaces = ccm_request.find("data")->second.get<picojson::object>().find("qos_interfaces")->second.get<picojson::array>();
			for (const auto& interface : qos_interfaces) {
				std::string qosInterface = interface.get<std::string>();
				CCM_LOG_INFO("CCMCUI", "[ipc] StartQosStreamingReceive params [streamingParam.qos_interfaces:%s]", qosInterface.c_str());
				streamingParam.qos_interfaces.push_back(qosInterface);
			}
			const picojson::array& meta_interfaces = ccm_request.find("data")->second.get<picojson::object>().find("meta_interfaces")->second.get<picojson::array>();
			for (const auto& interface : meta_interfaces) {
				std::string metaInterface = interface.get<std::string>();
				CCM_LOG_INFO("CCMCUI", "[ipc] StartQosStreamingReceive params [streamingParam.meta_interfaces:%s]", metaInterface.c_str());
				streamingParam.meta_interfaces.push_back(metaInterface);
			}

			client_model_name = client_id.substr(0, client_id.find("_"));
			client_serial_number = client_id.substr(client_id.find("_") + 1);
			ccm::NetworkClientID id = ccm::NetworkClientID(&client_model_name, &client_serial_number);

			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StartQosStreamingReceive - get network client");
			ccm::shared_ptr<ccm::NetworkClient> client = ccmsdk_->GetNetworkClient(id);
			if (client) {
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StartQosStreamingReceive - get streaming func");
				ccm::shared_ptr<ccm::StreamingFunction> streaming_function = client->GetStreamingFunction();
				if (streaming_function) {
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StartQosStreamingReceive calling...");
					ccm::ErrorCode err = streaming_function->StartQosStreamingRecieve(streamingParam);
					CCM_LOG_INFO("CCMCUI", "[ipc] StartQosStreamingReceive StartQosStreamingRecieve() error:%d", err);
					// For IPC
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StartQosStreamingReceive CreateStartQosStreamingReceiveResponse");
					std::string strResponse = ccm::CcmJsonParser::CreateStartQosStreamingReceiveResponse(id, err);
					CCM_LOG_INFO("CCMCUI", "[ipc] StartQosStreamingReceive response:%s", strResponse.c_str());
					printf("#StartQosStreamingReceive Send Response to IPC\n");
					Send(strResponse);
				}
				else {
					printf("#StartQosStreamingReceive Send Response to IPC CCM_NC_NOT_SUPPORTED\n");
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StartQosStreamingReceive - NO streaming func");
					// For IPC
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StartQosStreamingReceive CreateStartQosStreamingReceiveResponse");
					std::string strResponse = ccm::CcmJsonParser::CreateStartQosStreamingReceiveResponse(id, ccm::ErrorCode::CCM_NC_NOT_SUPPORTED);
					CCM_LOG_INFO("CCMCUI", "[ipc] StartQosStreamingReceive response:%s", strResponse.c_str());
					Send(strResponse);
				}
			}
			else {
				printf("#StartQosStreamingReceive Send Response to IPC CCM_UNKNOWN 1\n");
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StartQosStreamingReceive - NO network client");
				// For IPC
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StartQosStreamingReceive CreateStartQosStreamingReceiveResponse");
				std::string strResponse = ccm::CcmJsonParser::CreateStartQosStreamingReceiveResponse(id, ccm::ErrorCode::CCM_UNKNOWN);
				CCM_LOG_INFO("CCMCUI", "[ipc] StartQosStreamingReceive response:%s", strResponse.c_str());
				Send(strResponse);
			}
		}
		catch (...) {
			printf("#StartQosStreamingReceive Send Response to IPC CCM_UNKNOWN 2\n");
			printf("Request Error\n");
			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StartQosStreamingReceive Request Error : exception");
			// For IPC
			ccm::NetworkClientID id = ccm::NetworkClientID(&client_model_name, &client_serial_number);
			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StartQosStreamingReceive CreateStartQosStreamingReceiveResponse");
			std::string strResponse = ccm::CcmJsonParser::CreateStartQosStreamingReceiveResponse(id, ccm::ErrorCode::CCM_UNKNOWN);
			CCM_LOG_INFO("CCMCUI", "[ipc] StartQosStreamingReceive response:%s", strResponse.c_str());
			Send(strResponse);
		}
		CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StartQosStreamingReceive: end");
	}

	void CcmIpc::onCmdStopStreamingSend_(const picojson::object& ccm_request) {
		CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StopStreamingSend: start");
		std::string client_model_name;
		std::string client_serial_number;

		try {
			std::string client_id = ccm_request.find("data")->second.get<picojson::object>().find("ID")->second.get<std::string>();
			CCM_LOG_INFO("CCMCUI", "[ipc] StopStreamingSend client_id:%s", client_id.c_str());

			client_model_name = client_id.substr(0, client_id.find("_"));
			client_serial_number = client_id.substr(client_id.find("_") + 1);
			ccm::NetworkClientID id = ccm::NetworkClientID(&client_model_name, &client_serial_number);

			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StopStreamingSend - get network client");
			ccm::shared_ptr<ccm::NetworkClient> client = ccmsdk_->GetNetworkClient(id);
			if (client) {
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StopStreamingSend - get streaming func");
				ccm::shared_ptr<ccm::StreamingFunction> streaming_function = client->GetStreamingFunction();
				if (streaming_function) {
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StopStreamingSend calling...");
					ccm::ErrorCode err = streaming_function->StopStreamingSend();
					CCM_LOG_INFO("CCMCUI", "[ipc] StopStreamingSend StopStreamingSend() error:%d", err);
					// For IPC
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StopStreamingSend CreateStopStreamingSendResponse");
					std::string strResponse = ccm::CcmJsonParser::CreateStopStreamingSendResponse(id, err);
					CCM_LOG_INFO("CCMCUI", "[ipc] StopStreamingSend response:%s", strResponse.c_str());
					printf("#StopStreamingSend Send Response to IPC\n");
					Send(strResponse);
				}
				else {
					printf("#StopStreamingSend Send Response to IPC CCM_NC_NOT_SUPPORTED\n");
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StopStreamingSend - NO streaming func");
					// For IPC
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StopStreamingSend CreateStopStreamingSendResponse");
					std::string strResponse = ccm::CcmJsonParser::CreateStopStreamingSendResponse(id, ccm::ErrorCode::CCM_NC_NOT_SUPPORTED);
					CCM_LOG_INFO("CCMCUI", "[ipc] StopStreamingSend response:%s", strResponse.c_str());
					Send(strResponse);
				}
			}
			else {
				printf("#StopStreamingSend Send Response to IPC CCM_UNKNOWN 1\n");
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StopStreamingSend - NO network client");
				// For IPC
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StopStreamingSend CreateStopStreamingSendResponse");
				std::string strResponse = ccm::CcmJsonParser::CreateStopStreamingSendResponse(id, ccm::ErrorCode::CCM_UNKNOWN);
				CCM_LOG_INFO("CCMCUI", "[ipc] StopStreamingSend response:%s", strResponse.c_str());
				Send(strResponse);
			}
		}
		catch (...) {
			printf("#StopStreamingSend Send Response to IPC CCM_UNKNOWN 2\n");
			printf("Request Error\n");
			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StopStreamingSend Request Error : exception");
			// For IPC
			ccm::NetworkClientID id = ccm::NetworkClientID(&client_model_name, &client_serial_number);
			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StopStreamingSend CreateStopStreamingSendResponse");
			std::string strResponse = ccm::CcmJsonParser::CreateStopStreamingSendResponse(id, ccm::ErrorCode::CCM_UNKNOWN);
			CCM_LOG_INFO("CCMCUI", "[ipc] StopStreamingSend response:%s", strResponse.c_str());
			Send(strResponse);
		}
		CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StopStreamingSend: end");
	}

	void CcmIpc::onCmdStopStreamingRecieve_(const picojson::object& ccm_request) {
		CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StopStreamingRecieve: start");
		std::string client_model_name;
		std::string client_serial_number;

		try {
			std::string client_id = ccm_request.find("data")->second.get<picojson::object>().find("ID")->second.get<std::string>();
			CCM_LOG_INFO("CCMCUI", "[ipc] StopStreamingRecieve client_id:%s", client_id.c_str());

			client_model_name = client_id.substr(0, client_id.find("_"));
			client_serial_number = client_id.substr(client_id.find("_") + 1);
			ccm::NetworkClientID id = ccm::NetworkClientID(&client_model_name, &client_serial_number);

			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StopStreamingRecieve - get network client");
			ccm::shared_ptr<ccm::NetworkClient> client = ccmsdk_->GetNetworkClient(id);
			if (client) {
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StopStreamingRecieve - get streaming func");
				ccm::shared_ptr<ccm::StreamingFunction> streaming_function = client->GetStreamingFunction();
				if (streaming_function) {
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StopStreamingRecieve calling...");
					ccm::ErrorCode err = streaming_function->StopStreamingRecieve();
					CCM_LOG_INFO("CCMCUI", "[ipc] StopStreamingRecieve StopStreamingRecieve() error:%d", err);
					// For IPC
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StopStreamingRecieve CreateStopStreamingRecieveResponse");
					std::string strResponse = ccm::CcmJsonParser::CreateStopStreamingRecieveResponse(id, err);
					CCM_LOG_INFO("CCMCUI", "[ipc] StopStreamingRecieve response:%s", strResponse.c_str());
					printf("#StopStreamingRecieve Send Response to IPC\n");
					Send(strResponse);
				}
				else {
					printf("#StopStreamingRecieve Send Response to IPC CCM_NC_NOT_SUPPORTED\n");
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StopStreamingRecieve - NO streaming func");
					// For IPC
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StopStreamingRecieve CreateStopStreamingRecieveResponse");
					std::string strResponse = ccm::CcmJsonParser::CreateStopStreamingRecieveResponse(id, ccm::ErrorCode::CCM_NC_NOT_SUPPORTED);
					CCM_LOG_INFO("CCMCUI", "[ipc] StopStreamingRecieve response:%s", strResponse.c_str());
					Send(strResponse);
				}
			}
			else {
				printf("#StopStreamingRecieve Send Response to IPC CCM_UNKNOWN 1\n");
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StopStreamingRecieve - NO network client");
				// For IPC
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StopStreamingRecieve CreateStopStreamingRecieveResponse");
				std::string strResponse = ccm::CcmJsonParser::CreateStopStreamingRecieveResponse(id, ccm::ErrorCode::CCM_UNKNOWN);
				CCM_LOG_INFO("CCMCUI", "[ipc] StopStreamingRecieve response:%s", strResponse.c_str());
				Send(strResponse);
			}
		}
		catch (...) {
			printf("#StopStreamingRecieve Send Response to IPC CCM_UNKNOWN 2\n");
			printf("Request Error\n");
			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StopStreamingRecieve Request Error : exception");
			// For IPC
			ccm::NetworkClientID id = ccm::NetworkClientID(&client_model_name, &client_serial_number);
			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StopStreamingRecieve CreateStopStreamingRecieveResponse");
			std::string strResponse = ccm::CcmJsonParser::CreateStopStreamingRecieveResponse(id, ccm::ErrorCode::CCM_UNKNOWN);
			CCM_LOG_INFO("CCMCUI", "[ipc] StopStreamingRecieve response:%s", strResponse.c_str());
			Send(strResponse);
		}
		CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StopStreamingRecieve: end");
	}

	/**
	 * PlanningMetadata一覧取得要求
	 */
	void CcmIpc::onCmdGetPlanningMetadataList_(const picojson::object& ccm_request) {
		CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetPlanningMetadataList: start");
		std::string client_model_name;
		std::string client_serial_number;
		std::vector<ccm::PlanningMetadataFile> vecPlanningMetadataFile;

		try {
			std::string client_id = ccm_request.find("data")->second.get<picojson::object>().find("ID")->second.get<std::string>();
			CCM_LOG_INFO("CCMCUI", "[ipc] GetPlanningMetadataList client_id:%s", client_id.c_str());

			client_model_name = client_id.substr(0, client_id.find("_"));
			client_serial_number = client_id.substr(client_id.find("_") + 1);
			ccm::NetworkClientID id = ccm::NetworkClientID(&client_model_name, &client_serial_number);

			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetPlanningMetadataList - get network client");
			ccm::shared_ptr<ccm::NetworkClient> client = ccmsdk_->GetNetworkClient(id);
			if (client) {
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetPlanningMetadataList - get recorder func");
				ccm::shared_ptr<ccm::RecorderFunction> recorder_function = client->GetRecorderFunction();
				if (recorder_function) {
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetPlanningMetadataList calling...");
					ccm::ErrorCode err = recorder_function->GetPlanningMetadataList(&vecPlanningMetadataFile);
					CCM_LOG_INFO("CCMCUI", "[ipc] GetPlanningMetadataList GetPlanningMetadataList() error:%d", err);
					// For IPC
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetPlanningMetadataList CreateGetPlanningMetadataListResponse");
					std::string strResponse = ccm::CcmJsonParser::CreateGetPlanningMetadataListResponse(id, vecPlanningMetadataFile, err);
					CCM_LOG_INFO("CCMCUI", "[ipc] GetPlanningMetadataList response:%s", strResponse.c_str());
					printf("#GetPlanningMetadataList Send Response to IPC\n");
					Send(strResponse);
				}
				else {
					printf("#GetPlanningMetadataList Send Response to IPC CCM_NC_NOT_SUPPORTED\n");
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetPlanningMetadataList - NO planning metadata func");
					// For IPC
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetPlanningMetadataList CreateGetPlanningMetadataListResponse");
					ccm::ErrorCode err = ccm::ErrorCode::CCM_NC_NOT_SUPPORTED;
					std::string strResponse = ccm::CcmJsonParser::CreateGetPlanningMetadataListResponse(id, vecPlanningMetadataFile, err);
					CCM_LOG_INFO("CCMCUI", "[ipc] GetPlanningMetadataList response:%s", strResponse.c_str());
					Send(strResponse);
				}
			}
			else {
				printf("#GetPlanningMetadataList Send Response to IPC CCM_UNKNOWN 1\n");
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetPlanningMetadataList - NO network client");
				// For IPC
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetPlanningMetadataList CreateGetPlanningMetadataListResponse");
				ccm::ErrorCode err = ccm::ErrorCode::CCM_UNKNOWN;
				std::string strResponse = ccm::CcmJsonParser::CreateGetPlanningMetadataListResponse(id, vecPlanningMetadataFile, err);
				CCM_LOG_INFO("CCMCUI", "[ipc] GetPlanningMetadataList response:%s", strResponse.c_str());
				Send(strResponse);
			}
		}
		catch (...) {
			printf("#GetPlanningMetadataList Send Response to IPC CCM_UNKNOWN 2\n");
			printf("Request Error\n");
			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetPlanningMetadataList Request Error : exception");
			// For IPC
			ccm::NetworkClientID id = ccm::NetworkClientID(&client_model_name, &client_serial_number);
			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetPlanningMetadataList CreateGetPlanningMetadataListResponse");
			ccm::ErrorCode err = ccm::ErrorCode::CCM_UNKNOWN;
			std::string strResponse = ccm::CcmJsonParser::CreateGetPlanningMetadataListResponse(id, vecPlanningMetadataFile, err);
			CCM_LOG_INFO("CCMCUI", "[ipc] GetPlanningMetadataList response:%s", strResponse.c_str());
			Send(strResponse);
		}
		CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetPlanningMetadataList: end");
	}

	void CcmIpc::onCmdGetCurrentPlanningMetadata_(const picojson::object& ccm_request) {
		CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetCurrentPlanningMetadata: start");
		std::string client_model_name;
		std::string client_serial_number;
		std::string metadata;

		try {
			std::string client_id = ccm_request.find("data")->second.get<picojson::object>().find("ID")->second.get<std::string>();
			CCM_LOG_INFO("CCMCUI", "[ipc] GetCurrentPlanningMetadata client_id:%s", client_id.c_str());

			client_model_name = client_id.substr(0, client_id.find("_"));
			client_serial_number = client_id.substr(client_id.find("_") + 1);
			ccm::NetworkClientID id = ccm::NetworkClientID(&client_model_name, &client_serial_number);

			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetCurrentPlanningMetadata - get network client");
			ccm::shared_ptr<ccm::NetworkClient> client = ccmsdk_->GetNetworkClient(id);
			if (client) {
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetCurrentPlanningMetadata - get recorder func");
				ccm::shared_ptr<ccm::RecorderFunction> recorder_function = client->GetRecorderFunction();
				if (recorder_function) {
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetCurrentPlanningMetadata calling...");
					ccm::ErrorCode err = recorder_function->GetCurrentPlanningMetadata(&metadata);
					CCM_LOG_INFO("CCMCUI", "[ipc] GetCurrentPlanningMetadata GetCurrentPlanningMetadata() error:%d", err);
					// For IPC
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetCurrentPlanningMetadata CreateGetCurrentPlanningMetadataResponse");
					std::string strResponse = ccm::CcmJsonParser::CreateGetCurrentPlanningMetadataResponse(id, metadata, err);
					CCM_LOG_INFO("CCMCUI", "[ipc] GetCurrentPlanningMetadata response:%s", strResponse.c_str());
					printf("#GetCurrentPlanningMetadata Send Response to IPC\n");
					Send(strResponse);
				}
				else {
					printf("#GetCurrentPlanningMetadata Send Response to IPC CCM_NC_NOT_SUPPORTED\n");
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetCurrentPlanningMetadata - NO recorder func");
					// For IPC
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetCurrentPlanningMetadata CreateGetCurrentPlanningMetadataResponse");
					ccm::ErrorCode err = ccm::ErrorCode::CCM_NC_NOT_SUPPORTED;
					std::string strResponse = ccm::CcmJsonParser::CreateGetCurrentPlanningMetadataResponse(id, metadata, err);
					CCM_LOG_INFO("CCMCUI", "[ipc] GetCurrentPlanningMetadata response:%s", strResponse.c_str());
					Send(strResponse);
				}
			}
			else {
				printf("#GetCurrentPlanningMetadata Send Response to IPC CCM_UNKNOWN 1\n");
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetCurrentPlanningMetadata - NO network client");
				// For IPC
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetCurrentPlanningMetadata CreateGetCurrentPlanningMetadataResponse");
				ccm::ErrorCode err = ccm::ErrorCode::CCM_UNKNOWN;
				std::string strResponse = ccm::CcmJsonParser::CreateGetCurrentPlanningMetadataResponse(id, metadata, err);
				CCM_LOG_INFO("CCMCUI", "[ipc] GetCurrentPlanningMetadata response:%s", strResponse.c_str());
				Send(strResponse);
			}
		}
		catch (...) {
			printf("#GetCurrentPlanningMetadata Send Response to IPC CCM_UNKNOWN 2\n");
			printf("Request Error\n");
			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetCurrentPlanningMetadata Request Error : exception");
			// For IPC
			ccm::NetworkClientID id = ccm::NetworkClientID(&client_model_name, &client_serial_number);
			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetCurrentPlanningMetadata CreateGetCurrentPlanningMetadataResponse");
			ccm::ErrorCode err = ccm::ErrorCode::CCM_UNKNOWN;
			std::string strResponse = ccm::CcmJsonParser::CreateGetCurrentPlanningMetadataResponse(id, metadata, err);
			CCM_LOG_INFO("CCMCUI", "[ipc] GetCurrentPlanningMetadata response:%s", strResponse.c_str());
			Send(strResponse);
		}

		CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetCurrentPlanningMetadata: end");
	}

	void CcmIpc::onCmdGetPlanningMetadata_(const picojson::object& ccm_request) {
		CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetPlanningMetadata: start");
		std::string client_model_name;
		std::string client_serial_number;
		std::string name;
		std::string uri;
		std::string metadata;

		try {
			std::string client_id = ccm_request.find("data")->second.get<picojson::object>().find("ID")->second.get<std::string>();
			CCM_LOG_INFO("CCMCUI", "[ipc] GetPlanningMetadata client_id:%s", client_id.c_str());
			name = ccm_request.find("data")->second.get<picojson::object>().find("Name")->second.get<std::string>();
			CCM_LOG_INFO("CCMCUI", "[ipc] GetPlanningMetadata name:%s", name.c_str());
			uri = ccm_request.find("data")->second.get<picojson::object>().find("Uri")->second.get<std::string>();
			CCM_LOG_INFO("CCMCUI", "[ipc] GetPlanningMetadata uri:%s", uri.c_str());

			client_model_name = client_id.substr(0, client_id.find("_"));
			client_serial_number = client_id.substr(client_id.find("_") + 1);
			ccm::NetworkClientID id = ccm::NetworkClientID(&client_model_name, &client_serial_number);

			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetPlanningMetadata - get network client");
			ccm::shared_ptr<ccm::NetworkClient> client = ccmsdk_->GetNetworkClient(id);
			if (client) {
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetPlanningMetadata - get recorder func");
				ccm::shared_ptr<ccm::RecorderFunction> recorder_function = client->GetRecorderFunction();
				if (recorder_function) {
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetPlanningMetadata calling...");
					ccm::ErrorCode err = recorder_function->GetPlanningMetadataDetail(uri, &metadata);

					CCM_LOG_INFO("CCMCUI", "[ipc] GetPlanningMetadata GetPlanningMetadataDetail() error:%d", err);
					// For IPC
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetPlanningMetadata CreateGetPlanningMetadataResponse");
					std::string strResponse = ccm::CcmJsonParser::CreateGetPlanningMetadataResponse(id, name, uri, metadata, err);
					CCM_LOG_INFO("CCMCUI", "[ipc] GetPlanningMetadata response:%s", strResponse.c_str());
					printf("#GetPlanningMetadata Send Response to IPC\n");
					Send(strResponse);
				}
				else {
					printf("#GetPlanningMetadata Send Response to IPC CCM_NC_NOT_SUPPORTED\n");
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetPlanningMetadata - NO recorder func");
					// For IPC
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetPlanningMetadata CreateGetPlanningMetadataResponse");
					ccm::ErrorCode err = ccm::ErrorCode::CCM_NC_NOT_SUPPORTED;
					std::string strResponse = ccm::CcmJsonParser::CreateGetPlanningMetadataResponse(id, name, uri, metadata, err);
					CCM_LOG_INFO("CCMCUI", "[ipc] GetPlanningMetadata response:%s", strResponse.c_str());
					Send(strResponse);
				}
			}
			else {
				printf("#GetPlanningMetadata Send Response to IPC CCM_UNKNOWN 1\n");
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetPlanningMetadata - NO network client");
				// For IPC
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetPlanningMetadata CreateGetPlanningMetadataResponse");
				ccm::ErrorCode err = ccm::ErrorCode::CCM_UNKNOWN;
				std::string strResponse = ccm::CcmJsonParser::CreateGetPlanningMetadataResponse(id, name, uri, metadata, err);
				CCM_LOG_INFO("CCMCUI", "[ipc] GetPlanningMetadata response:%s", strResponse.c_str());
				Send(strResponse);
			}
		}
		catch (...) {
			printf("#GetPlanningMetadata Send Response to IPC CCM_UNKNOWN 2\n");
			printf("Request Error\n");
			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetPlanningMetadata Request Error : exception");
			// For IPC
			ccm::NetworkClientID id = ccm::NetworkClientID(&client_model_name, &client_serial_number);
			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetPlanningMetadata CreateGetPlanningMetadataResponse");
			ccm::ErrorCode err = ccm::ErrorCode::CCM_UNKNOWN;
			std::string strResponse = ccm::CcmJsonParser::CreateGetPlanningMetadataResponse(id, name, uri, metadata, err);
			CCM_LOG_INFO("CCMCUI", "[ipc] GetPlanningMetadata response:%s", strResponse.c_str());
			Send(strResponse);
		}
		CCM_LOG_INFO("CCMCUI", "%s", "[ipc] GetPlanningMetadata: end");
	}

	void CcmIpc::onCmdSetPlanningMetadata_(const picojson::object& ccm_request) {
		CCM_LOG_INFO("CCMCUI", "%s", "[ipc] SetPlanningMetadata: start");
		std::string client_model_name;
		std::string client_serial_number;
		std::string fileName;
		std::string uri;
		std::string error;

		try {
			std::string client_id = ccm_request.find("data")->second.get<picojson::object>().find("ID")->second.get<std::string>();
			CCM_LOG_INFO("CCMCUI", "[ipc] SetPlanningMetadata client_id:%s", client_id.c_str());
			uri = ccm_request.find("data")->second.get<picojson::object>().find("Uri")->second.get<std::string>();
			CCM_LOG_INFO("CCMCUI", "[ipc] SetPlanningMetadata uri:%s", uri.c_str());
			fileName = ccm_request.find("data")->second.get<picojson::object>().find("Name")->second.get<std::string>();
			CCM_LOG_INFO("CCMCUI", "[ipc] SetPlanningMetadata name:%s", fileName.c_str());

			client_model_name = client_id.substr(0, client_id.find("_"));
			client_serial_number = client_id.substr(client_id.find("_") + 1);
			ccm::NetworkClientID id = ccm::NetworkClientID(&client_model_name, &client_serial_number);

			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] SetPlanningMetadata - get network client");
			ccm::shared_ptr<ccm::NetworkClient> client = ccmsdk_->GetNetworkClient(id);
			if (client) {
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] SetPlanningMetadata - get recorder func");
				ccm::shared_ptr<ccm::RecorderFunction> recorder_function = client->GetRecorderFunction();
				if (recorder_function) {
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] SetPlanningMetadata calling...");
					ccm::ErrorCode err = recorder_function->SetPlanningMetadata(&error, uri);

					CCM_LOG_INFO("CCMCUI", "[ipc] SetPlanningMetadata SetPlanningMetadata() error:%d", err);
					// For IPC
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] SetPlanningMetadata CreateSetPlanningMetadataResponse");
					std::string strResponse = ccm::CcmJsonParser::CreateSetPlanningMetadataResponse(id, err, error, fileName, uri);
					CCM_LOG_INFO("CCMCUI", "[ipc] SetPlanningMetadata response:%s", strResponse.c_str());
					printf("#SetPlanningMetadata Send Response to IPC\n");
					Send(strResponse);
				}
				else {
					printf("#SetPlanningMetadata Send Response to IPC CCM_NC_NOT_SUPPORTED\n");
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] SetPlanningMetadata - NO recorder func");
					// For IPC
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] SetPlanningMetadata CreateSetPlanningMetadataResponse");
					ccm::ErrorCode err = ccm::ErrorCode::CCM_NC_NOT_SUPPORTED;
					std::string strResponse = ccm::CcmJsonParser::CreateSetPlanningMetadataResponse(id, err, error, fileName, uri);
					CCM_LOG_INFO("CCMCUI", "[ipc] SetPlanningMetadata response:%s", strResponse.c_str());
					Send(strResponse);
				}
			}
			else {
				printf("#SetPlanningMetadata Send Response to IPC CCM_UNKNOWN 1\n");
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] SetPlanningMetadata - NO network client");
				// For IPC
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] SetPlanningMetadata CreateSetPlanningMetadataResponse");
				ccm::ErrorCode err = ccm::ErrorCode::CCM_UNKNOWN;
				std::string strResponse = ccm::CcmJsonParser::CreateSetPlanningMetadataResponse(id, err, error, fileName, uri);
				CCM_LOG_INFO("CCMCUI", "[ipc] SetPlanningMetadata response:%s", strResponse.c_str());
				Send(strResponse);
			}
		}
		catch (...) {
			printf("#SetPlanningMetadata Send Response to IPC CCM_UNKNOWN 2\n");
			printf("Request Error\n");
			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] SetPlanningMetadata Request Error : exception");
			// For IPC
			ccm::NetworkClientID id = ccm::NetworkClientID(&client_model_name, &client_serial_number);
			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] SetPlanningMetadata CreateSetPlanningMetadataResponse");
			ccm::ErrorCode err = ccm::ErrorCode::CCM_UNKNOWN;
			std::string strResponse = ccm::CcmJsonParser::CreateSetPlanningMetadataResponse(id, err, error, fileName, uri);
			CCM_LOG_INFO("CCMCUI", "[ipc] SetPlanningMetadata response:%s", strResponse.c_str());
			Send(strResponse);
		}
		CCM_LOG_INFO("CCMCUI", "%s", "[ipc] SetPlanningMetadata: end");
	}

	void CcmIpc::onCmdPutPlanningMetadata_(const picojson::object& ccm_request) {
		CCM_LOG_INFO("CCMCUI", "%s", "[ipc] PutPlanningMetadata: start");
		std::string client_model_name;
		std::string client_serial_number;
		std::string fileName;
		std::string metadata;
		bool metadataOverride;
		std::string error;

		try {
			std::string client_id = ccm_request.find("data")->second.get<picojson::object>().find("ID")->second.get<std::string>();
			CCM_LOG_INFO("CCMCUI", "[ipc] PutPlanningMetadata client_id:%s", client_id.c_str());
			fileName = ccm_request.find("data")->second.get<picojson::object>().find("Name")->second.get<std::string>();
			CCM_LOG_INFO("CCMCUI", "[ipc] PutPlanningMetadata name:%s", fileName.c_str());
			metadata = ccm_request.find("data")->second.get<picojson::object>().find("PlanningMetadata")->second.get<std::string>();
			CCM_LOG_INFO("CCMCUI", "[ipc] PutPlanningMetadata metadata:%s", metadata.c_str());
			metadataOverride = ccm_request.find("data")->second.get<picojson::object>().find("Override")->second.get<bool>();
			CCM_LOG_INFO("CCMCUI", "[ipc] PutPlanningMetadata override:%s", (metadataOverride == true)? "True" : "False");

			client_model_name = client_id.substr(0, client_id.find("_"));
			client_serial_number = client_id.substr(client_id.find("_") + 1);
			ccm::NetworkClientID id = ccm::NetworkClientID(&client_model_name, &client_serial_number);

			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] PutPlanningMetadata - get network client");
			ccm::shared_ptr<ccm::NetworkClient> client = ccmsdk_->GetNetworkClient(id);
			if (client) {
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] PutPlanningMetadata - get recorder func");
				ccm::shared_ptr<ccm::RecorderFunction> recorder_function = client->GetRecorderFunction();
				if (recorder_function) {
					PutPlanningMetadataFile putData;
					putData.name = fileName;
					putData.xml = metadata;
					putData.override = metadataOverride;
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] PutPlanningMetadata calling...");
					ccm::ErrorCode err = recorder_function->PutPlanningMetadata(&error, putData);

					CCM_LOG_INFO("CCMCUI", "[ipc] PutPlanningMetadata PutPlanningMetadata() error:%d", err);
					// For IPC
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] PutPlanningMetadata CreatePutPlanningMetadataResponse");
					std::string strResponse = ccm::CcmJsonParser::CreatePutPlanningMetadataResponse(id, err, error, fileName);
					CCM_LOG_INFO("CCMCUI", "[ipc] PutPlanningMetadata response:%s", strResponse.c_str());
					printf("#PutPlanningMetadata Send Response to IPC\n");
					Send(strResponse);
				}
				else {
					printf("#PutPlanningMetadata Send Response to IPC CCM_NC_NOT_SUPPORTED\n");
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] PutPlanningMetadata - NO recorder func");
					// For IPC
					CCM_LOG_INFO("CCMCUI", "%s", "[ipc] PutPlanningMetadata CreatePutPlanningMetadataResponse");
					ccm::ErrorCode err = ccm::ErrorCode::CCM_NC_NOT_SUPPORTED;
					std::string strResponse = ccm::CcmJsonParser::CreatePutPlanningMetadataResponse(id, err, error, fileName);
					CCM_LOG_INFO("CCMCUI", "[ipc] PutPlanningMetadata response:%s", strResponse.c_str());
					Send(strResponse);
				}
			}
			else {
				printf("#PutPlanningMetadata Send Response to IPC CCM_UNKNOWN 1\n");
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] PutPlanningMetadata - NO network client");
				// For IPC
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] PutPlanningMetadata CreatePutPlanningMetadataResponse");
				ccm::ErrorCode err = ccm::ErrorCode::CCM_UNKNOWN;
				std::string strResponse = ccm::CcmJsonParser::CreatePutPlanningMetadataResponse(id, err, error, fileName);
				CCM_LOG_INFO("CCMCUI", "[ipc] PutPlanningMetadata response:%s", strResponse.c_str());
				Send(strResponse);
			}
		}
		catch (...) {
			printf("#PutPlanningMetadata Send Response to IPC CCM_UNKNOWN 2\n");
			printf("Request Error\n");
			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] PutPlanningMetadata Request Error : exception");
			// For IPC
			ccm::NetworkClientID id = ccm::NetworkClientID(&client_model_name, &client_serial_number);
			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] PutPlanningMetadata CreatePutPlanningMetadataResponse");
			ccm::ErrorCode err = ccm::ErrorCode::CCM_UNKNOWN;
			std::string strResponse = ccm::CcmJsonParser::CreatePutPlanningMetadataResponse(id, err, error, fileName);
			CCM_LOG_INFO("CCMCUI", "[ipc] PutPlanningMetadata response:%s", strResponse.c_str());
			Send(strResponse);
		}
		CCM_LOG_INFO("CCMCUI", "%s", "[ipc] PutPlanningMetadata: end");
	}

	void CcmIpc::onCmdStartVoIP_(const picojson::object& ccm_request) {
		CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StartVoIP: start");
		std::string client_model_name;
		std::string client_serial_number;

		try {
			std::string client_id = ccm_request.find("data")->second.get<picojson::object>().find("ID")->second.get<std::string>();
			CCM_LOG_INFO("CCMCUI", "[ipc] StartVoIP client_id:%s", client_id.c_str());

			VoIPConfig voipParam;
			voipParam.destination_address = ccm_request.find("data")->second.get<picojson::object>().find("address")->second.get<std::string>();
			CCM_LOG_INFO("CCMCUI", "[ipc] StartVoIP params [voipParam.address:%s]", voipParam.destination_address.c_str());

			voipParam.destination_port = (uint32_t)ccm_request.find("data")->second.get<picojson::object>().find("port_d")->second.get<std::double_t>();
			CCM_LOG_INFO("CCMCUI", "[ipc] StartVoIP params [voipParam.destination_port:%d]", voipParam.destination_port);

			voipParam.receiving_port = (uint32_t)ccm_request.find("data")->second.get<picojson::object>().find("port_r")->second.get<std::double_t>();
			CCM_LOG_INFO("CCMCUI", "[ipc] StartVoIP params [voipParam.receiving_port:%d]", voipParam.receiving_port);


			std::string str_a;
			std::uint32_t data_b;

			data_b = ccm_request.find("data")->second.get<picojson::object>().find("framelen_t")->second.get<std::double_t>();
			voipParam.frame_length.emplace(std::make_pair("framelen_t", data_b));
			CCM_LOG_INFO("CCMCUI", "[ipc] StartVoIP params [voipParam.framelen_t:%d]", data_b);

			data_b = ccm_request.find("data")->second.get<picojson::object>().find("framelen_r")->second.get<std::double_t>();
			voipParam.frame_length.emplace(std::make_pair("framelen_r", data_b));
			CCM_LOG_INFO("CCMCUI", "[ipc] StartVoIP params [voipParam.framelen_r:%d]", data_b);

			str_a = ccm_request.find("data")->second.get<picojson::object>().find("chiperkey_t")->second.get<std::string>();
			voipParam.cipher_key.emplace(std::make_pair("chiperkey_t", str_a));
			CCM_LOG_INFO("CCMCUI", "[ipc] StartVoIP params [voipParam.chiperkey_t:%s]", str_a);

			str_a = ccm_request.find("data")->second.get<picojson::object>().find("chiperkey_r")->second.get<std::string>();
			voipParam.cipher_key.emplace(std::make_pair("chiperkey_r", str_a));
			CCM_LOG_INFO("CCMCUI", "[ipc] StartVoIP params [voipParam.chiperkey_r:%s]", str_a);

			str_a = ccm_request.find("data")->second.get<picojson::object>().find("ssrc_t")->second.get<std::string>();
			voipParam.ssrc.emplace(std::make_pair("ssrc_t", str_a));
			CCM_LOG_INFO("CCMCUI", "[ipc] StartVoIP params [voipParam.ssrc_t:%s]", str_a);

			str_a = ccm_request.find("data")->second.get<picojson::object>().find("ssrc_r")->second.get<std::string>();
			voipParam.ssrc.emplace(std::make_pair("ssrc_r", str_a));
			CCM_LOG_INFO("CCMCUI", "[ipc] StartVoIP params [voipParam.ssrc_r:%s]", str_a);





			//			voipParam.ssrc = (uint32_t)ccm_request.find("data")->second.get<picojson::object>().find("ssrc")->second.get<std::double_t>();
//			CCM_LOG_INFO("CCMCUI", "[ipc] StartVoIP params [voipParam.ssrc:%d]", voipParam.ssrc);
			//

			client_model_name = client_id.substr(0, client_id.find("_"));
			client_serial_number = client_id.substr(client_id.find("_") + 1);
			ccm::NetworkClientID id = ccm::NetworkClientID(&client_model_name, &client_serial_number);

			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StartVoIP - get network client");
			ccm::shared_ptr<ccm::NetworkClient> client = ccmsdk_->GetNetworkClient(id);

			if (client) {
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StartVoIP - get streaming func");
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StartVoIP calling...");
				ccm::ErrorCode err = ccmsdk_->StartVoIP(id, voipParam);
				CCM_LOG_INFO("CCMCUI", "[ipc] StartVoIP StartVoIP() error:%d", err);

				// For IPC
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StartVoIP CreateStartVoIPResponse");
				std::string strResponse = ccm::CcmJsonParser::CreateStartVoIPResponse(id, err);
				CCM_LOG_INFO("CCMCUI", "[ipc] StartVoIP response:%s", strResponse.c_str());
				printf("#StartVoIP Send Response to IPC\n");
				Send(strResponse);

#ifdef CCMDEBUG_STREAMING
#endif //CCMDEBUG_STREAMING
			}
			else {
				printf("#StartVoIP Send Response to IPC CCM_UNKNOWN 1\n");
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StartVoIP - NO network client");
				// For IPC
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StartVoIP CreateStartVoIPResponse");
				std::string strResponse = ccm::CcmJsonParser::CreateStartVoIPResponse(id, ccm::ErrorCode::CCM_UNKNOWN);
				CCM_LOG_INFO("CCMCUI", "[ipc] StartVoIP response:%s", strResponse.c_str());
				Send(strResponse);
			}
		}
		catch (...) {
			printf("#StartVoIP Send Response to IPC CCM_UNKNOWN 2\n");
			printf("Request Error\n");
			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StartVoIP Request Error : exception");
			// For IPC
			ccm::NetworkClientID id = ccm::NetworkClientID(&client_model_name, &client_serial_number);
			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StartVoIP CreateStartVoIPResponse");
			std::string strResponse = ccm::CcmJsonParser::CreateStartVoIPResponse(id, ccm::ErrorCode::CCM_UNKNOWN);
			CCM_LOG_INFO("CCMCUI", "[ipc] StartVoIP response:%s", strResponse.c_str());
			Send(strResponse);
		}
		CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StartVoIP: end");
	}

	void CcmIpc::onCmdStopVoIP_(const picojson::object& ccm_request) {
		CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StopVoIP: start");
		std::string client_model_name;
		std::string client_serial_number;

		try {
			std::string client_id = ccm_request.find("data")->second.get<picojson::object>().find("ID")->second.get<std::string>();
			CCM_LOG_INFO("CCMCUI", "[ipc] StopVoIP client_id:%s", client_id.c_str());

			client_model_name = client_id.substr(0, client_id.find("_"));
			client_serial_number = client_id.substr(client_id.find("_") + 1);
			ccm::NetworkClientID id = ccm::NetworkClientID(&client_model_name, &client_serial_number);

			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StopVoIP - get network client");
			ccm::shared_ptr<ccm::NetworkClient> client = ccmsdk_->GetNetworkClient(id);
			if (client) {
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StopVoIP calling...");
				ccm::ErrorCode err = ccmsdk_->StopVoIP(id);
				CCM_LOG_INFO("CCMCUI", "[ipc] StopVoIP StopVoIP() error:%d", err);
				// For IPC
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StopVoIP CreateStopVoIPResponse");
				std::string strResponse = ccm::CcmJsonParser::CreateStopVoIPResponse(id, err);
				CCM_LOG_INFO("CCMCUI", "[ipc] StopVoIP response:%s", strResponse.c_str());
				printf("#StopVoIP Send Response to IPC\n");
				Send(strResponse);
			}
			else {
				printf("#StopVoIP Send Response to IPC CCM_UNKNOWN 1\n");
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StopVoIP - NO network client");
				// For IPC
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StopVoIP CreateStopVoIPResponse");
				std::string strResponse = ccm::CcmJsonParser::CreateStopVoIPResponse(id, ccm::ErrorCode::CCM_UNKNOWN);
				CCM_LOG_INFO("CCMCUI", "[ipc] StopVoIP response:%s", strResponse.c_str());
				Send(strResponse);
			}
		}
		catch (...) {
			printf("#StopVoIP Send Response to IPC CCM_UNKNOWN 2\n");
			printf("Request Error\n");
			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StopVoIP Request Error : exception");
			// For IPC
			ccm::NetworkClientID id = ccm::NetworkClientID(&client_model_name, &client_serial_number);
			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StopVoIP CreateStopVoIPResponse");
			std::string strResponse = ccm::CcmJsonParser::CreateStopVoIPResponse(id, ccm::ErrorCode::CCM_UNKNOWN);
			CCM_LOG_INFO("CCMCUI", "[ipc] StopVoIP response:%s", strResponse.c_str());
			Send(strResponse);
		}
		CCM_LOG_INFO("CCMCUI", "%s", "[ipc] StopVoIP: end");
	}

	void CcmIpc::OnMessage(std::string data) {
		printf("OnMessage");
		CCM_LOG_INFO("CCMCUI", "[ipc] OnMessage : %s", data.c_str());

		try {
			picojson::value ccm_request_value;
			const std::string err = picojson::parse(ccm_request_value, data);
			if (ccm_request_value.is<picojson::object>()) {
				picojson::object ccm_request = ccm_request_value.get<picojson::object>();
				if (ccm_request.find("command") != ccm_request.end()) {
					std::string commandRequest = ccm_request.find("command")->second.get<std::string>();
					printf("IPC Request Command: %s\n", commandRequest.c_str());

					if (commandRequest == "GetNetworkClient") {
						onCmdGetNetworkClient_(ccm_request);

#ifdef CCMDEBUG_GETNETWORKCLIENT
						for (int i = 0; i < 60; i++) {
							std::vector<ccm::McbNetworkInfo> vecMcbNetworkInfo;
							{
								McbNetworkInfo info1("NIC1", std::rand(), std::rand(), std::rand());
								vecMcbNetworkInfo.push_back(info1);

								McbNetworkInfo info2("NIC2", std::rand(), std::rand(), std::rand());
								vecMcbNetworkInfo.push_back(info2);

								McbNetworkInfo info3("NIC3", std::rand(), std::rand(), std::rand());
								vecMcbNetworkInfo.push_back(info3);

								McbNetworkInfo info4("NIC4", std::rand(), std::rand(), std::rand());
								vecMcbNetworkInfo.push_back(info4);
							}
							McbNetworkInfoHandler(id, vecMcbNetworkInfo);
							//							std::string strResponse = ccm::CcmJsonParser::CreateMcbNetworkInfoNotify(id, vecMcbNetworkInfo);
							//							Send(strResponse);
							Sleep(1000);
						}
#endif //CCMDEBUG_GETNETWORKCLIENT
					}
					else if (commandRequest == "GetNetworkClientList") {
						onCmdGetNetworkClientList_(ccm_request);
					}
					else if (commandRequest == "GetClipList") {
						onCmdGetClipList_(ccm_request);
					}
					else if (commandRequest == "GetUploadList") {
						onCmdGetUploadList_(ccm_request);
					}
					else if (commandRequest == "AddUploadFiles") {
						onCmdAddUploadFiles_(ccm_request);
					}
					else if (commandRequest == "StartQosStreamingSend") {
						onCmdStartQosStreamingSend_(ccm_request);
					}
					else if (commandRequest == "StartQosStreamingReceive") {
						onCmdStartQosStreamingReceive_(ccm_request);
					}
					else if (commandRequest == "StopStreamingSend") {
						onCmdStopStreamingSend_(ccm_request);
					}
					else if (commandRequest == "StopStreamingRecieve") {
						onCmdStopStreamingRecieve_(ccm_request);
					}
					else if (commandRequest == "GetPlanningMetadataList") {
						onCmdGetPlanningMetadataList_(ccm_request);
					}
					else if (commandRequest == "GetCurrentPlanningMetadata") {
						onCmdGetCurrentPlanningMetadata_(ccm_request);
					}
					else if (commandRequest == "GetPlanningMetadata") {
						onCmdGetPlanningMetadata_(ccm_request);
					}
					else if (commandRequest == "SetPlanningMetadata") {
						onCmdSetPlanningMetadata_(ccm_request);
					}
					else if (commandRequest == "PutPlanningMetadata") {
						onCmdPutPlanningMetadata_(ccm_request);
					}
					else if (commandRequest == "StartVoIP") {
						onCmdStartVoIP_(ccm_request);
					}
					else if (commandRequest == "StopVoIP") {
						onCmdStopVoIP_(ccm_request);
					}
				}
			}
		}
		catch (...) {
			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] IPC Request Error");
		}
	};

	void CcmIpc::Send(std::string data) {
      try {
        mutex_.lock();
      } catch (...) {
        ccm::shared_ptr<std::mutex> mutex = ccm::shared_ptr<std::mutex>(new std::mutex());
        ccm::shared_ptr<std::condition_variable> cond = ccm::shared_ptr<std::condition_variable>(new std::condition_variable());
        std::unique_lock<std::mutex> lock(*mutex);
        array_cond_.push_back(cond);
        cond->wait_for(lock, std::chrono::milliseconds(3 * 1000));
        try {
          mutex_.lock();
        } catch (...) {
          printf("IPC Send Lock Error\n");
          return;
        }
      }
      try {
		size_t buffer_size = 4/*Length*/ + data.length();
		int32_t data_length = (int32_t)data.length();
		CCM_LOG_INFO("CCMCUI", "[ipc] send ipc   |--->||%s", data.c_str());

		// 書き込み用構造体
		write_req_t *write_req = (write_req_t*)malloc(sizeof(write_req_t));
		write_req->buf = uv_buf_init((char*)malloc(buffer_size), (unsigned int)buffer_size);
		memset(write_req->buf.base, 0x00, buffer_size);
		memcpy(write_req->buf.base, &data_length, 4);
		memcpy(write_req->buf.base + 4, data.c_str(), data.length());
		//memcpy_s(write_req->buf.base, buffer_size, data.c_str(), data.length());

		// 書き込み
		int buf_count = 1;
		uv_write((uv_write_t*)write_req, (uv_stream_t*)&client_pipe_, &write_req->buf, buf_count, on_uv_written);

		CCM_LOG_INFO("CCMCUI", "%s", "[ipc] ==============");
      } catch (...) {
        printf("IPC Send Error\n");
      }

      try {
        // Unlock
        mutex_.unlock();

        // Send Notify
        if (array_cond_.size() > 1) {
          (*array_cond_.begin())->notify_all();
          array_cond_.erase(array_cond_.begin());
        }
      } catch (...) {
        printf("IPC UNLOCK ERROR\n");
      }
	};

	/**
	  * コンストラクタ
	  */
	CcmIpc::CcmIpc(ccm::ConnectionControlManager *ccmsdk)
	{
		ccmsdk_ = ccmsdk;
#ifdef _WIN32
		std::string ipc_path = "\\\\.\\pipe\\ccm_ipc_obj";
#else
		std::string ipc_path = "/tmp/ccm_ipc_obj";
        std::remove(ipc_path.c_str());
#endif
        
		if (self_ != NULL) {
			printf("CcmIpc Init Error");
			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] CCM IPC Init Error");
			return;
		}
		else {
			self_ = this;
		}
		int rtn;
		server_loop_ = NULL;

		// default loop
		server_loop_ = uv_default_loop();
		if (server_loop_ == NULL) {
			printf("IPC: uv_default_loop Error\n");
			CCM_LOG_INFO("CCMCUI", "%s", "[ipc] CCM IPC Make Loop Error");
		}
		else {

			// init pipe stdin stdout file
			uv_pipe_init(server_loop_, &server_pipe_, 0);
			//printf("IPC: uv_pipe_init OK\n");

			// bind named pipe
			if (rtn = uv_pipe_bind(&server_pipe_, ipc_path.c_str())) {
				printf("Bind error %s\n", uv_err_name(rtn));
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] CCM IPC Bind Error");
				return;
			}

			// connect listener
			if ((rtn = uv_listen((uv_stream_t*)&server_pipe_, 128, on_new_connection))) {
				printf("Listen error %s\n", uv_err_name(rtn));
				CCM_LOG_INFO("CCMCUI", "%s", "[ipc] CCM IPC Listen Error");
				return;
			}
			else {
				//printf("Listen OK\n");
			}
			
			// event start
		    //uv_run(server_loop_, UV_RUN_DEFAULT);
			std::thread onlineThread_(uv_run, server_loop_, UV_RUN_DEFAULT);
			onlineThread_.detach();

			printf("---- IPC Listen Started ----\n");
			CCM_LOG_INFO("CCMCUI", "%s", "---- IPC Listen Started ----");
		}
	}


	/**
	* デストラクタ
	*/
	CcmIpc::~CcmIpc() {
		//uv_fs_close(loop_, &pipe_, NULL, NULL);

		uv_fs_t req;
		uv_fs_unlink(server_loop_, &req, "ccm_ipc_obj", NULL);
		CCM_LOG_INFO("CCMCUI", "%s", "[ipc] CCM IPC Finished");
	}
}
