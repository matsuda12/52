#pragma once
#include "uv.h"
#include <string>
#include <picojson.h>

namespace ccm {

	class CcmIpc
	{
	public:
		CcmIpc(ccm::ConnectionControlManager *);
		virtual ~CcmIpc();

		void OnConnect(uv_stream_t *pipe);

		void OnDisconnect();

		void OnMessage(std::string data);

		void Send(std::string data);

	private:
		uv_loop_t *server_loop_;
		uv_pipe_t server_pipe_;

		uv_loop_t *client_loop_;
		uv_pipe_t client_pipe_;

		void onCmdGetNetworkClient_(const picojson::object& ccm_request);
		void onCmdGetNetworkClientList_(const picojson::object& ccm_request);
		void onCmdGetClipList_(const picojson::object& ccm_request);
		void onCmdGetUploadList_(const picojson::object& ccm_request);
		void onCmdAddUploadFiles_(const picojson::object& ccm_request);
		void onCmdStartQosStreamingSend_(const picojson::object& ccm_request);
		void onCmdStartQosStreamingReceive_(const picojson::object& ccm_request);
		void onCmdStopStreamingSend_(const picojson::object& ccm_request);
		void onCmdStopStreamingRecieve_(const picojson::object& ccm_request);
		void onCmdGetPlanningMetadataList_(const picojson::object& ccm_request);
		void onCmdGetCurrentPlanningMetadata_(const picojson::object& ccm_request);
		void onCmdGetPlanningMetadata_(const picojson::object& ccm_request);
		void onCmdSetPlanningMetadata_(const picojson::object& ccm_request);
		void onCmdPutPlanningMetadata_(const picojson::object& ccm_request);
	};
}
